// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                             stringMarkovGenerator.c                              //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Takes a base corpus C of documents in STARC, TSV or TREC format and builds an
// order k Markov model, based on letter sequences, then generates a random output corpus
// using the transition probabilities from the model.
//
// The set of symbols comprises the lower case ASCII letters plus '{' and '|' representing
// EOW and EOD.
//
// The transition matrix is represented using a hash table to access the context string.
// Each entry in the hash table includes a total occurrence count plus an array of one
// int counter for every possible following symbol.  During scanning of the input, the
// entries in the counter array are in normal ASCII order.   I.e. the zeroth element of the
// counter array for the 3-symbol context 'fox' is a count of how many times the letter 'a'
// follows 'fox', and the 28th element is a count of how many times fox is followed by
// end of document.  During scanning a histogram is made of all the symbol frequencies.
// After scanning, the histogram is used to make a mapping from the symbol to its position
// in a frequency ordering.  This mapping is then used to reorder the entries in the
// counter array, so that most frequent symbols come first, at the same time as the
// frequencies recorded in the counters are converted into cumulative probabilities.
//
// It's not clear that this is the optimal data structure, or algorithm, but let's give it a go.

#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "characterSetHandling/unicode.h"
#include "utils/general.h"
#include "utils/argParser.h"
#include "stringMarkovGenArgTable.h"
#include "stringMarkovGen.h"
#include "imported/TinyMT_cutdown/tinymt64.h"
#include "utils/randomNumbers.h"
#include "shuffling.h"


static char docCopy[MAX_DOC_LEN + 1], *docWords[MAX_DOC_WORDS];

globals_t globals;

params_t params;

static void initialiseGlobals(globals_t *globals) {
  globals->startTime = what_time_is_it();
  globals->numDocs = 0;
  globals->numEmptyDocs = 0;
  globals->totalPostingsIn = 0;
  globals->gMarkovHash = NULL;
  globals->vocabOut = 0;
  globals->debug = 0;
}

static int allASCIILetters(u_char *str) {
  // Check that all the characters in the string are ASCII letters (return zero otherwise) and
  // convert upper to lower case.
  if (str == NULL || *str == 0 || *str == '\n') return 0;  // ------------->
  while (*str && *str != '\n') {
    if (*str > 'z') return 0;  // Eliminates any char with leading bit set --------------->  
    if (*str >= 'A' && *str <= 'Z') *str += ('a' - 'A');  // convert to lower
    else if (*str < 'a') return 0;   // Already tested > 'z'   -------------------------->
    str++;
  }
  return 1;    // -------------------------->
}

static u_char simplifiedString[MAX_DOC_LEN + MAX_K + 2], *sSP = NULL;
static long long simplifiedDocNo = 0, wildcardStringsAdded = 0, sentenceCount = 0;


static int buildMarkovModel(globals_t *globals, char *docText, size_t docLen, int docNum) {
  // This function adds all the information from a single document represented by docText
  // to the Markov transition matrix (at this stage containing frequencies rather than
  // transition probabilities.)
  int numWords, w, i, *hashValueP, successorIndex;
  u_char *context, successor, *wP;
  size_t filteredTextLen;
  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.

  if (docLen > MAX_DOC_LEN) docLen = MAX_DOC_LEN;
  if ((docText[docLen] & 0xC0) == 0x80) {
    if (0) printf("Document is being truncated\n");
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80 && docLen > 0);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }
  
  // Copy document text so we can write on it
  memcpy(docCopy, docText, docLen);
  docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
  if (globals->debug) {
    printf("stringMarkovModel: len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n",
	   docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	   globals->inputSize - 1);
    //put_n_chars(docCopy, 100);
    printf("%s", docCopy);
    printf("\n\n\n");
  }

  numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							MAX_DOC_WORDS, MAX_WORD_LEN,
							TRUE,  // case-fold line before splitting
							FALSE, // remove accents before splitting
							FALSE,  // Perform some heuristic substitutions 
							FALSE   // True if words must contain an ASCII alnum
							);
  
  if (numWords <= 0) {
    globals->numEmptyDocs++;
    return 0;  // ----------------------------->
  }
  
  // For ease of processing, copy the words, plus EOW, EOS and EOD symbols into a character array.  But first start with k EODs
  sSP = simplifiedString;
  for (i = 0; i < params.k; i++) *sSP++ = EOD;
  
  for (w = 0; w < numWords; w++) {
  if (allASCIILetters(docWords[w])) {   // Side Effect: allASCIILetters converts upper to lower case 
      wP = docWords[w];
      while (*wP  && *wP != '\n') {
        *sSP++ = *wP++;
      }
      if (*wP == '\n') {
	*sSP++ = EOS;
	sentenceCount++;
      } else {
	*sSP++ = EOW;
      }
      globals->totalPostingsIn++;
    }
  }
  *sSP++ = EOD;
  *sSP = 0;
  
  // Now rip through the string thus created.
  filteredTextLen = sSP - simplifiedString;
  if (0) printf("filteredTextLen = %zu, String is:\n%s\n", filteredTextLen, simplifiedString);
  for (i = 0; i < filteredTextLen - params.k; i++) {
    context = simplifiedString + i;
    successor = context[params.k];
    context[params.k] = 0;
    
    // dahash_lookup() returns a pointer to the first byte of the value part of the entry.
    hashValueP = (int *)dahash_lookup(globals->gMarkovHash, context, 1);
    if (hashValueP == NULL) {
      printf("Error: buildMarkovModel(): lookup of %s in gMarkovHash failed\n", context);
      exit(1);
    }
    successorIndex = successor - 'a';  // convert 'a' - '|' into 0 - 29 for use as an index into counts array
    hashValueP[0]++;    // Total occurrence count for this context string
    hashValueP[successorIndex + 1]++;
    if (globals->debug)  
      printf("Appending %c to entry for %s\n",
	     successor, (byte *)hashValueP - globals->gMarkovHash->key_size);

    // ----- If applicable, add wildcard versions of context -----
    if (params.wildcards > 0 && context[0] != EOD) {
      // Wildcarding works by replacing the leading part of the string with wildcards
      int w;
      u_char context2[MAX_K + 1];
      strcpy(context2, context);
      for (w = 0; w < params.wildcards - 1; w++) context2[w] = WILDCARD;  // Fill in one less than the minimum number of leading wildcards
      for (w = params.wildcards - 1; w < params.k; w++) {  // Now fill in the rest
	context2[w] = WILDCARD;
	if (0) printf("Subsidiary context: '%s'\n", context2);
	hashValueP = (int *)dahash_lookup(globals->gMarkovHash, context2, 1);
	if (hashValueP == NULL) {
	  printf("Error: buildMarkovModel(): lookup of %s in gMarkovHash failed\n", context2);
	  exit(1);
	}
	if (hashValueP[0] == 0) wildcardStringsAdded++;   // Count the distinct wildcardStrings
	hashValueP[0]++;    // Total occurrence count for this context string
	hashValueP[successorIndex + 1]++;
	if (globals->debug)  
	  printf("Appending %c to [subsidiary] entry for %s\n",
		 successor, (byte *)hashValueP - globals->gMarkovHash->key_size);

      }      
    }
    // --------------------------------------------------------------

    context[params.k] = successor;
  }

  // If requested write the simplified version of the input text
  if (globals->simplifiedText != NULL) {
    fprintf(globals->simplifiedText, "<DOC>\n<DOCNO>DOC%lld</DOCNO>\n<TEXT>\n", ++simplifiedDocNo);
    // Replace all the '{'s in the simplified string with spaces
    // and the '}'s with newlines
    sSP = simplifiedString + params.k;
    while (*sSP && *sSP != EOD) {
      if (*sSP == EOW) *sSP = ' ';
      if (*sSP == EOS) *sSP = '\n';
      sSP++;
    }
    *sSP = 0;
    fprintf(globals->simplifiedText, "%s\n</TEXT>\n</DOC>\n", simplifiedString + params.k);
  }

  return 0;
}


static int numSuccessorsHisto[NUM_SYMBOLS + 1] = {0};
static long long countBuckets[33] = {0};  // Bucket 0 is a count of all the empty counters,
                                          // Bucket i is a count of all the non-zero counters showing less than 2^i

static void convertToProbs() {
  off_t htOff;
  u_char *htEntry;
  int *htVal, i, e, nonZeroElts;
  float *cumProbs;
  double totalCount, dProb, cumProb, overallNonZeroes = 0, cumTotal = 0;
  long long pow2;
  
  htOff = 0;
  for (e = 0; e < globals.gMarkovHash->capacity; e++) {
    htEntry = ((u_char *)(globals.gMarkovHash->table)) + htOff;
    if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
      cumProb = 0;
      htVal = (int *)(htEntry + globals.gMarkovHash->key_size);
      totalCount = (double)(*htVal);
      htVal++; // Move to the successor counts
      cumProbs = (float *)htVal;
      nonZeroElts = 0;
      for (i = 0; i < NUM_SYMBOLS; i++) {
	if (htVal[i] == 0) {
	  countBuckets[0]++;
	} else {
	  double logg = log2((double)htVal[i]);
	  countBuckets[(int)floor(logg) + 1]++;  // E.g. count = 1, log2(1) = 0 --> 1.  count = 2, log2(2) = 1 --> 2.
	                                         // count = 3, log2(3) = 1.xxx --> 2.  count = 4, log2(4) = 2 --> 3.
	  nonZeroElts++;
	  overallNonZeroes++;
	}
	dProb = (double)htVal[i] / totalCount;
	cumProb += dProb;
	if (0) printf("%s: %d: %d / %.0f = %.5f --> %.5f\n",
		      htEntry, i, htVal[i], totalCount, dProb, cumProb);
	cumProbs[i] = (float)cumProb;
      }
      numSuccessorsHisto[nonZeroElts]++; 
      if (fabsf((float)(cumProb - 1.0)) > 0.00001) {
	printf("Warning: convertToProbs(): Cummulative probabilities (%.5f) don't reach one for '%s'\n",
	       cumProb, htEntry);
      }			    
    }
    htOff += globals.gMarkovHash->entry_size;
  }

  // Display histograms
  printf("\nHistogram of number of successors found for each context.\n"
	 "=========================================================\n"
	 "Number_of_successors          Count    Cum. Percentage\n"
	 "======================================================\n");
  for (i = 0; i <= NUM_SYMBOLS; i++) {
    cumTotal += (double)numSuccessorsHisto[i];
    printf("%20d  %10d %15.2f%%\n", i, numSuccessorsHisto[i], 100.0 * cumTotal / globals.gMarkovHash->entries_used);
  }

  printf("\n\nHistogram of individual successor counts\n"
	     "========================================\n"
	 "Less_than      Count  Cum. Percentage (nonZeros)\n"
	 "================================================\n");
  pow2 = 1;
  cumTotal = 0;
  for (i = 0; i <= 32; i++) {
    if (i > 0) {
      cumTotal += (double)countBuckets[i];
      printf("%12lld  %12lld %15.2f%%\n", pow2, countBuckets[i], 100.0 * cumTotal / overallNonZeroes);
    } else {
      printf("%12lld  %12lld\n", pow2, countBuckets[i]);
    }
    pow2 *= 2;
  }
  printf("\n\n");
}



// The following three processXXXFormat functions are indirectly copied from Nomenclator.c
// ... which may have copied them from other modules in this directory.
// Note that they each version calls different functions

static void processTSVFormat(globals_t *globals) {
  // Input is assumed to be in TSV format with an arbitrary (positive) number of
  // columns (including one column), in which the document text is in
  // column one and the other columns are ignored.   (All LFs and TABs are assumed
  // to have been removed from the document.)
  char *lineStart, *p, *inputEnd;
  long long printerval = 1000;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  lineStart = globals->inputInMemory;
  globals->numDocs = 0; 
  p = lineStart;
  while (p <= inputEnd) {
    // Find the length of column 1, then process the doc.
    while (p <= inputEnd  && *p >= ' ') p++;  // Terminate with any ASCII control char
    docLen = p - lineStart;
    buildMarkovModel(globals, lineStart, docLen, globals->numDocs);
    globals->numDocs++;
    if (globals->numDocs % printerval == 0) {
      printf("   --- %lld docs scanned @ %.3f msec per record\n",
             globals->numDocs,
             (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs);
      if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
    }

    // Now skip to end of line (LF) or end of input.
    while (p <= inputEnd  && *p != '\n') p++;  // Terminate with LineFeed only
    p++;
    lineStart = p;
  }
}


static void processTRECFormat(globals_t *globals) {
  // Input is assumed to be in highly simplified TREC format, such as that produced by
  // the detrec program.  Documents are assumed to be DOC elements, each starting with
  // a DOCNO element.  It is also assumed that the content contains no other sub-elements
  // and no stray angle brackets.  Ideally, the text is just words separated by spaces.
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 1;
  size_t docLen;
  double startTime = what_time_is_it();

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  globals->numDocs = 0;
  while (p <= inputEnd) {
    if (0) printf("Looking for <TEXT>\n");
    q = mlstrstr(p, "<TEXT>", inputEnd - p - 6, 0, 0);
    if (q == NULL) break;   // Presumably the end of the file. ... or wrong format file.
    docStart = q + 6;
    if (0) printf("Looking for </TEXT>\n");
    q = mlstrstr(docStart, "</TEXT>", inputEnd - docStart - 7, 0, 0);
    if (q == NULL) break;   // Presumably file is incorrectly formatted.    
    docLen = q - docStart;
    buildMarkovModel(globals, docStart, docLen, globals->numDocs);
    globals->numDocs++;
    if (globals->numDocs % printerval == 0) {
      printf("   buildMarkovModel");
      printf(" --- %lld docs scanned @ %.3f msec per record\n",
      globals->numDocs,
      (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs);
      if (printerval < 10000) printerval *= 10;
    }
    p = q + 7;  // Move to char after </TEXT>
  }
  printf("   MarkovModel built");
  printf(" --- %lld docs scanned @ %.3f msec per record --- total postings: %lld\n\n",
	 globals->numDocs,
	 (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs,
	 globals->totalPostingsIn);

}




static void processSTARCFormat(globals_t *globals) {
  // Input is assumed to be docs in in a very simple <STARC
  // header><content> format. The STARC header begins and ends with a
  // single ASCII space.  Following the leading space is the content
  // length in decimal bytes, represented as an ASCII string,
  // immediately followed by a letter indicating the type of record.
  // Record types are H - header, D - document, or T - Trailer.  The
  // decimal length is expressed in bytes and is represented in
  // ASCII. If the length is L, then there are L bytes of document
  // content following the whitespace character which terminates the
  // length representation.  For example: " 13D ABCDEFGHIJKLM 4D ABCD"
  // contains two documents, the first of 13 bytes and the second of 4
  // bytes.
  //
  // Although this representation is hard to view with editors and
  // simple text display tools, it completely avoids the problems with
  // TSV and other formats which rely on delimiters, that it's very
  // complicated to deal with documents which contain the delimiters.
  //
  // This function skips H and T docs.
  
  size_t docLen;
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 10;
  byte recordType;

  globals->numDocs = 0; 
  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  while (p <= inputEnd) {
    // Decimal length should be encoded in ASCII at the start of this doc.
    if (*p != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     p - globals->inputInMemory);
      exit(1);

    }
    errno = 0;
    docLen = strtol(p, &q, 10);  // Making an assumption here that the number isn't terminated by EOF
    if (errno) {
      fprintf(stderr, "processSTARCFile: Error %d in strtol() at offset %zd\n",
	     errno, p - globals->inputInMemory);
      exit(1);
    }
    if (docLen <= 0) {
      printf("processSTARCFile: Zero or negative docLen %zd at offset %zd\n",
	     docLen, p - globals->inputInMemory);
      exit(1);
    }

    recordType = *q;    
    if (recordType != 'H' && recordType != 'D' && recordType != 'T') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);
    }
    q++;
    if (*q != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't end with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);

    }
    
    docStart = q + 1;  // Skip the trailing space.
    if (0) printf(" ---- Encountered %c record ---- \n", recordType);
    if (recordType == 'D') {
      if (0) printf("About to process a doc.\n");
      buildMarkovModel(globals, docStart, docLen, globals->numDocs);
      globals->numDocs++;
      if (globals->numDocs % printerval == 0) {
        printf("   --- %lld docs scanned @ %.3f msec per record\n",
          globals->numDocs,
          (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs);
        if (globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    }
    p = docStart + docLen;  // Should point to the first digit of the next length, or EOF
  }
}

 
static int rand_cumdist_bsearch3(float *cumProbs, int numProbs) {
  // Slightly modified from utils/randomNumbers.c to handle floats rather than doubles
  // CumProbs[] is an array 0 .. numProbs - 1 representing a cumulative
  // probability distribution.  It is assumed that cumProbs[numProbs - 1] = 1.0
  // and that the values in cumProbs are in ascending order;
  //
  // Returns an integer in 0 .. numProbs - 1
  double uniRand = rand_val(0);  // Uniform in range 0 -1
  int u = numProbs - 1, l = 0, m, counter = 0;

  if (numProbs < 3) {
    if (numProbs == 0) {
      printf("Error: rand_cumdist_bsearch3() called with %d elements\n", numProbs);
      exit(1);
    }
    // Special case when there are only 1 or 2 elements
    if (uniRand > cumProbs[0]) return 1;
    else return 0;
  }
  if (0) printf("rand_cumdist_bsearch3() called with %d elements\n", numProbs);
  do {
    m = (u + l) / 2;
    if (0) printf("m = %d, uniRand = %.5f, cumprobs[m] = %.5f\n",
		  m, uniRand, (double)cumProbs[m]);
    if (uniRand > cumProbs[m]) {
      l = m + 1;
    } else {
      if ((uniRand == cumProbs[m])
          || (m == 0) || (m == u) || (l > m)
          || (uniRand > cumProbs[m -1])) {
	return m;   // ----------------------->
      } else {
        u = m;
      }
    }
  } while (++counter < 1000);

  printf("Error: rand_cumdist_bsearch3() looping. m = %d, l = %d, u = %d, uniRand = %.5f\n",
         m, l, u, uniRand);
  exit(1);
}

static long long failed_lookups = 0, failed_mod_lookups = 0;

static int chooseSuccessor(u_char *context) {
  // Look up context in the hash table to find the array of cumulative probabilities
  // for all possible successors, then throw a random probability to choose which one.
  // If lookup fails or smoothing/shrinkage is in effect, choose the successor from a shortened
  // context, achieved by wild-carding
  float *cumProbs = NULL, *cumProbs2 = NULL;
  int index, index2, w = 0;
  u_char *modcon = NULL;
  if (0) printf("chooseSuccessor(%s)\n", context);

  if (params.wildcards > 0 && context[0] != '|') {
    // Make a minimally wild-carded version of the context and look it up, unless
    // this is start of document
    modcon = make_a_copy_of(context);
    while (w < params.wildcards) modcon[w++] = WILDCARD;
    while (w < params.k)  { // Add more wildcards until it works.
      cumProbs2 = (float *)dahash_lookup(globals.gMarkovHash, modcon, 0);
      if (cumProbs2 != NULL) break;    // --------->
      modcon[w++] = WILDCARD;
    }
    if (cumProbs2 == NULL) failed_mod_lookups++;
  }

  // If lambda > 0 roll a die and see if we should use modcon
  if (cumProbs2 != NULL && params.lambda > 0.0 && (rand_val(0) < params.lambda)) {
    cumProbs2++;   // Skip the total count (actually an integer).  Assume sizeof(int) == sizeof(float)
    index2 = rand_cumdist_bsearch3(cumProbs2, NUM_SYMBOLS);
    free(modcon);
    modcon = NULL;
    return 'a' + index2;   //  ----------------------------------------------->
  } else {
    // Look up original context
    cumProbs = (float *)dahash_lookup(globals.gMarkovHash, context, 0);
    if (cumProbs == NULL) {  // Failed.  If possible, fall back to modcon.
      failed_lookups++;
      if (cumProbs2 == NULL) {
	printf("Error: chooseSuccessor(): Fallback Lookup of '%s' failed.\n", context);
	if (modcon != NULL) printf("       modcon was '%s'\n", modcon);
	exit(1);
      } else {
	cumProbs2++;   // Skip the total count (actually an integer).  Assume sizeof(int) == sizeof(float)
	index2 = rand_cumdist_bsearch3(cumProbs2, NUM_SYMBOLS);
	free(modcon);
	modcon = NULL;
	return 'a' + index2;   //  ----------------------------------------------->
      }
    } else { // Succeeded
      cumProbs++;   // Skip the total count (actually an integer).  Assume sizeof(int) == sizeof(float)
      index = rand_cumdist_bsearch3(cumProbs, NUM_SYMBOLS);
      return 'a' + index;   //  ----------------------------------------------->
      free(modcon);
      modcon = NULL;
    }
  }
}


static void generateCorpusFromModel() {
  long long postingsOut = 0, numDocs = 0;
  int i;
  double startTime = what_time_is_it();
  int printerval = 1;
  u_char context[params.k + 1], successor;

  if (params.verbose) setvbuf(globals.corpusOut, NULL, _IONBF, 0);
  fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);

  // Establish initial start of document context.
  for (i = 0; i < params.k; i++) context[i] = EOD;
  context[params.k] = 0;
  
  while (postingsOut < globals.desiredTotalPostings) {
    successor = chooseSuccessor(context);
    if (successor == EOW) {
      fputc(' ', globals.corpusOut);
      for (i = 0; i < params.k - 1; i++) context[i] = context[i + 1];
      context[params.k - 1] = successor;
      postingsOut++;
    } else if (successor == EOS) {
      fputc('\n', globals.corpusOut);
      for (i = 0; i < params.k - 1; i++) context[i] = context[i + 1];
      context[params.k - 1] = successor;
      postingsOut++;
    } else if (successor == EOD) {
      fprintf(globals.corpusOut, "\n</TEXT>\n</DOC>\n");
      postingsOut++;   // EOD must be EOW as well
      if (postingsOut >= globals.desiredTotalPostings) break;   // ----------------------->
      fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);
      if (numDocs % printerval == 0) {
	printf("   stringMarkov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	       numDocs, postingsOut, globals.desiredTotalPostings, (double)postingsOut
	       / 1000000.0 / (what_time_is_it() - startTime));
	if (printerval < 10000) printerval *= 10;
      }

      // We don't want to carry context across document boundaries, so completely reset it
      for (i = 0; i < params.k; i++) context[i] = EOD;
    } else {
      fputc(successor, globals.corpusOut);
      for (i = 0; i < params.k - 1; i++) context[i] = context[i + 1];
      context[params.k - 1] = successor;
    }
  }
      
  if (successor != EOD) {   // Don't output end of document if we've done so immediately before
    fprintf(globals.corpusOut, "\n</TEXT>\n</DOC>\n");
  }
  printf("   stringMarkov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	 numDocs, postingsOut, globals.desiredTotalPostings, (double)postingsOut
	 / 1000000.0 / (what_time_is_it() - startTime));

  globals.postingsOut = postingsOut;
  globals.docsOut = numDocs;
  printf("\n\nSMG(%d): Corpus generated in %.3f sec.  \n"
	 "SMG(%d): Rate of generation (ignoring training): %.3f Mpostings/sec.\n",
	 params.k, what_time_is_it() - startTime,
	 params.k, (double)globals.desiredTotalPostings / 1000000.0 / (what_time_is_it() - startTime));

}


static void printSummary(globals_t *globals) {  
  printf("docs=%lld # Including %lld zero-length\n", globals->numDocs, globals->numEmptyDocs);
  printf("no. context strings=%zd\n", globals->gMarkovHash->entries_used);
  printf("total_postings=%lld\n", globals->postingsOut);
}

static void printUsage(char *progName, arg_t *args) {
  printf("Usage: %s inFile=<blah> outFile=<blah> [ <arg=val> ...]\n\n", progName);
  print_args(TEXT, args, "Default");
  exit(1);
}


int main(int argc, char **argv) {
  int error_code, a, keyLen = 0;
  u_char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
  char *ignore;
  double startTime, veryStart, MB, totalMB = 0; 

  veryStart = what_time_is_it();
  setvbuf(stdout, NULL, _IONBF, 0);
  if (sizeof(u_char *) != 8) {
    printf("\n\nWarning: Pointers are only 4-byte. It is recommended that %s should be compiled for 64 bit.\n\n", argv[0]);
  }

  if (sizeof(float) != sizeof(int)) {
    printf("Error: data structures in %s rely on floats and ints having the same size. However %zd != %zd!\n",
	   argv[0], sizeof(float), sizeof(int));
    exit(1);
  }

  activate_preservation_of_newlines_in_split_lines();
  
  initialiseParams();
  if (argc < 3) printUsage(argv[0], args);

  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);

  printf("Sizeof(size_t) = %zu, Sizeof(off_t) = %zu, Sizeof(byte *) = %zu\n",
	 sizeof(size_t), sizeof(off_t), sizeof(byte *));

  if (params.verbose) printf("Params initialised\n");
  initialiseGlobals(&globals);   // Includes the hashtables as well as scalar values
  if (params.verbose) printf("Globals initialised\n");
  
  // Process all the command line options
  for (a = 1; a < argc; a++) {
    assign_one_arg(argv[a], (arg_t *)(&args), &ignore);
  }
  
  if (params.randomSeed == -1) params.randomSeed = (u_ll)fmod(veryStart * 1000000.0, 1000000.0);
  rand_val((u_ll)params.randomSeed);  // Seed the random generator.
  printf("Random number generator seed: %llu\n", params.randomSeed);

  if (params.k > MAX_K) {
    printf("Warning value of k (%d) exceeds limit (%d).  Setting k = %d\n",
      params.k, MAX_K, MAX_K);
    params.k = MAX_K;
  }

  if (params.lambda < 0.0 || params.lambda > 1.0) {
    printf("Error: Value of lambda must lie between zero and one.\n");
    exit(1);
  }

  if (params.wildcards < 0 || params.wildcards >= params.k) {
    printf("Error: Value of wildcards must lie between zero and k - 1\n");
    exit(1);
  }

  // Set up global hash with room for overall occurrence frequency, and an array of symbol frequencies
  keyLen = 3; // keyLen should be a multiple of four, minus one.  The minus one is to allow for the terminating NUL
  while (keyLen < params.k) keyLen += 4;
  
  globals.gMarkovHash =
    dahash_create((u_char *)"globalVocab", params.hashBits, keyLen, sizeof(int) * (NUM_SYMBOLS + 1),
		  (double)0.9, FALSE);

  // Make sure we can write to the output file
  globals.corpusOut = fopen(params.outFileName, "wb");
  if (globals.corpusOut == NULL) {
    printf("Error: Can't open %s for writing.\n", params.outFileName);
    printUsage(argv[0], args);
  }
  printf("Output file %s opened for writing.\n", params.outFileName);
  
  // If applicable, make sure we can write to the simplified text file
  if (params.simplifiedFileName != NULL) {
    globals.simplifiedText = fopen(params.simplifiedFileName, "wb");
    if (globals.simplifiedText == NULL) {
      printf("Error: Can't open %s for writing.\n", params.simplifiedFileName);
    }
    printf("Output file %s opened for writing.\n", params.simplifiedFileName);
  }
  
  // Memory map the whole input file
  if (! exists(params.inFileName, "")) {
    printf("Error: Input file %s doesn't exist.\n", params.inFileName);
    printUsage(argv[0], args);
  }

  startTime = what_time_is_it();
  globals.inputInMemory = (char *)mmap_all_of(params.inFileName, &(globals.inputSize),
					       FALSE, &(globals.inputFH), &(globals.inputMH),
					       &error_code);
  if (globals.inputInMemory == NULL ) {
    printf("Error: mmap_all_of(%s) failed with code %d\n", params.inFileName, error_code);
    exit(1);
  }

  madvise(globals.inputInMemory, globals.inputSize, MADV_SEQUENTIAL);

  printf("Input mapped.\n");


  // Build the vocabulary hash table for the input corpus 

  if (tailstr(params.inFileName, ".tsv") != NULL || tailstr(params.inFileName, ".TSV") != NULL) {
    processTSVFormat(&globals);
   } else if (tailstr(params.inFileName, ".trec") != NULL || tailstr(params.inFileName, ".TREC") != NULL) {
    processTRECFormat(&globals);
  } else {
    processSTARCFormat(&globals);
  }
     

  printf("Initial scan took %.3f seconds.  Here are some statistics: \n",
	 what_time_is_it() - startTime);
  printSummary(&globals);
  dahash_print_key_stats(globals.gMarkovHash, TRUE, "after all docs scanned for buildVocab()");

  startTime = what_time_is_it();

  convertToProbs();
  
  printf("SMG(%d): Finalised transition table in %.3f sec.\n"
	   "SMG(%d): Overall time to train the model: %.3f sec.\n",
    params.k, what_time_is_it() - startTime,  params.k, what_time_is_it() - veryStart);

  fclose(globals.simplifiedText);
  unmmap_all_of(globals.inputInMemory, globals.inputFH, globals.inputMH, globals.inputSize);

  // Run the model to produce the outputCorpus, but first set the desired
  // number of postings to generate

  globals.desiredTotalPostings = (long long) ((double) globals.totalPostingsIn * params.postingsScaleFactor + 0.5);
  printf("Total postings In = %lld,  will try to generate %lld due to scale factor %.2f\n",
	 globals.totalPostingsIn, globals.desiredTotalPostings, params.postingsScaleFactor);

  generateCorpusFromModel();
  
  MB =  (double)dahash_memory_used(globals.gMarkovHash) / 1048576.0;
  totalMB += MB;
  printf("\nMemory used by main Markov hash table: %.1fMB\n", MB); 
  
  printf("===============================================\n\n"
	 "SMG(%d): Total memory used (ignoring mmapped input file): %.3fMB approx.\n"
	 "SMG(%d): Hash table entries: %zu.  Hash bits: %d\n",
	 params.k, totalMB, params.k, globals.gMarkovHash->entries_used, globals.gMarkovHash->bits);

  dahash_destroy(&(globals.gMarkovHash));
  fclose(globals.corpusOut);
#ifdef __clang__
  printf("SMG(%d): Version: %s\n", params.k, __TIMESTAMP__);
#endif

  printf("SMG(%d): Overall runtime: %.3f sec.\n"
	 "SMG(%D): Failed lookups %lld + %lld.\n"
	 "SMG(%d): Distinct wildcard strings added: %lld\n"
	 "SMG(%d): Input: %s\n"
	 "SMG(%d): Output: %s\n"
	 "SMG(%d): docs: %lld out v. %lld - %lld = %lld in\n"
	 "SMG(%d): sentences in: %lld\n"
	 "SMG(%d): total_postings: %lld out v. %lld in  (due to scaleFactor %.2f)\n"
	 "SMG(%d): randomSeed: %lld\n\n",
	 params.k, what_time_is_it() - veryStart,
	 params.k, failed_lookups, failed_mod_lookups,
	 params.k, wildcardStringsAdded,
	 params.k, params.inFileName,
	 params.k, params.outFileName,
	 params.k, globals.docsOut, globals.numDocs, globals.numEmptyDocs, globals.numDocs - globals.numEmptyDocs,
	 params.k, sentenceCount,
	 params.k, globals.postingsOut, globals.totalPostingsIn, params.postingsScaleFactor,
	 params.k,params.randomSeed);

 
}
