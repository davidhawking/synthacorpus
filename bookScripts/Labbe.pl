#! /usr/bin/perl -w

# Calculates the Labbe stylistic distance between two vocab_by_freq.tsv files by accumulating
# the absolute differences in frequency between the words in the union of the top k words
# in each text.

# The original version of this script assumed that the two texts were of equal length.  That was correct
# for the experiments reported in the M&C monograph but isn't correct in general.   This version (Aug 2024)
# compensates for length differences by scaling up the smaller frequencies by the ratio of the lengths.

die "Usage: $0 <text1> <text2> (vocab|bigrams|ngrams) [<num-terms>]\n"
  unless $#ARGV >= 2;

$type = $ARGV[2];

$fyl1 = $ARGV[0]."_${type}_by_freq.tsv";
$fyl2 = $ARGV[1]."_${type}_by_freq.tsv";

die "Can't open $fyl1\n"
  unless open F1, $fyl1;

die "Can't open $fyl2\n"
  unless open F2, $fyl2;

$k = 200;
if ($#ARGV >= 3) {$k = $ARGV[3];}


# ----------------------------- Scaling frequencies -------------------------------------------
$sumry1 = $ARGV[0]."_summary.txt";
$sumry2 = $ARGV[1]."_summary.txt";

die "can't open $sumry1\n" unless open S1, $sumry1;
die "can't open $sumry2\n" unless open S2, $sumry2;


while (<S1>) {
  $postings1 = $1 if (/total_postings=([0-9]+)/);
}
close(S1);
die "Can't find total_postings in $sumry1\n" unless defined $postings1;

while (<S2>) {
  $postings2 = $1 if (/total_postings=([0-9]+)/);
}
close(S2);
die "Can't find total_postings in $sumry2\n" unless defined $postings2;

if ($postings2 > $postings1) {
  $scale2 = 1;
  $scale1 = sprintf("%.3f", $postings2 / $postings1);
} else {
  $scale1 = 1;
  $scale1 = sprintf("%.3f", $postings1 / $postings2);
}  

print "$ARGV[0] frequencies will be scaled up by $scale1\n$ARGV[1] frequencies will be scaled up by $scale2\n\n";


# ----------------------------- Calculating Labbe distance  -------------------------------------------

$totFreq = 0;
$sumAbsDiffs = 0;

print "\nUsing top $k items ($type) from $fyl1 and $fyl2.  Words must comprise ASCII letters only.\n\n";

$c1 = 0;
while (<F1>) {
  next unless /([A-Za-z ]+)\t([0-9]+)/;
  $scaledfreq = $2 * $scale1;
  $h1{$1} = $scaledfreq;
  $totFreq += $scaledfreq;
  $c1++;
  last if ($c1 >= $k);
}
close(F1);

$c2 = 0;
while (<F2>) {
  next unless /([A-Za-z ]+)\t([0-9]+)/;
  $scaledfreq = $2 * $scale2;

  $totFreq += $scaledfreq;
  if (defined($h1{$1})) {
    if (0) {print "Olap $1\n";}
    $sumAbsDiffs += abs($h1{$1} - $scaledfreq);
    delete($h1{$1});
  } else {
    $sumAbsDiffs += $2;
  }
  $c2++;
  last if ($c2 >= $k);
}
close(F2);

# Now clean up the items which were only in F1
foreach $key (keys %h1) {
  $sumAbsDiffs += $h1{$key};
}


print "\n\nLabbe distance = ", sprintf("%.4f\n\n", $sumAbsDiffs / $totFreq);

exit(0);
