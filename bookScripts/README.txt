** bookScripts directory **

David.Hawking@acm.org   15 Apr 2020

The scripts in this directory were used in conducting and analysing the SynthaCorpus experiments
described in the forthcoming Morgan & Claypool book, "Simulating Information Retrieval Test 
Collections".

THIRD-PARTY SOFTWARE
================
The Chapter 9 experiments (ch09_*.pl) require the prior installation of
three open source search engines:  ATIRE, Indri and Terrier, plus the
trec_eval program from NIST.

    http://atire.org/
    https://github.com/diazf/indri/
    http://terrier.org/download/
    https://trec.nist.gov/trec_eval/

The scripts assume that all the third-party software resides under a
single directory, referenced by $SC_THIRD_PARTY_SW

ENVIRONMENT VARIABLES
================
SynthaCorpus itself assumes the definition of $SC_EXPERIMENT_ROOT

NOTES:
=====
0. These scripts rely heavily on the main SynthaCorpus package ../src

1. I treat perl as an up-market scripting language.   Rather than
treating a script as an immutable app where all variations must be
specified using options, I assume that the script will be edited.
Please try to avoid pushing temporary modifications into the central
repository.

2. Some of the scripts will typically run for days.  A particular
example is ch09MainExperiment.pl.  Nearly all scripts check first up
that all subsidiary scripts and executables are accessible and
executable using a check_exe() subroutine.  You don't want the
script failing after 2 days because it can't access an analysis
script.

3. A couple of long running scripts include a send_imessage_to_Dave()
subroutine, which is used to send messages to my phone as processing
milestones are reached, or when a crash occurs.  Currently this is
Apple specific.  If you don't have an Apple, a warning message will
appear each time a message would have been sent.  If you do have an
Apple, and you want to receive the messages, you may have to do a bit
of modification of your Contacts to get it to work.

4. I try to write scripts to be as stand-alone as possible.  You
shouldn't ever need to go to CPAN to download obscure packages.  I
also have the habit, which many software engineers don't like, of
repeating the code for check_exe() and runit() in each script, rather
than using a library.

5. I avoid advanced perl constructs like references because many perl
users don't understand them.

6. I am a great fan of the perl 'unless' construct.  Many scripts
begin with a series of:

      die "condition not satisfied\n"  unless condition_satisfied;

7. It may seem incredible but the perl statement '$|++;' means
unbuffer the current default output stream, usually STDOUT.  In some
cases I unbuffer another filehandle FH by

      select FH; $|++;  select STDOUT;

8. All of the scripts begin with the hashbang line:

     #! /usr/bin/perl -w

Doing that ensures that editors like emacs turn on perl-specific
features.  You can use any perl you like by running the script as e.g.

	  /some/other/perl ch09MainExperiment.pl

The script will record which perl you used and use it for running any
subsidiary perl scripts.

9.  I run with warnings active, but I never use 'use strict'.  Sorry, I
hate it.  My convention is to use 'my' for all local variables in
subroutines but not in main line code.  In a quarter of a century I've
very seldom run into trouble which might have been detected by
'strict'.

10. All (?) scripts assume that the environment variable
$SC_EXPERIMENTS_ROOT has been set to reference a directory under which
all the data and results for SynthaCorpus experiments is or can be
put.





