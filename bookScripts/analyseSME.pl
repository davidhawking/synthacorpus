#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# This is the script which analyses the results from runStringMarkovExperiments.pl and plots
# the results.   The aim is to investigate the dependency of degree of vocabulary overlap
# and sentence overlap on the value of k.
#
# It extracts data from $smRoot/*/${corpus}_base_v_mimic_summary.txt, where the * matches all the
# different values of k.  I assume that runStringMarkovExperiments.pl writes vocab overlap
# and sentence overlap values in the summary.txt files.
#
# Data files written by this script are stored in $smRoot, along with plot.cmds.   Generated
# PDF is stored in ../book/imageFiles/stringMarkov/overlaps


use File::Copy;

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$emuRoot = "$expRoot/Emulation";
$smRoot = "$emuRoot/stringMarkov";

die "Usage: $0 <corpus>\n"
  unless $#ARGV == 0;
$corpus = $ARGV[0];

$bookPlotDir = "$ENV{HOME}/GIT/SynthaCorpus/Book/imageFiles/stringMarkov/overlaps";
mkdir $bookPlotDir unless -e $bookPlotDir;

die "Directory $smRoot not found\n"
  unless -d $smRoot;

$datFile = "$smRoot/$corpus.dat";
die "Can't write to $datFile\n"
  unless open DAT, ">$datFile";
print DAT "#k vsize volap tolap\n";

$plotFile = "$smRoot/${corpus}_plot.cmds";
die "Can't write to $plotFile\n"
  unless open PLOT, ">$plotFile";

for $sumf (glob "$smRoot/*/${corpus}_base_v_mimic_summary.txt") {
  next unless $sumf =~ m@/([0-9]+)/[^/]*$@;
  $k = $1;
  die "Can't read $sumf\n"
    unless open SF, $sumf;
  undef $vsp;
  undef $volap;
  undef $tolap;
  while (<SF>) {
    if (/vocab_size.*\(([-+0-9.]+)%\)/) {
      $vsp = $1;
    } elsif (/Text overlap.*= ([-+0-9.]+)%/) {
      $tolap = $1;
    } elsif (/Vocab overlap.*= ([-+0-9.]+)%/) {
      $volap = $1;
    }
  }
  if (defined($vsp) && defined($tolap) && defined($volap)) {
    print DAT sprintf("%2d %7.2f %7.2f %7.2f\n", $k, $vsp, $volap, $tolap);
  } else {
    warn "Not all data found in $sumf - ignoring\n"
  }
}
close(SF);
close(DAT);

$sDatFile = "$datFile";
$sDatFile =~ s/\.dat/.sdat/;
system("sort -n -k1,1 $datFile > $sDatFile");
die "Sort failed\n" if $?;

print PLOT "set terminal pdf
set size ratio 1
set yrange [-50:200]
set output '$bookPlotDir/kTradeOff_$corpus.pdf'
set title \"stringMarkov emulation of $corpus\"
set xlabel 'Markov order k'
set ylabel 'Percentage change relative to simplified base'
plot '$sDatFile' using 1:2 with linespoints title '% vocab size', '$sDatFile' using 1:3 with linespoints title '% vocab overlap', '$sDatFile' using 1:4 with linespoints title '% sent. overlap'
";
close(PLOT);

system("gnuplot $plotFile");
die "Gnuplot $plotFile failed\n" if $?;

print "
      acroread $bookPlotDir/kTradeOff_$corpus.pdf

";

exit(0);

