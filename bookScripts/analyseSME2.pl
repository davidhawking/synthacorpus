#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

use File::Copy;

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$emuRoot = "$expRoot/Emulation";
$smRoot = "$emuRoot/stringMarkov";

die "Usage: $0 <corpus>\n"
  unless $#ARGV == 0;
$corpus = $ARGV[0];

die "Directory $smRoot not found\n"
  unless -d $smRoot;

$bookPlotDir = "$ENV{HOME}/GIT/SynthaCorpus/Book/imageFiles/stringMarkov/overlaps";
mkdir $bookPlotDir unless -e $bookPlotDir;


$datFile = "$smRoot/$corpus.dat2";
die "Can't write to $datFile\n"
  unless open DAT, ">$datFile";
print DAT "# In the below, vsize is the % change relative to the base vocab size.
#Overlaps are the % of items in the emulated corpus which are present in the base.
#In volap the items are words in the vocab.  In tolap they are sentences in the text.

#log10(lambda)  vsize   volap    tolap
#=====================================\n";
$plotFile = "$smRoot/${corpus}_plot2.cmds";
die "Can't write to $plotFile\n"
  unless open PLOT, ">$plotFile";

for $sumf (glob "$smRoot/*_*_*/${corpus}_base_v_mimic_summary.txt") {
  die "Pat match failed.\n" unless $sumf =~ m@/([0-9]+)_([0-9]+)_([0-9.]+)/[^/]*$@;
  $k = $1; $wildcards = $2; $lambda = $3;

  next unless $k == 23;   # Temporary measure

  
  print " -- scanning $sumf:  $k -- $wildcards -- $lambda\n";
  die "Can't read $sumf\n"
    unless open SF, $sumf;
  undef $vsp;
  undef $volap;
  undef $tolap;
  while (<SF>) {
    if (/vocab_size.*\(([-+0-9.]+)%\)/) {
      $vsp = $1;
    } elsif (/Text overlap.*= ([-+0-9.]+)%/) {
      $tolap = $1;
    } elsif (/Vocab overlap.*= ([-+0-9.]+)%/) {
      $volap = $1;
    }
  }
  if (defined($vsp) && defined($tolap) && defined($volap)) {
    print DAT sprintf("%.2f %7.2f %7.2f %7.2f\n", log10($lambda), $vsp, $volap, $tolap);
  } else {
    warn "Not all data found in $sumf - ignoring\n"
  }
}
close(SF);
close(DAT);

$sDatFile = "$datFile";
$sDatFile =~ s/\.dat/.sdat/;
system("sort -n -k1,1 $datFile > $sDatFile");
die "Sort failed\n" if $?;

print PLOT "set terminal pdf
set size ratio 1
set output '$bookPlotDir/lambdaTradeOff_$corpus.pdf'
set title \"stringMarkov emulation of $corpus\"
set xlabel 'log10(lambda)'
set ylabel 'Percentage change relative to simplified base'
plot [-7:0][-50:200] '$sDatFile' using 1:2 with linespoints title '% vocab size', '$sDatFile' using 1:3 with linespoints title '% vocab overlap', '$sDatFile' using 1:4 with linespoints title '% sent. overlap'
";

# set output 'shrinkage_${corpus}_vocab_size.pdf'
# set title \"stringMarkov emulation of $corpus - vocab size\"
# set xlabel 'No. wildcards'
# set ylabel 'Lambda'
# set zlabel '% change w.r.t. simplified base'
# splot '$datFile' using 2:3:4 title ''

# set output 'shrinkage_${corpus}_vocab_overlap.pdf'
# set title \"stringMarkov emulation of $corpus - vocab overlap\"
# set zlabel '% change w.r.t. simplified base'
# splot '$datFile' using 2:3:5 title ''

# set output 'shrinkage_${corpus}_sentence_overlap.pdf'
# set title \"stringMarkov emulation of $corpus - sentence overlap\"
# set zlabel '% change w.r.t. simplified base'
# splot '$datFile' using 2:3:6 title ''

# ";
close(PLOT);

system("gnuplot $plotFile");
die "Gnuplot $plotFile failed\n" if $?;

print "
      cat $datFile
      
      acroread $bookPlotDir/lambdaTradeOff_$corpus.pdf

";

exit(0);

# -------------------------------------------------
sub log10 {
  my $n = shift;
  return log($n)/log(10);
}
