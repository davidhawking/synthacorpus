#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# Evaluate how well runs over the GPT-2 emulated AP corpus predict those over the Base.
# Now added stringMarkov methods to experiment


use File::Copy "cp";
use Time::HiRes qw(time);
use Math::Round;

use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
  $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

chomp($hostname = `hostname`);

$|++;

$veryStart = time();

$perl = $^X;
$perl =~ s@\\@/@g;


die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";
$emuRoot= "$expRoot/Emulation";

die "The chapter 9 (ch09_*.pl) scripts require the installation of third-party 
retrieval software, located under a directory identified by the environment 
variable SC_THIRD_PARTY_SW. If you use Bash you may need something like:   

     export SC_THIRD_PARTY_SW=/opt/local/

"
  unless defined($ENV{SC_THIRD_PARTY_SW});

$softwareDir = $ENV{SC_THIRD_PARTY_SW};


@corpora = (
	    #"t8nlqs", "patents", "fr",
	    "ap"
	   );

@emus = (
	 #"GPT-2",
	 #"WBMarkov1",
	 #"SophSynth",
	 #"SimpleSynth"
	 "stringMarkov/5_0_0",
	 "stringMarkov/15_13_0.000006"
	);

$timingObservations = 5;  # Number of times to run a timed trial

@retrievalSystems = ("ATIRE", "Indri", "Terrier"
		    );

$trec_evalCmd = "$softwareDir/trec_eval.9.0/trec_eval -c -M1000";  # then <qrels> <results>

$atireDir = "$softwareDir/atire-caa2f2598c19";
$indriDir = "$softwareDir/indri";
$terrierDir = "$softwareDir/terrier-project-5.1";

%ixCmds = ( ATIRE => "$atireDir/bin/index -sa -findex wipeMe.index -rtrec",
	    Indri => "$indriDir/buildindex/IndriBuildIndex wipeMe.indriIndexParams",
	    Terrier => "$terrierDir/bin/terrier batchindexing -j",   # -j suppresses building of direct index (faster)
	  );

%qpCmds = ( ATIRE => "$atireDir/bin/atire -QN:d -sa -l1000 -et -findex wipeMe.index",
	    Indri => "$indriDir/runquery/IndriRunQuery wipeMe.indriQPParams",
	    Terrier => "$terrierDir/bin/terrier batchretrieve",
	  );

$qGenerator = check_exe("../src/queryGenerator.exe");

# Unfortuntely we need separate output files for each of the 8 dependent variables.
# I don't know how to do this in a loop :-(
dye("Can't write to ch09FollowUp2.results.it file\n")
  unless open RF1, ">ch09FollowUp2.results.it";
print RF1 "#Corpus\tIR sys\tEmuMeth\tGen\tidx time\n";
dye("Can't write to ch09FollowUp2.results.im file\n")
  unless open RF2, ">ch09FollowUp2.results.im";
print RF2"#Corpus\tIR sys\tEmuMeth\tGen\tidx MB\n";
dye("Can't write to ch09FollowUp2.results.q3t file\n")
  unless open RF3, ">ch09FollowUp2.results.q3t";
print RF3 "#Corpus\tIR sys\tEmuMeth\tGen\t3wd QP time\n";
dye("Can't write to ch09FollowUp2.results.q6t file\n")
  unless open RF4, ">ch09FollowUp2.results.q6t";
print RF4 "#Corpus\tIR sys\tEmuMeth\tGen\t6wd QP time\n";
dye("Can't write to ch09FollowUp2.results.q9t file\n")
  unless open RF5, ">ch09FollowUp2.results.q9t";
print RF5 "#Corpus\tIR sys\tEmuMeth\tGen\t9wd QPtime\n";
dye("Can't write to ch09FollowUp2.results.q3rr file\n")
  unless open RF6, ">ch09FollowUp2.results.q3rr";
print RF6 "#Corpus\tIR sys\tEmuMeth\tGen\t3wd MRR\n";
dye("Can't write to ch09FollowUp2.results.q6rr file\n")
  unless open RF7, ">ch09FollowUp2.results.q6rr";
print RF7 "#Corpus\tIR sys\tEmuMeth\tGen\t6wd MRR\n";
dye("Can't write to ch09FollowUp2.results.q9rr file\n")
  unless open RF8, ">ch09FollowUp2.results.q9rr";
print RF8 "#Corpus\tIR sys\tEmuMeth\tGen\t9wd MRR\n";

for $corpus  (@corpora) {
  runExperiments($corpus, "Base", $baseDir, $timingObservations);
    
  send_imessage_to_Dave("$0: Baseline runs complete for corpus $corpus");
    
  for $emu (@emus) {
    $emuDir = "$emuRoot/$emu";
    runExperiments($corpus, $emu, "$emuDir", $timingObservations);
    
    send_imessage_to_Dave("$0: $emu runs complete for corpus $corpus");
  }
}


close(RF1);  close(RF2);  close(RF3);  close(RF4);  close(RF5);  close(RF6);  close(RF7);  close(RF8);
print "\n\n\n   ---------  $0 finished normally. Elapsed time: ", elapsedTimeInDHMS($veryStart), " Results in ch09FollowUp2.results.* (except .pl)  ---------\n\n\n";

exit(0);
# -----------------------------------------------------------------------------------


sub elapsedTimeInDHMS {
  my $start = shift;
  my $elapsed = round(time() - $start);
  my $sex = $elapsed % 60;
  $elapsed /= 60;
  my $min = $elapsed % 60;
  $elapsed /= 60;
  my $hrs = $elapsed % 24;
  $elapsed /= 24;
  my $days = int($elapsed);
  return "$days:$hrs:$min:$sex";
}
  
sub echoToFile {
  my $string = shift;
  my $file = shift;
  dye("Can't echo to $file\n")
    unless open EF, ">$file";
  print EF $string;
  close(EF);
}


sub runExperiments {
  
  my $corpus= shift;    # The name of the corpus, e.g. "ap"
  my $methName = shift; # The name of the emulation method, or "Base"
  my $dir= shift;       # Directory containing the corpus and all the extracted property files
  my $R = shift;        # The number of observations averaged for each measurement

  my $i;
  my $j = 1;
  my $ignore;
  my $startTime;
  my $elapsed;
  my $memUse = 0;
  my %h;
  my $k;


  ifnec_generate_queries_for_corpus($dir, $corpus);

  for my $RS (@retrievalSystems) {

    setUpForRS($RS, $dir, $corpus);

    # -------------- Indexing time ------------------
    for ($i = 1; $i <= $R; $i++) {
      $ixCmd = $ixCmds{$RS};
      $qpCmd = $qpCmds{$RS};
      $conditionCols = "$corpus\t$RS\t$methName"; 
      
      if ($RS eq "Terrier") {
	# Must remove old index before making new one: "Cannot index while an index exists"
	$cmd = "/bin/rm -rf $terrierDir/var/index/*";
	print $cmd, "\n";
	system($cmd);
	dye("Can't remove old Terrier index\n") if $?;
	
	$cmd = "$terrierDir/bin/trec_setup.sh $dir/$corpus.trec &> wipeMe.terrier";
	print $cmd, "\n";
	system($cmd);
	dye("Can't set up for Terrier indexing.  Command failed.\n") if $?;
	
	# We seem to have to specifically write just the line we want in collection.spec
	# otherwise it accumulates multiple lines.
	$cmd = "echo $dir/$corpus.trec > $terrierDir/etc/collection.spec";
	system($cmd);
	dye("$cmd failed\n") if $?;
      } elsif ($RS eq "Indri") {
	# Must also remove old Indri index before making new one
	$cmd = "/bin/rm -rf wipeMe.IndriIndex/*";
	#print $cmd, "\n";
	system($cmd);
	dye("Can't remove old Indri index\n") if $?;
      } else {
	$ixCmd .= " $dir/$corpus.trec";
	system("/bin/rm -rf wipeMe.index");   # ATIRE produces a directory
	dye("Can't remove old ATIRE index\n") if $?;
      }
      print "[$j:$i/$R $methName ", elapsedTimeInDHMS($veryStart), "] Indexing $corpus with $RS: ";
      $startTime = time();
      my $fixCmd = "$ixCmd &> wipeMeIX.log";
      #print $fixCmd, "\n";
      system($fixCmd);
      dye("Indexing command $ixCmd failed\n")
	if $?;
      $elapsed = time() - $startTime;
      if ($RS eq "ATIRE") {
	$memUse = `grep -i "^Total memory used" wipeMeIX.log`;
	$memUse =~ /([0-9]+) bytes/;
	$memUse = $1 / 1024 / 1024;
	#print sprintf("%.3f sec. %.2fMB used\n", $elapsed, $memUse);
	print RF2 "$conditionCols\t$j\t$memUse\t\n";
      }
      print sprintf("%.3f sec.\n", $elapsed);
      print RF1 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
    }
    
    # ------------- Query processing times ------------
    for my $wds (3, 6, 9) {   # Loop over number of words in the query
      for ($i = 1; $i <= $R; $i++) {  # Repeated timing observations
	print "[$j:$i/$R $methName ", elapsedTimeInDHMS($veryStart), " Processing ${wds}wd $corpus queries using $RS: ";
	$startTime = time();
	if ($RS eq "ATIRE") {
	  $cmd = "$qpCmd -q $dir/${corpus}_${wds}wds.topics -owipeMe${wds}wds.results &> wipeMeQP.log";
	  #print $cmd;
	  system($cmd);
	  dye("ATIRE query processing failed. $cmd\n") if $?;
	} elsif ($RS eq "Terrier") {
	  $cmd = "${qpCmd} -t $dir/${corpus}_${wds}wds.topics -o $cwd/wipeMe${wds}wds.results &> wipeMeQP.log";
	  system($cmd);
	  dye("Terrier query processing failed. $cmd\n") if $?;
	} else {
	  $cmd = "${qpCmd}_${wds}wds > wipeMe${wds}wds.results";
	  system($cmd);
	  dye("Indri query processing failed. $cmd\n") if $?;
	}
	
	$elapsed = time() - $startTime;
	print sprintf("%.3f sec.\n", $elapsed);
	
	if ($wds == 3) {
	  print RF3 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
	} elsif ($wds == 6) {
	  print RF4 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
	} elsif ($wds == 9) {
	  print RF5 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
	}
      }   # End of loop over repeated observations
    } # End of loop over query lengths
    
    
    # ------------- Query processing effectiveness ------------
    $cmd = "$trec_evalCmd $dir/${corpus}_3wds.qrels wipeMe3wds.results | grep recip_rank";
    $mrr = `$cmd`;
    dye("Trec_eval ($cmd) failed\n") if $?;
    if ($mrr =~ /([0-9.]+)/) {$mrr3 = $1;};
    print RF6 "$conditionCols\t$j\t$mrr3\n";
    
    $cmd = "$trec_evalCmd $dir/${corpus}_6wds.qrels wipeMe6wds.results | grep recip_rank";
    $mrr = `$cmd`;
    dye("Trec_eval ($cmd) failed\n") if $?;
    if ($mrr =~ /([0-9.]+)/) {$mrr6 = $1;};
    print RF7 "$conditionCols\t$j\t$mrr6\n";
    
    $cmd = "$trec_evalCmd $dir/${corpus}_9wds.qrels wipeMe9wds.results | grep recip_rank";
    $mrr = `$cmd`;
    dye("Trec_eval ($cmd) failed\n") if $?;
    if ($mrr =~ /([0-9.]+)/) {$mrr9 = $1;};
    print RF8 "$conditionCols\t$j\t$mrr9\n";
    
    print "[$corpus, $RS, $methName, $j] Mean reciprocal ranks for 3 word, 6 word and 9 word queries:  $mrr3, $mrr6, $mrr9\n";
  }   #End of loop over retrieval systems
}


sub setUpForRS {
  my $RS = shift;
  my $emuDir = shift;
  my $corpus = shift;
  
  #  ++++++++++++++++ Do the RS-specific setups +++++++++++++++++++++++
  if ($RS eq "ATIRE") {
    # Nothing really to do here
  } elsif ($RS eq "Indri") {
    dye("Can't write to Indri index params file\n")
      unless open IP, ">wipeMe.indriIndexParams";
    print IP "<parameters>
  <index>wipeMe.IndriIndex</index>
  <memory>12G</memory>
  <corpus>
    <path>$emuDir/$corpus.trec</path>
    <class>trectext</class>
  </corpus>
</parameters>
";
    close(IP);
    
    # For Indri, we need to write three separate parameters files for queries
    for my $suff ("_3wds", "_6wds", "_9wds") {
      dye("Can't write to Indri runQuery params file wipeMe.indriQPParams$suff\n")
	unless open IP, ">wipeMe.indriQPParams$suff";
      print IP "<parameters>
<index>wipeMe.IndriIndex</index>
<runID>UNKNOWN</runID>
<trecFormat>true</trecFormat>
<threads>9</threads>

";
      # Now read the .topics file and convert into the body of the Indri params file
      dye("Can't read from $emuDir/$corpus$suff.topics\n")
	unless open TOP, "$emuDir/$corpus$suff.topics";
      while (<TOP>) {
	if (/^<num> Number: ([0-9]+)/) {
	  my $topnum = $1;
	  print IP "<query>\n<number>$topnum</number>\n";
	} elsif (/<title> (.*)\n/) {
	  my $q = $1;
	  print IP "<text> $1 </text>\n</query>\n";
	  }
      }
      print IP "</parameters>\n";
      close(IP);
      }
  } elsif ($RS eq "Terrier") {
    # Terrier reads config stuff from files in $TERRIER_HOME/etc and writes to files in
    # $TERRIER_HOME/var, particularly under $TERRIER_HOME/var/index.  It seems you can
    # only have configs for one index and one query set in $TERRIER_HOME.  That's OK for
    # us.  First we use trec_setup.sh to set TERRIER_HOME and set up for indexing the current index
    # We now rerun trec_setup.sh prior to each indexing run.
    $cmd = "$terrierDir/bin/trec_setup.sh $emuDir/$corpus.trec &> wipeMe.terrier";
    print $cmd, "\n";
    system($cmd);
      dye("Can't set up for Terrier indexing.  Command failed.\n") if $?;
    
    # Copy in a modified terrier.models file to turn off stopping and stemming
    cp("$terrierDir/etc/terrier.properties.main_experiment","$terrierDir/etc/terrier.properties");
    # Request the use of the PL2 divergence from randomness ranking model
      echoToFile("PL2", "$terrierDir/etc/trec.models");
  } else {
    dye("Unrecognised retrieval system $RS\n");
  }
  # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}



sub ifnec_generate_queries_for_corpus {
  # Generate 3 sets of 1000 KI queries of mean lengths 3, 6, and 9 
  my $dir = shift;
  my $corpus = shift;
  # forceASCII to cope with problem in ATIRE.
  my $baseCmd = "$qGenerator corpusFileName=$dir/$corpus.trec propertiesStem=$dir/$corpus -numQueries=1000 -forceASCII=TRUE";
  if ((! -r "$dir/${corpus}_3wds.topics") || (! -r "$dir/${corpus}_6wds.topics") || (! -r "$dir/${corpus}_9wds.topics")) {
    # Assume that if .topics exists so do .qrels and .q
    for my $qCmd ("$baseCmd -meanQueryLength=2.82 -queriesStem=$dir/${corpus}_3wds",
		  "$baseCmd -meanQueryLength=5.85 -queriesStem=$dir/${corpus}_6wds",
		  "$baseCmd -meanQueryLength=8.85 -queriesStem=$dir/${corpus}_9wds",) {
      print "Command '$qCmd'\n";
      system($qCmd);
      dye("    Failed.\n")  if $?;
    }
  }
}


sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl $exe" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl $cwd/$exe";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "$cwd/$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}



sub send_imessage_to_Dave {
  my $msg = shift;
  my $cmd = "osascript -e 'tell application \"messages\" to send \"$hostname: $msg\" to buddy \"Myself\"'";
  system($cmd);
  warn "$cmd failed\n" if $?
}


sub dye {
  my $msg = shift;
  send_imessage_to_Dave($msg);
  die $msg;
}
