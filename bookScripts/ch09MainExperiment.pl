#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# A script to compare the ability of different emulation methods to predict the performance and resource requirements of
# retrieval systems over a real (Base) corpus.

$perl = $^X;
$perl =~ s@\\@/@g;

use File::Copy "cp";
use Time::HiRes qw(time);
use Math::Round;

use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
  $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

chomp($hostname = `hostname`);

$|++;

$veryStart = time();

if ($#ARGV >= 0 && $ARGV[0] =~ /skipBase/i) { $skipBaseline = 1;}
else {$skipBaseline = 0;}

$timingObservations = 5;  # Number of times to run a timed trial
$randomEmulations = 5;    # Number of times to run a SynthaCorpus emulation (different random seeds each time)

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";
$emuRoot = "$expRoot/MainExperiment";

die "The chapter 9 (ch09_*.pl) scripts require the installation of third-party 
retrieval software, located under a directory identified by the environment 
variable SC_THIRD_PARTY_SW. If you use Bash you may need something like:   

     export SC_THIRD_PARTY_SW=/opt/local/

"
  unless defined($ENV{SC_THIRD_PARTY_SW});

$softwareDir = $ENV{SC_THIRD_PARTY_SW};


$generator = check_exe("../src/corpusGenerator.exe");
$extractor = check_exe("../src/corpusPropertyExtractor.exe");
$qGenerator = check_exe("../src/queryGenerator.exe");
$caesarEncoder = check_exe("../src/Caesar.exe");
$nomenEncoder = check_exe("../src/Nomenclator.exe");

print "Running $timingObservations obs on each of $randomEmulations on host '$hostname'\n";


@corpora = (
	    "ap",
	    "fr", "patents", "WT10g",
	   );

@emuMethodsSynth = (# In the following cases other parameters will be read from the summary file for the base corpus
	       "SophSynth % Piecewise % markov-5e_dlhisto_ngrams3 % $generator -synth_term_repn_method=markov-5e -synth_input_ngrams=BASE_NGRAMS -file_synth_docs=EMU_CORPUS -synth_dl_read_histo=BASE_DLHISTO", 
	       "SimpleSynth % Uniform % tnum_dluniform_ind % $generator -zipf_alpha=0 -synth_term_repn_method=tnum -synth_dl_uniform=TRUE -synth_doc_length=DL_MEAN -file_synth_docs=EMU_CORPUS",
		   );

@emuMethodsMarkov = (# These methods are also non-deterministic, must be run multiple times.
		     #"WBMarkov1 % $MarkovGenerator inFile=BASE_CORPUS outFile=EMU_CORPUS"
		    );

@emuMethodsEncrypt = ("Caesar1 % $caesarEncoder BASE_CORPUS 1 EMU_CORPUS",
		      "Nomenclator % $nomenEncoder BASE_CORPUS CIPHER_VOCAB EMU_CORPUS CIPHER_MAPPING",
		      "Cp % /bin/cp BASE_CORPUS EMU_CORPUS"
		     );


@retrievalSystems = ("ATIRE", "Indri", "Terrier"
		    );

$trec_evalCmd = "$softwareDir/trec_eval.9.0/trec_eval -c -M1000";  # then <qrels> <results>

$atireDir = "$softwareDir/atire-caa2f2598c19";
#$atireDir = "$softwareDir/atire-2101d8082ebb";   # NEWER one didn't handle numbers
$indriDir = "$softwareDir/indri";
$terrierDir = "$softwareDir/terrier-project-5.1";

%ixCmds = ( ATIRE => "$atireDir/bin/index -sa -findex wipeMe.index -rtrec",
	    Indri => "$indriDir/buildindex/IndriBuildIndex wipeMe.indriIndexParams",
	    Terrier => "$terrierDir/bin/terrier batchindexing -j",   # -j suppresses building of direct index (faster)
	  );

%qpCmds = ( ATIRE => "$atireDir/bin/atire -QN:d -sa -l1000 -et -findex wipeMe.index",
	    Indri => "$indriDir/runquery/IndriRunQuery wipeMe.indriQPParams",
	    Terrier => "$terrierDir/bin/terrier batchretrieve",
	  );
	  
# Unfortuntely we need separate output files for each of the 8 dependent variables.
# I don't know how to do this in a loop :-(
dye("Can't write to main_experiment.results.it file\n")
  unless open RF1, ">main_experiment.results.it";
select RF1; $|++;
print RF1 "#Corpus\tIR sys\tEmuMeth\tGen\tidx time\n";
dye("Can't write to main_experiment.results.im file\n")
  unless open RF2, ">main_experiment.results.im";
select RF2; $|++;
print RF2"#Corpus\tIR sys\tEmuMeth\tGen\tidx MB\n";
dye("Can't write to main_experiment.results.q3t file\n")
  unless open RF3, ">main_experiment.results.q3t";
select RF3; $|++;
print RF3 "#Corpus\tIR sys\tEmuMeth\tGen\t3wd QP time\n";
dye("Can't write to main_experiment.results.q6t file\n")
  unless open RF4, ">main_experiment.results.q6t";
select RF4; $|++;
print RF4 "#Corpus\tIR sys\tEmuMeth\tGen\t6wd QP time\n";
dye("Can't write to main_experiment.results.q9t file\n")
  unless open RF5, ">main_experiment.results.q9t";
select RF5; $|++;
print RF5 "#Corpus\tIR sys\tEmuMeth\tGen\t9wd QPtime\n";
dye("Can't write to main_experiment.results.q3rr file\n")
  unless open RF6, ">main_experiment.results.q3rr";
select RF6; $|++;
print RF6 "#Corpus\tIR sys\tEmuMeth\tGen\t3wd MRR\n";
dye("Can't write to main_experiment.results.q6rr file\n")
  unless open RF7, ">main_experiment.results.q6rr";
select RF7; $|++;
print RF7 "#Corpus\tIR sys\tEmuMeth\tGen\t6wd MRR\n";
dye("Can't write to main_experiment.results.q9rr file\n")
  unless open RF8, ">main_experiment.results.q9rr";
select RF8; $|++;
print RF8 "#Corpus\tIR sys\tEmuMeth\tGen\t9wd MRR\n";
select STDOUT;

for $corpus  (@corpora) {
  makePreparations($corpus);
  print "\n\n\n   ---------  Preparations complete for corpus $corpus  ---------\n\n\n";
  sleep(1);

  send_imessage_to_Dave("Preparations complete for corpus $corpus");
  
  # Establish baseline measurements for the base version of this corpus and this retrieval system.
  # This version of runExperiments() now internally loops over retrieval systems

  if ($skipBaseline) {
    print "\n\n******* Skipping BASELINE runs *************\n\n\n";
    send_imessage_to_Dave("Baseline runs skipped for corpus $corpus");
  } else {
    runExperiments($corpus, "Base", $baseDir, $timingObservations, "",  1);
    send_imessage_to_Dave("Baseline runs complete for corpus $corpus");
  }


  for $EMString (@emuMethodsMarkov) {
    ($EM) = split / % /, $EMString;    
    # Establish differences between measurements for this emulation and for the baseline, writing
    # lines in results file RF. (Uses the %hBase hash as the basis for comparison.)
    runExperiments($corpus, $EM, "$emuRoot/$EM", $timingObservations, $EMString, $randomEmulations);
    send_imessage_to_Dave("Markov run $EM complete for corpus $corpus");
  }


  for $EMString (@emuMethodsSynth) {
    ($EM, $upperDir, $lowerDir) = split / % /, $EMString;
    # Establish differences between measurements for this emulation and for the baseline, writing
    # lines in results file RF. (Uses the %hBase hash as the basis for comparison.)
    runExperiments($corpus, $EM, "$emuRoot/$upperDir/$lowerDir", $timingObservations, $EMString, $randomEmulations);
    send_imessage_to_Dave("SynthaCorp run $EM complete for corpus $corpus");
  }

  for $EMString (@emuMethodsEncrypt) {
    ($EM) = split / % /, $EMString;    
    # Establish differences between measurements for this emulation and for the baseline, writing
    # lines in results file RF. (Uses the %hBase hash as the basis for comparison.)
    runExperiments($corpus, $EM, "$emuRoot/$EM", $timingObservations, $EMString, 1);
    send_imessage_to_Dave("Encryption run $EM complete for corpus $corpus");
  }
  
}



close(RF1);  close(RF2);  close(RF3);  close(RF4);  close(RF5);  close(RF6);  close(RF7);  close(RF8);
print "\n\n\n   ---------  $0 finished normally. Elapsed time: ", elapsedTimeInDHMS($veryStart), " Results in main_experiment.results  ---------\n\n\n";

exit(0);
# -----------------------------------------------------------------------------------


sub elapsedTimeInDHMS {
  my $start = shift;
  my $elapsed = round(time() - $start);
  my $sex = $elapsed % 60;
  $elapsed /= 60;
  my $min = $elapsed % 60;
  $elapsed /= 60;
  my $hrs = $elapsed % 24;
  $elapsed /= 24;
  my $days = int($elapsed);
  return "$days:$hrs:$min:$sex";
}
  

sub percDeviation {
  my $exptl = shift;
  my $control = shift;
  # Return a string representing the percentage deviation (always positive) between
  # an experimental value and a control one.  If either parameter is undefined
  # return "N.A"
  return "N.A" if !defined($exptl) || !defined($control);
  return "N.A." if $control == 0.0;
  
  my $perc = 100.0 * $exptl / $control;
  if ($perc < 100.0) { $perc = 100.0 - $perc;}
  else { $perc = $perc - 100.0;}
  return sprintf("%.3f", $perc);
}


sub echoToFile {
  my $string = shift;
  my $file = shift;
  dye("Can't echo to $file\n")
    unless open EF, ">$file";
  print EF $string;
  close(EF);
}


sub runExperiments {
  
  my $corpus= shift;  # The name of the corpus, e.g. "ap"
  my $methName = shift;    # The name of the emulation method, or "Base"
  my $emuDir= shift;  # Directory containing the corpus and all the extracted property files
  my $R = shift;      # The number of observations averaged for each measurement
  my $EMString = shift;
  my $S = shift;      # The number of times we should generate a new emulation ( = 1, unless the method is non-deterministic

  my $i;
  my $j;
  my $ignore;
  my $startTime;
  my $elapsed;
  my $memUse = 0;
  my %h;
  my $k;

  if ($methName ne "Base") {   # $EMString is "" if Base
    my @methFields = split / % /, $EMString;
    if ($#methFields == 3) {$methCmd = $methFields[3];}
    elsif ($#methFields == 1) {$methCmd = $methFields[1];}
    else {
      dye("Wrong number of fields $#EMString (+1) in EMString: $EMString\n");
    }
  }

  for ($j = 1; $j <= $S; $j++) {   # Outer loop:  Potentially run multiple ($S) emulations.

    if ($methName eq "Base") {
      generate_queries_for_corpus($emuDir, $corpus);
    } else {
      print "Doing $methName emulation of corpus $corpus, including query generation.\n\n";
      doEmulationAndQGen($corpus, $emuDir, $methName, $methCmd);
      print "Doing the measurements for this version of the emulation ... \n";
    }

    for my $RS (@retrievalSystems) {

      setUpForRS($RS, $emuDir, $corpus);

      # -------------- Indexing time ------------------
      for ($i = 1; $i <= $R; $i++) {
	$ixCmd = $ixCmds{$RS};
	$qpCmd = $qpCmds{$RS};
	$conditionCols = "$corpus\t$RS\t$methName"; 
	
	if ($RS eq "Terrier") {
	  # Must remove old index before making new one: "Cannot index while an index exists"
	  $cmd = "/bin/rm -rf $terrierDir/var/index/*";
	  print $cmd, "\n";
	  system($cmd);
	  dye("Can't remove old Terrier index\n") if $?;

	  $cmd = "$terrierDir/bin/trec_setup.sh $emuDir/$corpus.trec &> wipeMe.terrier";
	  print $cmd, "\n";
	  system($cmd);
	  dye("Can't set up for Terrier indexing.  Command failed.\n") if $?;

	  # We seem to have to specifically write just the line we want in collection.spec
	  # otherwise it accumulates multiple lines.
	  $cmd = "echo $emuDir/$corpus.trec > $terrierDir/etc/collection.spec";
	  system($cmd);
	  dye("$cmd failed\n") if $?;
	} elsif ($RS eq "Indri") {
	  # Must also remove old Indri index before making new one
	  $cmd = "/bin/rm -rf wipeMe.IndriIndex/*";
	  #print $cmd, "\n";
	  system($cmd);
	  dye("Can't remove old Indri index\n") if $?;
	} else {
	  $ixCmd .= " $emuDir/$corpus.trec";
	  system("/bin/rm -rf wipeMe.index");   # ATIRE produces a directory
	  dye("Can't remove old ATIRE index\n") if $?;
	}
	print "[$j:$i/$R $methName ", elapsedTimeInDHMS($veryStart), "] Indexing $corpus with $RS: ";
	$startTime = time();
	my $fixCmd = "$ixCmd &> wipeMeIX.log";
	#print $fixCmd, "\n";
	system($fixCmd);
	dye("Indexing command $ixCmd failed\n")
	  if $?;
	$elapsed = time() - $startTime;
	if ($RS eq "ATIRE") {
	  $memUse = `grep -i "^Total memory used" wipeMeIX.log`;
	  $memUse =~ /([0-9]+) bytes/;
	  $memUse = $1 / 1024 / 1024;
	  #print sprintf("%.3f sec. %.2fMB used\n", $elapsed, $memUse);
	  print RF2 "$conditionCols\t$j\t$memUse\t\n";
	}
	print sprintf("%.3f sec.\n", $elapsed);
	print RF1 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
      }
    
      # ------------- Query processing times ------------
      for my $wds (3, 6, 9) {   # Loop over number of words in the query
	for ($i = 1; $i <= $R; $i++) {  # Repeated timing observations
	  print "[$j:$i/$R $methName ", elapsedTimeInDHMS($veryStart), " Processing ${wds}wd $corpus queries using $RS: ";
	  $startTime = time();
	  if ($RS eq "ATIRE") {
	    $cmd = "$qpCmd -q $emuDir/${corpus}_${wds}wds.topics -owipeMe${wds}wds.results &> wipeMeQP.log";
	    #print $cmd;
	    system($cmd);
	    dye("ATIRE query processing failed. $cmd\n") if $?;
	  } elsif ($RS eq "Terrier") {
	    $cmd = "${qpCmd} -t $emuDir/${corpus}_${wds}wds.topics -o $cwd/wipeMe${wds}wds.results &> wipeMeQP.log";
	    system($cmd);
	    dye("Terrier query processing failed. $cmd\n") if $?;
	  } else {
	    $cmd = "${qpCmd}_${wds}wds > wipeMe${wds}wds.results";
	    system($cmd);
	    dye("Indri query processing failed. $cmd\n") if $?;
	  }
	  
	  $elapsed = time() - $startTime;
	  print sprintf("%.3f sec.\n", $elapsed);
	  
	  if ($wds == 3) {
	    print RF3 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
	  } elsif ($wds == 6) {
	    print RF4 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
	  } elsif ($wds == 9) {
	    print RF5 "$conditionCols\t$j\t".sprintf("%.3f\n", $elapsed);
	  }
	}   # End of loop over repeated observations
      } # End of loop over query lengths
    

      # ------------- Query processing effectiveness ------------
      $cmd = "$trec_evalCmd $emuDir/${corpus}_3wds.qrels wipeMe3wds.results | grep recip_rank";
      $mrr = `$cmd`;
      dye("Trec_eval ($cmd) failed\n") if $?;
      if ($mrr =~ /([0-9.]+)/) {$mrr3 = $1;};
      print RF6 "$conditionCols\t$j\t$mrr3\n";
    
      $cmd = "$trec_evalCmd $emuDir/${corpus}_6wds.qrels wipeMe6wds.results | grep recip_rank";
      $mrr = `$cmd`;
      dye("Trec_eval ($cmd) failed\n") if $?;
      if ($mrr =~ /([0-9.]+)/) {$mrr6 = $1;};
      print RF7 "$conditionCols\t$j\t$mrr6\n";
      
      $cmd = "$trec_evalCmd $emuDir/${corpus}_9wds.qrels wipeMe9wds.results | grep recip_rank";
      $mrr = `$cmd`;
      dye("Trec_eval ($cmd) failed\n") if $?;
      if ($mrr =~ /([0-9.]+)/) {$mrr9 = $1;};
      print RF8 "$conditionCols\t$j\t$mrr9\n";
    
      print "[$corpus, $RS, $methName, $j] Mean reciprocal ranks for 3 word, 6 word and 9 word queries:  $mrr3, $mrr6, $mrr9\n";
    }   #End of loop over retrieval systems


    # If we are doing multiple emulations, and this is not the last one, then we need to remove traces of the previous emulation
    # before we generate another.  We don't do this on the last iteration, as we can save it in case we re-run the script
    if ($j != $S  && $methName ne "Base") {   # the second clause is belt and braces
      print "\nRemoving all the files relating to $methName emulation of corpus $corpus.\n\n";
      system("/bin/rm -rf $emuDir/${corpus}*");
      dye("  ... removal failed.\n\n") if $?;
    }
    
  } # End of loop over different emulations over the same set of conditions.  
}


sub setUpForRS {
  my $RS = shift;
  my $emuDir = shift;
  my $corpus = shift;
  
  #  ++++++++++++++++ Do the RS-specific setups +++++++++++++++++++++++
  if ($RS eq "ATIRE") {
    # Nothing really to do here
  } elsif ($RS eq "Indri") {
    dye("Can't write to Indri index params file\n")
      unless open IP, ">wipeMe.indriIndexParams";
    print IP "<parameters>
  <index>wipeMe.IndriIndex</index>
  <memory>12G</memory>
  <corpus>
    <path>$emuDir/$corpus.trec</path>
    <class>trectext</class>
  </corpus>
</parameters>
";
    close(IP);
    
    # For Indri, we need to write three separate parameters files for queries
    for my $suff ("_3wds", "_6wds", "_9wds") {
      dye("Can't write to Indri runQuery params file wipeMe.indriQPParams$suff\n")
	unless open IP, ">wipeMe.indriQPParams$suff";
      print IP "<parameters>
<index>wipeMe.IndriIndex</index>
<runID>UNKNOWN</runID>
<trecFormat>true</trecFormat>
<threads>9</threads>

";
      # Now read the .topics file and convert into the body of the Indri params file
      dye("Can't read from $emuDir/$corpus$suff.topics\n")
	unless open TOP, "$emuDir/$corpus$suff.topics";
      while (<TOP>) {
	if (/^<num> Number: ([0-9]+)/) {
	  my $topnum = $1;
	  print IP "<query>\n<number>$topnum</number>\n";
	} elsif (/<title> (.*)\n/) {
	  my $q = $1;
	  print IP "<text> $1 </text>\n</query>\n";
	  }
      }
      print IP "</parameters>\n";
      close(IP);
      }
  } elsif ($RS eq "Terrier") {
    # Terrier reads config stuff from files in $TERRIER_HOME/etc and writes to files in
    # $TERRIER_HOME/var, particularly under $TERRIER_HOME/var/index.  It seems you can
    # only have configs for one index and one query set in $TERRIER_HOME.  That's OK for
    # us.  First we use trec_setup.sh to set TERRIER_HOME and set up for indexing the current index
    # We now rerun trec_setup.sh prior to each indexing run.
    $cmd = "$terrierDir/bin/trec_setup.sh $emuDir/$corpus.trec &> wipeMe.terrier";
    print $cmd, "\n";
    system($cmd);
      dye("Can't set up for Terrier indexing.  Command failed.\n") if $?;
    
    # Copy in a modified terrier.models file to turn off stopping and stemming
    cp("$terrierDir/etc/terrier.properties.main_experiment","$terrierDir/etc/terrier.properties");
    # Request the use of the PL2 divergence from randomness ranking model
      echoToFile("PL2", "$terrierDir/etc/trec.models");
  } else {
    dye("Unrecognised retrieval system $RS\n");
  }
  # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}



sub generate_queries_for_corpus {
  # Generate 3 sets of 1000 KI queries of mean lengths 3, 6, and 9 
  my $dir = shift;
  my $corpus = shift;
  # forceASCII to cope with problem in ATIRE.
  my $baseCmd = "$qGenerator corpusFileName=$dir/$corpus.trec propertiesStem=$dir/$corpus -numQueries=1000 -forceASCII=TRUE";
  for my $qCmd ("$baseCmd -meanQueryLength=2.82 -queriesStem=$dir/${corpus}_3wds",
		"$baseCmd -meanQueryLength=5.85 -queriesStem=$dir/${corpus}_6wds",
		"$baseCmd -meanQueryLength=8.85 -queriesStem=$dir/${corpus}_9wds",) {
    print "Command '$qCmd'\n";
    system($qCmd);
    dye("    Failed.\n")  if $?;
  }
}


sub doEmulationAndQGen {  # corpus, methodName, emuDir, type
  # For a given corpus and a given emulation method, do an emulation, then extract properties,
  # then generate a set of queries.
  #
  # type is Encryption|SynthaCorpus
  #
  # This function may be called once from makePreparations() and possibly multiple times from
  # runExperiment() 
  my $corpus = shift;
  my $emuDir = shift;
  my $methName = shift;
  my $methCmd = shift;


  # The following can be applied to all the methods
  $methCmd =~ s@BASE_CORPUS@$baseDir/$corpus.trec@;    # baseDir is a global but it will be set before we are called
  $methCmd =~ s@EMU_CORPUS@$emuDir/$corpus.trec@;
  $methCmd =~ s@BASE_NGRAMS@$baseDir/${corpus}_ngrams.termids@;
  $methCmd =~ s@BASE_DLHISTO@$baseDir/${corpus}_docLenHist.tsv@;
  $methCmd =~ s@DL_MEAN@$base{doclen_mean}@;

  if ($methName =~ /Synth/i) {    # --- SynthaCorpus Methods ----
    $methCmd .= " -synth_postings=$base{total_postings} -synth_vocab_size=$base{vocab_size}";
    if ($methCmd =~ /markov/i) {
      $methCmd .= " -synth_input_vocab=$baseDir/${corpus}_vocab.tsv";
    }
    if ($methName =~ /Soph/i) {
      $methCmd .= " -zipf_tail_perc=$options{zipf_tail_perc} -head_term_percentages=$options{head_term_percentages} -zipf_middle_pieces=$options{zipf_middle_pieces}"
    }
  } elsif ($methName =~ /Markov/i) {    # --- WBMarkov Methods ----
    # I don't think any specific substitutions are needed here.
  } else {                      # --- Encryption Methods ----
    if ($methName =~/Nomenclator/i) {
      my $cipherVocab = "$emuRoot/Piecewise/markov-5e_dlhisto_ngrams3/${corpus}_vocab.tsv";
      if (! -r $cipherVocab) {
	dye("Cipher vocab file $cipherVocab doesn't exist.  Must run method SophSynth before Nomenclator.\n");
      }	  
      $methCmd =~ s@CIPHER_VOCAB@$cipherVocab@;
      $methCmd =~ s@CIPHER_MAPPING@$emuDir/$corpus.nomenMapping@;
    }
  }

  $methCmd .= " &> wipeMe.Emulation.log";
  print $methCmd, "\n";
  system($methCmd);
  dye("\nEmulation command failed.  See wipeMe.Emulation.log\n")
    if $?;
  print "\nEmulation command succeeded.\n\n\n";
  sleep(2);
  
  # Extract properties for this newly emulated corpus
  $cmd = "$extractor inputFileName=$emuDir/$corpus.trec outputStem=$emuDir/$corpus &>wipeMe.Extraction.log";
  print $cmd, "\n";
  system($cmd);
  dye("\nProperty extraction command failed.  See wipeMe.Extraction.log\n")
      if $?;
  print "\nProperty extraction command succeeded.\n\n\n";
  sleep(2);
  
  generate_queries_for_corpus($emuDir, $corpus);
}
  

sub makePreparations {
  # 1. Make sure that the base corpora are in blah/Experiments/Base
  # 2. And that their properties have been extracted.
  # 3. Make sure that the directory structures for emulations are in place
  
  my $corpus = shift;
  my $emuDir;
  my $methName;
  my $methCmd;
  
  if (! -d "$baseDir") {
    print "$baseDir doesn't exist.  Creating ...\n";
    dye("Can't create.\n")
      unless mkdir("$baseDir");
  } 
  
  # Check that the base corpus exists in the right place.
  if (! -e "$baseDir/$corpus.trec") {
    die "Base corpus $baseDir/$corpus.trec doesn't exist.  You should be able to use
./setup_for_main_experiment.pl to create it from the raw TREC data. ...\n";
  }
  
  # Check whether base corpus properties have been extracted.  Extract if not.
  if (! -e "$baseDir/${corpus}_summary.txt") {
    print "Doesn't look as though properties have been extracted for $baseDir/$corpus.trec.  Extracting ...\n";
    my $cmd = "$extractor inputFileName=$baseDir/$corpus.trec outputStem=$baseDir/$corpus";
    system($cmd);
    dye("Command '$cmd' failed\n") if $?;
  }

  generate_queries_for_corpus($baseDir, $corpus);

  # Make sure the Emulation root directory exists
  if (! -d "$emuRoot") {
    print "$emuRoot doesn't exist.  Creating ...\n";
    dye("Can't create.\n")
      unless mkdir("$emuRoot");
  }
    
  # Check whether all the emulations have been done for this corpus. Remedy any deficiencies.
  # Must do the SynthaCorpus methods first so that the synthetic vocabulary is available for the Nomenclator method
  # Synth methods rely on properties recorded in the Base _summary.txt file, and possibly options from the _vocab.tfd file.

  undef %base;
  # Read key parameters from the _summary.txt file for Base
  dye("Can't open $baseDir/${corpus}_summary.txt\n")
    unless open S, "$baseDir/${corpus}_summary.txt";
  while (<S>) {
    s/\#.*//;
    /(.*)=(.*)/;
    $base{$1} = $2;
  }
  close(S);

  undef %options;
  # Read in synthesis options from the Base .tfd file
  dye("Can't open ${baseDir}/${corpus}_vocab.tfd\n")
    unless open TFD, "${baseDir}/${corpus}_vocab.tfd";
  while (<TFD>) {
    chomp;
    if (m@-*(.*)=([^#]*)@) {  # Ignore hyphens.  Ignore trailing comments
      $attr = $1;
      $val = $2;
      $options{$attr} = $val;
    }
  }
  close(TFD);

  
  # -------- First the Markov and SynthaCorpus methods, which are non-deterministic -----------

  for $method (@emuMethodsMarkov) {
    ($methName, $methCmd) = split / % /, $method;
    $emuDir = "$emuRoot/$methName";
    # Make sure the emulation directory exists
    if (! -d "$emuDir") {
      print "$emuDir doesn't exist.  Creating ...\n";
      dye("Can't create.\n")
	unless mkdir("$emuDir");
    }
   
  }

  for my $method (@emuMethodsSynth) {
    ($methName, $upperDir, $lowerDir, $methCmd) = split / % /, $method;
    $emuDir = "$emuRoot/$upperDir/$lowerDir";

    # Make sure the upper directory exists
     if (! -d "$emuRoot/$upperDir") {
      print "$emuRoot/$upperDir doesn't exist.  Creating ...\n";
      dye("Can't create.\n")
	unless mkdir("$emuRoot/$upperDir");
    }
   
    
    # Make sure the emulation directory exists
    if (! -d "$emuDir") {
      print "$emuDir doesn't exist.  Creating ...\n";
      dye("Can't create.\n")
	unless mkdir("$emuDir");
    }


  }
   

  # -------- Now the encryption methods, which don't use SynthaCorpus -----------
  for $method (@emuMethodsEncrypt) {
    ($methName, $methCmd) = split / % /, $method;
    $emuDir = "$emuRoot/$methName";
    # Make sure the emulation directory exists
    if (! -d "$emuDir") {
      print "$emuDir doesn't exist.  Creating ...\n";
      dye("Can't create.\n")
	unless mkdir("$emuDir");
    }
    
  }
}


sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl $exe" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl $cwd/$exe";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "$cwd/$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}



sub send_imessage_to_Dave {
  my $msg = shift;
  my $cmd = "osascript -e 'tell application \"messages\" to send \"$hostname: $msg\" to buddy \"Myself\"'";
  system($cmd);
  warn "$cmd failed\n" if $?
}

sub dye {
  my $msg = shift;
  send_imessage_to_Dave($msg);
  die $msg;
}
