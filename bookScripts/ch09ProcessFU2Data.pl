#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# This is a substantially cut down (and modified) version of processMEData.pl which is designed only to produce a
# few tables, no plots.   There may be some unnecessary code still in here. 

# Read data files produced by ch09FollowUp2.pl and produce tables and figures for the SynthaCorpus paper.
# 1. Read the file and calculate the mean base score for each (corpus, RS, emu) combination, while saving
#    the raw data for everything other than Base in an array emuData.
# 2. Also calculate mean base scores for each (corpus, emu) combination and each (RS, emu) combination.
# 3. Pass through emuData and store ratios and PAs (prediction accuracies) for each entry in @ratio and @PA
# 4. Output the table of ratios and PAs (columns are emu methods and rows are corpus.RS combinations sorted by RS then by corpus

# Measures used:
# PA - Precision Accuracy.  If real and emulated scores are R and E, PA = min(R,E) / max(R,E), called R_1 in the book
# Ratio - E/B, call R_2 in the book

$|++;

$plotdir = "tmpPlots";
if (! (-d $plotdir)) {
  mkdir $plotdir;
  die "Can't make plot directory $plotdir\n"
    if $?;
}

die "Usage $0 <results file> ...\n"
  unless $#ARGV >= 1;




# 1. A. Process the input files into a form where the repeated observations of each individual
#       condition are averaged into a single number.  Write the results into a file for future
#       reference and also store in an array.
#       The input files contain raw underlying measures, not accuracy scores
#       The name of themeasure is the suffix of the file name, e.g. q6t
#    Here's a few records from a results.it file.  (All the lines for a given condition are grouped.)
#
# #Corpus IR sys  EmuMeth Gen     idx time
# ap      ATIRE   Base    1       7.679
# ap      ATIRE   Base    1       7.959
# ap      ATIRE   Base    1       7.689
# ...
# ap      Indri   Base    1       79.915
# ap      Indri   Base    1       79.814
# ap      Indri   Base    1       79.885

  



foreach $inFile (@ARGV) {    # ---------------  loop over the list of .results files
  $lions = 0;    # Line count
  undef @meanLines;
  ($measure) = $inFile =~ /\.([^.]+)$/;
  print "Measure: $measure\n";

  # A. aggregate the repeated observations.
  die "Can't open input file $inFile\n"
    unless open INN, "$inFile";
  $meansFile = $inFile;
  $meansFile =~ s/results/means/;
  die "Can't write to $meansFile\n"
    unless open MF, ">$meansFile";
  
  $currCond = "";    # $cond is concatenation of corpus, RS, and emuMeth
  $k = 0;   # How many observations for a particular $cond group
  $sum = 0; # sum of observations for a $cond group
  while (<INN>) {
    next if /^\s*#/;  #skip comment lines
    @f = split /\t/;
    die "problem $#f in $_.\n" unless $#f >= 4;
    $lions++;
    $cond = "$f[0]\t$f[1]\t$f[2]";
    #print "$cond: $k\n";
    if ($cond ne $currCond) {
      if ($k > 0) {
	$mean = sprintf("%.4f", $sum / $k);  #  Calculate the mean of the primary measure for this condition. 
	if ($currCond =~ /Base/) {  # Assumes that Base records precede all other records which may relate to them
	  $meanB{$currCond} = $mean;
	  $ratio = 1.0;
	} else {
	  # We've come to the end of a condition group which doesn't relate to the base.
	  $baseCond = $currCond;
	  $baseCond =~ s/\t[^\t]+$/\tBase/;   # Identify the corresponding base condition.
	  die "Can't compute ratio for $_\n"
	    unless defined($meanB{$baseCond}) && $meanB{$baseCond} > 0.0001;
	  $ratio = sprintf("%.4f", $mean / $meanB{$baseCond});  # Calculate the R_2 ratio between this condition and base.
	}
	# Write a line 
	print MF "$currCond\t0\t$mean\t$ratio\n";
	push @meanLines, "$currCond\t0\t$mean\t$ratio\n";
	$sum = 0;
	$k = 0;
      }
      $currCond = $cond;
    }
    $k++;
    $sum += $f[4];
  }    # End of loop over lines in input file

  # Now deal with the group that ended this .results file.
  #   - last line in file can't be a Base line
  $mean = sprintf("%.3f", $sum / $k);
  $baseCond = $currCond;
  $baseCond =~ s/\t[^\t]+$/\tBase/;
  die "Can't compute ratio for $_\n"
    unless defined($meanB{$baseCond}) && $meanB{$baseCond} > 0.0001;
  $ratio = sprintf("%.4f", $mean / $meanB{$baseCond});
  print MF "$currCond\t0\t$mean\t$ratio\n";
  push @meanLines, "$currCond\t0\t$mean\t$ratio\n";
  
  close(MF);
  close(INN);
  print "Lines read from $inFile: $lions.\nMeans for each condition written to $meansFile\n\n";

  
  # B. Process the lines in the meanLines array created from this single .results file.  They have
  #    "corpus RS Emu Generation mean ratio"
  #      Generation is useless. It's always zero. I guess I left it there to retain the field numbering.
  foreach $lyn (@meanLines) {
    @f = split /\t/, $lyn;
    die "problem $#f in $_.\n" unless $#f >= 5;

    $condX = "$f[2].$f[1]";                 # emu.RS                 e.g. SophSynth.q6t          
    $condY = $f[2];                         # emu                    e.g. SophSynth              

    # The following assumes all the Base records come first in the @meanLines array.
    if ($f[2] eq "Base") {
      foreach $k ($condX, $condY) {
	$meanB{$k} = $f[4];
      }
    } else {
      $condsX{$condX}++;
      $condsY{$condY}++;
      $baseCond{$condX} = "$f[0].Base";
      $baseCond{$condY} = "Base";
      foreach $k ($condX, $condY) {
	# For computing the mean and standard deviation of the averaged score for a condition
	$emuN{$k}++;

	$ratio = $f[5];    # Called R_2 in the book
	$PA = $ratio;      # Called R_1 in the book.
	if ($PA > 1.0) {$PA = 1.0 / $PA};

	# Now we compute sums and sums of squares for ratios and PAs
	$ratioSum{$k} += $ratio;
	$ratioSumsq{$k} += $ratio * $ratio;
	$PASum{$k} += $PA;
	$PASumsq{$k} += $PA * $PA;
      }
    }
  }        # Matches foreach lyn in @meanLines
}       # Matches foreach $infile


# 2. Calculate the means and standard deviations for ratios and PA scores
#     keys %emuN enumerates all the distinct conditions
foreach $k (keys %emuN) {

  $ratioMean{$k} = $ratioSum{$k} / $emuN{$k};
  $mean = $ratioMean{$k};
  $ratioStDev{$k} = "NA";
  $ratioStDev{$k} = sqrt($ratioSumsq{$k} / $emuN{$k} - $mean * $mean)
    unless ($emuN{$k} < 3);   # Hayes Eq 6.18.2

  $PAMean{$k} = $PASum{$k} / $emuN{$k};
  $mean = $PAMean{$k};
  $PAStDev{$k} = "NA";
  $PAStDev{$k} = sqrt($PASumsq{$k} / $emuN{$k} - $mean * $mean)
    unless ($emuN{$k} < 3);   # Hayes Eq 6.18.2
}

# Now print some LaTex tables

#First overall numbers per emulation method.

print "\\begin{tabular}{lr}
          & \\multicolumn{2}{c}{Ratio E/B} & \\multicolumn{2}{c}{Accuracy} &  \\\\
Condition & Mean & St. Dev & Mean & St. Dev & \$G\$\\\\ 
\\hline
";
for $cond (sort keys %condsY) {
  print "$cond & ", sprintf("%.4f & %.4f & %.4f & %.4f & ", $ratioMean{$cond}, $ratioStDev{$cond}, $PAMean{$cond}, $PAStDev{$cond}), "$emuN{$cond}\\\\
";
}

print "\\hline
\\end{tabular}
";



print "\\begin{tabular}{lr}
          & \\multicolumn{2}{c}{Ratio E/B} & \\multicolumn{2}{c}{Accuracy} &  \\\\
Condition & Mean & St. Dev & Mean & St. Dev & \$G\$\\\\ 
\\hline
";
for $cond (sort keys %condsX) {
  print "$cond & ", sprintf("%.4f & %.4f & %.4f & %.4f & ", $ratioMean{$cond}, $ratioStDev{$cond}, $PAMean{$cond}, $PAStDev{$cond}), "$emuN{$cond}\\\\
";
}

print "\\hline
\\end{tabular}

";



print "\n\n  *** All done. ***\n\n";

exit(0);



