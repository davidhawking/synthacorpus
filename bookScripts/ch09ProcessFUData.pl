#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# Warning: This script was copied from ch09MainExperiment.pl and hacked about a bit.   It almost certainly
# includes useless code, and needs a good clean-up.


# Read data files produced by ch09FollowUp.pl and produce tables and figures for the SynthaCorpus paper.
# 1. Read the file and calculate the mean base score for each (corpus, RS, emu) combination, while saving
#    the raw data for everything other than Base in an array emuData.
# 2. Also calculate mean base scores for each (corpus, emu) combination and each (RS, emu) combination.
# 3. Pass through emuData and store ratios and PAs (prediction accuracies) for each entry in @ratio and @PA
# 4. Generate overall plot to compare emulation methods:
#         getStats(1, 2);  # generates the %results hash from the PA array
#         plot();  # plots the %results hash
# 5. Generate plot to show interaction of emulation method and retrieval system
#         getStats(2, 1, 2);
#         plot();   # Will this order the bars the way we want?
# 6. Generate plot to show interaction of emulation method and corpus
#         getStats(2, 0, 2);
#         plot();
# 7. Output the table of ratios and PAs (columns are emu methods and rows are corpus.RS combinations sorted by RS then by corpus

# Measures used:
# PA - Precision Accuracy.  If real and emulated scores are R and E, PA = min(R,E) / max(R,E)


# getStats function:  getstats <numcols> <col> ....  Return results in %results hash whose keys are the concatenation of
# the different combinations of the specified columns and whose values are the following , needed for plotting in
# candlestick fashion:
# key box_min  whisker_min  whisker_high  box_high mean   

# 
#  ------ sample of input data --------
#  #Corpus	IR sys	EmuMeth	Gen	6wd QP time
#  ap	ATIRE	Base	1	4.258
#  ap	ATIRE	Base	1	4.251
#
# the condition in the above truncated example would be ap.ATIRE.Base

$|++;

$plotdir = "tmpPlots";
if (! (-d $plotdir)) {
  mkdir $plotdir;
  die "Can't make plot directory $plotdir\n"
    if $?;
}

die "Usage $0 <label> <results file> ...\n"
  unless $#ARGV >= 1;

$MC = shift @ARGV;   # label included in all output filenames




# 1. A. Process the input files into a form where the repeated observations of each individual
#       condition are averaged into a single number.  Write the results into a file for future
#       reference and also store in an array.
#       The input files contain raw underlying measures, not accuracy scores
#       The name of the measure is the suffix of the file name, e.g. q6t
$lions = 0;    # Overall line count

foreach $inFile (@ARGV) {
  ($measure) = $inFile =~ /\.([^.]+)$/;
  print "Measure: $measure\n";

  # A. aggregate the repeated observations.
  die "Can't open input file $inFile\n"
    unless open INN, "$inFile";
  $meansFile = $inFile;
  $meansFile =~ s/results/means/;
  die "Can't write to $meansFile\n"
    unless open MF, ">$meansFile";
  
  $currCond = "";    # $cond is concatenation of corpus, RS, and emuMeth
  $k = 0;   # How many observations for a particular $cond group
  $sum = 0; # sum of observations for a cond group
  while (<INN>) {
    next if /^\s*#/;  #skip comment lines
    @f = split /\t/;
    die "problem $#f in $_.\n" unless $#f >= 4;
    $lions++;
    $cond = "$f[0]\t$f[1]\t$f[2]";
    #print "$cond: $k\n";
    if ($cond ne $currCond) {
      if ($k > 0) {
	$mean = sprintf("%.4f", $sum / $k);
	if ($currCond =~ /Base/) {  # Assumes that Base records precede all other records which may relate to them
	  $meanB{$currCond} = $mean;
	  $ratio = 1.0;
	} else {
	  $baseCond = $currCond;
	  $baseCond =~ s/\t[^\t]+$/\tBase/;
	  die "Can't compute ratio for $_\n"
	    unless defined($meanB{$baseCond}) && $meanB{$baseCond} > 0.0001;
	  $ratio = sprintf("%.4f", $mean / $meanB{$baseCond});
	}
	print MF "$currCond\t$f[3]\t$mean\t$ratio\n";
	push @meanLines, "$currCond\t$f[3]\t$mean\t$ratio\n";
	$sum = 0;
	$k = 0;
      }
      $currCond = $cond;
    }
    $k++;
    $sum += $f[4];
  }
  # Last line in file can't be a Base line
  $mean = sprintf("%.3f", $sum / $k);
  $baseCond = $currCond;
  $baseCond =~ s/\t[^\t]+$/\tBase/;
  die "Can't compute ratio for $_\n"
    unless defined($meanB{$baseCond}) && $meanB{$baseCond} > 0.0001;
  $ratio = sprintf("%.4f", $mean / $meanB{$baseCond});
  print MF "$currCond\t$f[3]\t$mean\t$ratio\n";
  push @meanLines, "$currCond\t$f[3]\t$mean\t$ratio\n";
  
  close(MF);
  close(INN);
  print "Lines read from $inFile: $lions.\nGroup means written to $meansFile\n\n";



  
  # B. Process the lines in the meanLines array.  They have "corpus RS Emu Generation mean ratio"
  foreach $lyn (@meanLines) {
    @f = split /\t/, $lyn;
    die "problem $#f in $_.\n" unless $#f >= 5;
   
    $condA = "$f[0].$f[1].$f[2].$measure";  # corpus.RS.emu.measure
    $condR = "$f[1].$f[2].$measure";        # RS.emu.measure
    $condC = "$f[0].$f[2].$measure";        # corpus.emu.measure
    if ($f[2] eq "Base") {
      foreach $k ($condA, $condR, $condC) {
	$meanB{$k} = $f[4];
      }
    } else {
      push @emuData, "$measure\t$lyn";
      $condsA{$condA}++;
      $condsR{$condR}++;
      $condsC{$condC}++;
      $baseCond{$condA} = "$f[0].$f[1].Base.$measure";
      $baseCond{$condR} = "$f[1].Base.$measure";
      $baseCond{$condC} = "$f[0].Base.$measure";
      foreach $k ($condA, $condR, $condC) {
	# For computing the mean and standard deviation of the averaged score for a condition
	$emuN{$k}++;
	$emuSum{$k} += $f[4];
	$emuSumsq{$k} += $f[4] * $f[4];

	$ratio = $f[5];
	$PA = $ratio;
	if ($PA > 1.0) {$PA = 1.0 / $PA};

	# Now we compute sums and sums of squares for ratios and PAs
	$ratioSum{$k} += $ratio;
	$ratioSumsq{$k} += $ratio * $ratio;
	$PASum{$k} += $PA;
	$PASumsq{$k} += $PA * $PA;
      }
    }
  }
}       # Matches foreach $infile

# Now that we have read all the input, change to the correct plot sub-directory of paper
die "Can't chdir to $plotdir\n"
  unless chdir $plotdir;


# 2. Calculate the emulation means and standard deviations plus ratios and PA scores
foreach $k (keys %emuN) {
  $emuMean{$k} = $emuSum{$k} / $emuN{$k};
  $mean = $emuMean{$k};
  $emuStDev{$k} = "NA";
  $emuStDev{$k} = sqrt($emuSumsq{$k} / $emuN{$k} - $mean * $mean)
    unless ($emuN{$k} < 3);   # Hayes Eq 6.18.2

  $ratioMean{$k} = $ratioSum{$k} / $emuN{$k};
  $mean = $ratioMean{$k};
  $ratioStDev{$k} = "NA";
  $ratioStDev{$k} = sqrt($ratioSumsq{$k} / $emuN{$k} - $mean * $mean)
    unless ($emuN{$k} < 3);   # Hayes Eq 6.18.2

  $PAMean{$k} = $PASum{$k} / $emuN{$k};
  $mean = $PAMean{$k};
  $PAStDev{$k} = "NA";
  $PAStDev{$k} = sqrt($PASumsq{$k} / $emuN{$k} - $mean * $mean)
    unless ($emuN{$k} < 3);   # Hayes Eq 6.18.2
}




# 3. Calculate ratios and PA scores from the @emuData array.  Because we include the measure
#    in the emuData array elements, the accuracy scores are per (corpus, retrieval system, measure).
for ($i = 0; $i <= $#emuData; $i++) {
  @f = split /\t/, $emuData[$i];   # Column zero now contains the measure.
  $condB = "$f[1].$f[2].Base.$f[0]";  # corpus.RS.emu
  if ($meanB{$condB} < 0.001) {
    $ratio[$i] = 1000.0;
  } else {
    $ratio[$i] = $f[5] / $meanB{$condB};
  }
  $PA[$i] = $ratio[$i];
  if ($ratio[$i] > 1.0) {
    $PA[$i] = 1.0 / $ratio[$i];
  }
  $ratio[$i] = sprintf("%.3f", $ratio[$i]);
  $PA[$i] = sprintf("%.3f", $PA[$i]);
  #print "Set PA[$i] to $PA[$i]\n";
}


@colours = (" 255 0 0 ", " 0 255 0", " 0 0 255", " 255 0 255");
# 4. Generate plot data for overall PA results for each emulation method.
$barNum = 0;
die "Can't open overallPA$MC.dat for writing" unless
  open DAT, ">overallPA$MC.dat";
die "Can't open overallPA$MC.means for writing" unless
  open MEANS, ">overallPA$MC.means";

print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  getStats($emu);
}

close(DAT);
close(MEANS);
print "Overall plot data for candlesticks plot written to overallPA$MC.dat\n";


# 5. Generate plot data for PA results for each combination of RS and emu
$barNum = 0;
die "Can't open RSPA$MC.dat for writing" unless
  open DAT, ">RSPA$MC.dat";
die "Can't open RSPA$MC.means for writing" unless
  open MEANS, ">RSPA$MC.means";
print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
foreach $RS ("ATIRE", "Indri", "Terrier") {
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    getStats("$RS.$emu");
  }
  $colour++;
}

close(DAT);
close(MEANS);
print "Plot data for candlesticks plot written to RSPA$MC.dat\n";

# 6. Generate plot data for PA results for each combination of corpus and emu
$barNum = 0;
die "Can't open corpusPA$MC.dat for writing" unless
  open DAT, ">corpusPA$MC.dat";
die "Can't open corpusPA$MC.means for writing" unless
  open MEANS, ">corpusPA$MC.means";
print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
foreach $corpus ("ap", "fr", "patents", "WT10g") {
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    getStats("$corpus.$emu");
  }
  $colour++;
}

  close(DAT);
close(MEANS);
print "Plot data for candlesticks plot written to corpusPA$MC.dat\n";

# Now write the gnuplot commands to draw the necessary graphs
die "Can't write to plot$MC.cmds"
  unless open PLC, ">plot$MC.cmds";

print PLC "set terminal pdf
set size ratio 0.707
set boxwidth 0.25
set style fill solid
set key off
set title \"\"
rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)

set output \"overallPA$MC.pdf\"
set xrange [0:6]
set yrange [0:1.1]
set xtics 0,1,5
set xtics ('Cp' 1, 'Caesar1' 2, 'Nomen.' 3, 'SophS.' 4, 'SimpleS' 5)
plot 'overallPA$MC.dat' using 1:2:3:4:5 with candlesticks whiskerbars, 'overallPA$MC.means' with linespoints

set output \"RSPA$MC.pdf\"
set xrange [0:16]
set yrange [0:1.1]
set xtics rotate
set xtics ('ATIRE.Cp' 1, 'ATIRE.Caesar1' 2, 'ATIRE.Nomen.' 3, 'ATIRE.SophS.' 4, 'ATIRE.SimpleS' 5, 'Indri.Cp' 6, 'Indri.Caesar1' 7, 'Indri.Nomen.' 8, 'Indri.SophS.' 9, 'Indri.SimpleS.' 10, 'Terrier.Cp' 11, 'Terrier.Caesar1' 12, 'Terrier.Nomen.' 13, 'Terrier.SophS.' 14, 'Terrier.SimpleS.' 15)
plot 'RSPA$MC.dat' using 1:2:3:4:5:(rgb(\$6,\$7,\$8)) with candlesticks fillcolor rgb variable

set output \"corpusPA$MC.pdf\"
set xrange [0:21]
set yrange [0:1.1]
set xtics rotate
set xtics ('ap.Cp' 1, 'ap.Caesar1' 2, 'ap.Nomen.' 3, 'ap.SophS.' 4, 'ap.SimpleS' 5, 'fr.Cp' 6, 'fr.Caesar1' 7, 'fr.Nomen.' 8, 'fr.SophS.' 9, 'fr.SimpleS.' 10, 'patents.Cp' 11, 'patents.Caesar1' 12, 'patents.Nomen.' 13, 'patents.SophS.' 14, 'patents.SimpleS.' 15, 'WT10g.Cp' 16, 'WT10g.Caesar1' 17, 'WT10g.Nomen.' 18, 'WT10g.SophS.' 19, 'WT10g.SimpleS' 20)

plot 'corpusPA$MC.dat'  using 1:2:3:4:5:(rgb(\$6,\$7,\$8)) with candlesticks fillcolor rgb variable
";

close(PLC);

system("gnuplot plot$MC.cmds");
 

# Now print some tables 
print 
"\n\nPer corpus, retrieval-system.
----------------------------------------------------------------------\n";

print "#                    Condition         Base            Mean (StDev)           Ratio (StDev)       Accuracy (StDev)\n";
for $cond (sort keys %condsA) {
  $base = $meanB{$baseCond{$cond}};

  print sprintf("%30s  %11.4f  %11.4f(%9.4f) %11.4f (%9.4f) %11.4f(%9.4f) - %d\n",
		$cond, $base, $emuMean{$cond}, $emuStDev{$cond},
		$ratioMean{$cond}, $ratioStDev{$cond}, $PAMean{$cond}, $PAStDev{$cond}, $emuN{$cond});

  print "standard deviations as % of means:  ", sprintf("%.1f%%  %.1f%%  %.1f%%\n",
							100 * $emuStDev{$cond}/$emuMean{$cond},
							100 * $ratioStDev{$cond}/$ratioMean{$cond},
							100 * $PAStDev{$cond}/$PAMean{$cond}); 
}

print "----------------------------------------------------------------------\n\n\n";


# The LaTeX version  (cut down cos we don't want to compare systems)

print "\\begin{tabular}{lrrrrr}
          & \\multicolumn{2}{c}{Ratio E/B} & \\multicolumn{2}{c}{Accuracy} &  \\\\
Condition & Mean & St. Dev & Mean & St. Dev & \$G\$\\\\ 
\\hline
";
for $cond (sort keys %condsA) {
  print "$cond & ", sprintf("%.4f & %.4f & %.4f & %.4f & ", $ratioMean{$cond}, $ratioStDev{$cond}, $PAMean{$cond}, $PAStDev{$cond}), "$emuN{$cond}\\\\
";
}

print "\\hline
\\end{tabular}
";

print 
"\n\nPer retrieval system.
----------------------------------------------------------------------\n";

print "#                    Condition         Base            Mean (StDev)           Ratio (StDev)       Accuracy (StDev)\n";
for $cond (sort keys %condsR) {
  $base = $meanB{$baseCond{$cond}};
  print sprintf("%30s  %11.4f  %11.4f(%9.4f) %11.4f (%9.4f) %11.4f(%9.4f) - %d\n",
		$cond, $base, $emuMean{$cond}, $emuStDev{$cond},
		$ratioMean{$cond}, $ratioStDev{$cond}, $PAMean{$cond}, $PAStDev{$cond}, $emuN{$cond});
}

print "----------------------------------------------------------------------\n\n\n";


print 
"\n\nPer corpus.
----------------------------------------------------------------------\n";

print "#                    Condition         Base            Mean (StDev)           Ratio (StDev)       Accuracy (StDev)\n";
for $cond (sort keys %condsC) {
  $base = $meanB{$baseCond{$cond}};
  print sprintf("%30s  %11.4f  %11.4f(%9.4f) %11.4f (%9.4f) %11.4f(%9.4f) - %d\n",
		$cond, $base, $emuMean{$cond}, $emuStDev{$cond},
		$ratioMean{$cond}, $ratioStDev{$cond}, $PAMean{$cond}, $PAStDev{$cond}, $emuN{$cond});
}

print "----------------------------------------------------------------------\n\n\n";

exit(0);

$rows = 0;
undef %colsum;
print "
\\begin{table} \\centering
   \\caption{Ratios for the XXXXXX measure for each emulation
     method across 12 combinations of corpus and retrieval system.
When computing the mean accuracies, we used the reciprocal of ratios less than 1.0.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
Corpus.System&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
foreach $RS ("ATIRE", "Indri", "Terrier") {
  next unless defined($ratio{"ap.$RS.Caesar1"});   #No .im readings for other than ATIRE
  foreach $corpus ("ap", "fr", "patents", "WT10g") {
    $corpRS = "$corpus.$RS";
    print $corpRS;
    $rows++;
    foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
      $cond = "$corpRS.$emu";
      print "&", $ratio{$cond};
      $colsum{$emu} += accuracy($ratio{$cond});
    }
    print "\\\\  % $N{$cond} obs in last cell\n";
  }
}

# Calculate and print column averages.
print "\\textbf{Mean accuracies}";
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  print "&", sprintf("\\textbf{%.3f}", $colsum{$emu} / $rows);
}
print "\\\\\n";
print "
\\end{tabular}\n\\end{table}\n\n";


# Averaging across corpora to compare systems---------------------------------------------

print "
\\begin{table} \\centering
   \\caption{Mean accuracies for the XXXXXX measure for each emulation
     method across 3 retrieval systems.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
System&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
foreach $RS ("ATIRE", "Indri", "Terrier") {
  next unless defined($RSN{"$RS.Caesar1"});  # In the case of .im only ATIRE results are available
  print "$RS";
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    $cond = "$RS.$emu";
    $base = "$RS.Base";
    $acc = accuracy($RSmean{$cond} / $RSmean{$base});
    print "&", $acc;
  }
  print "\\\\  % $RSN{$cond} obs in last cell\n";
}
print "
\\end{tabular}\n\\end{table}\n\n";

# Averaging across systems to compare corpora ---------------------------------------------

print "
\\begin{table} \\centering
   \\caption{Mean accuracies for the XXXXXX measure for each emulation
     method across 4 corpora.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
System&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
foreach $corpus ("ap", "fr", "patents", "WT10g") {
  print "$corpus";
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    $cond = "$corpus.$emu";
    $base = "$corpus.Base";
    $acc = accuracy($corpMean{$cond} / $corpMean{$base});
    print "&", $acc;
  }
  print "\\\\  % $corpN{$cond} obs in last cell\n";
}
print "
\\end{tabular}\n\\end{table}\n\n";

# Averaging over both systems and corpora ----------------------------------
print "
\\begin{table} \\centering
   \\caption{Mean accuracies for the XXXXXX measure for each emulation
     method across all combinations of retrieval system and corpus.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
Method&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
print "XXXXX";
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  print "&";
  print accuracy($emuMean{$emu} / $emuMean{"Base"});
}
print "\\\\  % $emuN{SimpleSynth} obs in last cell\n";

print "
\\end{tabular}\n\\end{table}\n\n";


  exit(0);

  
# ----------------------------------------------------------------------------------

sub getStats {
  my $keystring = shift;
  my @keys = split /\./, $keystring;
  my @data;
  my @sorted;
  my $N = 0;
  my $sum;
  undef @data;

  #print "Trying to match keys @keys\n";

  for (my $h = 0; $h <= $#emuData; $h++) {  # Examine each element of @emuData
    # Each key must match;
    $failed = 0;
    for (my $k = 0; $k <= $#keys; $k++) {
      if (!($emuData[$h] =~ /$keys[$k]/i)) {
	$failed = 1;
	last;
      }
    }
    if (! $failed) {
      #print "Apparently this line matched /$keys[0]/:  $emuData[$h]";
      push @data, $PA[$h];
      $N++;
      $sum += $PA[$h];
    }
  }

  return if $N == 0;   # ------------------------->
  my $mean = $sum / $N;
    
  @sorted = sort @data;
  # key box_min  whisker_min  whisker_high  box_high mean

  #calculate first quartile
  my $qi = ($N + 1) *.25;
  my $qii = int($qi);
  my $prop = $qi - $qii;
  if ($prop == 0) {
    $q1 = $sorted[--$qii];
    #print "$N items:      using element $qii for 1st quartile.\n";
  } else {
    $q1 = $sorted[$qii - 1] * $prop + $sorted[$qii] * (1.0 - $prop);
    #print "$N items:   using $prop of $qii - 1 and the rest of element $qii for 1st quartile.\n";   
  }
  #calculate third quartile
  $qi = ($N + 1) *.75;
  $qii = int($qi);
  $prop = $qi - $qii;
  if ($prop == 0) {
    $q3 = $sorted[--$qii];
    #print "$N items:      using element $qii for 3rd quartile.\n";
  } else {
    $q3 = $sorted[$qii - 1] * $prop + $sorted[$qii] * (1.0 - $prop);
    #print "$N items:   using $prop of $qii - 1 and the rest of element $qii for 3rd quartile.\n";   
 }

  
  $barNum++;
  print DAT "$barNum $q1 $sorted[0] $sorted[$#sorted] $q3 $colours[$colour]#  $N items ...\n";
  print MEANS "$barNum $mean\n";

}


sub ratio {
  my $exptl = shift;
  my $control = shift;
  # Return a string representing the ratio between
  # an experimental value and a control one.  If either parameter is undefined
  # return "N.A"
  return "N.A" if !defined($exptl) || !defined($control);
  return "N.A." if $control == 0.0;
  
  my $ratio = $exptl / $control;
  #if ($ratio < 1) { $ratio = 1.0 / $ratio;}
  return sprintf("%13.3f", $ratio);
}

sub accuracy {
  my $ratio = shift;
  # If the incoming ratio is greater than than one, return its reciprocal unless it is too
  # close to zero, in which case return 1000.0
  return sprintf("%.3f", $ratio) if ($ratio >= 1.0);
  return 1000.0 if $ratio < 0.001;
  return sprintf("%.3f", 1.0 / $ratio);
}

