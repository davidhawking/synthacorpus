#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.

# Read a data file produced by main_experiment.pl and produce tables and figures for the SynthaCorpus paper.
# 1. Read the file and calculate the mean base score for each (corpus, RS, emu) combination, while saving
#    the raw data for everything other than Base in an array emuData.
# 2. Also calculate mean base scores for each (corpus, emu) combination and each (RS, emu) combination.
# 3. Pass through emuData and store ratios and PAs (prediction accuracies) for each entry in @ratio and @PA
# 4. Generate overall plot to compare emulation methods:
#         getStats(1, 2);  # generates the %results hash from the PA array
#         plot();  # plots the %results hash
# 5. Generate plot to show interaction of emulation method and retrieval system
#         getStats(2, 1, 2);
#         plot();   # Will this order the bars the way we want?
# 6. Generate plot to show interaction of emulation method and corpus
#         getStats(2, 0, 2);
#         plot();
# 7. Output the table of ratios (columns are emu methods and rows are corpus.RS combinations sorted by RS then by corpus




# getStats function:  getstats <numcols> <col> ....  Return results in %results hash whose keys are the concatenation of
# the different combinations of the specified columns and whose values are the following , needed for plotting in
# candlestick fashion:
# key box_min  whisker_min  whisker_high  box_high mean   

# 
#  ------ sample of input data --------
#  #Corpus	IR sys	EmuMeth	Gen	6wd QP time
#  ap	ATIRE	Base	1	4.258
#  ap	ATIRE	Base	1	4.251
#
# the condition in the above truncated example would be ap.ATIRE.Base

$|++;

die "Usage $0 <results file> ...\n"
  unless $#ARGV >= 0;

$debug = 0;

$paperRoot = "/Users/dave/GIT/SynthaCorpus/BitBucket";

# 1. Process the input files  They contain raw underlying measures, not accuracy scores
#    The name of the measure is the suffix of the file name, e.g. q6t
$lions = 0;    # Overall line count

foreach $arg (@ARGV) {
  ($measure) = $arg =~ /\.([^.]+)$/;
  print "Measure: $measure\n";
  die "Can't open input file $arg\n"
    unless open INN, "$arg";
  
  while (<INN>) {
    next if /^\s*#/;  #skip comment lines
    @f = split /\t/;
    die "problem $#f in $_.\n" unless $#f >= 4;
    $lions++;
    $condA = "$f[0].$f[1].$f[2].$measure";  # corpus.RS.emu.measure
    $condR = "$f[1].$f[2].$measure";        # RS.emu.measure
    $condC = "$f[0].$f[2].$measure";  # corpus.emu.measure
    if ($f[2] eq "Base") {
      $NB{$condA}++;
      $NB{$condR}++;
      $NB{$condC}++;
      $sumB{$condA} += $f[4];
      $sumB{$condR} += $f[4];
      $sumB{$condC} += $f[4];
      #print "Setting Base values for $condA, $condR and $condC\n";
    } else {
      push @emuData, "$measure\t$_";
    }
  }
  close(INN);
}

print "Lines read: $lions.  Highest index in emuData array is $#emuData\n";

# Now that we have read all the input, change to the correct plot sub-directory of paper
#$plotdir = "$paperRoot/Imagefiles/MainExptPlots";
$plotdir = "tmpPlots";
mkdir $plotdir unless -e $plotdir;
die "Can't chdir to $plotdir" unless chdir $plotdir;


# 2. Calculate the base means.  These have to be calculated separately for each measure
#    That happens automatically because the keys all include the measure. 
foreach $k (keys %NB) {
  $meanB{$k} = $sumB{$k} / $NB{$k};
}

# 3. Calculate ratios and PA scores from the @emuData array.  Because we include the measure
#    in the emuData array elements, the accuracy scores are per (corpus, retrieval system, measure).
$PAvaluesFile = "PAvalues.dat";
die "Can't open $PAvaluesFile\n" unless open PAF, ">$PAvaluesFile";
for ($i = 0; $i <= $#emuData; $i++) {
  @f = split /\t/, $emuData[$i];   # Column zero now contains the measure.
  $condB = "$f[1].$f[2].Base.$f[0]";  # corpus.RS.emu
  if ($meanB{$condB} < 0.001) {
    $ratio[$i] = 1000.0;
  } else {
    $ratio[$i] = $f[5] / $meanB{$condB};
  }
  $PA[$i] = $ratio[$i];
  if ($ratio[$i] > 1.0) {
    $PA[$i] = 1.0 / $ratio[$i];
  }
  $ratio[$i] = sprintf("%.3f", $ratio[$i]);
  $PA[$i] = sprintf("%.3f", $PA[$i]);
  #print "Set PA[$i] to $PA[$i]\n";
  print PAF "$f[0]\t$f[1]\t$f[2]\t$f[3]\t$PA[$i]\t$ratio[$i]\n";
}

close PAF;
print "\nIndividual R1 and R2 scores in $PAvaluesFile\n\n";

# --------------------------------------------------------------------------------------------------

@colours = (" 255 0 0 ", " 0 255 0", " 0 0 255", " 255 0 255", "255 255 0", "0 255 255");
# 4. Generate plot data for overall PA results for each emulation method.  Fig. 9.2
$barNum = 0;
die "Can't open overallPAall.dat for writing" unless
  open DAT, ">overallPAall.dat";
die "Can't open overallPAall.means for writing" unless
  open MEANS, ">overallPAall.means";

print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
$globalMinPAcond = "RATS";
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  getStats($emu);
}

close(DAT);
close(MEANS);
print "Overall plot data for candlesticks plot written to overallPAall.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";


# 5. Generate plot data for PA results for each combination of RS and emu  Fig 9.7
$barNum = 0;
die "Can't open RSPAall.dat for writing" unless
  open DAT, ">RSPAall.dat";
die "Can't open RSPAall.means for writing" unless
  open MEANS, ">RSPAall.means";
print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
foreach $RS ("ATIRE", "Indri", "Terrier") {
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    getStats("$RS.$emu");
  }
  $colour++;
}

close(DAT);
close(MEANS);
print "Plot data for candlesticks plot written to RSPAall.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";

# 6. Generate plot data for PA results for each combination of corpus and emu Fig. 9.8
$barNum = 0;
die "Can't open corpusPAall.dat for writing" unless
  open DAT, ">corpusPAall.dat";
die "Can't open corpusPAall.means for writing" unless
  open MEANS, ">corpusPAall.means";
print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
foreach $corpus ("ap", "fr", "patents", "WT10g") {
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    getStats("$corpus.$emu");
  }
  $colour++;
}

close(DAT);
close(MEANS);
print "Plot data for candlesticks plot written to corpusPAall.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";

#7. Generate plot data for indexing time averaged across RS and corpora Fig. 9.3
$barNum = 0;
die "Can't open overallPAit.dat for writing" unless
  open DAT, ">overallPAit.dat";
die "Can't open overallPAit.means for writing" unless
  open MEANS, ">overallPAit.means";

print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  getStats("it\t.$emu");
}

close(DAT);
close(MEANS);
print "Overall plot data for candlesticks plot written to overallPAit.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";



#8. Generate plot data for indexing memory averaged across corpora Fig. 9.4
$barNum = 0;
die "Can't open overallPAim.dat for writing" unless
  open DAT, ">overallPAim.dat";
die "Can't open overallPAim.means for writing" unless
  open MEANS, ">overallPAim.means";

print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  getStats("im\t.$emu");
}

close(DAT);
close(MEANS);
print "Overall plot data for candlesticks plot written to overallPAim.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";


#9. Generate plot data for qp time averaged across RS, corpora and q lengths  Fig 9.5
$barNum = 0;
die "Can't open overallPAqt.dat for writing" unless
  open DAT, ">overallPAqt.dat";
die "Can't open overallPAqt.means for writing" unless
  open MEANS, ">overallPAqt.means";

print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  getStats("q[369]t\t.$emu");
}

close(DAT);
close(MEANS);
print "Overall plot data for candlesticks plot written to overallPAqt.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";



#10 Generate plot data for qp mrr averaged across RS, corpora and q lengths  Fig 9.6

$barNum = 0;
die "Can't open overallPAqrr.dat for writing" unless
  open DAT, ">overallPAqrr.dat";
die "Can't open overallPAqrr.means for writing" unless
  open MEANS, ">overallPAqrr.means";

print DAT "#key box_min  whisker_min  whisker_high  box_high mean\n";

$colour = 0;
$globalMinPA = 1;
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  getStats("q[369]rr\t.$emu");
}

close(DAT);
close(MEANS);
print "Overall plot data for candlesticks plot written to overallPAqrr.dat -- Minimum R1 $globalMinPA for $globalMinPAcond\n";


# -------------------------------------------------------------------------------------------



# Now write the gnuplot commands to draw the necessary graphs
die "Can't write to plot.cmds"
  unless open PLC, ">plot.cmds";

print PLC "set terminal pdf
set size ratio 1
set boxwidth 0.25
set style fill solid
set key off
set title \"\"
rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)

set output \"overallPAall.pdf\"
set xrange [0:6]
set yrange [0:1.1]
set xtics rotate
set xtics ('Cp' 1, 'Caesar1' 2, 'Nomen.' 3, 'SophS.' 4, 'SimpleS' 5)
plot 'overallPAall.dat' using 1:2:3:4:5 with candlesticks whiskerbars, 'overallPAall.means' with linespoints

set output \"overallPAit.pdf\"
plot 'overallPAit.dat' using 1:2:3:4:5 with candlesticks whiskerbars, 'overallPAit.means' with linespoints

set output \"overallPAim.pdf\"
plot 'overallPAim.dat' using 1:2:3:4:5 with candlesticks whiskerbars, 'overallPAim.means' with linespoints

set output \"overallPAqt.pdf\"
plot 'overallPAqt.dat' using 1:2:3:4:5 with candlesticks whiskerbars, 'overallPAqt.means' with linespoints

set output \"overallPAqrr.pdf\"
plot 'overallPAqrr.dat' using 1:2:3:4:5 with candlesticks whiskerbars, 'overallPAqrr.means' with linespoints

set output \"RSPAall.pdf\"
set xrange [0:16]
set yrange [0:1.1]
set xtics rotate
set xtics ('ATIRE.Cp' 1, 'ATIRE.Caesar1' 2, 'ATIRE.Nomen.' 3, 'ATIRE.SophS.' 4, 'ATIRE.SimpleS' 5, 'Indri.Cp' 6, 'Indri.Caesar1' 7, 'Indri.Nomen.' 8, 'Indri.SophS.' 9, 'Indri.SimpleS.' 10,  'Terrier.Cp' 11, 'Terrier.Caesar1' 12, 'Terrier.Nomen.' 13, 'Terrier.SophS.' 14, 'Terrier.SimpleS.' 15)
plot 'RSPAall.dat' using 1:2:3:4:5:(rgb(\$6,\$7,\$8)) with candlesticks fillcolor rgb variable, 'RSPAall.means' with points

set output \"corpusPAall.pdf\"
set xrange [0:21]
set yrange [0:1.1]
set xtics rotate
set xtics ('ap.Cp' 1, 'ap.Caesar1' 2, 'ap.Nomen.' 3, 'ap.SophS.' 4, 'ap.SimpleS' 5, 'fr.Cp' 6, 'fr.Caesar1' 7, 'fr.Nomen.' 8, 'fr.SophS.' 9, 'fr.SimpleS.' 10, 'patents.Cp' 11, 'patents.Caesar1' 12, 'patents.Nomen.' 13, 'patents.SophS.' 14, 'patents.SimpleS.' 15, 'WT10g.Cp' 16, 'WT10g.Caesar1' 17, 'WT10g.Nomen.' 18, 'WT10g.SophS.' 19, 'WT10g.SimpleS' 20)

plot 'corpusPAall.dat'  using 1:2:3:4:5:(rgb(\$6,\$7,\$8)) with candlesticks fillcolor rgb variable, 'corpusPAall.means' with points
";

close(PLC);

system("gnuplot plot.cmds");

print "\nAll done.   Results (*.dat, *.means, plot.cmds and *.pdf) now in $plotdir.

If you're happy with the pdfs, please:

   cp $plotdir/*.pdf $paperRoot/Imagefiles/MainExptPlots/

";


exit(0);
  
sub getStats {
  my $keystring = shift;
  my @keys = split /\./, $keystring;
  my @data;
  my @sorted;
  my $N = 0;
  my $sum = 0;
  my $minPA = 1.0;
  my $maxPA = 0.0;
  undef @data;

  #print "Trying to match keys @keys\n";

  for (my $h = 0; $h <= $#emuData; $h++) {  # Examine each element of @emuData
    # Each key must match;
    $failed = 0;
    for (my $k = 0; $k <= $#keys; $k++) {
      if (!($emuData[$h] =~ /$keys[$k]/i)) {
	$failed = 1;
	last;
      }
    }
    if (! $failed) {
      print "Apparently this line matched /$keys[0]/:  $emuData[$h]"
	if $debug;
      push @data, $PA[$h];
      $N++;
      $sum += $PA[$h];
      if ($PA[$h] < $minPA) {
	$minPA = $PA[$h];
	$minPAcond = $keystring;
	$minPAcond =~ s/\t/./;
      }
      $maxPA = $PA[$h] if $PA[$h] > $maxPA;
    }
  }

  return if $N == 0;   # ------------------------->
  my $mean = $sum / $N;

  print "\n$keystring: $N $mean $minPA $maxPA. (N, mean, min, max)\n"
    if $debug;
  if ($minPA < $globalMinPA) {
    $globalMinPA = $minPA;
    $globalMinPAcond = $minPAcond;
  }
  
  @sorted = sort @data;
  # key box_min  whisker_min  whisker_high  box_high mean

  #calculate first quartile
  my $qi = ($N + 1) *.25;
  my $qii = int($qi);
  my $prop = $qi - $qii;
  if ($prop == 0) {
    $q1 = $sorted[--$qii];
    #print "$N items:      using element $qii for 1st quartile.\n";
  } else {
    $q1 = $sorted[$qii - 1] * $prop + $sorted[$qii] * (1.0 - $prop);
    #print "$N items:   using $prop of $qii - 1 and the rest of element $qii for 1st quartile.\n";   
  }
  #calculate third quartile
  $qi = ($N + 1) *.75;
  $qii = int($qi);
  $prop = $qi - $qii;
  if ($prop == 0) {
    $q3 = $sorted[--$qii];
    #print "$N items:      using element $qii for 3rd quartile.\n";
  } else {
    $q3 = $sorted[$qii - 1] * $prop + $sorted[$qii] * (1.0 - $prop);
    #print "$N items:   using $prop of $qii - 1 and the rest of element $qii for 3rd quartile.\n";   
 }

  
  $barNum++;
  print DAT "$barNum $q1 $sorted[0] $sorted[$#sorted] $q3 $colours[$colour]#  $N items ...\n";
  print MEANS "$barNum $mean\n";

}















for $cond (sort keys %emuN) {
  $emuMean{$cond} = $emuSum{$cond}/$emuN{$cond};
  $emuStdev = "NA";
  $emuStdev = sprintf("%11.4f", $emuSumsq{$cond} / $emuN{$cond} - $emuMean{$cond} * $emuMean{$cond})
    unless $emuN{$cond} < 3;   # Hayes Eq 6.18.2
}

print 
"Means, standard deviations, and deviations from Base for each condition.
--------------------------------------------------------------\n\n";

print "\n#                    Condition         Mean          StDev Ratio\n";
for $cond (sort keys %N) {
  ($corpRS) = $cond =~ /^([a-z0-9]+\.[a-z0-9]+)\./i;
  $base = "$corpRS.Base";
  $mean = $mean{$cond};
  $stdev = "NA";
  $stdev = sprintf("%11.4f", $sumsq{$cond} / $N{$cond} - $mean * $mean)
    unless $N{$cond} < 3;   # Hayes Eq 6.18.2
  $ratio{$cond} = ratio($mean, $mean{$base});
  print sprintf("%30s  %11.4f    ", $cond, $mean),
    "$stdev   $ratio{$cond}\n";
}

print "----------------------------------------------------------------------\n\n\n";

$rows = 0;
undef %colsum;
print "
\\begin{table} \\centering
   \\caption{Ratios for the XXXXXX measure for each emulation
     method across 12 combinations of corpus and retrieval system.
When computing the mean inaccuracies, we used the reciprocal of ratios less than 1.0.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
Corpus.System&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
foreach $RS ("ATIRE", "Indri", "Terrier") {
  next unless defined($ratio{"ap.$RS.Caesar1"});   #No .im readings for other than ATIRE
  foreach $corpus ("ap", "fr", "patents", "WT10g") {
    $corpRS = "$corpus.$RS";
    print $corpRS;
    $rows++;
    foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
      $cond = "$corpRS.$emu";
      print "&", $ratio{$cond};
      $colsum{$emu} += inaccuracy($ratio{$cond});
    }
    print "\\\\  % $N{$cond} obs in last cell\n";
  }
}

# Calculate and print column averages.
print "\\textbf{Mean inaccuracies}";
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  print "&", sprintf("\\textbf{%.3f}", $colsum{$emu} / $rows);
}
print "\\\\\n";
print "
\\end{tabular}\n\\end{table}\n\n";


# Averaging across corpora to compare systems---------------------------------------------

print "
\\begin{table} \\centering
   \\caption{Mean inaccuracies for the XXXXXX measure for each emulation
     method across 3 retrieval systems.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
System&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
foreach $RS ("ATIRE", "Indri", "Terrier") {
  next unless defined($RSN{"$RS.Caesar1"});  # In the case of .im only ATIRE results are available
  print "$RS";
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    $cond = "$RS.$emu";
    $base = "$RS.Base";
    $inacc = inaccuracy($RSmean{$cond} / $RSmean{$base});
    print "&", $inacc;
  }
  print "\\\\  % $RSN{$cond} obs in last cell\n";
}
print "
\\end{tabular}\n\\end{table}\n\n";

# Averaging across systems to compare corpora ---------------------------------------------

print "
\\begin{table} \\centering
   \\caption{Mean inaccuracies for the XXXXXX measure for each emulation
     method across 4 corpora.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
System&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
foreach $corpus ("ap", "fr", "patents", "WT10g") {
  print "$corpus";
  foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
    $cond = "$corpus.$emu";
    $base = "$corpus.Base";
    $inacc = inaccuracy($corpMean{$cond} / $corpMean{$base});
    print "&", $inacc;
  }
  print "\\\\  % $corpN{$cond} obs in last cell\n";
}
print "
\\end{tabular}\n\\end{table}\n\n";

# Averaging over both systems and corpora ----------------------------------
print "
\\begin{table} \\centering
   \\caption{Mean inaccuracies for the XXXXXX measure for each emulation
     method across all combinations of retrieval system and corpus.}
   \\label{tab:res:XXXXX}
\\begin{tabular}{lrrrrr}
Method&Cp      &    Caesar1   &  Nomen.    &   SophS.  &   SimpleS.\\\\\n\\hline\n";   
print "XXXXX";
foreach $emu ("Cp", "Caesar1", "Nomenclator", "SophSynth", "SimpleSynth") {
  print "&";
  print inaccuracy($emuMean{$emu} / $emuMean{"Base"});
}
print "\\\\ \n";

print "
\\end{tabular}\n\\end{table}\n\n";


exit(0);
# ----------------------------------------------------------------------------------

sub ratio {
  my $exptl = shift;
  my $control = shift;
  # Return a string representing the ratio between
  # an experimental value and a control one.  If either parameter is undefined
  # return "N.A"
  return "N.A" if !defined($exptl) || !defined($control);
  return "N.A." if $control == 0.0;
  
  my $ratio = $exptl / $control;
  #if ($ratio < 1) { $ratio = 1.0 / $ratio;}
  return sprintf("%13.3f", $ratio);
}

sub inaccuracy {
  my $ratio = shift;
  # If the incoming ratio is less than one, return its reciprocal unless it is too
  # close to zero, in which case return 1000.0
  return sprintf("%.3f", $ratio) if ($ratio >= 1.0);
  return 1000.0 if $ratio < 0.001;
  return sprintf("%.3f", 1.0 / $ratio);
}

