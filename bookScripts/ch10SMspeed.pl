#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Run stringMarkovGen.exe over a bunch of corpora to measuer the speed of generation

use Time::HiRes qw(time);

 die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";
die "Base directory $baseDir must exist and contain the necessary corpus files\n"
  unless -d $baseDir;

@corpora = (
             "wt10g", "t8nlqs", "ap",
           );

# Sizes taken from Base/corpus_summary.txt total_postings values
%size = ( wt10g => 1037152449,
	   ap => 113078786,
	  t8nlqs => 1226417);

%hashBits = ( wt10g => 28,
	   ap => 26,
	  t8nlqs => 25);

$genOpts = "-k=9 -wildcards=0 -lambda=0 -predefinedAlphabet=TRUE -outFile=wipeMe.trec -hashBits=$hashBits{$corpus}";

$iters = 3;

foreach $corpus (@corpora) {
  $sumTimes = 0;
  for ($i = 0; $i < $iters; $i++) {
    $start = time();
    runit("../src/stringMarkovGen.exe -inFile=$baseDir/$corpus.trec $genOpts");
    $sumTimes += (time() - $start);
  }
  $meanTime = $sumTimes / $iters;
  $meanRate = $size{$corpus} / $meanTime / 1000000.0;

  print "$corpus &", sprintf("%.3f & %.3f\\\\", $meanTime, $meanRate);
}
    


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
