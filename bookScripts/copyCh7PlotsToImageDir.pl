#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

use File::Copy;


$destRoot = "/Users/dave/Git/SynthaCorpus/BitBucket/Imagefiles";
die "This script is probably only of use to David Hawking.  Used when 
gathering plots for the Morgan & Claypool book.

" unless -e $destRoot;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};


# First bulk copies using /usr/bin/rsync
runit("/usr/bin/rsync -t $expRoot/Sampling/Sample/scaling_*.pdf $destRoot/SamplePlots");

runit("/usr/bin/rsync -t $expRoot/Sampling/TemporalSubset/scaling_*.pdf $destRoot/Temporal");

# The rest has to be copied directory by directory
mkdir "$destRoot/ScaledUpPlots" unless -e "$destRoot/ScaledUpPlots";
foreach $corpus ("ap", "apto", "wsj") {
  foreach $fyl (glob "$expRoot/Scalingup/*/${corpus}_*.pdf") {
    ($d) = ($fyl =~ m@.*/(.*)/[^/]+@);   # Get the sub-directory name
    $dest = "$destRoot/ScaledUpPlots/$d/";
    mkdir $dest unless -e $dest;
    print "Copying $fyl to $dest\n";
    copy($fyl, $dest);
    die "Copy($fyl, $dest) failed.\n" if $?;
  }
}

# Now the WBMarkov ones
foreach $corpus ("ap") {
  foreach $fyl (glob "$expRoot/Scalingup/Markov*/*/${corpus}_*.pdf") {
    ($d) = ($fyl =~ m@.*/(.*/.*)/[^/]+$@);   # Get the double sub-directory name
    $dest = "$destRoot/ScaledUpPlots/$d/";
    mkdir $dest unless -e $dest;   # Will fail if parent doesn't exist -- unlikely
    print "Markov: Copying $fyl to $dest\n";
    copy($fyl, $dest);
    die "Copy($fyl, $dest) failed.\n" if $?;
  }
}
    
   

exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
    
