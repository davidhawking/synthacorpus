#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

use File::Copy;

# Copy both the base_v_mimic_wdlens.pdf and base_v_mimic_wdfreqs.pdf files from a list of emulation
# directories to the appropriate subdirectories of the book Imagefiles directories
# Now copies "_base_v_mimic_letters.pdf", "_base_v_mimic_letter_pairs.pdf" as well

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$emuRoot = "$expRoot/Emulation";
$bookDir = "$ENV{HOME}/GIT/SynthaCorpus/Book";
die "Book directory $bookDir not found\n"
  unless -e $bookDir;

$destDir = "$bookDir/Imagefiles/WordLengths";


@corpora = ("ap",
	    "fr", "patents", "wt10g", "t8nlqs", "alladhoc"
	   );
@suffs = (#"_base_v_mimic_wdlens.pdf", "_base_v_mimic_wdfreqs.pdf", "_base_v_mimic_letters.pdf",
	  "_base_v_mimic_letter_pairs.pdf");
@dirs = ("$emuRoot/Piecewise/markov-1_dlhisto_ind/", "$emuRoot/Piecewise/markov-1e_dlhisto_ind/", "$emuRoot/Piecewise/markov-5_dlhisto_ind/",
	 "$emuRoot/Piecewise/markov-5e_dlhisto_ind/",
	 "$emuRoot/Piecewise/base26_dlhisto_ind/", "$emuRoot/Piecewise/base26s_dlhisto_ind/", "$emuRoot/Piecewise/bubble_babble_dlhisto_ind/",
	 "$emuRoot/Piecewise/simpleWords_dlhisto_ind/", "$emuRoot/Caesar1/", "$emuRoot/Nomenclator/"
	);

foreach $tldir (@dirs) {
  foreach $corpus (@corpora) {
    foreach $suff (@suffs) {
      $file = "$tldir$corpus$suff";
      ($subDir) = $file =~ m@Emulation/(.*)/${corpus}_@;
      $subDir =~ s@_[^/]+$@@;
      $subDir =~ s@.*/@@;
      $subDir .= "_babble" if $subDir =~ /bubble/;
      print "   Subdir: $subDir\n";
      $dir = "$destDir/$subDir";
      print "Copying $file to $dir\n";   # Silently ignore failures, due to non-existent $file
      copy($file, $dir);
    }
  }
}

exit(0);
