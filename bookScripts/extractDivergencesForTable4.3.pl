#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Modified to get JSDs rather than KLDs

die "\nUsage: $0 <pat>\n\n   Must specify the pattern for extracting the relevant line from *_KLKS.txt.
   Allowable patterns are:
      vocab_by_freq
      bigrams_by_freq
      ngrams_by_freq
      docLenHist
      vocab_wdlens

" unless $#ARGV == 0 && $ARGV[0] =~ /vocab_by_freq|bigrams_by_freq|ngrams_by_freq|docLenHist|vocab_wdlens/;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};


$pat = $ARGV[0];   # To pick the relevant line out of KLKS.txt
$emuDir = "$expRoot/Emulation";
@corpora = ("ap", "fr", "patents", "alladhoc", "wt10g", "t8nlqs");  # This is the order of the columns we want to show.
$" = '&';  # The delimiter used when an array is interpolated
print "\n&@corpora\\\\\n";

$currMeth = "None";
$valuesFound = 0;
foreach $KLFile ((glob "$emuDir/*/*_KLKS.txt"), (glob "$emuDir/*/*/*_KLKS.txt")) {
  $condition = $KLFile;
  $condition =~ s/$emuDir\///;
  ($method, $corpus) = $condition =~ /(.*)\/([^_]+)_KLKS.txt/;


  # Skip if method or corpus are not wanted in the table.
  next unless $corpus =~ m@ap|fr|patents|alladhoc|t8nlqs|wt10g@;   # 27 Feb 2020 dropped wikititles
  next unless $method =~ m@Caesar|GPT-2|Nomenclator|Markov|Linear.*dlhisto_ind|Piecewise.*dlhisto_ind|Uniform.*dlhisto_ind@;
  
  if ($method ne $currMeth) {
    if ($currMeth ne "None") {
      showRow($currMeth);
    }
    undef %row;
    $currMeth = $method;
  }

  $row{$corpus}  = extractKL($KLFile, $pat);
  $valuesFound++;
}
showRow($currMeth);

print "\n\nFound $valuesFound observations. \n\n";

exit(0);
# -----------------------------------------------------------------------------------

sub extractKL {
  my $fyl = shift;
  my $linePicker = shift;
  die "Can't open $fyl\n"
    unless open F, $fyl;
  while (<F>) {
    if (/$linePicker/) {
      if (/JSD=([0-9.]+) /) {
	close(F);
	return sprintf("%.4f", $1);
      } else {
	die "$fyl: Pattern match failure in $_\n";
      }
    }
  }
  close(F);
  die "$fyl: Failed to match pattern $linePicker\n";
}


sub showRow {
  # Print a table row 
  my $meth = shift;
  print "$meth";
  foreach $c (@corpora) {
    $e = "-";
    $e = $row{$c} if defined $row{$c};
    print "&", $e;
  }
  print "\\\\\n";

}


