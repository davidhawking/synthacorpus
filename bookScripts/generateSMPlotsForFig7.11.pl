#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Generate Unigram plots for stringMarkovGen emulations based on 1%, 10% and 50% samples
  
use File::Copy;

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$SUPRoot = "$expRoot/Scalingup/stringMarkov";
mkdir $SUPRoot unless -e $SUPRoot;
die "Failed to make directory $SUPRoot\n" if $?;
$baseDir = "$expRoot/Base";
die "Base directory $baseDir must exist and contain the necessary corpus files\n"
  unless -d $baseDir;

$imageRoot = "$ENV{HOME}/GIT/SynthaCorpus/Book/Imagefiles";
die "Image directory $imageRoot not found\n" unless -e $imageRoot;
$imageParent = "$imageRoot/ScaledUpPlots/stringMarkov";
mkdir $imageParent unless -e $imageParent;

$SMGOpts = "-k=23 -wildcards=21 -lambda=0.0000025 -hashBits=27";

@dirs = ("M-1-100", "M-10-10", "M-50-2");
$corpus = "ap";

foreach $dir (@dirs) {
  ($frac, $sup) = ($dir =~ /M-([0-9]+)-([0-9]+)/);
  $frac /= 100;
  $path = "$SUPRoot/$dir";
  mkdir $path unless -e $path;
  $imagePath = "$imageParent/$dir";
  mkdir $imagePath unless -e $imagePath;
  runit("../src/stringMarkovGen.exe -inFile=$baseDir/$corpus.trec -outFile=$path/$corpus.trec $SMGOpts -trainingFraction=$frac -postingsScaleFactor=$sup");
    #unless -e "$path/$corpus.trec";
  runit("perl ../src/compareBaseWithMimic.pl $corpus $path 2 3 -force");
  # Copy the relevant image files across
  foreach $suff ("unigrams", "bigrams", "doclens", "wdlens", "wdfreqs") {
    copy("$path/${corpus}_base_v_mimic_${suff}.pdf", "$imagePath");
  }
}


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}


  
