#! /usr/bin/perl -w

# Emulate the principal corpora using StringMarkov with fixed k, wildcards and lambda, and 
# extract the compression ratios for Table 6.3

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/

$perl = $^X;
$perl =~ s@\\@/@g;


$k = 5;
$wildcards = 0;
$lambda = 0;
@corpora = ("ap", "fr", "patents", "alladhoc", "wt10g", "t8nlqs", );

die "Many SynthaCorpus scripts require that the environment variable \$SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
die "Can't find experiment root: $expRoot\n" unless -d $expRoot;
$baseDir = "$expRoot/Base";
die "Can't find base directory: $baseDir\n" unless -d $baseDir;
$SMRoot = "$expRoot/Emulation/StringMarkov";
if (! -e $SMRoot) { mkdir $SMRoot; }


$emulator = check_exe("../src/stringMarkovGen.exe");
$comparator = check_exe("../src/compareBaseWithMimic.pl");
$labbe = check_exe("Labbe.pl");

foreach $corpus (@corpora) {
  $baseFile = "$baseDir/$corpus.trec";
  die "Base corpus $baseFile doesn't exist\n"
    unless -e $baseFile;

  #unlink "$baseDir/${corpus}_summary.txt";   # To force property extraction once for base
 
  $emuDir = "$SMRoot/${k}_${wildcards}_$lambda";
  if (! -e $emuDir) {mkdir $emuDir;}
  $emuFile = "$emuDir/$corpus.trec";
  runit("$emulator -inFile=$baseFile -outFile=$emuFile -k=$k -wildcards=$wildcards -lambda=$lambda -predefinedAlphabet=TRUE -hashBits=28");
  runit("$comparator $corpus $emuDir 2 3");
  scanSumFile($corpus, "$emuDir/${corpus}_summary.txt");
}

print "\n\nMeasure";
print join("&", @corpora), "\\\\\ngzip_ratio";
foreach $corpus (@corpora) {print "&$gzr{$corpus}"};
print "\\\\\nsize_on_disk";
foreach $corpus (@corpora) {print "&$sod{$corpus}"};
print "\\\\\n\n";
exit(0);

# ----------------------------------------------------------------------------

sub scanSumFile {
  my $corpse = shift;
  my $fyl = shift;
  die "Can't open $fyl\n" unless open F, $fyl;
  while (<F>) {
    if (/gzip_ratio=([0-9.]+)/) {$gzr{$corpse} = $1;}
    elsif (/size_on_disk=([0-9.]+)/) {$sod{$corpse} = $1;}
  }
  close(F);
}


sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}


sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
