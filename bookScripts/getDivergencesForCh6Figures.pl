#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.


die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$baseDir = "$expRoot/Base";
$emuRoot = "$expRoot/Emulation";


## Do letters first

$corpus = "ap";
$baseFile = "$baseDir/${corpus}_letters_ranked.dat";
foreach $subDir ("Caesar1", "Nomenclator", "Piecewise/base26_dlhisto_ind", "Piecewise/base26s_dlhisto_ind", "Piecewise/bubble_babble_dlhisto_ind",
		 "Piecewise/simpleWords_dlhisto_ind",
		 "Piecewise/markov-0_dlhisto_ind", "Piecewise/markov-1_dlhisto_ind", "Piecewise/markov-1e_dlhisto_ind",
		 "Piecewise/markov-5_dlhisto_ind", "Piecewise/markov-5e_dlhisto_ind") {
  $emuFile = "$emuRoot/$subDir/${corpus}_letters_ranked.dat";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile 7\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "Letters/$subDir/$corpus: $JSD\n\n";
}

$subDir = "Piecewise/markov-5e_dlhisto_ind";
foreach $corpus ("fr", "patents", "wt10g", "t8nlqs") {
  $baseFile = "$baseDir/${corpus}_letters_ranked.dat";
  $emuFile = "$emuRoot/$subDir/${corpus}_letters_ranked.dat";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile 7\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "Letters/$subDir/$corpus: $JSD\n\n";
}

$subDir = "Piecewise/base26s_dlhisto_ind";
foreach $corpus ("fr", "patents") {
  $baseFile = "$baseDir/${corpus}_letters_ranked.dat";
  $emuFile = "$emuRoot/$subDir/${corpus}_letters_ranked.dat";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile 7\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "Letters/$subDir/$corpus: $JSD\n\n";
}


# Do letter pairs next
$corpus = "ap";
$baseFile = "$baseDir/${corpus}_letter_pairs_ranked.dat";
foreach $subDir ("Caesar1", "Nomenclator", "Piecewise/base26_dlhisto_ind", "Piecewise/base26s_dlhisto_ind", "Piecewise/bubble_babble_dlhisto_ind",
		 "Piecewise/simpleWords_dlhisto_ind",
		 "Piecewise/markov-0_dlhisto_ind", "Piecewise/markov-1_dlhisto_ind", "Piecewise/markov-1e_dlhisto_ind",
		 "Piecewise/markov-5_dlhisto_ind", "Piecewise/markov-5e_dlhisto_ind") {
  $emuFile = "$emuRoot/$subDir/${corpus}_letter_pairs_ranked.dat";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile 3\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "LetterPairs/$subDir/$corpus: $JSD\n\n";
}

$subDir = "Piecewise/markov-5e_dlhisto_ind";
foreach $corpus ("fr", "patents", "wt10g", "t8nlqs") {
  $baseFile = "$baseDir/${corpus}_letter_pairs_ranked.dat";
  $emuFile = "$emuRoot/$subDir/${corpus}_letter_pairs_ranked.dat";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile 3\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "LetterPairs/$subDir/$corpus: $JSD\n\n";
}

$subDir = "Piecewise/base26s_dlhisto_ind";
foreach $corpus ("fr", "patents") {
  $baseFile = "$baseDir/${corpus}_letter_pairs_ranked.dat";
  $emuFile = "$emuRoot/$subDir/${corpus}_letter_pairs_ranked.dat";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile 3\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "LetterPairs/$subDir/$corpus: $JSD\n\n";
}

# Do wordlengths next - we want vocab counts, so use column 2 of _vocab.wdlens
$corpus = "ap";
$suff = "_vocab.wdlens";
$col = 2;
$baseFile = "$baseDir/${corpus}$suff";
foreach $subDir ("Caesar1", "Nomenclator", "Piecewise/base26_dlhisto_ind", "Piecewise/base26s_dlhisto_ind", "Piecewise/bubble_babble_dlhisto_ind",
		 "Piecewise/simpleWords_dlhisto_ind",
		 "Piecewise/markov-0_dlhisto_ind", "Piecewise/markov-0e_dlhisto_ind",
		 "Piecewise/markov-1_dlhisto_ind", "Piecewise/markov-1e_dlhisto_ind",
		 "Piecewise/markov-5_dlhisto_ind", "Piecewise/markov-5e_dlhisto_ind") {
  $emuFile = "$emuRoot/$subDir/${corpus}$suff";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile $col\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "$suff/$subDir/$corpus: $JSD\n\n";
}

$subDir = "Piecewise/markov-5e_dlhisto_ind";
foreach $corpus ("fr", "patents", "wt10g", "t8nlqs") {
  $baseFile = "$baseDir/${corpus}$suff";
  $emuFile = "$emuRoot/$subDir/${corpus}$suff";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile $col\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "$suff/$subDir/$corpus: $JSD\n\n";
}

$subDir = "Piecewise/base26s_dlhisto_ind";
foreach $corpus ("fr", "patents") {
  $baseFile = "$baseDir/${corpus}$suff";
  $emuFile = "$emuRoot/$subDir/${corpus}$suff";
  $cmd = "../src/computeKLDKSForTwoDats.exe $baseFile $emuFile $col\n";
  $rslt = `$cmd`;
  warn "Command $cmd failed\n" if $?;
  ($JSD) = $rslt =~ /JSD=([-0-9.]+)/;
  print "$suff/$subDir/$corpus: $JSD\n\n";
}

