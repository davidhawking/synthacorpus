#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# 


use File::Copy;

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$emuRoot = "$expRoot/Emulation/Piecewise";

@ranks = (1, 10, 100, 1000, 10000, 100000);
$R = $#ranks;


print "
\\begin{tabular}{l|";
for ($i = 0; $i <= $R; $i++) {print "l"};
print "}
Method&", join("&",@ranks), "\\\\\n\\hline\n";

foreach $vbf (glob "$emuRoot/*/ap_vocab_by_freq.tsv") {
  # Extract wordstring method from $vbf
  ($metho) = ($vbf =~ m@Piecewise/([^_]+)@);
  next if (defined($methHash{$metho}));
  next if ($metho =~ /bubble|-1|-3/);
  $methHash{$metho} = 1;
  die "Can't open $vbf\n" unless open VBF, $vbf;
  print "$metho";
  $lyn = 1;
  $ri = 0;
  while (<VBF>) {
    # Is this a a word we want?
    if ($lyn == $ranks[$ri]) {
      # Yes
      m@^(\S+)\s@;
      print "&$1";
      $ri++;
      last if $ri > $R;
    }
    $lyn++;
  }
  print "\\\\\n";
  close(VBF);
}
print "\\hline\n\\end{tabular}\n";
