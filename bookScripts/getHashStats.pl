#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$emuRoot = "$expRoot/Emulation";

foreach $file ((glob "$emuRoot/*/*_vocab.tsv"), (glob "$emuRoot/*/*/*_vocab.tsv")) {
  $oFile = $file;
  $oFile =~ s/vocab.tsv/hashStats.txt/;
  $cmd = "../src/calculateVocabHashRate.exe $file > $oFile";
  runit($cmd);
}





    exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  #print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
