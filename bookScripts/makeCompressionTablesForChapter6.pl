#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Gather gzip_ratio and size_on_disk data for a matrix of corpus, emuMethod combinations
# Produce latex tables for each measure.
# Assumes all summary_txt files include gzip_ratio and size_on_disk.  If not run updateSummaryFiles.pl

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$baseDir = "$expRoot/Base";
$emuRoot = "$expRoot/Emulation";

@corpora = ("ap", "fr", "patents", "alladhoc", "wt10g", "t8nlqs");
$table1 = "";
$table2 = "";
foreach $corpus (@corpora) {
  $table1 .= "& $corpus";
  $table2 .= "& $corpus";
}
$table1 .= "\n";
$table2 .= "\n";

for $method (#"Caesar1", "Nomenclator", "Markov0", "Markov1"
	     #"stringMarkov/13",
	     "stringMarkov/5_0_0"
	    ) {
  $table1 .= $method;
  $table2 .= $method;
  for $corpus (@corpora) {
    $baseSumry = "$baseDir/${corpus}_summary.txt";
    die "$baseSumry doesn't exist\n" unless -e $baseSumry;
    $t = `grep gzip_ratio= $baseSumry`;
    if ($t =~ m@([0-9.]+)@) {
      $baseGzip = $1;
    } else {
      die "$baseSumry contains no gzip_ratio line. Perhaps run updateSummaryFiles.pl?\n";
    }
    $t = `grep size_on_disk= $baseSumry`;
    if ($t =~ m@([0-9.]+)@) {
      $baseSize = $1;
    } else {
      die "$baseSumry contains no size_on_disk line. Perhaps run updateSummaryFiles.pl?\n";
    }

    $emuSumry = "$emuRoot/$method/${corpus}_summary.txt";
    if (-e $emuSumry) {
      $t = `grep gzip_ratio= $emuSumry`;
      ($emuGzip) = $t =~ m@([0-9.]+)@;
      $table1 .= sprintf("& %.3f", $emuGzip / $baseGzip);
      $t = `grep size_on_disk= $emuSumry`;
      ($emuSize) = $t =~ m@([0-9.]+)@;
      $table2 .= sprintf("& %.3f", $emuSize / $baseSize);
    } else {
      $table1 .= "& - ";
      $table2 .= "& - ";
    }
  }
  $table1 .= "\\\\\n";
  $table2 .= "\\\\\n";
}


for $method ("base26", "base26s", "bubble_babble", "simpleWords", "markov-1e", "markov-5e") {
  $table1 .= $method;
  $table2 .= $method;
  for $corpus (@corpora) {
    $baseSumry = "$baseDir/${corpus}_summary.txt";
    die "$baseSumry doesn't exist\n" unless -e $baseSumry;
    $t = `grep gzip_ratio= $baseSumry`;
    ($baseGzip) = $t =~ m@([0-9.]+)@;
    $t = `grep size_on_disk= $baseSumry`;
    ($baseSize) = $t =~ m@([0-9.]+)@;

    $emuSumry = "$emuRoot/Piecewise/${method}_dlhisto_ind/${corpus}_summary.txt";
    if (-e $emuSumry) {
      $t = `grep gzip_ratio= $emuSumry`;
      ($emuGzip) = $t =~ m@([0-9.]+)@;
      $table1 .= sprintf("& %.3f", $emuGzip / $baseGzip);
      $t = `grep size_on_disk= $emuSumry`;
      ($emuSize) = $t =~ m@([0-9.]+)@;
      $table2 .= sprintf("& %.3f", $emuSize / $baseSize);
    } else {
      $table1 .= "& - ";
      $table2 .= "& - ";
    }
  }
  $table1 .= "\\\\\n";
  $table2 .= "\\\\\n";
}

print "Gzip ratio table\n================\n", $table1, "\n\n";
print "Size ratio table\n================\n", $table2, "\n\n";


exit(0);
