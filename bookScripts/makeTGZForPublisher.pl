#! /usr/bin/perl -w

use File::Copy;

$bookDir = "/Users/dave/GIT/SynthaCorpus/Book";

die "Can't find book dir $bookDir\n" unless -d $bookDir;

$pubDir = "/Users/dave/MorganNClaypool";
runit("/bin/rm -rf $pubDir");   # Get rid of residues
mkdir $pubDir or die "mkdir $pubDir failed\n";
mkdir "$pubDir/Imagefiles" or die "mkdir $pubDir/Imagefiles failed\n";


# Copy over the .pl, .tex and .bib files
copy("$bookDir/p.bib", $pubDir);
copy("$bookDir/compileBook.pl", $pubDir);
foreach $f (glob "$bookDir/*.tex") {
  copy($f, $pubDir);
}

# Now find and copy over all the Imagefiles into the relevant directory tree
die "Can't open $bookDir/p.log\n"
  unless open L, "$bookDir/p.log";

while (<L>) {
  if (m@File: (Imagefiles/\S+)@ || m@File: (plots-pathom/\S+)@) {
    $f = $1;
    $f =~ s/\.pd$/.pdf/;   #Two lines were truncated by one char
    dcopy("$bookDir/$f", "$pubDir/$f");
  }
}

close(L);

chdir $pubDir or die "Can't chdir to $pubDir\n";

runit("tar czf ../HawkingEtAl.tgz .");
runit("du -h /Users/dave/HawkingEtAl.tgz");
mkdir "/tmp/wipeMe" unless -e "/tmp/wipeMe";
chdir "/tmp/wipeMe";
runit("tar xzf /Users/dave/HawkingEtAl.tgz");
runit("perl compileBook.pl");

exit(0);

#-----------------------------------------------

sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}


sub dcopy {
  my $src = shift;
  my $dest = shift;
  # May have to create parent directories for the dest
  my @ancestors = split /\//, $dest;
  pop @ancestors;   # Get rid of the filename
  shift @ancestors;  # Get rid of the leading slash
  my $path = "";
  foreach my $elt (@ancestors) {
    $path .= "/$elt";
    print "$path\n";
    mkdir $path unless -e $path;
  }
  print "copy($src, $dest)\n";
  copy($src, $dest);
}
