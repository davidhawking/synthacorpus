#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# Make a table of co-occurrence criterion frequencies based on df_A, f_A, df_B, f_B, and N

$N = 250000;
#$occsPerDoc = 3;
@rows = ("1  1000",
	 "10 1000",
	 "100 1000",
	 "1000 1000",
	 "10000 1000",
	 "100000 1000 ",
	 "100000 100",
	 "100000 10",
	 "100000 1",
	 );
	 

print "\\begin{tabular}[ht]{rr|rr|rrrrr}
\$df_A\$  & \$Pr(A)\$ & \$df_B\$ & \$Pr(B)\$ & \$Pr(A,B)\$ &\$T\$ & Mean & St.dev & Crit. Freq. \\\\
\\hline
";

foreach $row (@rows) {
  ($dfA, $dfB) = $row =~ m@([0-9]+)\s+([0-9]+)@;
  #$fA = $occsPerDoc * $dfA;
  #$fB = $occsPerDoc * $dfB;
  #$T = $fA * $fB;
  $T = $N;
  $pA = $dfA / $N;
  $pB = $dfB  / $N;
  $PrAB = $pA * $pB;
  $mean = $T * $PrAB;
  $stdev = sprintf("%.4f", sqrt($mean * (1 - $PrAB)));
  $critFreq = int($mean + 1.65 * $stdev + 0.999);
  print "$dfA  & $pA & $dfB & $pB & $PrAB & $T & $mean & $stdev & $critFreq\\\\\n";
}

print "\\hline
\\end{tabular}
";


exit (0);
  

