#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

use File::Copy;
# Scripts are now called via $^X in case the perl we want is not in /usr/bin/

$perl = $^X;
$perl =~ s@\\@/@g;

$|++;
$reEmulate = 0;
$reEmulate = 1 if ($#ARGV == 0 && $ARGV[0] =~ /reEmulate/i);

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$corpus = "wt10g";
$baseStem = "$expRoot/Base/${corpus}";
$baseVocab = "${baseStem}_vocab.tsv";

$emulator = check_exe("../src/emulateARealCorpus.pl");
$hashStatsGetter = check_exe("../src/calculateVocabHashRate.exe");

# Get vocabSize from base.
$vSizeS = `wc -l $baseVocab`;
die "wc -l $baseVocab failed\n" if $?;
($vSize) = ($vSizeS =~ /([0-9]+)/);
	  
# Get hash collisions and collisions count from base.
$t = getHashStats($baseVocab);
($baseCollisions, $baseMaxChain) = $t =~ /(.*),(.*)/;

@vGenMethods = ("tnum", "base26",
		"base26s",
		"simpleWords", "markov-0", "markov-0e", "markov-1", "markov-1e", "markov-5", "markov-5e"
	       );
if ($reEmulate) {
  foreach $metho (@vGenMethods) {
    $cmd = "$emulator $corpus Piecewise $metho dlhisto ind";
    runit($cmd);
  }
}

foreach $metho (@vGenMethods) {
  # Now we get the data from the emulations to fill the columns in the table

  print $metho;
  $emuVocab = "$expRoot/Emulation/Piecewise/${metho}_dlhisto_ind/${corpus}_vocab.tsv";
  
  # 1. % overlap with base vocab.
  # print sprintf(" & %.1f\\%%", getOverlap() * 100.0);    ######## Suppressed because of apparent problem with comm -- different sort order in C v. Bash?

  # 2,3. Number of collisions compared with base, Length of longest collision chain compared with base.
  $t = getHashStats($emuVocab);
  ($emuCollisions, $emuMaxChain) = $t =~ /(.*),(.*)/;
  print sprintf(" & %.1f\\%% & %.1f\\%%", $emuCollisions * 100 / $baseCollisions, $emuMaxChain * 100 / $baseMaxChain);

  # 4, 5, 6. state memory, microsec/word, tries/word
  print getBuildWordStats("$expRoot/Emulation/Piecewise/${metho}_dlhisto_ind/${corpus}_gencorp.log");

  print "\\\\\n";
}



exit(0);

#----------------------------------------------------------------------------------
sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl $exe" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl $cwd/$exe";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "$cwd/$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}


sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}

sub getOverlap {
  mkdir "wipeMe" unless -e "wipeMe";
  die "mkdir wipeMe failed\n" if $?;
  system("awk '{print \$1}' $baseVocab > wipeMe/A") unless -r "wipeMe/A";
  die "Awk A failed\n" if $?;
  system("awk '{print \$1}' $emuVocab > wipeMe/B");
  die "Awk B failed\n" if $?;
  my $rslt = `comm -1 -2 wipeMe/A wipeMe/B | wc -l`;
  die "Comm failed\n" if $?;
  print "\n -----\n$rslt\n";
  my $overlap = $rslt * 100.0 / $vSize;
  return $overlap;
}


sub getHashStats {
  my $vFile = shift;
  my $rslt = `$hashStatsGetter $vFile | grep -v '#'`;
  die "CVHR failed\n" if $?;
  my @f = split /\s+/, $rslt;
  return "$f[3],$f[4]";   # collisions, max chain length.
}

sub getBuildWordStats {
  my $logFile = shift;
  my $mem = "manual";
  my $tries = "not found";
  my $musec = "notfound";
  undef $HTM;
  undef $MMM;
  
  my $rslt = `grep BuildVocab: $logFile`;
  while ($rslt =~ /(.*)\n/g) {
    my $lyn = $1;
    if ($lyn =~ /Tries per word = ([0-9.]+)/) {
      $tries = $1;
    } elsif ($lyn =~ /([0-9.]+)\s+microseconds \/ term/) {
      $musec = $1;
    } elsif ($lyn =~ /Hashtable memory = ([0-9.]+)MB/) {
      $HTM = $1;
    } elsif ($lyn =~ /Markov matrices.*?([0-9.]+)MB/) {
      $MMM = $1;
    } else {
      die "\nBuildWord line not recognized:\n$lyn\n";
    }
  }
  if (defined($HTM) && defined($MMM)) {
    $mem = "$MMM + $HTM MB";
  }

  return " & $mem & $musec & $tries";
}
