#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Run a bunch of emulations to generate graphs for p.tex
#
# It is expected that the five arrays defined a few lines down will be routinely edited to control
# what emulations are actually done.   Note that the Cartesian product of 5 dimensions translates to a
# very large number of emulations if each dimension has several values.
#

$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$exptRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$exptRoot/Base";
$emuRoot = "$exptRoot/Emulation";

@corpora = ("ap", "alladhoc", "wikititles", 
	    "fr", "patents", "wt10g", "t8nlqs"
	   );
@emutypes = ("ind");
@dlModels = ("dlhisto");
@tfModels = ("Piecewise");
@trModels = ("base26", "simpleWords", "markov-1", "markov-5", "markov-1e",
	     "markov-5e");

@basicCommands = (
		  #"./Caesar.exe $baseDir/CORP.trec 1 $emuRoot/Caesar1/CORP.trec",
		  "./Nomenclator.exe $baseDir/CORP.trec $baseDir/CORP_vocab_by_freq.tsv  $emuRoot/Nomenclator/CORP.trec wipeMe.mapping",
		  #"./MarkovGenerator.exe inFile=$baseDir/CORP.trec outFile=$emuRoot/Markov0/CORP.trec -useOrderZero=TRUE ",
		  #"./MarkovGenerator.exe inFile=$baseDir/CORP.trec outFile=$emuRoot/Markov1/CORP.trec",
				  
		 );
		  

foreach $corps (@corpora) {
  print "   ^^^ Doing the basic methods for $corps ...\n";
  foreach $belt (@basicCommands) {
    # Goal is to extract properties from base version, create an emulated version and extract its properties
    # Don't bother if all that has already been done.
    $bGen = $belt;
    $bGen =~ s/CORP/$corps/g;
    undef $outFile;
    if ($bGen =~ /Caesar/) {
      ($outFile) = $bGen =~ /(\S+)$/;
    } elsif ($bGen =~ /Nomenclator/) {
      ($outFile) = $bGen =~ m@ (\S+/Emulation/Nomenclator/\S+\.trec)@;
    } elsif ($bGen =~/Markov/) {
      ($outFile) = $bGen =~ m@outFile=(\S+)@;
    } else {
      die "Nothing matched for $bGen\n";
    }

    
    $sumFile = $outFile;
    $sumFile =~ s/\.trec/_summary.txt/;
    $emuDir = $outFile;
    $emuDir =~ s/\/[^\/]+\.trec//;
    if ((-r "$baseDir/${corps}_summary.txt")
	&& (-r $outFile)
	&& (-r $sumFile)) {
      print "Skipping:  $outFile, because job already done.\n\n\n";      
      next;
    } else {
      $cmd = "$bGen";
      print "\n\n ---> $cmd\n\n\n";
      system $cmd;
      die "Failed!: $cmd\n" if $?;
      # Now extract comparison data
      $cmd = "$perl compareBaseWithMimic.pl $corps $emuDir 2 2\n";
      print "\n\n -----> $cmd\n\n\n";
      system $cmd;
      die "Failed!: $cmd\n" if $?;
    }
  }


  if (0) {
    print "   ^^^ Doing the SynthaCorpus methods for $corps next ...\n";

    foreach $etype (@emutypes) {
      foreach $tfModel (@tfModels) {
	foreach $dlModel (@dlModels) {
	  foreach $trModel (@trModels) {
	    # Goal is to extract properties from base version, create an emulated version and extract its properties
	    # Don't bother if all that has already been done.
	    $emuDir = "$emuRoot/$tfModel/${trModel}_${dlModel}_$etype";
	    if ((-r "$baseDir/${corps}_summary.txt")
		&& (-r "$emuDir/${corps}.trec")
		&& (-r "$emuDir/${corps}_summary.txt")) {
	      print "Skipping:  Dependence model $etype, Corpus $corps, because job already done.\n\n\n";
	      next;
	    } else {
	      $cmd = "$perl emulateARealCorpus.pl $corps $tfModel $trModel $dlModel $etype";
	      print "\n\n ---> $cmd\n\n\n";
	      system $cmd;
	      die "Failed!: $cmd\n" if $?;
	    }
	  }
	}
      }
    }
  }
}


print "\n\nAll done!\n\n";

exit(0)
