#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

use File::Copy;
# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
 
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$imageRoot = "$ENV{HOME}/GIT/SynthaCorpus/Book/Imagefiles";
die "Image directory $imageRoot not found\n" unless -e $imageRoot;

@corpora = (
	    "t8nlqs", "ap", "fr", "patents", "alladhoc", "wt10g"
	   );  

@ngOpts = ("ind", "ngrams2", "ngrams3");

@fileTypes = ("unigrams", "bigrams", "ngrams", "repetitions", "distinct_terms");

$emOpts = "Piecewise tnum dlhisto";    # Use tnum because its very much faster than markov-5e for large vocabs


foreach $corpus (@corpora) {
  foreach $ngopt (@ngOpts) {
    $cmd = "/bin/rm -f $expRoot/Emulation/Piecewise/tnum_dlhisto_$ngopt/${corpus}*";  # Make sure nothing is left from previous runs
    runit($cmd);
    $cmd = "$perl emulateARealCorpus.pl $corpus $emOpts $ngopt -force";
    runit($cmd);
    # Copy the relevant PDFs to the applicable image directory
    $imageDir = "$imageRoot/Piecewise/tnum_dlhisto_$ngopt";
    foreach $ft (@fileTypes) {
      $file = "$expRoot/Emulation/Piecewise/tnum_dlhisto_$ngopt/${corpus}_base_v_mimic_$ft.pdf";
      copy($file, $imageDir);
      print "\n       $file copied to $imageDir\n";
    }
  }
}


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
    
