#! /usr/bn/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.

# Run multiple Markov emulations of a list of corpora (both order 0 and order 1)
# and record the number of documents generated and the vocabulary size for each
# run. Calculate mean and range as percentages of the original numbers.

@corpora = ("ap");
$iters = 10;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";
@var = ("docs", "vocab_size", "total_postings");


foreach $corpus (@corpora) {
  foreach $meth ("Markov1") {
    foreach $var (@var) {
      $count{$var} = 0;
      $sum{$var} = 0;
      $sumsq{$var} = 0;
      $low{$var} = 9999999999;
      $high{$var} = 0;
    }

    for ($i = 0; $i < $iters; $i++) {
      print "     #$corpus  $meth  $i\n";
      $emuDir = "$expRoot/Emulation/$meth";
      $cmd = "./MarkovGenerator.exe inFile=$baseDir/$corpus.trec outFile=$emuDir/$corpus.trec";  
      if ($meth eq "Markov0") {$cmd .= " -useOrderZero=TRUE";}
      $cmd .= "| egrep 'MG\\\(.\\\):' | egrep 'docs:|vocab_size:|total_postings'";
      $rezo = `$cmd`;
      die "$cmd failed with code $?\n" if $?;
      #print $rezo;
      foreach $var (@var) {
	$count{$var}++;
	if ($rezo =~ /$var:\s+([0-9.]+) v\. ([0-9.]+)/s) {
	  $em = $1;
	  $ba = $2;
	  $perc = 100.0 * $em / $ba;
	  $sum{$var} +=  $perc;
	  $sumsq{$var} += $perc * $perc;
	  $low{$var} = $perc if $perc < $low{$var};
	  $high{$var} = $perc if $perc > $high{$var};
	}
      }
    }
    print "$meth on $corpus\n";
    foreach $var (@var) {
      $mean = $sum{$var} / $count{$var};
      $stdev = sqrt($sumsq{$var} / $count{$var} - $mean * $mean);
      print "    $var: ", sprintf("%d, %.2f (%.3f) %.2f -- %.2f\n",
				  $count{$var}, $mean, $stdev,
				  $low{$var}, $high{$var});
    }

    
  } # End of loop over methods
}   # End of loop over corpora


exit(0);

# -----------------------------------------------------------------------------------------------
