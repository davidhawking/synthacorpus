#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.


# Check whether all the necessary sampling has been done for the various corpora.  Do it
# if necessary.

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
 
$perl = $^X;
$perl =~ s@\\@/@g;


chomp($hostname = `hostname`);

$|++;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$temporalDir = "$expRoot/Sampling/TemporalSubset";
$sampleDir = "$expRoot/Sampling/Sample";
$genericDir = "$expRoot/GenericGrowth";
$basicScalingOptions = "Linear dlgamma markov-5e ind";

if ($#ARGV >= 0 && $ARGV[0] =~ /force/i) {
  print "\n\nDirectories Sampling, GenericGrowth, and Scalingup will be deleted in 10 seconds, to ensure a clean rerun\n\n";
  sleep(10);
  system("/bin/rm -rf $expRoot/Sampling $expRoot/GenericGrowth $expRoot/Scalingup");
  die "Recursive removal failed.\n" if $?;
}



@corpora = (
	    "ap", "wsj",
	    #"fr", "alladhoc", "wt10g", "t8nlqs"
	   );   # Corpora on which sampling is to be done. Patents has too few documents.

@tcorpora = (
	     #"apto","wsj"
	    );  # Time ordered corpora.  apto is time ordered. ap is not.

@gcorpora = (
	     #"ap", "wsj"
	    );   # Corpora on which to try generic scaling.

# Samples
foreach $corpus (@corpora) {
  if (-e "$sampleDir/scaling_model_for_${corpus}_50.txt") {
    print "\nScaling model for $corpus exists - no need to run sampling again.\n\n";
  } else {
    $cmd = "$perl samplingExperiments.pl $corpus";
    print $cmd, "\n";
    system($cmd);
    die "Command $cmd failed\n" if $?;
  }
}

send_imessage_to_Dave("Scaling models for samples are ready");

# Temporal Subsets -- only applicable for time-ordered corpora
foreach $corpus (@tcorpora) {
  if (-e "$temporalDir/scaling_model_for_${corpus}_50.txt") {
    print "\nScaling model for $corpus exists - no need to run sampling again.\n\n";
  } else {
    $cmd = "$perl samplingExperiments.pl $corpus -temporalGrowth";
    print $cmd, "\n";
    system($cmd);
    die "Command $cmd failed\n" if $?;
  }
}


send_imessage_to_Dave("Scaling models for temporal subsets are ready");


# Now do the scaling up experiments - the results are put in subdirectories of
# Experiments/Scalingup which are named as in the following examples
# CT-1-100 - Scaling up by a factor of a hundred from a 1% sample, using a corpus-specific growth model
# GS-50-2 - Scaling up by a factor of 2 from a 50% sample, using a generic growth model
# CS-50-2 - Scaling up by a factor of 2 from a 50% sample, using a corpus-specific growth model


@supDirs = ("CS-1-100", "CS-10-10", "CS-50-2", "CT-1-100", "CT-10-10", "CT-50-2");

if_necessary_make_supDirs();

for $corpus (@corpora) {
  runit("$perl scaleUpASample.pl $corpus $sampleDir/scaling_model_for_${corpus}_1.txt 100 CS-1-100 $sampleDir/1% $basicScalingOptions -compareWithBase");
}


send_imessage_to_Dave("CS-1-100 ready for @corpora");


for $corpus (@corpora) {
  runit("$perl scaleUpASample.pl $corpus $sampleDir/scaling_model_for_${corpus}_10.txt 10 CS-10-10 $sampleDir/10% $basicScalingOptions -compareWithBase");
}

send_imessage_to_Dave("CS-10-10 ready for @corpora");

for $corpus (@corpora) {
  runit("$perl scaleUpASample.pl $corpus $sampleDir/scaling_model_for_${corpus}_50.txt 2 CS-50-2 $sampleDir/50% $basicScalingOptions -compareWithBase");
}

send_imessage_to_Dave("CS-50-2 ready for @corpora");

for $corpus (@tcorpora) {
  runit("$perl scaleUpASample.pl $corpus $temporalDir/scaling_model_for_${corpus}_1.txt 100 CT-1-100 $temporalDir/1% $basicScalingOptions -compareWithBase");
}

send_imessage_to_Dave("CT-1-100 ready for @tcorpora");

for $corpus (@tcorpora) {
  runit("$perl scaleUpASample.pl $corpus $temporalDir/scaling_model_for_${corpus}_10.txt 10 CT-10-10 $temporalDir/10% $basicScalingOptions -compareWithBase");
}

send_imessage_to_Dave("CT-10-10 ready for @tcorpora");

for $corpus (@tcorpora) {
  runit("$perl scaleUpASample.pl $corpus $temporalDir/scaling_model_for_${corpus}_50.txt 2 CT-50-2 $temporalDir/50% $basicScalingOptions -compareWithBase");
}

send_imessage_to_Dave("CT-50-2 ready for @tcorpora");


if (0) {
  runit("$perl makeGenericGrowthModels.pl");

  # Note that in the following, there is no generic sample, so we use a specific one instead.

  for $corpus (@gcorpora) {
    runit("$perl scaleUpASample.pl $corpus $genericDir/generic_model_1.txt 100 GS-1-100 $sampleDir/1% $basicScalingOptions -compareWithBase");
  }
  
  for $corpus (@gcorpora) {
    runit("$perl scaleUpASample.pl $corpus $genericDir/generic_model_10.txt 10 GS-10-10 $sampleDir/10% $basicScalingOptions -compareWithBase");
  }
  
  for $corpus (@gcorpora) {
    runit("$perl scaleUpASample.pl $corpus $genericDir/generic_model_50.txt 2 GS-50-2 $sampleDir/50% $basicScalingOptions -compareWithBase");
  }
} else {
  print "Skipping Generic scaling -- work needed to get it right.\n\n";
}

if (0) {
  print "Running Markov scaling from 1%, 10%, 50% samples. markov_scaling.pl chooses which corpora are used.\n";
  runit("$perl markov_scaling.pl");
} else {
  print "Skipping Markov scaling\n";
}

print "\n\nAll done:  $0\n\n";

send_imessage_to_Dave("All done:  $0");

exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  dye("Command $cmd failed\n")  if $?;
}
    

sub if_necessary_make_supDirs {

  mkdir "$expRoot/Scalingup" unless -e "$expRoot/Scalingup";
  foreach $subDir (@supDirs) {
    $dir = "$expRoot/Scalingup/$subDir";
    mkdir $dir unless -e $dir;
  }
}


sub send_imessage_to_Dave {
  my $msg = shift;
  my $cmd = "osascript -e 'tell application \"messages\" to send \"$hostname: $msg\" to buddy \"Myself\"'";
  system($cmd);
  warn "$cmd failed\n" if $?
}


sub dye {
  my $msg = shift;
  send_imessage_to_Dave($msg);
  die $msg;
}
