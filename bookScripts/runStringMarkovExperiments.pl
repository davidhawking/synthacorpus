#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# For a set of corpora, observe the change in vocab overlap and sentence overlap as k increases.
# For each corpus and for each value of k, do a stringMarkov emulation and run a comparison with
# the base.  Copy the relevant graphs into the book image directory.
# Also extract overlap data and write them into the base_v_mimic_summary.txt files for later
# analysis with analyseSME.pl

use File::Copy;

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
$perl = $^X;
$perl =~ s@\\@/@g;

 die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$emuRoot = "$expRoot/Emulation";
mkdir $emuRoot unless -e $emuRoot;
$smRoot = "$emuRoot/stringMarkov";
mkdir $smRoot unless -e $smRoot;
$smBase = "$expRoot/Simplified";   # where the simplified version of the base will be put
mkdir $smBase unless -e $smBase;
$baseDir = "$expRoot/Base";
die "Base directory $baseDir must exist and contain the necessary corpus files\n"
  unless -d $baseDir;

$imageRoot = "$ENV{HOME}/GIT/SynthaCorpus/Book/Imagefiles";
die "Image directory $imageRoot not found\n" unless -e $imageRoot;
$imageParent = "$imageRoot/stringMarkov";
mkdir $imageParent unless -e $imageParent;

@corpora = (
	    "t8nlqs", "apss"
	   );  

@k = (5, 6, 7, 8, 10, 12, 15, 18, 23
     );

@fileTypes = ("unigrams", "bigrams", "ngrams", "repetitions", "distinct_terms");

$genOpts = "-hashBits=28 -wildcards=0 -predefinedAlphabet=TRUE -lambda=0";

foreach $corpus (@corpora) {
  $corpusIn = "$baseDir/$corpus.trec";
  die "Base file $corpusIn not found.\n"
    unless -r $corpusIn;
  $simplifiedProcessed = 0;
  foreach $k (@k) {
    $emuDir = "$smRoot/$k";
    mkdir $emuDir unless -e $emuDir;
    $corpusOut = "$emuDir/$corpus.trec";
    
    runit("/bin/rm -f $emuDir/${corpus}*");  # Make sure nothing is left from previous runs
    if ($simplifiedProcessed) {
      runit("../src/stringMarkovGen.exe inFile=$corpusIn outFile=$corpusOut -k=$k $genOpts");
      runit("../src/checkTRECFile.exe $corpusOut");
      # Now compare the emulation with the base version of the corpus
      runit("perl ../src/compareBaseWithMimic.pl $corpus $emuDir 2 3 -force -simplified");
    } else {
      runit("../src/stringMarkovGen.exe inFile=$corpusIn outFile=$corpusOut simplifiedFile=$smBase/$corpus.trec -k=$k $genOpts");
      runit("../src/checkTRECFile.exe $corpusOut");
      runit("../src/checkTRECFile.exe $smBase/$corpus.trec");
      # Now compare the emulation with the base version of the corpus and force recalculation of the base
      runit("perl ../src/compareBaseWithMimic.pl $corpus $emuDir 2 3 -force -simplified");
      runit("grep -v '<' $smBase/$corpus.trec | sort -u > $smBase/$corpus.ust");  # A sorted file of unique text-only lines
      runit("awk -F\\t '{print \$1}' $smBase/${corpus}_vocab.tsv > $smBase/$corpus.wdlist");
     $simplifiedProcessed = 1;
    }
    runit("grep -v '<' $corpusOut | sort -u > $emuDir/$corpus.ust");  # A sorted file of unique text-only lines
    runit("awk -F\\t '{print \$1}' $emuDir/${corpus}_vocab.tsv > $emuDir/$corpus.wdlist");

    # Now measure the overlap of texts and vocab words
    $tlines = `wc -l $emuDir/$corpus.ust`;
    die "wc -l $emuDir/$corpus.ust failed\n" if $?;
    $tlines =~ /([0-9]+)/;
    $tlines = $1;
    $vlines = `wc -l $emuDir/$corpus.wdlist`;
    die "wc -l $emuDir/$corpus.wdlist failed\n" if $?;
    $vlines =~ /([0-9]+)/;
    $vlines = $1;
    $toverlap = `comm -12 $emuDir/$corpus.ust $smBase/$corpus.ust | wc -l`;
    die "First comm failed\n" if $?;
    chomp($toverlap);
    $voverlap = `comm -12 $emuDir/$corpus.wdlist $smBase/$corpus.wdlist | wc -l`;
    die "Second comm failed\n" if $?;
    chomp($voverlap);

    $bmsumfile = "$emuDir/${corpus}_base_v_mimic_summary.txt";
    die "Can't append to $bmsumfile\n" unless open BMS, ">>$bmsumfile";
    print BMS "Text overlap: $toverlap / $tlines = ", sprintf("%.2f%%\n", 100.0  * $toverlap / $tlines);
    print BMS "Vocab overlap: $voverlap / $vlines = ", sprintf("%.2f%%\n", 100.0  * $voverlap / $vlines);
    close(BMS);
    print "Text overlap: $toverlap / $tlines = ", sprintf("%.2f%%\n", 100.0  * $toverlap / $tlines);
    print "Vocab overlap: $voverlap / $vlines = ", sprintf("%.2f%%\n", 100.0  * $voverlap / $vlines);

    # Copy the relevant compareBaseWithMimic PDFs to the applicable image directory
    $imageDir = "$imageParent/$k";
    mkdir $imageDir unless -e $imageDir;
    foreach $ft (@fileTypes) {
      $file = "$emuDir/${corpus}_base_v_mimic_$ft.pdf";
      copy($file, $imageDir);
      print "\n       $file copied to $imageDir\n";
    }
  }
}


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
    
