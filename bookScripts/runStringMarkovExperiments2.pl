#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Testing Paul's shrinkage ideas -- wildcards & lambda

use File::Copy;

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$emuRoot = "$expRoot/Emulation";
mkdir $emuRoot unless -e $emuRoot;
$smRoot = "$emuRoot/stringMarkov";
mkdir $smRoot unless -e $smRoot;
$smBase = "$expRoot/Simplified";   # where the simplified version of the base will be put
mkdir $smBase unless -e $smBase;
$baseDir = "$expRoot/Base";
die "Base directory $baseDir must exist and contain the necessary corpus files\n"
  unless -d $baseDir;

$imageRoot = "$ENV{HOME}/GIT/SynthaCorpus/Book/Imagefiles";
die "Image directory $imageRoot not found\n" unless -e $imageRoot;
$imageParent = "$imageRoot/stringMarkov";
mkdir $imageParent unless -e $imageParent;

@corpora = (
	    "apss",
	    "t8nlqs",
	    #"ap", "fr", "patents",
	    #"alladhoc", "wt10g",
	    #"sjmss",
	    #"ftss"
	   );

@wildcards = (21);

$k = 23;

@lambda10 = (-6.5, -6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2, -1.5, -1);   # Powers of ten

@fileTypes = ("unigrams", "bigrams", "ngrams", "repetitions", "distinct_terms");

$genOpts = "-hashBits=28 -predefinedAlphabet=TRUE ";


foreach $corpus (@corpora) {
  $corpusIn = "$baseDir/$corpus.trec";
  die "Base file $corpusIn not found.\n"
    unless -r $corpusIn;
  $simplifiedProcessed = 0;  #(-e "$smBase/$corpus.trec");
  foreach $wildcard (@wildcards) {
    foreach $lambda10 (@lambda10) {
      $lambda = sprintf("%.8f", 10.0 ** $lambda10);
      $emuDir = "$smRoot/${k}_${wildcard}_${lambda}";
      mkdir $emuDir unless -e $emuDir;
      $corpusOut = "$emuDir/$corpus.trec";
    
      runit("/bin/rm -f $emuDir/${corpus}*");  # Make sure nothing is left from previous runs
      if ($simplifiedProcessed) {
	# --- We've already created a simplified version of the base corpus ---
	runit("../src/stringMarkovGen.exe inFile=$corpusIn outFile=$corpusOut -k=$k -wildcards=$wildcard -lambda=$lambda $genOpts");
	runit("../src/checkTRECFile.exe $corpusOut");
      } else {
	# --- To permit fair comparisons, we need to create a simplified version of the base corpus
	runit("../src/stringMarkovGen.exe inFile=$corpusIn outFile=$corpusOut simplifiedFile=$smBase/$corpus.trec -k=$k -wildcards=$wildcard -lambda=$lambda $genOpts");
	runit("../src/checkTRECFile.exe $corpusOut");
	runit("../src/checkTRECFile.exe $smBase/$corpus.trec");
	$simplifiedProcessed = 1;
      }

      # Now compare the emulation with the base version of the corpus and force recalculation of the base
      runit("perl ../src/compareBaseWithMimic.pl $corpus $emuDir 2 3 -force -simplified");
      runit("perl ../src/calculateOverlaps.pl $corpus $smBase $emuDir"); 
      
      # Copy the relevant PDFs to the applicable image directory
      $imageDir = "$imageParent/$lambda";
      mkdir $imageDir unless -e $imageDir;
      foreach $ft (@fileTypes) {
	$file = "$emuDir/${corpus}_base_v_mimic_$ft.pdf";
	copy($file, $imageDir);
	print "\n       $file copied to $imageDir\n";
      }
    }
  }
}


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
    
