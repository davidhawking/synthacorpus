#! /usr/bin/perl -w

# Emulate a corpus using StringMarkov with multiple values of k and fixed lambda, and calculate the
# Labbe distance LD between each emulation and the base.   Enabling a plot of LD vs. k.

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/

$perl = $^X;
$perl =~ s@\\@/@g;


$k = 10;
@lambda = (0, 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5);

die "Many SynthaCorpus scripts require that the environment variable \$SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
die "Can't find experiment root: $expRoot\n" unless -d $expRoot;
$baseDir = "$expRoot/Base";
die "Can't find base directory: $baseDir\n" unless -d $baseDir;
$SMRoot = "$expRoot/Emulation/StringMarkov";
if (! -e $SMRoot) { mkdir $SMRoot; }

$plotter = `which gnuplot 2>&1`;
chomp($plotter);
if ($plotter =~/^which: no/) {
  undef $plotter;
} else {
  $plotter = "gnuplot";   # Rely on it being in our path
}

$emulator = check_exe("../src/stringMarkovGen.exe");
$comparator = check_exe("../src/compareBaseWithMimic.pl");
$labbe = check_exe("Labbe.pl");

die "Usage: $0 <corpusName>
   We expect to find $baseDir/<corpusName>.trec\n"
  unless $#ARGV == 0;

$corpus = $ARGV[0];
$baseFile = "$baseDir/$corpus.trec";
die "Base corpus $baseFile doesn't exist\n"
  unless -e $baseFile;

unlink "$baseDir/${corpus}_summary.txt";   # To force property extraction once for base

$plotDat = "$SMRoot/${corpus}_Labbe_vs_lambda.dat";
die "Can't write to $plotDat\n"
  unless open DAT, ">$plotDat";

$plotCmds = "$SMRoot/${corpus}_Labbe_vs_lambda.plot.cmds";
die "Can't write to $plotCmds\n"
  unless open PC, ">$plotCmds";

$plotPDF = "$SMRoot/${corpus}_Labbe_vs_lambda.pdf";

foreach $lambda (@lambda) {
  $emuDir = "$SMRoot/$lambda";
  if (! -e $emuDir) {mkdir $emuDir;}
  if (! -e "$emuDir/${corpus}_ngrams_by_freq.tsv") {  # Skip if it's already been done
    runit("/bin/rm -rf $emuDir/*");
    $emuFile = "$emuDir/$corpus.trec";
    runit("$emulator -inFile=$baseFile -outFile=$emuFile -k=$k -lambda=$lambda");
    runit("$comparator $corpus $emuDir 2 3");
  }
  $labOut = `$labbe $baseDir/$corpus $emuDir/$corpus vocab`;
  $labOut .= `$labbe $baseDir/$corpus $emuDir/$corpus bigrams`;
  $labOut .= `$labbe $baseDir/$corpus $emuDir/$corpus ngrams`;
  print DAT "$lambda";
  while ($labOut =~ /Labbe distance\s+=\s+([0-9.]+)/sg) {
    print DAT "\t$1";
  }
  print DAT "\n";
}
close(DAT);

# Now write the plot commands
print PC "
set terminal pdf
set size ratio 1
set xlabel \"lambda (with k = $k)\"
set ylabel \"Labbe distance from base\"
set style line 1 linewidth 2
set style line 2 linewidth 2
set style line 3 linewidth 2
set style line 4 linewidth 2
set style line 5 linewidth 2
set style line 6 linewidth 2
set style line 7 linewidth 2
set style line 8 linewidth 2
set style line 9 linewidth 2
set style line 10 linewidth 2
set style line 11 linewidth 2
set pointsize 0.5

set output \"$plotPDF\"
plot \"$plotDat\" using 1:2 with linespoints title \"words\",  \"$plotDat\" using 1:3 with linespoints title \"bigrams\",  \"$plotDat\" using 1:4 with linespoints title \"ngrams, n=2,3\"
";

runit("gnuplot $plotCmds");

print "

   acroread $plotPDF

";

exit(0);

# ----------------------------------------------------------------------------

sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}


sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
