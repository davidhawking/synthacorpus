#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

$perl = $^X;
$perl =~ s@\\@/@g;

chomp($hostname = `hostname`);
$|++;

@corpora = (
	    "t8nlqs",
	    "fr", "patents",
	    "ap"
	   );


die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";
$emuRoot= "$expRoot/Emulation";

runit("ln -s $emuRoot/Uniform/tnum_dluniform_ind $emuRoot/SimpleSynth")
  unless -e "$emuRoot/SimpleSynth";

runit("ln -s $emuRoot/Piecewise/markov-5e_dlhisto_ngrams3 $emuRoot/SophSynth")
  unless -e "$emuRoot/SophSynth";

$qGenerator = check_exe("../src/queryGenerator.exe");


foreach $corpse (@corpora) {
  # The GPT-2 corpora have been copied in via Bodo, we just need to extract their properties.
  runit("$perl ../src/compareBaseWithMimic.pl $corpse '$emuRoot'/GPT-2  2 3 -force");
  ifnec_generate_queries_for_corpus("'$emuRoot'/GPT-2", $corpse);
  
  mkdir "$emuRoot/WBMarkov1" unless -e "$emuRoot/WBMarkov1";
  die "Mkdir $emuRoot/WBMarkov1 failed\n" if $?;
  runit("../src/MarkovGenerator.exe inFile='$baseDir'/$corpse.trec outFile='$emuRoot'/WBMarkov1/$corpse.trec");
  runit("$perl ../src/compareBaseWithMimic.pl $corpse '$emuRoot'/WBMarkov1  2 3 -force");
  ifnec_generate_queries_for_corpus("'$emuRoot'/WBMarkov1", $corpse);

  runit("$perl ../src/emulateARealCorpus.pl $corpse Uniform tnum dluniform ind");  # SimpleSynth
  ifnec_generate_queries_for_corpus("'$emuRoot'/SimpleSynth", $corpse);

  runit("$perl ../src/emulateARealCorpus.pl $corpse Piecewise markov-5e dlhisto ngrams3");   # SophSynth
  ifnec_generate_queries_for_corpus("'$emuRoot'/SophSynth", $corpse);
  
}

print "\nAll done\n";

exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
    
sub ifnec_generate_queries_for_corpus {
  # Generate 3 sets of 1000 KI queries of mean lengths 3, 6, and 9 
  my $dir = shift;
  my $corpus = shift;
  # forceASCII to cope with problem in ATIRE.
  my $baseCmd = "$qGenerator corpusFileName=$dir/$corpus.trec propertiesStem=$dir/$corpus -numQueries=1000 -forceASCII=TRUE";
  if ((! -r "$dir/${corpus}_3wds.topics") || (! -r "$dir/${corpus}_6wds.topics") || (! -r "$dir/${corpus}_9wds.topics")) {
    # Assume that if .topics exists so do .qrels and .q
    for my $qCmd ("$baseCmd -meanQueryLength=2.82 -queriesStem=$dir/${corpus}_3wds",
		  "$baseCmd -meanQueryLength=5.85 -queriesStem=$dir/${corpus}_6wds",
		  "$baseCmd -meanQueryLength=8.85 -queriesStem=$dir/${corpus}_9wds",) {
      print "Command '$qCmd'\n";
      system($qCmd);
      dye("    Failed.\n")  if $?;
    }
  }
}


sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl $exe" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl $cwd/$exe";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "$cwd/$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}



sub send_imessage_to_Dave {
  my $msg = shift;
  my $cmd = "osascript -e 'tell application \"messages\" to send \"$hostname: $msg\" to buddy \"Myself\"'";
  system($cmd);
  warn "$cmd failed\n" if $?
}


sub dye {
  my $msg = shift;
  send_imessage_to_Dave($msg);
  die $msg;
}
