#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Check every one of the _summary.txt files under ExperimentsRoot to make sure that each of
# them has a line for gzip_ratio and size_on_disk.
# Fill in any missing info.

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$tmpDir = "$expRoot/tmp";
mkdir $tmpDir unless -e $tmpDir;

foreach $sumry ((glob "$expRoot/Base/*_summary.txt"), (glob "$expRoot/Emulation/*/*_summary.txt"),
		(glob "$expRoot/Emulation/*/*/*_summary.txt")) {

  next if $sumry =~ /base_v_mimic/;   # ---------------->
  undef %props;
  die if defined($props{alpha});   # Just confirm that the undef hash works as expected.
  print "$sumry\n";
  $corpusFile = $sumry;
  $corpusFile =~ s/_summary.txt/.trec/;   # Assume it's not .starc or .tsv
  die "Can't read $corpusFile \n"
    unless -r $corpusFile;
  die "Can't open $sumry\n"
    unless open SF, $sumry;
  while (<SF>) {
    next if /^\s*#/;  # skip comments
    if (/(\S+)=(\S+)/) {
      $props{$1} = $2;
    }
  }
  close(SF);

  if (!defined($props{gzip_ratio})) {
    $gzipRatio = get_compression_ratio($corpusFile, "$tmpDir/wipeMe.gz");
    die "Can't append to $sumry\n"
      unless open SF, ">>$sumry";
    print SF "gzip_ratio=", sprintf("%.4f\n", $gzipRatio);
    close SF;
  }
  if (!defined($props{size_on_disk})) {     
    $size_on_disk = -s $corpusFile;
    die "Can't append to $sumry\n"
      unless open SF, ">>$sumry";
    print SF "size_on_disk=$size_on_disk\n";
    close SF;
  }
}

exit(0);


# ---------------------------------------------------------------------
sub get_compression_ratio {
    my $file = shift;
    my $tmpFile = shift;  
    my $cmd = "gzip -c $file > $tmpFile";
    print "   $cmd\n";
    my $code = `$cmd`;
    if ($code) {
	warn "Command $cmd failed with code $code\n";
	return "N/A";
    }

    my $uncoSize = -s $file;
    my $coSize = -s $tmpFile;
    
    $ratio = sprintf("%.3f", $uncoSize / $coSize);
    
    unlink $tmpFile;
    return $ratio;
}

