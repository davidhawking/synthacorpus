detrec is a simple utility to convert collections of documents in a range of different formats:

  - an individual file
  - a bundle of documents
  - a gzipped bundle of documents, such as those distributed in the TREC web track
  - a tar archive
  - a WARC archive
  - a directory tree containing a mixture of the above

into a single bundle in simplified TREC format:  DOC, DOCNO, and TEXT elements only.

  * Other markup removed
  * Text content converted to UTF-8.

detrec was developed to prepare document sets for experiments with SynthaCorpus which are
described in a forthcoming Morgan and Claypool book, "Simulating Information Retrieval Test Collections".
It may have other applications.

detrec is a heavily modified, cut-down version of some document processing code developed by
Funnelback Pty Ltd.

