/* COPYRIGHT (C) Andrew Tridgell 1995 */
/*************************************************************************/
/*				bmg2.h			                 */
/*		   Digraph Boyer-Moore-Gosper algorithm                  */
/*									 */
/*************************************************************************/


extern void bmg_search(u_char *buf,int len,void (*fn)());

extern int bmg_build_table(u_char *target_string,u_char *char_map,int mode);
