/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*	                                                              */
/*                                  detrec.c                          */
/*	                                                              */
/**********************************************************************/

// A program to take documents in various TREC formats and convert them to
// a much simpler format in which documents are just sequences of indexable
// words.

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>

#include "pcre/pcre.h"

#include "detrec.h"
#include "bmg2/bmg2.h"
#include "unicode/utf8.h"
#include "unicode/utf8_mapping.h"
#include "unicode/unicode_init.h"
#include "scan.h"
#include "lscan.h"
#include "extractor.h"
#include "warcfile.h"
#include "zlib/zlib.h"

long long g_docs_written = 0;

int debug = 0, warc_flag = 0;    // The latter is set on a bundle-by-bundle basis if docs are in warc format
int quiet_flag = 0;    /* suppress verbose logging */
int punc_mode = 0;  // 0 - suppress punctuation; 1 - emit newlines as sentence breaks (no punc.); 2 - emit [some] punctuation

int CHAMBER_SIZE = DFLT_CHAMBER_SIZE;
int chamber_hwm = 0;
u_char *chamberpot = NULL;

u_char *assumed_charset = NULL;  // Docs with no explicit charset are assumed to
                                 // be written in this one.  See iconv --list for
                                 // possible values.
u_char *assumed_utf8 = "UTF-8";  // XML documents default to UTF-8


// Storage buffers for words should be allocated much more than MAX_WORD_LEN
// bytes because of UTF-8 multibyte sequences.  It is the case that all 
// characters in the Basic Multilingual Plane can be encoded in 3 UTF-8 
// bytes so a 3-times scale-up should suffice.  There are some characters
// which take up to 4 UTF-8 bytes but these are v. unlikely to be indexable
// and will hopefully be compensated for in a string by single byte chars.


u_char *getstring(u_char *str, int *len, int lowercase) {
  /* Identify the the next sequence of printable characters (except
     space) in str by means of a pointer and a length.
     Skip over non-graphics in null-terminated str and return a 
     pointer to the first graphic (or NULL if there are none.  
     len records how many graphic chars there are in the sequence.
     Iff lowercase, convert the chars to lower case on the way through
  */
  int cnt = 0;
  u_char *p = str, *ws;
  
  if (str == NULL) return NULL;
  while (*p && !isgraph(*p)) p++;
  if (*p == 0) { *len = 0;  return NULL;}
  ws = p;
  while (*p && isgraph(*p)) { 
    if (lowercase) *p = tolower(*p); 
    p++; 
    cnt++;
  }
  *len = cnt;
  return ws;
}


int tokmatch(u_char *tok, int len, u_char *target) {
  /* Compare a token described in terms of pointer and length, such as
     those returned by getword() and getstring() (above) with a 
     null-terminated target string.  Return 1 if they compare equal,
     0 otherwise.  Note that strncmp() by itself is inadequate 
     because it will signal equality whenever the first len characters
     of target match tok, even if target is longer.
     e.g. strncmp("junk", "junkers", 4) == 0 */

  // Made case-insensitive in 5.0.0.4
  int targlen = strlen(target);
  if (targlen != len) return 0;
  if (!strncasecmp(tok, target, len)) return 1;
  return 0;
}


int load_bundle_via_zlib(u_char *ifname) {
  /* Open the specified input file and read its contents into chamberpot
     up to CHAMBER_SIZE bytes using Zlib library.
     The number of bytes read is returned or -1 if either an error is 
     encountered or the file is too big. 
  */

  gzFile GZF;
  int red, err;

  if (0) printf("load_bundle_via_zlib(%s).  CHAMBER_SIZE = %d\n", ifname, CHAMBER_SIZE);
  if (chamberpot == NULL) {
    printf("Chamberpot is NULL!!!!!!!!!!\n");
    exit(1);
  }
  GZF = gzopen(ifname, "rb");
  red = gzread(GZF, chamberpot, CHAMBER_SIZE);

  /* The chamber is full - If we are not at EOF, signal an error */
  if (red == CHAMBER_SIZE) {

    while (red == CHAMBER_SIZE) {
        err = gzrewind(GZF);
        if (err) {
          printf("\nError: gzrewind error %d for %s\n", err, ifname);
        }

        if (CHAMBER_SIZE >= MAX_DOUBLED_CHAMBER_SIZE) {
              printf("\nFatal Error: file %s bigger than max size allowed for automatic\n"
                     "chamber growth (currently chamber is %.0fMB).\n"
                     "  - Use -chamb option to increase beyond that limit.\n", 
                     ifname, (double)CHAMBER_SIZE / 1048576);
              exit(1);
        }

        CHAMBER_SIZE *= 2;
        free(chamberpot);
        chamberpot = (u_char *) femalloc(CHAMBER_SIZE + 1, 1, "chamber");
       
        printf("Info: Chamber size doubled to accommodate large file %s.\n"
               "Indo: Chamber size is now %.0fMB.\n",
               ifname, (double)CHAMBER_SIZE / 1048576);
        red = gzread(GZF, chamberpot, CHAMBER_SIZE);
    }
  } else if (red == -1) {
    printf("\nWarning: gzread error for %s\n", ifname);
  } else if ( red == 0) {
    printf("\nWarning: gzread empty file %s\n", ifname);
  }
  err = gzclose(GZF);
  if (err) {
    printf("\nWarning: gzclose error %d for %s\n", err, ifname);
  }
  return red;
}

void error_exit(u_char *msg, u_char *detail, u_char *context, int code) {
  /* code is the error code to exit with 
     msg is a message to display to end-users
     detail is a message to display to admin staff
     context is a message with more technical detail
  */
  fprintf(stdout, "Error exit(code %d)\nmsg:%s\ndetail: %s\nContext: %s\n", 
          code, msg, detail, context);
  exit(code);
}

void die(u_char *msg) {
  printf("DIE: %s\n", msg);
  exit(1);
}

void *femalloc(size_t num, size_t size, u_char *msg) {
  /*malloc num blocks of specified size and check the result  */
  void *rslt;
  size_t arg;
  arg = num * size;

#if 0
  rslt = malloc (arg);
#else
  rslt = calloc((size_t) num, size);
#endif


  if (rslt == NULL) {
    printf("Error: Malloc failed (%s) of %ld bytes\n", msg, 
    (long) arg);
    exit(1);
  }
  return rslt;
}

u_char *strmalcpy(u_char *instring, u_char *msg) {
  /*malloc sufficient space to make a null-terminated copy of instring,
    copy instring into it and then return a pointer to the copy. */
  u_char *rslt;
  int len, arg;
  if (instring == NULL) return NULL;

  len = strlen(instring);
  arg = len + 1;

  rslt = malloc (arg);
  if (rslt == NULL) {
    printf("Error: Malloc failed (%s) of %d bytes\n", msg, 
	    (len + 1));
    exit(1);
  } 
  if (arg > 1) strcpy(rslt, instring);
  else rslt[0] = 0;
  return rslt;
}

void putnchars(u_char *s, int n) {
  /* print up to n bytes of string s.  Don't Stop at null.*/
  u_char c;
  while ((n > 0)) {
    c = *s;
    if (c >= 32 || c == '\n' || c == '\r') putchar(c);
    else printf("\\%03o", c);
    s++;
    n--;
  }
}
void putchars(u_char *s, int n) {
  /* print up to n chars of string s.  Stop at null.*/
  u_char c;
  while ((n > 0) && (c = *s)) {
    if (c >= 32 || c == '\n' || c == '\r') putchar(c);
    else printf("\\%03o", c);
    s++;
    n--;
  }
}


u_char *mlstrstr(u_char *haystack, u_char *needle, int maxlen, int stop_on_null) {
  // Like strstr but doesn't examine more than maxlen chars in haystack.
  // Return a pointer to the first occurrence of needle or
  // NULL.  Stop_on_null determines whether scanning continues when a null is
  // encountered.
  // Needle is null-terminated but haystack is not.

  int l2 = strlen(needle);
  u_char *p = haystack, *q, *match_start, 
    *lastposs;
  if (maxlen < l2) return NULL;
  lastposs = haystack + maxlen - l2 + 1; 
  if (0) printf("mlstrstr: '%s' v. '%s'(%d)<br />\n", haystack, 
                needle, l2);
  if (l2 == 0) return haystack;  /* see strstr(3) */
  if (haystack[0] == 0) return NULL;  /* see strstr(3) */


  while (p <= lastposs) {
    if (*p == 0 && stop_on_null) return NULL;
    if (*p == *needle) {
      if (0) printf("mlstrstr: possible match at '%s'<br />\n", p);
      /* we've found a possible start */
      match_start = p;
      q = needle + 1;
      p++;
      while (*q && (*p == *q)) {
        p++; q++;
      }
      if (*q) {
        /* We didn't succeed - backtrack */
        p = match_start + 1;
      } else 
        /* Hurrah */
        return match_start;
    } else {      
      p++;
    }
  }
  return NULL;
}

 
u_char *mlstrcasestr(u_char *haystack, u_char *needle, int maxlen, int stop_on_null) {
  // Like strcasestr but doesn't examine more than maxlen chars in haystack.
  // Return a pointer to the first occurrence of needle or
  // NULL -  Just like mlstrstr but case-insensitive.
  // Stop_on_null determines whether scanning continues when a null is
  // encountered.
  // Needle is null-terminated but haystack is not.
  int l2 = strlen(needle);
  u_char *p = haystack, *q, *match_start, 
    *lastposs = haystack + maxlen - l2 + 1; 
  if (0) {
    printf("mlstrcasestr(max %d): '", maxlen);
    putchars(haystack, 20);
     printf(" ... ' v. '%s'(%d)<br />\n", needle, l2);
  }
  if (l2 == 0) return haystack;  /* see strstr(3) */
  if (haystack[0] == 0) return NULL;  /* see strstr(3) */


  while (p <= lastposs) {
    if (*p == 0 && stop_on_null) return NULL;
    if (tolower(*p) == tolower(*needle)) {
      if (0) printf("Mlstrcasestr: possible match at position %ld<br />\n", 
      (long) (p - haystack));
      /* we've found a possible start */
      match_start = p;
      q = needle + 1;
      p++;
      while (*q && (tolower(*p) == tolower(*q))) {
        p++; q++;
      }
      if (*q) {
        /* We didn't succeed - backtrack */
        p = match_start + 1;
      } else {
        if (0) printf("Returning position %ld\n", (long) (match_start - haystack));
        /* Hurrah */
        return match_start;
      }
    } else {      
      p++;
    }
  }
  return NULL;
}

u_char *url_decode(u_char *dest, u_char *src, int length_max,
		   int *outlen) {
  /* copy src to destination replacing sequences of the form %HH,
     where HH are hexadecimal digits, with the single ascii 
     character represented by 0XHH.

     Also replace + with space unless match_crawler is set.

     No more than length_max characters will be copied and a null 
     will always be appended.  ie. dest must have storage for
     length_max + 1 characters.
     
     If outlen is not-NULL, the length of the output string will be
     returned in it.

  */
  u_char *r = src, *w = dest, h[3] = {0}, rslt;
  if (length_max < 1) {
    *dest = 0;
    return dest;
  }
  while (*r && ((w - dest) < length_max)) {
    if (*r == '%') {
      h[0] = *(r+1);
      if ((h[0] != 0) && (strchr("0123456789ABCDEFabcdef", h[0]) != NULL)) {
        h[1] = *(r+2);
        if ((h[1] != 0) && (strchr("0123456789ABCDEFabcdef", h[1]) != NULL)) {
          rslt = (u_char ) strtol(h, NULL, 16);
          *w++ = rslt; r += 3;
        } else  *w++ = *r++; 
      } else  *w++ = *r++; 
    } else if (*r == '+') {
      *w++ = ' ';
      r++;
    } else {
      *w++ = *r++;  
    }
  }
  *w = 0;
  if (outlen != NULL) *outlen = w - dest;
  return dest;
}



int main(int argc, char**argv) {
  FILE *OUTFILE;
  int err;
  u_char *docmarkers = NULL;  // String (e.g. "<PanDOC>|<DOC>|warc/0.9") listing

  docmarkers = strmalcpy("<DOC>|warc/0.9", "Default new-document markers");
  
  if (argc < 3) {
    printf("Usage: %s <output file> (<directory> | <single bundle of documents>) [-sentences|-punctuation]\n"
	   "  - By default, %s will output words separated only by spaces.  If -punctuation is given, \n"
	   "    a variety of punctuation may be emitted.  Note that this relies on heuristics.\n"
	   "  - If -sentences is given, %s will issue a new line at the end of each sentence, but no other\n"
	   "    punctuation. Unfortunately, the heuristics for detecting sentence ends are not yet very good,\n"
	   "    and so far only likely to work on the TREC corpora, like AP.  The two options are mutually\n"
	   "    exclusive.\n", argv[0], argv[0], argv[0]);
    exit(1);
  }

  if (argc > 3) {
    if (!strcmp(argv[3], "-sentences")) punc_mode = 1;
    else if  (!strcmp(argv[3], "-punctuation")) punc_mode = 2;
    else {
      printf("Error: unrecognized option %s\n", argv[3]);
      exit(1);
    }
  }
  
  unicode_init(); 
  utf8_check_case_bytes( );

    // Initialise the BMG2 tables for searching for docstart markers within bundles
  if ((err = bmg_build_table(docmarkers, NULL , 0))) {
    printf("Error: Code %d initialising BMG2 tables in index/main.c:pass1()\n",
            err);
  } else {
    printf("BMG2 tables initialised for %s\n", docmarkers);
  }

  chamberpot = (u_char *) femalloc(CHAMBER_SIZE + 1, 1, "chamberpot");
  assumed_charset = strmalcpy("UTF-8", "assumed_charset");   // Don't do any charset conversion if no charset is given.

  // Open the output file.  
  OUTFILE = fopen(argv[1], "w");
  if (OUTFILE == NULL) {
    printf("Error: Can't open %s for writing\n", argv[1]);
    exit(1);
  }

  setvbuf(OUTFILE, NULL, _IOFBF, 1024*1024);

  scan_fe(argv[2], OUTFILE);

  // Close the output
  fclose(OUTFILE);
  free(docmarkers);
  if (chamberpot != NULL) free(chamberpot);
  if (assumed_charset != NULL) free(assumed_charset);
  chamberpot = NULL;

  printf("detrec: Normal exit. %lld docs written.\n", g_docs_written);
}
