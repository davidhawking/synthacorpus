/* Copyright (C) Funnelback Pty Ltd */

#ifndef MAX_WORD_LEN  // May be defined to compiler by -DMAX_WORD_LEN=xxx
#define MAX_WORD_LEN 5000  //
#endif

typedef unsigned int u_int;
typedef u_int doctype_t;

typedef unsigned int ucs_t;  // UCS character

typedef unsigned char byte;
typedef unsigned char u_char;


typedef struct{ucs_t lhs, rhs;} unimap_t;

typedef unsigned int date_only_t;

// Case modes
typedef enum {
  CM_ALL_LOWER,
  CM_FIRSTCAP,
  CM_ALLCAPS,
  CM_MIXED,
  CM_DONT_CARE
} case_mode_t;




#define DT_VAGUE 0
#define DT_EMAIL 10
#define DT_HTML 20
#define DT_SGML 50
#define DT_XML 55
#define DT_TEXT 60
#define DT_BINARY 70
#define DT_PDF 80
#define DT_TREC 90


#define MAX_URL_LEN 1024
#define ONE_MEG 1048576
#define DFLT_CHAMBER_SIZE (32*ONE_MEG)
#define MAX_DOUBLED_CHAMBER_SIZE (1024*ONE_MEG)
#define MAX_DOCNAME_LEN MAX_URL_LEN /* cos of URLs */
#define MAX_PATH MAX_URL_LEN
#define MAX_LINE 8192

extern int debug, warc_flag, quiet_flag, punc_mode;
extern u_char *assumed_charset, *assumed_utf8;
extern u_char current_url[MAX_URL_LEN + 1];
extern u_char *chamberpot;
extern int chamber_hwm;  /* Chamber highwater mark  */
extern int CHAMBER_SIZE;
extern int WORD_BUF_SIZE;

extern u_char *assumed_charset ;
extern u_char *assumed_utf8;

extern unimap_t *unimapping;
extern int unimapsize;   
extern u_char *unimapname;

extern long long g_docs_written;

int load_bundle_via_zlib(u_char *ifname);

void *femalloc(size_t num, size_t size, u_char *msg);

u_char *strmalcpy(u_char *instring, u_char *msg);

void putnchars(u_char *s, int n);

void putchars(u_char *s, int n);
                 
u_char *mlstrstr(u_char *haystack, u_char *needle, int maxlen, int stop_on_null);

u_char *mlstrcasestr(u_char *haystack, u_char *needle, int maxlen, int stop_on_null);

int tokmatch(u_char *tok, int len, u_char *target);

u_char *url_decode(u_char *dest, u_char *src, int length_max,
		   int *outlen);
