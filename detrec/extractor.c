/* Copyright (C) Funnelback Pty Ltd */
/*************************************************************************/   
/*			   extractor.c			                 */
/*	            text extraction routines             		 */
/*								       	 */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "detrec.h"
#include "unicode/utf8.h"
#include "unicode/utf8_mapping.h"
#include "unicode/unicode_init.h"
#include "zlib/zlib.h"
#include "pcre/pcre.h"
#include "lscan.h"
#include "extractor.h"

// Storage buffers for words should be allocated much more than MAX_WORD_LEN
// bytes because of UTF-8 multibyte sequences.  It is the case that all 
// characters in the Basic Multilingual Plane can be encoded in 3 UTF-8 
// bytes so a 3-times scale-up should suffice.  There are some characters
// which take up to 4 UTF-8 bytes but these are v. unlikely to be indexable
// and will hopefully be compensated for in a string by single byte chars.

int WORD_BUF_SIZE = (3 * MAX_WORD_LEN);

int SORT_SIGNIF = 20;


#ifndef nan
extern double nan(const char *tagp);  // Getting round problems with Linux-32
#endif 

int urlchars;
int url_processed;  /* Used to make sure we only index one version
			  of the document's URL. */
int within_head = 0;  // only relevant to HTML documents

int wiwd = 0;       /* word-index within doc.  Reset to zero at each new document 		  */

int doc_len;     /* Only record content length nowadays  */

unsigned long long fields_present = 0ULL;


u_char *scanword(u_char *start, u_char *limit, u_char field, FILE *OUTFILE) {
  // This function:
  //   1. Scans a word starting at start
  //   4. Returns a pointer to a character after the end of the
  //      word.  (Note that some non-indexable characters 
  //      following the word may be skipped in some cases which
  //      shouldn't matter.)

  // This function:
  //   - Is called from several places in the scanning code.
  //   - Handles the contorted logic dealing with things like C++ and C#
  //   - Won't scan past limit.  Limit is the last character to scan.
  //   - Won't scan past a null character.
  //   - May be called with the first byte of a multi-byte sequence which
  //   - doesn't turn out to be indexable

  //   *** VITAL NOTE: wiwd is now zeroed in reset_current_doc() and 
  //   *** also in process_distilled(). 
  ucs_t ucs1 = 0, prev = 0, curr;
  u_char copy[WORD_BUF_SIZE + 3], *cp = copy, *cplast = copy + WORD_BUF_SIZE - 1,
    *p = start, *q, utfbuf[7];  
  int entwid, ucs1_is_indexable, ubytes, truncated = 0, 
    u8_written = 0;  // Keeps a count of the no. of UTF-8 characters written
                     // to be compared with SORT_SIGNIF
  if (start > limit) return start;
  *cp = 0;

#if 0
  printf("First char = '%c'\n", *p);
#endif
  // +++++++ STAGE 1 ++++++ scanning the input text

  // Generally, when scanning a UTF-8 char using  utf8_getchar, p indicates the
  // first byte (where the scan started) and q indicates the byte
  // immediately after the UTF8 char.

  while (*p && p <= limit) { // process one character per iteration ----------
    // Stage 1.1: Get the next character.  ASCII, Entity or UTF-8 byte sequence
    // Note that this could easily be extended to handle other sequences
    // such as %XX
    if (*p < 0X80) {
      if (*p == '&') {
        entwid = limit - p;
        p++;
        ucs1 = ucs_convert_entity(p, &entwid);
        // If it's just a floating ampersand, not a real entity then 
        // ucs1 and entwid will both be zero. That's fine because
        // zero is not indexable.
        q = p + entwid;
        p = q;
#if 0
        printf("Entity width: %d\n", entwid);
#endif
      } else {
        ucs1 = *p;
        q = p + 1;
      }
    } else {
      q = p;
      ucs1 = utf8_getchar(&q);
    }

    curr = ucs1;

    // Stage 1.2. ucs1 now contains the character and q indicates where we should
    // scan for the next one.  
    // Now test it for indexability and case
    // Efficient test for indexability is hard to maintain - call the function
    // ucs1_is_indexable = (ucs1 > 255 || (unicode_type[ucs1] & UNI_INDEXABLE));  
    ucs1_is_indexable = ucs_indexable(ucs1);

    if (ucs1_is_indexable 
        || (ucs1 == '+') 
        || (ucs1 == '#' && u8_written == 1)) {

      // Stage 1.3. check special cases involving trailing characters
      // 1. eg. adsl+ and c++.  one or two trailing pluses are counted 
      //    as part of the word unless followed by an indexable character. 
      // 2. eg. C#. a single trailing hash is counted as part of the word
      //    provided that the resulting word is only two letters.

      if (ucs1_is_indexable && ((prev == '#' && !ucs_indexable(prev)) 
                                || prev == '+')) {
        // Have to back-track to the previous character.
        p--;
        while (*p == prev) {
	  cp--; 
          u8_written--;
          p--;
        } 
        p++;
        q = p;
        // Now q points to the right place and counts have been adjusted
        break;
      } else {
        // Store ucs1 in copy buffer and ucsax in axcopy buffer
        // checking for buffer over-runs and for num chars < SORT_SIGNIF
        if (u8_written < SORT_SIGNIF) {
          if (ucs1 < 0X80) {
            if (cp < cplast) *cp++ = (u_char) ucs1;
          } else {
            ubytes = ucs_to_utf8(ucs1, utfbuf);
            if (cp + ubytes <= cplast) {
              strcpy(cp, utfbuf);
              cp += ubytes;
            }
          }
          u8_written++;
        } else truncated = 1;
      }  // End of storing chars in buffers.        
    }  else {
      q = p;  // It might be an angle bracket or a quote.
      break;  // Char in ucs1 is not indexable.
    }

    p = q;
    prev = curr;
  } // End of scanning loop  -------------------------------------------------

    // After the end of scanning the word to be indexed should be in the copy buffer
    // and an accent-conflated version may be in axcopy.  For convenience let's
    // null-terminate them.
  *cp = 0;
 

  if (0) printf("scanword end of stage 1: word = '%s', fld = %c\n", 
                       copy, field);

  *cp = 0;  // Temporarily terminate

  if (u8_written < 1) return(q);   // ----------------->  Return pointer to char after word

  if (punc_mode == 0) {
    fprintf(OUTFILE, "%s ", copy);
  } else if (punc_mode == 1) {
    // Sorry this all assumes ASCII
    int emit_sentence_break = 0, found_preceding_capital = 0, i;
    u_char *z;
    if (ucs1 == '?' || ucs1 == '!') emit_sentence_break = 1;  // Assume this is a sentence break;
    else if (ucs1 == '.' && isspace(*(q + 1))) {
      // Promising but the . might be part of initials or part of an abbreviation such as Rev. or Maj. Gen.
      // First check backwards for a capital within the last few chars
      for (i = 1; i <= 3; i++) {
	if (isupper(q[-i])) found_preceding_capital = 1;
      }
      if (!found_preceding_capital) {
	// Still OK. Now check whether letter following spaces is a capital.
	z = q + 2;
	while (z <= limit && isspace(*z))  z++;
	if (z >= limit) emit_sentence_break = 1;
	else if (isupper(*z)) emit_sentence_break = 1;
      }
    }
    if (emit_sentence_break) fprintf(OUTFILE, "%s\n", copy);
    else fprintf(OUTFILE, "%s ", copy);
  } else {
    if (ucs1 == '.' || ucs1 == ';' || ucs1 == '?' || ucs1 == '!' || (ucs1 == ',' && !isdigit(*(p + 1))))
      fprintf(OUTFILE, "%s%c ", copy, (char)ucs1);   // Punctuation which [usually] needs a following space
    else if (ucs1 == '-' || ucs1 == '&' || ucs1 == ',' || (ucs1 == '\'' && isalpha(*(p + 1))))
      fprintf(OUTFILE, "%s%c", copy, (char)ucs1);   // Punctuation with no need for following space.
    else
      fprintf(OUTFILE, "%s ", copy);
  }

  return(q);
}


u_char trec_docno[MAX_DOCNAME_LEN +1] = {0}; 




  
