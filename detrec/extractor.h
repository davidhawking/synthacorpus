/* Copyright (C) Funnelback Pty Ltd */
/*************************************************************************/   
/*			       extractor.h		                 */
/*	                 Word extraction routines     	                 */
/*									 */
/*************************************************************************/

extern int last_pass, wiwd, doc_len;

extern int urlchars;

extern int url_processed, indexing_suppressed, suppressed_sections;

extern int within_head;

extern unsigned long long fields_present;

extern void index_words_in_url(u_char *url, u_char *caller_code);

extern void flushbufs();

extern void write_durl(u_char *dcu, int strip_pantxt);

extern void process_thisdox_url(int url_is_real);

extern void process_thisdox_bnam();

extern void index_words_in_bundlename();

extern void index_fb_marker(u_char field);

extern void index_meta_words(u_char *str, u_char field, FILE *OUTFILE);

extern u_char *scan_past_endtag(u_char *str, u_char *et);

extern u_char trec_docno[];

extern u_char *process_http_header(u_char *str);

extern void indexable_wd(u_char *wd, u_char field, int sm);

extern void fast_extract_words(int curwin, int check_docnums); 

extern void durl_store(u_char *str);

extern void dtitle_store(u_char *str);

extern void title_store(u_char *str, int priority);


extern void reset_current_doc();

extern void nudoc(off_t tagoff, int phantom);

extern void end_doc(int phantom);

extern byte is_an_rs_punc_char[];

extern void rs_append_punc(u_char punc);

extern u_char *scanword(u_char *start, u_char *limit, u_char field,
                        FILE *OUTFILE);

