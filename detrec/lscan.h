/* Copyright (C) Funnelback Pty Ltd */
/*************************************************************************/   
/*			          lscan.h	                   	 */
/*	      Header files for FSM scanners for all applications         */
/*									 */
/*************************************************************************/

extern void extract_words(u_char *sp, u_char *ep, doctype_t doctype, u_char *trec_docno, FILE *OUTFILE);


extern void load_metatag_mapping(u_char *metatag_mappings_fn);

extern u_char get_field(u_char *nameval);

extern int process_meta_tag(u_char *str);

extern void free_xml_paths();

extern void free_metamap_names();

extern void process_numeric_metadata(u_char *str, u_char field);

extern void process_date(u_char *nameval, u_char *contentval);

extern void free_tams();

extern u_char **tags_as_meta, *tam_field;

extern int num_tams, num_tams_used;

extern int num_metamaps;

extern u_char metafields_mapped[];

extern u_char collfield;

extern u_char latfield;

extern void process_latlong(u_char *str);

extern u_char numericfields[];  

extern u_char numfields_in_order[];  

extern int num_numfields;

extern double *thisdox_numeric;

extern void initialise_metafields();
