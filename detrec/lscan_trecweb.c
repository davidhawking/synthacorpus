/* Copyright (C) Funnelback Pty Ltd */
/*************************************************************************/   
/*			      lscan_TRECweb.c				 */
/*      FSM scanner for intranet search and TREC web application       	 */
/*									 */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <string.h>

#include "detrec.h"
#include "unicode/utf8.h"
#include "unicode/unicode_init.h"
#include "lscan.h"
#include "extractor.h"


// ---- New structures 10.0.1 (Nov 2010) to handle indexing of content ----
// ---- of elements like <h1> as metadata.


static char prevailing_field = 0;

static int index_comments = 0;



// --------------------  Functions for processing specific tag types --------------

// Most of the entries in the table of non-content elements are to facilitate
// extraction of the "content" of TREC Ad Hoc documents.  Well-known HTML element
// names are marked with a comment. The different parts
// of the TREC Ad Hoc collection use different sets of markup tags.  Some are
// unlikely to be well-formed, and none have a schema or DTD.   Identification
// of non-content tags was by manual inspection using an approach guided by
// trying never to exclude useful content (as opposed to metadata).

// Note that the patents sub-collection of TREC is like structured data.  I've
// basically counted everything as content


static u_char *non_content_elements[] = {   // Must be alphabetically sorted for bsearch()
  "!doctype",   // a bit of a trick sorry
  "1st_line",
  "2nd_line",
  "access",
  "an",
  "area",    // HTML
  "author",
  "base",    // HTML
  "byline",
  "c",
  "cellrule",
  "city",
  // "co",  Has content in Financial Times
  "code",
  "code-213",
  "copyrght",
  "correction-date",
  "country",
  "date",
  "date1",
  "dateline",
  "day",
  // "dd", It's content in HTML
  "descript",
  "docid",
  "docno",
  "edition",
  "feature",
  "fileid",
  "first",
  "fld001",
  "fld002",
  "fld003",
  "ftag",
  "g",
  "g7.",
  "g7.b",
  "ht",
  "img",    // HTML
  // "in",   Has content in Financial Times
  "journal",
  "language",
  "length",
  "limlen",
  "link",    // HTML
  "meta",    // HTML
  "memo",
  "month",
  "name",
  "note",
  "page",
  // "p",  Can't exclude p because in HTML it's definitely content and in LA Times
  "pg.col",
  // "pre", It's a content element in HTML
  "pub",
  "pubdate",
  "pubyear",
  "r",
  "region",
  "script",    // HTML
  "second",
  "section",
  "so",
  "state",
  "style",    // HTML
  "subject",
  "tp",
  "type",
  "word.ct",
};

#define NUM_NON_CONTENTS 64

  
  
static int tagcmp(u_char **i, u_char **j) {
  return strcasecmp(*i, *j);
}
  

static u_char *possibly_skip_element(u_char *tagstart, u_char *rab, u_char *end_of_doc,
				     u_char *trec_docno) {
  // tagstart points to the beginning of an SGML-style tag.  It is null-terminated
  // where the closing angle bracket might be.   The string may contain attributes.
  // E.g. if the original tag were <a href="blah">  we would get 'a href="blah"'
  // Our job is to recognize whether this tag begins an element which contains
  // no indexable content.
  u_char *p = tagstart, saveendchar, *q, *endtag = NULL, *found, *eltnameend;
  size_t etlen, scanlimit;
  int is_html_unterminated_element = 0;

  // End tag can't possibly be the start of an element
  if (*tagstart == '/') return rab + 1;    // -------------------------------->
  
  while (*p  && *p > ' ') p++;  // find the end of the element name
  eltnameend = p;
  saveendchar = *p;
  *p = 0;  // temporarily null-terminate
  if (0) printf("possibly_skip_element(%s)\n", tagstart);

  // Lookup the tag in the non_content_elements table
  found = bsearch(&tagstart, non_content_elements, NUM_NON_CONTENTS, sizeof(u_char *),
		  (int (* _Nonnull)(const void *, const void *))tagcmp);
  if (found == NULL) {
    if (0) printf("possibly_skip_element(%s) -  Not found.\n", tagstart);
    *eltnameend = saveendchar;
    return rab + 1;    // -------------------------------->
  }
  
  // Work out what the endtag must be for this non_content element
  endtag = femalloc((p - tagstart) + 4, 1, "endtag");
  q = endtag;
  *q++ = '<';
  *q++ = '/';
  p = tagstart;
  while (*p) *q++ = *p++;  // Copy element name
  *q++ = '>';
  *q = 0;
  etlen = q - endtag;

  if (!strcasecmp(tagstart, "img") || !strcasecmp(tagstart, "base") || !strcasecmp(tagstart, "link")
      || !strcasecmp(tagstart, "area") || !strcasecmp(tagstart, "link") || !strcasecmp(tagstart, "meta")
      || !strcasecmp(tagstart, "!doctype"))
    is_html_unterminated_element = 1;
  
  if (is_html_unterminated_element) scanlimit = 1000;   // Don't waste time looking to the end of the document.
  else scanlimit = end_of_doc - rab - 1;
  q = mlstrcasestr(rab + 1, endtag, scanlimit, 0);
  if (q == NULL) {
    if (1 && !is_html_unterminated_element) {
      printf("Warning: end tag(%s) not found in doc %s.  Content will be emitted.\n",
	     endtag, trec_docno);
    }
    p = rab + 1;
  }
  else p = q + etlen;
  *eltnameend = saveendchar;
  free(endtag);
  if (0) printf("possibly_skip_element(%s) -  Element skipped.\n", tagstart);
  return p;    // -------------------------------->
}

    
void extract_words(u_char *sp, u_char *ep, doctype_t doctype, u_char *trec_docno, FILE *OUTFILE) {
  /*

    Scanner for Web and TRECWeb applications.

    Scan the text starting at sp and ending at ep for words,
    defined as maximal sequences of UTF-8 alphanumerics.  

  */

  /* FSM states ... */
#define SKIPPING 0    /* skipping spaces and punctuation - also initial */
#define TAG 1         /* scanning an sgml tag of the form <[a-zA-Z]*  */
#define LABSEEN 2     /* Left angle bracket seen.  */
#define TAGEQSEEN 3       /* We've encountered an equal sign in a tag, e.g. 
			<a href=           */
#define TAGQUOTE 4    /* We've encountered a quoted string in an attr value  */

  int state, is_indexable; 
  u_int ucschar;
  u_char *p = sp, *tagstart = NULL, *pp, *rab, *r;
  u_char quotechar = 0;       //Need to deal with quoted strings as attr vals  
  u_char *first_rab = NULL;   //Safety mechanism when processing quoted strings
  int scantags = doctype != DT_PDF;  // Assume DT_BINARY are HTML with garbage
  int indexing_suppressed = 0, this_tag_has_attributes = 0;

  if (0) {
    printf("extract_words(%s): ", trec_docno);
    putchars(sp, 50);
    printf(")\n");
  }

  state = SKIPPING;

  /* If we're using this scanner, it makes sense to process
     DT_EMAIL as DT_TEXT.  (Particularly as some false DT_EMAILs 
     have been observed.) */
  if (doctype == DT_EMAIL) doctype = DT_TEXT;

  while (p <= ep) { 
    // This loop originally implemented a simple byte-at-a-time FSM but a 
    // variety of features (e.g. noindex) require tests for whole strings
    // which complicate things.  UTF-8 characters add to the complexity.

    // The following assumptions are made:

    // 1. Multibyte UTF-8 characters are only used for text, not for
    // control purposes.  I.e. characters like < and & and strings
    // like "endnoindex" and '<--' are always single-byte ASCII.

    // 2. Entities consist of ASCII chars only

    if (0) printf("At head of loop ep-p = %lld and *p is '%c'\n", (long long) (ep-p), *p);

    pp = p;  // p and pp are byte rather than character pointers.
             // Generally, after the call to utf8_getchar() p points to the 
             // first byte of the utf-8 character and pp points to the
             // first byte after the utf-8 character.

             // ucschar is the Unicode value of the last UTF-8 character read.
    
    if (*pp < 0X80) {
      // simple ASCII
      ucschar = (u_int) *pp++;  
    } else {
      // multibyte
      ucschar = utf8_getchar(&pp);  
      if (debug) {
        if (ucschar == UTF8_INVALID_CHAR && *(pp-1) != 32) 
          printf ("got invalid: %d, - %c, %c, - %d, %d\n", ucschar, 
                *(pp-1), *pp, (int)*(pp-1), (int)*pp);
      }

      if (0) printf("utf8_gotchar(%d), state = %d, pp-p = %ld, off = %ld\n", 
      ucschar, state, (long) (pp - p), (long) (p - sp));
    }


    // During the following switch, p points to the first byte of the
    // current character and pp points to the first byte of the next
    // character.  Each exit from the switch (i.e. break) is responsible
    // for updating p either to pp or to some character further down the track.
    // These exits are marked  // ---------------------------------------->

    switch (state) {
    case SKIPPING:
      if (ucschar > 255) is_indexable = ucs_indexable(ucschar);
      else is_indexable = (ucschar == '&' || (unicode_type[ucschar] & UNI_INDEXABLE));
      if (is_indexable) {

        r = scanword(p, ep, prevailing_field, OUTFILE);

        if (0) printf("Return from scanword, char = '%c'\n", *r);
        if (r <= p) 
          printf("Shouldn't happen(T): scanword: no advance! %p, %p, %p. Char = %c\n",
                                       p, r, ep, *p);
        p = r;
        if (0) printf("Char after scanword = '%c'\n", *p);

        if (scantags && *p == '<') {
          state = LABSEEN;
          p++;
        }
        break; // --------------------------------------------------->
      }

      if (scantags && *p == '<') state = LABSEEN;
      p = pp;
      break;// --------------------------------------------------->
    case LABSEEN:
      if (0) printf("LABSEEN: first char is '%c'\n", *p);
      this_tag_has_attributes = 0;
      if (!strncmp(p, "!--", 3)) {
        // HTML comments are no longer indexed unless explicitly requested 
        // In either case need to check for the special start/stop indexing tags.
        p+=3;
        while (*p && p < ep && isspace(*p)) p++;
        if (!strncasecmp(p, "*stop_indexing*", 15)) {
          indexing_suppressed = 1;
          p+=15;
        } else if (!strncasecmp(p, "beginnoindex", 12)) {
          indexing_suppressed = 1;
          p+=12;
        } else if (!strncasecmp(p, "noindex", 7)) {
          indexing_suppressed = 1;
          p+=7;
        } else if (!strncasecmp(p, "googleoff:", 10)) {
          // Now support googleoff: index and googleoff: all.  See 
          // https://developers.google.com/search-appliance/documentation/46/admin_crawl/Preparing#pagepart
          p += 10;
          while (*p && p < ep && isspace(*p)) p++;
          if (!strncasecmp(p, "index", 5)) {
            indexing_suppressed = 1;
            p+=5;
          } else if (!strncasecmp(p, "all", 3)) {
            indexing_suppressed = 1;
            p+=3;
         }
        } else if (!strncasecmp(p, "*start_indexing*", 16)) {
          indexing_suppressed = 0;
          p+=16;
        } else if (!strncasecmp(p, "endnoindex", 10)) {
          indexing_suppressed = 0;
          p+=10;
        } else if (!strncasecmp(p, "googleon:", 9)) {
          // See googleoff comment above.
          p+=9;
          while (*p && p < ep && isspace(*p)) p++;
          if (!strncasecmp(p, "index", 5)) {
            indexing_suppressed = 0;
            p+=5;
          } else if (!strncasecmp(p, "all", 3)) {
            indexing_suppressed = 0;
            p+=3;
          }
        }

        if (! index_comments) {
          // skip to the end of the comment as quickly as possible
          p = mlstrstr(p, "-->", ep - p + 1, 0);
          if (p == NULL) p = ep;
          else p += 3;
          pp = p;
        }
        state = SKIPPING;
      } else {
        tagstart = p; // remember where the text of the tag started
        state = TAG;
      }
      p = pp;
      break; // --------------------------------------------------->
    case TAGEQSEEN:
      this_tag_has_attributes = 1;
      if (*p == '\''  || *p == '"') {
        first_rab = NULL;
        quotechar = *p;
        state = TAGQUOTE;
        p++;
        break;// --------------------------------------------------->
      }
      // Otherwise just drop back to TAG state    
      state = TAG;
      break;
    case TAGQUOTE:
      if (*p == quotechar) {
        quotechar = 0;
        state = TAG;
        p++;
        break; // --------------------------------------------------->
      }
      // Is it possible someone forgot to close the quotes?
      if (*p == '>') {
        if (first_rab == NULL) first_rab = p;
      } else if (first_rab != NULL && *p == '<') {
        // I can't see any reason why these tags should be part of an
        // attribute value.  If we've already seen a right angle bracket,
        // it's probable that someone has forgotten to close the quotes
        // I know NineMSN wants to include <i> etc in attributes.
        if (!strncasecmp(p, "<meta", 5) || !strncasecmp(p, "<body", 5)
            || !strncasecmp(p,"<doc>", 5) || !strncasecmp(p,"</doc>", 6)
            || !strncasecmp(p,"</html>", 7)) {
          quotechar = 0;
          p = first_rab ;  // So that TAG sees the > 
          first_rab = NULL;
          state = TAG;
          break; // --------------------------------------------------->
        }
      }
      p = pp;
      break; // --------------------------------------------------->
    case TAG:
      if (!scantags) {
        printf("Error: shouldn't ever get into TAG state\n");
        state = SKIPPING;
        p = pp;
        break; // --------------------------------------------------->
      }
      if (0) printf("TAG, char is '%c'\n", *p);
      // Check for invalid XML / SGML, i.e author has included raw '<' character
      // rather than using $lt;  If we have seen more than
      // 150 bytes and we haven't seen an attribute, drop back to tagstart.
      if (!this_tag_has_attributes && (p - tagstart > 150)) {
	p = tagstart;
	state = SKIPPING;
	break;
      }
      
      
      if (*p == '=') {
        // Assume that this must introduce the value part of an attribute
        // e.g. <a href=
        state = TAGEQSEEN;
        p++;
        break; // --------------------------------------------------->
      }

      if (*p == '>') {
        rab = p;
        *rab = 0;
	if (0) printf("Checking tag '%s'\n", tagstart);
	p = possibly_skip_element(tagstart, rab, ep, trec_docno);  // If this is an element like <script> or <BYLINE>
	                                               // which shouldn't count as part of document content,
	                                               // move to the end of the closing tag.  Otherwise
	                                               // position ourselves just after the RAB
	*rab = '>';
        state = SKIPPING;	
      } else { // End of if (*p == '>') in TAG state
        p = pp;
      }
      break;  // --------------------------------------------------->
    default:
      fprintf(stderr, "Error Exit: Finite state machine derailed! State = %d\n", state);
      exit(1);
    }
    // No more. p++;
    if (0) printf("End of scan loop, state = %d\n", state);
  }
  if (0) printf("\nEnd of extract_words()\n");
}

