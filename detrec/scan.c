/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*                                                                    */
/*                              scan.c                                */
/*	                                           	              */
/**********************************************************************/


#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>



#include "detrec.h"
#include "unicode/utf8.h"

#include "lscan.h"
#include "extractor.h"
#include "tarfile.h"
#include "warcfile.h"
#include "pcre/pcre.h"
#include "bmg2/bmg2.h"
#include "zlib/zlib.h"
#include "scan.h"

int max_doctype_scan_depth = 20480;

extern int valid_iconv_charset(u_char *str);
extern int scan_dir_fe(u_char *dname);

#include <iconv.h>

extern u_char data_root[];

static int ignore_prefix_len = 0;



/**********************************************************************/
/*                  looking at document charset and features          */
/**********************************************************************/

pcre *compiled_csetpat_http = NULL, *compiled_meta_hequiv = NULL,
  *compiled_csetpat_hequiv = NULL,
  *compiled_csetpat_xml = NULL, *compiled_filelength_pat = NULL;
pcre_extra *charsetextra_http = NULL, *charsetextra_metahequiv = NULL, 
  *charsetextra_hequiv = NULL,
  *charsetextra_xml = NULL, *filelength_extra = NULL;

static int cs_unspec = 0;

u_char chset_fromdoc[MAX_CHARSET_LEN+1]= {0},
  chset_fromhdr[MAX_CHARSET_LEN+1]= {0},
  chset_fromcrawler[MAX_CHARSET_LEN+1] = {0};

void compile_cset_patterns() {
  const char *errmsg;
  int erroffset;

  compiled_meta_hequiv =
    pcre_compile("<meta [^>]*http-equiv[^>]*>", PCRE_CASELESS,
    &errmsg, &erroffset, NULL);
  if (compiled_meta_hequiv == NULL) {
    printf("Error: compiling zeroth PCRE charset pattern '%s'\n", errmsg);
    exit(1);
  }

  charsetextra_metahequiv = pcre_study((const pcre *)compiled_meta_hequiv, 0, 
                                       &errmsg);
  if (charsetextra_metahequiv == NULL) {
    if (errmsg != NULL) {
      printf("Error: studying PCRE zeroth charset pattern '%s'\n", errmsg);
      exit(1);
    }
  }

  compiled_csetpat_http = 
    pcre_compile("charset\\s*=\\s*", PCRE_CASELESS,
    &errmsg, &erroffset, NULL);
  if (compiled_csetpat_http == NULL) {
    printf("Error: compiling first PCRE charset pattern '%s'\n", errmsg);
    exit(1);
  }

  charsetextra_http = pcre_study((const pcre *)compiled_csetpat_http, 0, 
                                       &errmsg);
  if (charsetextra_http == NULL) {
    if (errmsg != NULL) {
      printf("Error: studying PCRE first charset pattern '%s'\n", errmsg);
      exit(1);
    }
  }
 
  compiled_csetpat_hequiv = 
    pcre_compile("\\Wcharset\\s*=\\s*", PCRE_CASELESS, 
    &errmsg, &erroffset, NULL);
  if (compiled_csetpat_hequiv == NULL) {
    printf("Error: compiling second PCRE charset pattern '%s'\n", errmsg);
    exit(1);
  }
  
  compiled_csetpat_xml = 
    pcre_compile("<\\?xml[^>]+ encoding\\s*=\\s*", PCRE_CASELESS, 
    &errmsg, &erroffset, NULL);
  if (compiled_csetpat_xml == NULL) {
    printf("Error: compiling XML PCRE charset pattern '%s'\n", errmsg);
    exit(1);
  }
  
  
  charsetextra_hequiv = pcre_study((const pcre *)compiled_csetpat_hequiv, 0, &errmsg);
  if (charsetextra_hequiv == NULL) {
    if (errmsg != NULL) {
      printf("Error: studying PCRE second charset pattern '%s'\n", errmsg);
      exit(1);
    }
  }
  
  charsetextra_xml = pcre_study((const pcre *)compiled_csetpat_xml, 0, &errmsg);
  if (charsetextra_xml == NULL) {
    if (errmsg != NULL) {
      printf("Error: studying PCRE XML charset pattern '%s'\n", errmsg);
      exit(1);
    }
  }
}


static u_char *get_charset(u_char *str, int len) {
  // str is a null-terminated string such as one passed by get_doctype() 
  // immediately below.  This routine runs one (xml) or two (html) 
  // compiled pcre patterns against the string and returns 1 if and only 
  // if there is a match.
  // the pattern looks for something similar to:
  // <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  // but of course, attributes may be in reverse order:
   // <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
 

  // Ideally, this has already been detected by the crawler and is 
  // in the X-Funnelback-Charset header. We report that if we can. Otherwise,
  // we choose the charset:
  //
  // 1. from the http header
  // 2. from an xml introducer <?xml ... encoding=blah
  // 3. from an html <http-equiv> tag
  // 4. we guess.

  // In versions prior to 11.5, the priority was 2, 3, 1, guess. 
  // In order to be consistent with browsers and the HTML standard,
  // the priority is now 1,2,3, guess.

  // Note: the str we are passed does not include the http header
  // so that must be checked elsewhere. chset_fromcrawler and
  // chset_fromhdr will both already be populated (if appropriate)
  // by process_http_header_line()
 
  u_char *p = NULL, *q = NULL, endchar = '>', *cset = NULL, savep;
  int ovector[3], rslt;


  if (compiled_csetpat_hequiv == NULL) {
    // Compile the patterns only the first time this fn is called.

    compile_cset_patterns();
  }
  if (chset_fromcrawler[0]) {
    if(0) printf("Charset from crawler\n");
    return chset_fromcrawler;
  }
  if (chset_fromhdr[0]) {
    if(0) printf("Charset from header\n");
    return chset_fromhdr;
  }

  if (0) {
    printf("CHARSET: Looking in text(len %d) starting with '", len);
    putchars(str, 100);
    printf("' ...\n");
  }

  // Try the XML pattern first
  p = NULL;  // Assume the worst
  if ((rslt = pcre_exec(compiled_csetpat_xml, charsetextra_xml, str, len, 
			0, 0, ovector, 3)) > 0)  {
    p = str + ovector[1];  // ovector[1] contains offset of the first character
                               // after the end of a substring
  } else {
    q = str;
#if 0
    // First find a match for <meta [^>]*http-equiv[^>]*>
    if ((rslt = pcre_exec(compiled_meta_hequiv, charsetextra_metahequiv, 
    str, len, 0, 0, ovector, 3)) > 0)  {
      q = str + ovector[0];  
      len = ovector[1] - ovector[0];
      if (0) {
        printf("Found metatag with http-equiv\nString to check is:");
        putchars(q, len);
        printf("\n");
      }
      // If found, look for charset part within the meta tag.
#endif
      if ((rslt = pcre_exec(compiled_csetpat_hequiv, charsetextra_hequiv, 
      q, len, 0, 0, ovector, 3)) > 0)  {
        p = q + ovector[1];  // ovector[1] contains offset of the first character
                               // after the end of a substring
      if (0) printf("   Found charset part.\n");
      }
#if 0
    }
#endif
  } 

  if (p == NULL) {
    cs_unspec++;		  
    if (0) printf("Charset not found in doc\n");
    return NULL;
  }


  cset = p;
  if (*p == '\'' || *p == '\"') {
    endchar = *p++;
    cset = p;
  }
  while (*p && *p != endchar && (ucs_indexable(*p) || *p == '-' || *p == '_')) p++;
  savep = *p;

  if (p-cset > MAX_CHARSET_LEN) {
    if (0) printf("Charset too long\n");
    return NULL;
  }

  *p = 0;
  strcpy(chset_fromdoc, cset);
  *p = savep;
  if (0) printf("Charset in doc said: '%s'\n", chset_fromdoc);
  return chset_fromdoc;
}


static int is_iso8859(u_char *str) {
  // Return 1 if str is a variant of the charset name ISO-8859-1
  // Otherwise return 0
  u_char *p;
  if (strncasecmp(str, "iso", 3)) return 0;
  p = str + 3;
  if (*p == '-' || *p == '_') p++;
  if (strncmp(p, "8859", 4)) return 0;
  p += 4;
  if (*p == '-' || *p == '_') p++;
  if (strcmp(p, "1")) return 0;
  return 1;
}

/**********************************************************************/
/*                            get_doctype()                           */
/**********************************************************************/

static pcre *compiled_xml_or_html_pattern = NULL;
static pcre_extra *xml_or_html_extra = NULL;
static pcre *compiled_xml_only_pattern = NULL;
static pcre_extra *xml_only_extra = NULL;
static pcre *compiled_xhtml_pattern = NULL;
static pcre_extra *xhtml_extra = NULL;

static void free_doctype_patterns() {
  if (compiled_xml_or_html_pattern != NULL) pcre_free(compiled_xml_or_html_pattern);
  compiled_xml_or_html_pattern = NULL;
  if (xml_or_html_extra != NULL) pcre_free(xml_or_html_extra);
  xml_or_html_extra = NULL;
  if (compiled_xml_only_pattern != NULL) pcre_free(compiled_xml_only_pattern);
  compiled_xml_only_pattern = NULL;
  if (xml_only_extra != NULL) pcre_free(xml_only_extra);
  xml_only_extra = NULL;

  if (compiled_xhtml_pattern != NULL) pcre_free(compiled_xhtml_pattern);
  compiled_xhtml_pattern = NULL;
  if (xhtml_extra != NULL) pcre_free(xhtml_extra);
  xhtml_extra = NULL;
  if (compiled_csetpat_http != NULL) pcre_free(compiled_csetpat_http);
  compiled_csetpat_http = NULL;
  if (compiled_csetpat_hequiv != NULL) pcre_free(compiled_csetpat_hequiv);
  compiled_csetpat_hequiv = NULL;
  if (compiled_csetpat_xml != NULL) pcre_free(compiled_csetpat_xml);
  compiled_csetpat_xml = NULL;
  if (charsetextra_http != NULL) pcre_free(charsetextra_http);
  charsetextra_http = NULL;
  if (charsetextra_hequiv != NULL) pcre_free(charsetextra_hequiv);
  charsetextra_hequiv = NULL;
  if (charsetextra_xml != NULL) pcre_free(charsetextra_xml);
  charsetextra_xml = NULL;
}

static void print_doctype(doctype_t dt) {

  switch (dt) {
    case DT_VAGUE: printf("VAGUE ");
      break;
    case DT_EMAIL: printf("EMAIL ");
      break;
    case DT_HTML: printf("HTML ");
      break;
    case DT_SGML: printf("SGML ");
      break;
    case DT_XML: printf("XML ");
      break;
    case DT_TEXT: printf("TEXT ");
      break;
    case DT_BINARY: printf("BINARY ");
      break;
    case DT_PDF: printf("PDF ");
      break;
    case DT_TREC: printf("TREC ");
      break;
    default: printf("UNRECOGNIZED ");
      break;
  }
}


void compile_doctype_patterns() {
  const char *errmsg;
  int erroffset;
  if (compiled_xml_or_html_pattern == NULL) {
    compiled_xml_or_html_pattern = 
    pcre_compile("<\\?xml|<html|<head>|<body>|<title>|<script|<!doctype", 
    PCRE_CASELESS, &errmsg, &erroffset, NULL);
    xml_or_html_extra = pcre_study((const pcre *)compiled_xml_or_html_pattern, 0, &errmsg);
	  
    compiled_xml_only_pattern = pcre_compile("^<\\?xml|<!doctype\\s+xml", 
					     PCRE_CASELESS, &errmsg, &erroffset, NULL);
    xml_only_extra = pcre_study((const pcre *)compiled_xml_only_pattern, 0, &errmsg);

    compiled_xhtml_pattern = pcre_compile("<!doctype\\s+html|<html", 
					  PCRE_CASELESS, &errmsg, &erroffset, NULL);
    xhtml_extra = pcre_study((const pcre *)compiled_xhtml_pattern, 0, &errmsg);

  }
}


doctype_t get_doctype(u_char *str, int len, u_char **cset, int silent) {
  /* Look at the beginning of str to determine what sort of document
     we have.   Return the detected character set or NULL in cset.

  */

  doctype_t rslt = DT_VAGUE;
  u_char *c = str, save, *t, *csetp = NULL;
  int limit = max_doctype_scan_depth, binary_scan_limit = 100; 
  int npc = 0;  /* count of non-printable, non-CR/LF/FF characters in
		   the sample we look at. */
  int ovector[3];

  if (0) printf("get_doctype(%s, %d)\n", str, len);

 	if (compiled_xml_or_html_pattern == NULL) {
	  // Compile the patterns only the first time.
    compile_doctype_patterns();
	}

  

  if (binary_scan_limit > len) binary_scan_limit = len;
  if (limit > len) limit = len;
  save = str[limit];
  str[limit] = 0;   /* TEMPORARY termination, restore before return */

  // ----- Check for document charset first ----------- //

  csetp = get_charset(str, limit);
  if (csetp == NULL) csetp = assumed_charset;
 
  if (is_iso8859(csetp)) {
    // Treat ISO-8859-1 and its variant names as Windows-1252
    // as per http://en.wikipedia.org/wiki/Windows-1252
    if (!silent) printf("ISO->%s ", assumed_charset);
    csetp = assumed_charset;
  } else if (csetp != NULL && !silent) printf("%s ", csetp);


  *cset = csetp;

  // ----- Check if this is an unfiltered PDF file --------- //
  // I don't know whether lower case is actually allowed but just in case use casecmp
  if (!strncasecmp(str, "%PDF-", 5)) {
    rslt = DT_PDF;
    str[limit] = save;
    if (!silent || debug) print_doctype(rslt);
    return rslt;
  }

  // ----- Check if it's a TREC Ad Hoc document -------- //
  c = str;
  while (isspace(*c)) c++;
  if (!strncmp(c, "<DOC>", 5)) {
    c += 5;
    while (isspace(*c)) c++;
    if (!strncmp(c, "<DOCNO>", 7)) {
      rslt = DT_TREC;
     str[limit] = save;
     if (!silent || debug) print_doctype(rslt);
      return rslt;
    }
  }


  /* ----- check for binary --------- */
  /* One day, we'll do this more intelligently, but for now
     Are there control chars in the first limit bytes which is
     not HT, LF, VT, FF or CR?   [Also now allow NUL]

     In the present case, documents detected as binary are completely 
     rejected/ignored  */

  for (c = str; c < (str + binary_scan_limit); c++) {
    if (((*c > 0) && (*c < 011)) || ((*c > 015) && (*c < 040))) {
      npc++;
      if (npc > 4) break;
      if (debug) {
        printf ("Binary trigger character %o octal\n  Context: ", *c); 
        putchars(c - 5, 20);
        printf("\n");
      }
    }
  } 

  if (npc > 4) {  /* No longer zero tolerance - Fran says this 
		       works better on test cases than npc >0 */
    rslt = DT_BINARY;
    str[limit] = save;
    if (!silent || debug) print_doctype(rslt);
    return rslt;
  }

  if (0) 
    printf("getdoctype: \n------------------%s\n\n------------------\n", str);
  c = str;
  while (*c && isspace(*c)) c++;  /* Skip white space */
  if (*c == 0) {
    // The bundle starts with too much whitespace
    // Warn and assume HTML
    if (!silent) printf("[ TOO-MUCH-LEADING-WHITESPACE-ASSUME ] ");
    rslt =  DT_HTML;
    str[limit] = save;
    if (!silent || debug) print_doctype(rslt);
    if (csetp == NULL) {
      if (!silent) printf(" Assumed_%s ", 
                              assumed_charset);
      *cset = assumed_charset;
    }
    return rslt;
  }

  
	// The idea is to run an XML-or-HTML pattern to find the first indication
	// of document type and then to run a left-anchored XML-only pattern to
	// see whether what matched was an XML indication.  Previous ways of
	// doing it were prone to looking too far into the document and finding
	// spurious occurrences of "xml" eg in <html xmlns=...

	// Default is HTML unless proven otherwise.


  t = c;

  if (pcre_exec(compiled_xml_or_html_pattern, xml_or_html_extra, t, limit - (t - str), 
		0, 0, ovector, 3) > 0) {
    // An XML or HTML indicator string has been found after the DOCHDR
    c = t + ovector[0];
    // Note that XHTML should be scanned as HTML because of script tags and
    // observed frequent lack of /> on e.g. meta tags.

    // Extracts from standards:
    // 1: http://www.w3.org/TR/2006/REC-xml-20060816/#dt-stag: 
    //    - No white space allowed between < and element name or </ and element name.
    // 2: http://www.w3.org/TR/xhtml1/#normative:
    //    - XML declaration "<?xml version ..." is optional  for XHTML
    //    - DOCYPE declaration "<!DOCTYPE html ..." is mandatory  for XHTML
    //    - Root element "<html... is mandatory  for XHTML


    if (pcre_exec(compiled_xml_only_pattern, xml_only_extra, c, limit - (c - str), 
                  0, 0, ovector, 3) > 0) {
      // Starts with an XML declaration.  Is it XHTML?
      // - look for DOCTYPE html or an html element
      if (pcre_exec(compiled_xhtml_pattern, xhtml_extra, c, limit - (c - str), 
                    0, 0, ovector, 3) > 0) {
	      if (!silent) printf("X");
        rslt =  DT_HTML;
      } else {   
        rslt = DT_XML;
        if (csetp == NULL) {
          if (!silent) printf(" Assumed_%s ", 
                                               assumed_utf8);
          *cset = assumed_utf8;
        }
      }
    } else {
      // No XML declaration, assume HTML
      rslt =  DT_HTML;
    } 
  }else if (!strncmp(c, "From ", 5) && strstr(c, "To: ") != NULL
	     && strstr(c, "Subject: ") != NULL) {
    /* This test sometimes gives false positives */
    rslt =  DT_EMAIL;
  } else {
    /* Not XML/HTML */
    rslt = DT_TEXT;
  }
 
  str[limit] = save;

  if (!silent || debug) print_doctype(rslt);
  return rslt;

}

/**********************************************************************/
/*            Process one bundle and the docs within it               */
/**********************************************************************/

u_char *conversion_buf = NULL;
int cbsize;

// To avoid a hard limit on the maximum number of docs in a bundle
// we go for an initial allocation of 1 and a mechanism for 
// dynamically increasing it as many times as necessary.  
// If the allocated size is fully used,
// a new buffer ten times as large is allocated and the content
// copied across.
static int MAX_DIB = 1;    // Max docs in a bundle 100, 1000, 10000 etc
u_char **doc_starts = NULL;  
static int dib_cnt = 0;

static void record_docstart(u_char *matchptr) {
  // Mostly called from bmg_search to insert a pointer in the docstart table
  if (doc_starts == NULL) {
    doc_starts = femalloc(MAX_DIB + 1, sizeof(u_char *), "doc_starts0");
  } 
  if (dib_cnt == MAX_DIB) {
    // This means that the doc_starts array has filled.  Time to 
    // grow it.
    u_char **tmp = doc_starts;
    doc_starts = femalloc(10 * MAX_DIB + 1, sizeof(u_char *), "doc_starts10");
    memcpy(doc_starts, tmp, dib_cnt * sizeof(u_char *));
    free(tmp);
    MAX_DIB *= 10;
    if (0) printf("doc_start array successfully increased to %d\n",
                         MAX_DIB);
  }
  if (0) {
    printf("record_docstart: %d '", dib_cnt);
    putchars(matchptr, 10);
    printf("'...\n");
  }
  doc_starts[dib_cnt++] = matchptr;
}


static u_char *process_DOCNO(u_char *str) {
  /* str is a pointer to the character immediately following <DOCNO>
    This function skips over everything up to the closing </DOCNO>
    and save the stuff in between for later storage.  

    Only useful in TREC mode 

    Return ptr to > in "</DOCNO>"
  */
  u_char *p = str;

  if (0) {
    printf("\n\nprocess_DOCNO: '");
    putchars(str, 50);
    printf("'\n");
  }
  while (*p) {
    if (*p == '<') {
      // DOCNO Can't contain <
      if (strncmp(p+1, "/DOCNO>", 7)) {
        return str;
      } else {
       *p = 0;  /* Zap out the angle bracket */
       strncpy(trec_docno, str, MAX_DOCNAME_LEN);
       trec_docno[MAX_DOCNAME_LEN] = 0;
       if (0) printf("DOCNO: %s\n", str);
       *p = '<';
       return p + 8;
      } 
    }
    p++;
  }
  return str;
}

 
static u_char http_code[4];

void process_http_header_line(u_char *line, int line_len) {
  // Each line in an HTTP header line apart from the first consists of:
  // <attribute>: <value>, so the first task is to 
  // split it up and see if we are interested in this attribute
  // In detrec, we are only really interested in determining character-set.
  
  u_char *p, *q, *cset, saveq, *attrib, *value;
  int len, conlen;
  int ovector[30];

  

  if (0) printf("\nprocess_http_header_line(%d,%s) \n", 
                       line_len, line);
  if (!strncasecmp(line, "HTTP/", 5)) {
    // Expecting something like "HTTP/1.1 200 OK"
    p = line + 5;
    while (*p > ' ') p++; // skip over the http version
    while (isspace(*p)) p++; // skip white space
    if (!isdigit(*p)) {
      printf("Warning:  http header in WARC file gives invalid response code. (%s)\n",
      line);
    } else strncpy(http_code, p, 3);
    return;   // -------------------------------------------------------->
  }

  p = line;
  attrib = line;
  while (*p && *p != ':') p++;
  if (*p != ':' || *(p+1) != ' ') return; // ----------------------------->
  *p = 0;
  value = p + 1;

  
  // Now have an attribute and a value.  Some attributes are 
  // are treated specially, others are looked up in metamap.cfg
  // to see if value should be treated as metadata.

  if (!strcmp(attrib, "Date")) {
    // This date is useless to us
    return;  // ----------------------------->
  } else if (!strcasecmp(attrib, "Content-Type")) {
    len = line_len - (value - attrib);
    p = value;
    chset_fromhdr[0] = 0; // Fear the worst
    if (compiled_csetpat_http == NULL) compile_cset_patterns();
    if (pcre_exec(compiled_csetpat_http, charsetextra_http, 
    p, len, 0, 0, ovector, 3) > 0)  {
      cset = p + ovector[1];  // ovector[1] contains offset of the first character
      // after the end of a substring
      q = cset;
      while (*q && (ucs_indexable(*q) || *q == '-' || *q == '_')) q++;
      saveq = *q;

      if (q-cset <= MAX_CHARSET_LEN) {
        *q = 0;
        strcpy(chset_fromhdr, cset);
        if (0) 
          printf("Character set '%s' extracted from WARC http header\n", 
            chset_fromhdr);
        *q = saveq;
      }
    }
  } else if (!strcasecmp(attrib,"X-Funnelback-Charset")) {
    while(isspace(*value)) value++;
    if(strlen(value) <= MAX_CHARSET_LEN) {
      strcpy(chset_fromcrawler,value);
      if (0)  {
        printf("Character set '%s' extracted from WARC X-Funnelback header\n", 
        chset_fromcrawler);
      }
    }
  } else if (!strcasecmp(attrib, "Content-Length") 
    || !strcasecmp(attrib, "X-Funnelback-Content-Length")) {
    // We believe content-length ahead of stored-length so unconditionally
    // accept it. (Fran's advice)
    conlen = strtol(value, NULL, 10);
    if (0) printf("--- Size: %d\n", conlen);
  } else if (!strcasecmp(attrib, "Stored-length")
    || !strcasecmp(attrib, "X-Funnelback-Stored-Length")) {
      conlen = strtol(value, NULL, 10);
      if (0) printf("--- Stored size: %d\n", conlen);
  } else {
    // Any other attribute might signal a metadata field name
  }
}


static u_char *process_http_headers(u_char *start_of_header, int *length, int regardless) {
  // Given a string which is understood to start with an HTTP response
  // header, parse it line by line, extracting needed info using calls
  // to  process_http_header_line().  

  // If regardless is set, always scan until blank line or </DOCHDR>
  // Otherwise, if string doesn't start with an HTTP header return start_of_header
  // and unchanged length,

  // If a real or imaginary header is processed, return a pointer to the first character
  // after the HTTP header.
  
  /*  E.g. from ClueWeb09:
      HTTP/1.1 200 OK
      Content-Type: text/html
      Date: Tue, 13 Jan 2009 18:05:10 GMT
      Pragma: no-cache
      Cache-Control: no-cache, must-revalidate
      X-Powered-By: PHP/4.4.8
      Server: WebServerX
      Connection: close
      Last-Modified: Tue, 13 Jan 2009 18:05:10 GMT
      Expires: Mon, 20 Dec 1998 01:00:00 GMT
      Content-Length: 16254
  */

  u_char *linestart, *linep = NULL, saveit;
  int linelen, loclen = *length;
  if (!regardless && strncasecmp(start_of_header, "HTTP", 4)) return(start_of_header);

  http_code[0] = 0;

  // we haven't seen a charset yet:
  chset_fromcrawler[0] = '\0';
  chset_fromhdr[0] = '\0';

  linestart = start_of_header;
  // Loop until we find a blank line or a </DOCHDR>
  while (loclen > 0) {
    linep = linestart;
    while (*linep && *linep != '\012' && loclen > 0) { linep++;  loclen--;}
    if (loclen <= 0) {
      *length = 0;
      return linep;  // --------------------------> RETURN
    }
    linelen = linep - linestart;
    if (linelen && *(linep -1) == '\015') linelen--;  // Ignore CR in CRLF
    saveit = linestart[linelen];
    linestart[linelen] = 0;  //Null out either the CR (in CRLF) or the LF
    if (linelen == 0 || (linelen >= 9 && !strncmp(linestart, "</DOCHDR>", 9))) {
      *length = loclen;
      linestart[linelen] = saveit;
      return linep;  // --------------------------> RETURN
    }
    process_http_header_line(linestart, linelen);  // -------> ACTION
    linestart[linelen] = saveit;
    linep++;  // One beyond the final linefeed.
    linestart = linep;
  }
  *length = 0;
  return linep;  // --------------------------> RETURN
}


static u_char *process_DOCHDR(u_char *start_of_doc, int length){
  // If the document starting at start_of_doc and of length chars
  // has a Funnelback, or TREC header, process it and 
  // return a pointer to the document proper.

  // If there is no header, return start_of_doc


  // Start_of_doc starts with different things depending
  // upon the origin of the file:
  // A. Funnelback mirror store: <DOCHDR>\n<BASE HREF ...
  // B. Funnelback bundle: <PanDOC>\n<DOCHDR>\n<BASE HREF ...
  // C. TREC Web: <DOC>\n<DOCNO>...</DOCNO>\n<DOCHDR>\nhttp://blah
  // D. TREC ad hoc: As for C but without the DOCHDR.
  // E. Content of WARC file:  several variants.

  // In cases A-C, we expect to find a standard HTTP header after
  // the line containing the URL.  When we find that header, 
  // we call process_http_headers() to deal with it.  Our job
  // is just to process the URL if there is one and to deal
  // appropriately with the four cases.

  // In this version (detrec), the URL is determined but it's component
  // words don't form part of the output.

  u_char *p;
  int ulen, looklen = length;

  if (0) {
    printf("process_DOCHDR(");
    putchars(start_of_doc, 50);
    printf(" ...\")\n");
  }

  p = start_of_doc;   
  // A TREC doc will have a <DOCNO> tag right at the beginning
  // before the <DOCHDR> (if there is one)


  p =  mlstrcasestr(p, "<DOCNO>", 50, 0);
  if (p != NULL) {
    // It's either case C or case D  (two TREC variants)
    p = process_DOCNO(p + 7);
  } else {
    p = start_of_doc;
    strcpy(trec_docno, "???");
  }

  looklen = length - (p - start_of_doc);
  // Both Funnelback and TREC Web formats should have a 
  // <DOCHDR> tag soon after the <DOC> or <PanDOC> tag
  if (looklen > 100) looklen = 100;
    
  p =  mlstrcasestr(p, "<DOCHDR>", looklen, 0);
  if (p == NULL) {
    // Case D: It doesn't have headers
    return start_of_doc;  // ------------------------------->
  }

  p+= 9;  // strlen("<DOCHDR>\n")
  while (isspace(*p)) p++;
  

  // Skip to end-of-line
  while (*p > '\015') p++;  // Look for first CR or LF
  while (*p && *p <= '\015') p++;  // Find end of sequence of CR and/or LF
 
  ulen = length - (p - start_of_doc);
  // We must now be positioned on the HTTP/  of the HTTP header proper
  //if (!strncmp(p, "HTTP/", 5)) {
  p = process_http_headers(p, &ulen, 1); 

  if (0) {
    printf("Process_DOCHDR: Returning '");
    putchars(p, 50);
    printf("'\n");
  }
  

  return p;  // -------------------------------------->
}



void process_doc(int n, u_char *url, u_char *fbmeta,
                 size_t sitemap_downloadlen, date_only_t sitemap_mod_date, 
                 int http_header_expected, int dnwib, 
                 off_t offset, FILE *OUTFILE) {
  // All the processing of the nth document within a bundle (or a
  // whole bundle in the non-bundled case), including doc type
  // determination, character set conversion if required, and
  // lexical scanning.


  // http_header_expected signals whether we should scan an HTTP
  // header before processing the content of the document.

  // dnwib is the document number within a bundle for printing
  // unless the value passed in is negative, which will be
  // the case for regular bundles.

  // offset is the offset of the start of this document within the 
  // bundle, WARCfile or CSVfile, etc.  Does it make any sense for 
  // tarfiles?

  int i, length, doclen , extract_words_flag = 1;
  u_char *cset = NULL, *start_of_doc, 
    *start_scanning_from = conversion_buf;
  doctype_t doctype = DT_VAGUE;
  iconv_t iconversion;
  size_t rslt;

  // offset = doc_starts[n] - chamberpot;  Now passed in
  length = doc_starts[n + 1]  - doc_starts[n];
  start_of_doc = doc_starts[n];
  doclen = 0;

  // Note that a large buffer always referenced by chamberpot is
  // malloced in main.c.  The uncompressed original data is
  // loaded into the start of this buffer.  It's length is uncolen.
  // If a character set conversion is performed the conversion is
  // written in the tail of the buffer starting at conversion_buf

  // Input text lies consists of length characters within the 
  // decompression chamber starting at start_of_doc

  if (0) {
    printf("\nprocess_doc[%d]: start of %d byte doc with header at offset %ld is: '", 
    n, length, (long) offset);
    putnchars(start_of_doc, 72);
    printf("'\n");
  }

  

  // Process HTTP and other document headers here
  // start_of_doc is unaltered if there is no header.
  if (warc_flag) {
    int len_actual_doc = length;
    if (0) printf("Came in through the WARCroom window\n");
    if (http_header_expected == HDR_YES)
      start_of_doc = process_http_headers(doc_starts[n], &len_actual_doc, 1); 
    else if (http_header_expected == HDR_POSSIBLE) 
      start_of_doc = process_http_headers(doc_starts[n], &len_actual_doc, 0); 
  } else {
    if (0) printf("About to process doc-header\n");
    start_of_doc = process_DOCHDR(doc_starts[n], length);
  }

  if (trec_docno[0]) fprintf(OUTFILE, "<DOC>\n<DOCNO>%s</DOCNO>\n<TEXT>\n", trec_docno);
  else fprintf(OUTFILE, "<DOC>\n<DOCNO>Blah</DOCNO>\n<TEXT>\n");


  length = doc_starts[n + 1] - start_of_doc;  //Orig length included header

  if (0) printf("process_doc() - length = %d\n", 
                       (int) length);

  if (length <= 0) {
    printf(" EMPTY [not scanned] - %s\n", trec_docno);
    length = 0;
    extract_words_flag = 0;
  } else {
    if (0) printf("calling get_doctype(%s, %d)\n", start_of_doc, length);
    doctype = get_doctype(start_of_doc, length, &cset, 1);   // One means silent
    if (doctype == DT_BINARY) {
      if (!quiet_flag) printf("Warning:  %s may be BINARY [scanning anyway]\n", trec_docno);
    } else if (doctype == DT_PDF) {
      if (!quiet_flag) printf("PDF [not scanned] - %s\n", trec_docno);
      extract_words_flag = 0;
    } else {
      if (0) {
        printf("\nstart of %d byte (doctype = %d) actual doc is: '", length, 
          doctype);
        putchars(start_of_doc, 72);
        printf("\n");
      }
    }
  }


  
  if (extract_words_flag) {
    if (0) 
      printf("process_doc::: Doc %d starts at offset %ld, length = %d, doctype = %d, cset = %s\n", 
             n, (long)offset, length, doctype, cset);
    
    // -----------  Character set conversion to UTF-8 performed here
    // ------ Will need to be able to force conversions.


    // Some charactersets (such as shift-JIS) do not leave the ASCII
    // characters alone.  For example the 7E code (tilde in ASCII) represents
    // the Yen sign in shift-JIS and translates to a multi-byte character
    // in UTF-8.  This can muck up BASE HREFS, so we should not 
    // character translate the DOCHDR part.

    // iconv doesn't know about x-sjis, I think it's the same as shift-JIS
  
    if (cset == NULL) {
      printf("Error: process_doc(): cset points to empty buffer.\n");
      exit(1);
    }
    if (!strcasecmp(cset, "x-sjis")) strcpy(cset, "SHIFT-JIS");

    if (strcasecmp(cset, "UTF-8") != 0) { 
      if (0) printf("iconv_open: %s\n", cset);
      iconversion = iconv_open("UTF-8", cset);  // to, from
      if (iconversion != (iconv_t)(-1)) {
        // A character set conversion is going to occur -----------
        u_char *inp = start_of_doc, *outp = conversion_buf;
        size_t ibleft = length, obleft = cbsize;
     
        if (0) {
          printf(" -- Converting from %s to UTF-8: ibleft = %ld, obleft = %ld '", 
          cset, (long) ibleft, (long) obleft);
          putchars(inp, 15);
          printf("' ...\n");
        }
        rslt = iconv(iconversion, (char **)&inp, &ibleft, (char **)&outp, &obleft);
        if (0) printf("Conversion done: ibleft = %ld, obleft = %ld, rslt = %ld\n", 
                      (long) ibleft, (long) obleft, (long) rslt);
        iconv_close(iconversion);  // Free resources
        if (0) {
          printf("Immediately after conversion: '");
          putchars(conversion_buf, 15);
          printf("' ...\n");
        }
     
        if (rslt == (size_t) -1) {
           if (debug) {
            if (errno == EILSEQ || errno == EINVAL) {
              if (!quiet_flag)  
                printf(" [Warning: iconv invalid or incomplete multi-byte sequence -- ");
            } else if (errno == E2BIG) {
              if (!quiet_flag)  
              printf(" [Warning: iconv output buffer filled -- ");
            } else {
              if (!quiet_flag)  
                printf(" [Warning: iconv error %d -- ", errno);
            }
            if (!quiet_flag) { 
              printf("After %ld bytes.  Next 10 input bytes = ", 
              (long) (inp - start_of_doc));
              for (i = 0; i < 10; i++) {
                printf("%2X, ", inp[i]); 
              }
        
              printf("%2X.  Doc truncated.", inp[10]);
              printf("   Unconverted doc started with: '");
              for (i = 0; i < 100; i++) {
                if (i >= length) break;
                if (start_of_doc[i] == '\n') break;
                putchar(start_of_doc[i]);
              }
              printf("' at bundle off %ld] ", (long) (start_of_doc - chamberpot));
            }
          } else {
            printf("Warning: Charset conversion error: docno=%s, cset=%s].  Content may be truncated.\n", trec_docno, cset);
          }
        }
     
        // Get here even if outbuf is truncated.
        doclen = cbsize - obleft;  // In bytes
      } else {
        if (!quiet_flag) printf("Warning: Charset '%s' unrecognized in doc %s.  Assuming UTF-8.\n", cset, trec_docno);
        start_scanning_from = start_of_doc;
        //iconv_close(iconversion);  
        doclen = length;
      }
    } else {
      // It's already in UTF-8
      start_scanning_from = start_of_doc;
      doclen = length;
    }

    if (0) {
      printf("Scanning will start with: '");
      putchars(start_scanning_from, 15);
      printf("' ...\n");
    }
  }

  // Must get length out of the HTTP header if possible because it
  // gives the length of the file before document extraction.


  if (extract_words_flag) { 
    // At last we're sure we have a document worth indexing

    //  ------------  Here's where we actually scan the charset converted
    //  ------------  document content.
  
    /* Default scanner handles html, text, etc.  */
      extract_words(start_scanning_from, start_scanning_from + doclen -1, 
                    doctype, trec_docno, OUTFILE);
  }
  fprintf(OUTFILE, "\n</TEXT>\n</DOC>\n");
  g_docs_written++;
  if (g_docs_written % 100000 == 0) printf("   Docs so far: %10lld\n", g_docs_written);
}


void process_tar_doc(u_char *start, u_char *end, int dnwib, off_t taroff, FILE *OUTFILE) {
  // This is just a front end to process_doc() which allows a caller
  // to avoid manipulating the doc_starts[] array
  // dnwib is the document number within the tar archive
  int datasize = end - start;
  conversion_buf = chamberpot + datasize;
  cbsize = CHAMBER_SIZE - datasize;
  if (0) {
    printf("About to process_doc %p %p, len = %d\n", 
    doc_starts[0], doc_starts[1],
    datasize);
    putchars(start, 50);
    printf("\n");
  }

  trec_docno[0] = 0;
  process_doc(0, NULL, NULL, 0, 0, HDR_POSSIBLE, dnwib, 0, OUTFILE); 
}



#define TMAGIC "ustar"

static int num_btypes = 7;

static int get_bundle_type(u_char *fp) {
  // To allow the 
  // possibility of indexing directories containing mixtures of bundle
  // types, it makes sense to try to automatically determine 
  // the filetype of a bundle before scanning it.
  // This function does that and returns 
  //  -1 - Error trying to open or read
  //   0 - No particular identification found
  //   1 - TAR header found
  //   2 - WARC header found
  //   3 - <PanDOC> found
  //   4 - <DOC> found
  //   5 - IFFF format

  // Read the first 512 bytes
  gzFile BUNDLE;
  u_char bunbuf[513], *p;
  int bun_used, rslt = 0;

  if (0) printf("get_bundle_type(%s)\n", fp);
  
  BUNDLE = gzopen(fp, "rb");
  if (BUNDLE == NULL) return -1;

  bun_used = gzread(BUNDLE, bunbuf, 512);
  gzclose(BUNDLE);
  if (bun_used <= 8) {
    // End of file or error.
    return -1;
  } 
  if (bun_used > 257 + strlen(TMAGIC) &&  !strncmp(bunbuf + 257, TMAGIC, 5)) {
    // TMAGIC is just 'ustar' so it is possible, indeed it happened
    // that a plain file is detected as a tar file, e.g.
    // because it contained 'mustard' at just the right point.  Let's 
    // tighten things up so that only a filepath ending in .tar, .tar.gz
    // or .tgz can be detected as a tarfile
    p = fp; 
    while (*p) p++;
    if (!strcmp(p - 4, ".tar") || !strcmp(p - 4, ".tgz")  
        || !strcmp(p - 7, ".tar.gz")) return 1;  // TAR file
  } 

  // Otherwise skip white space and check the beginning of the file
  p = bunbuf;
  while (isspace(*p)) {
    p++;
    if (p - bunbuf >= 512) {
      printf("Warning: more than 512 bytes of whitespace at head of file. Will be processed as single doc.\n");
      return 0;
    }
  }
  

  if (!strncasecmp(p, "WARC/", 5)) {
    rslt = 2;  // WARC file
  } else if (!strncmp(p, "<PanDOC>", 8)) {
     rslt = 3;  // Funnelback bundle file
  } else if (!strncmp(p, "<DOC>", 5)) {
     rslt = 4;  // TREC ad hoc bundle file
  } else if (!strncmp(p, "--IFFF--00", 10)) {
     rslt = 5;  // Indexer-Friendly File Format
  }

  if (rslt == 0) printf("Warning: No known doc marker or filetype found. Will be processed as single doc.\n");
  if (0) printf("get_bundle_type(%s) returned %d\n", fp, rslt);
  
  return (rslt); 
}


void process_bundle(u_char *fp, FILE *OUTFILE) {
  /*
  A file believed to be a doc bundle has been encountered.  It's 
  full path is fp.   It is to be decompressed and processed according to the
  bundle_type determined by get_bundle_type().
  */
  int d, btype = 0;
  off_t uncolen, offset;   

  warc_flag = 0;
  btype = get_bundle_type(fp);
  if (0) printf("process_bundle(%s): type is %d\n", fp, btype);
  if (btype < 0 || btype >= num_btypes) return;

  if (btype == 1) {
    uncolen = process_tarfile(fp, OUTFILE);
  } else if (btype == 2) {
    //  ==================== Dealing with very long (50GB) gzipped WARC
    //  ==================== archives.
    if (doc_starts == NULL) {
      doc_starts = femalloc(MAX_DIB + 1, sizeof(u_char *), "doc_starts0");
    } 
    uncolen = process_warcfile(fp, OUTFILE);
    return;
  } else if (btype == 5) {
    printf("Indexer Friendly File Format (IFFF) not supported.\n");
  } else {    // btypes 0, 3, or 4
    // ==================== Conventional way of dealing with input bundles
    uncolen = load_bundle_via_zlib(fp);
    if (0) printf("process_bundle(%s). uncolen = %d\n",
                         fp, (int) uncolen);

    if (uncolen > chamber_hwm) chamber_hwm = uncolen;

    cbsize = CHAMBER_SIZE - uncolen;
    conversion_buf = chamberpot + uncolen;  // The character-set-converted data goes above the uncompressed.

    // ------------- Here's where we treat document by document --------
    // bmg_search() will call record_docstart() once for each document in
    // the bundle.

    dib_cnt = 0;

    if (btype == 3 || btype == 4) {
      if (0) printf("Calling bmg_search with length %lld\n", (long long) uncolen);
      bmg_search(chamberpot, uncolen, record_docstart);
    } else {
      // btype 0: Assume the whole file is a single document
      if (1) printf("Assuming whole bundle is a single doc\n");
      record_docstart(chamberpot);
    }
    if (0) printf("dib_cnt = %d, uncolen = %lld\n", dib_cnt, (long long) uncolen);
    
    if (dib_cnt > MAX_DIB) dib_cnt = MAX_DIB;  // Some docs won't be processed.
    else 
      doc_starts[dib_cnt] = chamberpot + uncolen;  // One after the end of the text of the 
    // last document.
    
    if (0) {
      int i;
      printf("Unco decompression chamber starts with:\n");
      for (i = 0; i < 200; i++) 
        if (i >= uncolen) break; else putchar(chamberpot[i]);
      printf("\n\n");
    }
    
    // Now process all the documents in this bundle
    for (d = 0; d < dib_cnt; d++) {
      offset = doc_starts[d] - chamberpot;
      trec_docno[0] = 0;
      process_doc(d, NULL, NULL, 0, 0, HDR_YES, d, offset, OUTFILE);
    }
  }
  if (0) printf("End of bundle %s\n", fp);
}


/**********************************************************************/
/*                Recursive directory scanning                        */
/**********************************************************************/


static int scan_dir (u_char *dname, FILE *OUTFILE) {
  DIR *dirp;
  struct dirent *dp;
  struct stat buf;
  int rslt, dplen, fnlen;
  u_char fullpath[MAX_PATH], dirpath[MAX_PATH];

  strcpy(dirpath,dname);
  dplen = strlen(dirpath);

  // Don't scan directories created by subversion 
  if (dplen == 4 && !strcmp(dname, ".svn"))
    return 0;
  if (dplen > 4 && !strcmp(dname + dplen - 5, "/.svn"))
    return 0;

  dplen++;
                                        
  if (dplen > (MAX_PATH - 20)) {
    printf("\nWarning:  overlength directory pathname '%s'.  ",dname);
    printf("Recursive scanning will go no deeper.\n");
    return 1;
  }
  strcat(dirpath,"/");
   
  dirp = opendir(dname);
  if (dirp == NULL) {
    printf("\nWarning:  can't open directory '%s'.\n",dname);
    perror("scan_dir");
    return 1;
  }

  for (dp = readdir(dirp); dp != NULL; dp = readdir(dirp)) {
    if (strcmp(dp->d_name,".")  && strcmp(dp->d_name,"..")) {
      strcpy(fullpath,dirpath);
      fnlen = strlen(dp->d_name);
      if (dplen + fnlen >=  MAX_PATH) {
        printf("\nWarning:  overlong pathname for file '%s'. Ignored\n",
	       dp->d_name);
	continue;
      }

      strcat(fullpath, dp->d_name);

      if (0) printf("FULLPATH '%s'\n", fullpath);

      if ((rslt = stat(fullpath,&buf))) {
        printf("\nWarning:  Can't stat '%s'.u File ignored\n",
	       dp->d_name);
 	perror("stat failed:");
        continue;
      }

      if (S_ISDIR(buf.st_mode)) {
	scan_dir(fullpath, OUTFILE);
      } else if (S_ISREG(buf.st_mode)) {
	/* We used to check here to avoid indexing
	   .info, README, readme or readchg from the TREC ADHOC CDs
	   but this seems a bit silly.  If they shouldn't be indexed,
	   remove them from the collection.  */
	if (access(fullpath, R_OK) != 0) {
	  printf("\nWarning: unreadable file: %s", fullpath);
	} else if (buf.st_size == 0) {
	  printf("\nWarning: empty file: %s", fullpath);
	} else {
	  if (0) printf("About to call process_bundle(%s)\n", fullpath);
	  process_bundle(fullpath, OUTFILE);
        }
      }
    }
  }

  closedir (dirp);
  return 0;
}



void tidy_up_after_scan( ) {
  free_doctype_patterns();
}



void scan_fe(u_char  *from, FILE *OUTFILE)  {
  /* 
        
     From can either be a directory containing a hierarchy of 
     possibly compressed files or a single possibly compressed file.

     Each file in either case may be a bundle of TREC documents, a WARC file,
     a TAR file or a CSV file.

     */


  u_char fbuf[MAX_LINE+1];
  struct stat stbuf;
  int err;
  size_t path_size;
  u_char execpath[MAX_PATH];
 
  
  if ((err = stat(from,&stbuf))) {
    printf("\nError:  scan_fe: Can't stat '%s'.\n", from);
    perror("stat failed:");
    exit(1);
  }


  if (S_ISDIR(stbuf.st_mode)) {
    /* READING FROM A DIRECTORY. */
    printf("\nRecursively scanning %s\n", from);

    // ignore_prefix_len is now always 2 because prefix is "./"
    // now that we are chdiring to the data root dir.
    //    ignore_prefix_len = strlen(data_root);
    //    if (data_root[ignore_prefix_len - 1] != '/') ignore_prefix_len++;
    ignore_prefix_len = 2;

    strcpy(fbuf,from);
    strcat(fbuf,"/");
    
    // Store the current directory so that it can be restored
    path_size = sizeof(execpath);
    if (getcwd(execpath, path_size) == NULL) {
      printf("\nError:  could not determine the current working dir.");
      perror("get cwd failed:");
      exit(1);
    }

    // CD to the document collection root
    if (chdir(from) != 0) {
      printf("\nError:  could not CD to dir %s.", from);
      perror("get cwd failed:");
      exit(1);
    }

    if (0) printf("Calling scan_dir(.)\n");
    
    scan_dir(".", OUTFILE);   

    // Restore the working directory
    err = chdir(execpath);
    if (err < 0) {
      perror("Error: scan_fe(): chdir()");
      exit(1);
    }
  } else if (S_ISREG(stbuf.st_mode)) {
    process_bundle(from, OUTFILE);
  } else {
    printf("Error: scan_fe(): wrong file type\n");
    exit(1);
  }
}
