/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*                              scan.h                                */
/*	                                            	              */
/**********************************************************************/

#define MAX_CHARSET_LEN 100

#define HDR_NO -1
#define HDR_POSSIBLE 0
#define HDR_YES 1

extern int max_doctype_scan_depth;

void scan_fe(u_char *from, FILE *OUTFILE);

void tidy_up_after_scan();

  
off_t new_bundle(u_char *fp);

void process_bundle(u_char *fp, FILE *OUTFILE);

void process_tar_doc(u_char *start, u_char *end, int dnwit, off_t taroff, FILE *OUTFILE);

void process_http_header_line(u_char *line, int line_len);

void process_doc(int n, u_char *url, u_char *fbmeta,
size_t sitemap_downloadlen, date_only_t sitemap_mod_date, 
                        int http_header_expected, int dnwib, off_t offset, FILE *OUTFILE);

doctype_t get_doctype(u_char *str, int len, u_char **cset, int silent);

void flush_bun_buf();

extern u_char **doc_starts;

extern u_char *conversion_buf;

extern int cbsize;


// Some of the following are shared with warcfile.c

extern pcre *compiled_csetpat_http, *compiled_meta_hequiv,
  *compiled_csetpat_hequiv,
  *compiled_csetpat_xml, *compiled_filelength_pat;
extern pcre_extra *charsetextra_http, *charsetextra_metahequiv, 
  *charsetextra_hequiv,
  *charsetextra_xml, *filelength_extra;


extern u_char chset_fromdoc[], chset_fromhdr[];

void compile_cset_patterns();
