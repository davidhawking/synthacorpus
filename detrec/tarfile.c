/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*	                                            		      */
/*                             index/tarfile.c                        */
/*	                                            		      */
/**********************************************************************/

/*
      Functions for indexing and otherwise manipulating tarfiles
                        David Hawking 12 Dec 2007

      See /usr/include/tar.h (or Wikipedia entry etc.) for full details
      of tar format.  In essence, a tar file is a sequence of 512-byte
      blocks.  For each file in the archive, there is a header block
      followed by zero or more data blocks.  The archive ends with two
      Null blocks.

      I envisage that we might want to index a (compressed) tarfile
      of a crawl or of a database dump.  In both cases it is likely
      that the document URL will be recorded among the data, either
      as an HTTP header or as a docurl element in the case of XML.
      In these cases, we don't need to worry too much about the 
      filename, which is stored in quite unfortunate format in 
      tar.

*/ 

#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h> 
#include <fcntl.h>
#include <stdlib.h>

// Windows doesn't have tar.h so we'll manually put in the definitions we need
#ifdef __MINGW32__
#define TMAGIC "ustar"
#define REGTYPE '0'
#define AREGTYPE '\0'
#else
#include <tar.h>
#endif

#include "zlib/zlib.h"
#include "detrec.h"
#include "pcre/pcre.h"
#include "scan.h"

#define TARBUFSIZE 1048576
static   gzFile TARFILE;

static u_char *tarbuf = NULL;
static int tarbufused = 0, tarbufp = 0;
static off_t tarbytes_read = 0;


static int get_from_tarfile(u_char *buf, int len) {
  // Copy up to len decompressed bytes from the tarfile via
  // tarbuf.  Return the number of bytes actually transferred.

  // If buf is NULL this is just the same as seek for len bytes
  // from current position.

  int i, red;

  if (tarbuf == NULL) tarbuf = femalloc(TARBUFSIZE, 1, "tarbuf");

  for (i = 0; i < len; i++) {
    if (tarbufp >= tarbufused) {
      tarbufused = 0;
      tarbufp = 0;
      red = gzread(TARFILE, tarbuf, TARBUFSIZE);
      if (red <= 0) {
        // End of file or error.
        tarbytes_read += i;
        return i;
      } else tarbufused = red;
    }
    if (buf == NULL) {
      // Seek
      tarbufp++;
    } else {
      // Read
      buf[i] = tarbuf[tarbufp++];
    }
  }
  tarbytes_read += len;
  return len;
}

u_char name_of_file_in_archive[256];


off_t process_tarfile(u_char *tfn, FILE *OUTFILE){
  int red = 0, cnt, regularfile, dnwib;
  long long lldsize;
  off_t datasize, numdb, unused, wontfit, uncolen = 0, toffset;
  u_char blok[512], *nw, tarfilename[MAX_URL_LEN + 1];
  int HALF_CHAMBER = CHAMBER_SIZE / 2;

  dnwib = 0; 
  tarbytes_read = 0;

  // tfb is passed from data_root and this can cause problems if we don't
  // copy it into our own storage.
  strcpy(tarfilename, tfn);
  if (tarbuf == NULL) tarbuf = femalloc(TARBUFSIZE, 1, "tarbuf");


  if (0) 
    printf("process_tarfile: tarfilename = %s, chamber addr = %p, CHAMBER_SIZE = %d\n",
           tarfilename, chamberpot, CHAMBER_SIZE);

  TARFILE = gzopen(tarfilename, "rb");
  if (TARFILE == NULL) {
    fprintf(stderr, "Error: Cannae open tarfile %s\n", tarfilename);
    exit(1);
  }

  red = get_from_tarfile(blok, 512);  // Read first header.
  while (red == 512) {
    // We're currently looking at a header block
    
    if (*blok == 0) {
      // The first character of the name part should only be null
      // if this is end-of-archive.
      break;
    } else {
      // Byte 156 within the header tells us the type of the file
      // --- we're only interested in regular files.
      regularfile = 0;
      if (blok[156] == REGTYPE || blok[156] == AREGTYPE) regularfile = 1;
  
      cnt = sscanf(blok+124, "%12llo", &lldsize);// numbers are in bloody octal!
      datasize = (off_t) lldsize;
      numdb = (datasize + 511) >> 9;
      unused = numdb * 512 - datasize;
      if (0) printf("Tar header: datasize = %lld, blocks = %lld\n", 
      (long long) datasize, (long long) numdb);
      if (regularfile) {
        // Save the name (possibly represented in two separate fields!)
        nw = name_of_file_in_archive;
        if (blok[345]) {
          // prefix field is used.  Copy it in to the name buffer
          memcpy(nw, blok + 345, 155); 
          nw += 155;
        }
        // Copy the name field
        memcpy(nw, blok, 100);
        // In the worst case, there's no trailing null, so append one.
        nw[100] = 0;

        if (0) printf("       [tarfile %s]\n", name_of_file_in_archive);  

        wontfit = datasize - HALF_CHAMBER;
        if (wontfit > 0) {
          // This file is bigger than the chamber
          printf("       [%lld bytes of file truncated]\n", (long long) wontfit);
          red = get_from_tarfile(chamberpot, HALF_CHAMBER);
          if (red < HALF_CHAMBER) printf("       [short read]\n");
          chamberpot[HALF_CHAMBER - 1] = 0;
          conversion_buf = chamberpot + HALF_CHAMBER;
          cbsize = 0;
          red = get_from_tarfile(NULL, wontfit);
        } else {
          red = get_from_tarfile(chamberpot, datasize);
          if (red < datasize) printf("      [short read]\n");
        }

	toffset = tarbytes_read - datasize;
	if (wontfit > 0) toffset -= red;
	process_tar_doc(chamberpot, chamberpot + datasize, dnwib, toffset, OUTFILE);
	dnwib++;
	uncolen += datasize;
               
        // Now skip over the unused data 
        if (unused > 0) red = get_from_tarfile(NULL, unused);
      } else {
        // Need to skip uninteresting blocks
        if (cnt > 0) red = get_from_tarfile(NULL, numdb * 512);
      }

      red = get_from_tarfile(blok, 512);  // Read the next header.
    }
  } 
  gzclose(TARFILE);
  if (tarbuf != NULL) free(tarbuf);
  tarbuf = NULL;

  return(uncolen);
  //write(bunfd, &curr_bundle, sizeof(bundle_t));
}
