/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*	                                            		      */
/*                          index/tarfile.h                           */
/*	                                            		      */
/**********************************************************************/

/*
   Functions for indexing and otherwise manipulating tarfiles
                        David Hawking 12 Dec 2007
*/ 

extern off_t process_tarfile(u_char *tarfilename, FILE *OUTFILE);

