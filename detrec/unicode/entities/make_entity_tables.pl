#! /usr/bin/perl -w

# Copyright (C) Funnelback Pty Ltd

# Runs over the .ent files published by W3C and downloaded to this
# directory and produces C tables for pasting into padre/src/common.c

# curl http://www.w3.org/TR/xhtml1/DTD/xhtml-symbol.ent > xhtml-symbol.ent
# curl http://www.w3.org/TR/xhtml1/DTD/xhtml-special.ent > xhtml-special.ent
# curl http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent > xhtml-lat1.ent

$cnt = 0;
foreach $entfile (glob '*.ent') {
    die "Can't open $entfile" unless open EF, $entfile;
    die "Can't read $entfile" unless read EF, $txt, 1000000;
    close(EF);
    # The pattern in the following is a bit weird because some of the
    # definitions in shtml-special.ent have two numenric codes
    # e.g. <!ENTITY lt      "&#38;#60;"> <!--  less-than sign, U+003C ISOnum -->
    #      where the code we want is #60
    while ($txt =~ m@<!ENTITY\s+([a-zA-Z0-9]+)\s+"&.*?#([0-9]+);">@sg) {
        $name = $1;
        $decimal_ucode = $2;
        $entity{$name} = $decimal_ucode; 
        #print "$name $decimal_ucode\n";
        $cnt++;
    }
    #print "$entfile: $cnt so far.\n";
}

print "
// ----------------------------------------------------------------------------
// The following table created by unicode_tables/entities/make_entity_tables.pl
static entity_t ents[] = {
";

$cnt = 0;
foreach $k (sort keys %entity) {
    $cnt++;
    $l = length $k;
    print "  {\"$k\", $l, $entity{$k}},\n";
}

print "};

static int ent_cnt = $cnt;
// ----------------------------------------------------------------------------
";
        
exit(0);
