#! /usr/bin/perl -w

# Copyright (C) Funnelback Pty Ltd

die "Can't open Unihan_Variants.txt\n" unless open U, "Unihan_Variants.txt";

$cnt = 0;
$multialts = 0;
$maxalts = 0;
while (<U>) {
    chomp;
    next unless /(U\+[0-9A-F]+)\s+k(.*?)Variant\s+(.*)/;
    $cnt++;
    $lhc = $1;
    $type = $2;
    $rhs = $3;
    $cnts{$type}++;

    next unless $type =~ /Simplified|Traditional/;;
    $rhs =~ s/<\S+//g;
    #print "$lhc => $rhs\n";
    @f = split /\s+/, $rhs;
    if ($#f > 0) {
        $multialts++;
        $maxalts = $#f + 1 if ($#f >= $maxalts);
        print "SimpTrad multialt: ", $_, "\n";
    }
}
   

close(U);

foreach $k (keys %cnts) {
    print "  $k: $cnts{$k}\n";
}
print "Total Lines: $cnt\nSimpTrad Multialts: $multialts\nSimpTrad Maxalts: $maxalts\n";

exit(0);
