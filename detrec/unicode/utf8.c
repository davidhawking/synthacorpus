/* Copyright (C) Funnelback Pty Ltd */
/*****************************************************************************/
/*                                                                           */
/*                   utf8.c  (David Hawking 07 Feb 2006)                     */
/*                                                                           */
/*****************************************************************************/

/*                          UTF-8/Unicode Functions                          */



#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>

#include "../detrec.h"
#include "utf8.h"
#include "unicode_init.h"
#include "chinese_init.h"

int utf8_debug = 0;

/* For a full explanation of all the Unicode, UCS, ISO 10646, UTF 
   character sets, see /home/dave/padre/doc/unicode.html downloaded from
         http://www.cl.cam.ac.uk/~mgk25/unicode.html
   This bunch of code implements the following externally visible 
   functions:

 1. u_int utf8_getchar(byte **sp) - read a multi-byte UTF-8 sequence from
   sp and return the Unicode value, while updating the string pointer
   to indicate the byte immediately following the UTF-8 sequence.  This
   handles all of Markus Kuhn's listed error conditions.
 2. int utf8_to_iso88591_inplace(byte *str, int map_controls) - scan
   str and replace any UTF-8 sequences with the corresponding ISO8859-1
   equivalent if possible or UTF8_INVALID_CHAR otherwise.  If map_controls
   is set, ASCII and ISO8859-1 control characters are also mapped
   to UTF8_INVALID_CHAR.  Inplace copying can be performed because
   the result is guaranteed to be no longer than the original.  The
   result is null-terminated and its length is returned.
 2A. int utf8_to_iso88591_inplace_len(byte *str, int map_controls, int len)
   does the same but for a specified number of characters.
 3. u_char ucs_to_utf8(u_int unicode, byte *buf) - generates a string of
   up to six bytes in buf (seven including null termination) 
   containing the UTF-8 encoding of the integer argument.  Note that because
   ISO8859-1 characters map to the low 255 Unicode positions, this
   function can be used to convert ISO8859-1 to UTF-8.
   It is the callers responsibility to ensure that buf points to 
   at least the necessary amount of storage.  Returns length of UTF-8 string.
*/

static inline int classify(byte abyte) {
  // Decide whether abyte is:
  if (abyte == 0) return 0;              //      0 - NULL
  if (abyte < 0X80) return 1;            //      1 - 7-bit ASCII
  if ((abyte & 0XC0) == 0X80) return 2;  //      2 - UTF-8 trailing byte
  if ((abyte & 0XC0) == 0XC0) return 3;  //      3 - UTF-8 first byte
  return 4;                             //      4 - other
}

static byte *skip_utf8_trailing_bytes(byte *str) {
  byte *s = str;
  while (classify(*s) == 2) {s++;}
  return s;
}

static int overlong(byte byte1, byte byte2) {
  // http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8 defines a number of
  // "overlong" representations which it claims should be flagged as errors
  // This function tests the full list of five cases and returns non-zero
  // iff the representation is overlong.
  if ((byte1 & 0XFE) == 0XC0) return 1;
  if (byte1 == 0XE0 && (byte2 & 0XE0) == 0X80)  return 1;  
  if (byte1 == 0XF0 && (byte2 & 0XF0) == 0X80)  return 1;
  if (byte1 == 0XF8 && (byte2 & 0XF8) == 0X80)  return 1;
  if (byte1 == 0XFC && (byte2 & 0XFc) == 0X80)  return 1;
  return 0;
}

ucs_t utf8_getchar(byte **sp) {
  // see section comment above
  int i, b, nbytes = 0, ndb;
  byte *s, bight, bight2;
  ucs_t rslt = 0;
  // See http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8 for encoding
  if (sp == NULL) return 0;
  s = *sp;
  if (s == NULL) return 0;
  if (utf8_debug) printf("utf8_getchar: s='%s'\n", s);
  bight = *s++;   // Should be 110xxxxx or 1110xxxx or 11110xxx or ...

  if (bight < 0X80) {
    // 7-bit ascii or null
    *sp = s;  
    return (u_int)bight;  
  } else if (! ((bight & 0XC0) == 0XC0)) {
    // Not a valid UTF-8 head byte
    *sp = s;    
    if (utf8_debug) printf("utf8_getchar: invalid head byte\n");
    return (u_int)UTF8_INVALID_CHAR;
  }

  bight2 = *s;
  if (!((bight2 & 0XC0) == 0X80)) {
    // Not a valid UTF-8 trailing byte
    *sp = s;  
    if (utf8_debug) printf("utf8_getchar: invalid trailing byte\n");
    return (u_int)'U';
  }

  if (utf8_debug) printf("utf8_getchar: bight = %X, bight2 = %X\n", bight, bight2);
  if (overlong(bight, bight2)) {
    if (utf8_debug) printf("utf8_getchar: Overlong detected\n");
    *sp = skip_utf8_trailing_bytes(s+1);
    return (u_int) UTF8_INVALID_CHAR;
  }

  // Find all the leading ones
  while (bight & 0X80) {
    nbytes++;
    bight <<= 1; // shift left 1 bit
  }
  

  // Skip over the terminating zero
  bight <<= 1;
 
  // Get the data bits out of the rh part of the first byte
  ndb = 7 - nbytes;
  if (utf8_debug) printf("utf8_getchar: %d bytes, ndb = %d\n", nbytes, ndb);
  for (b = 0; b < ndb; b++) {
    rslt <<= 1;
    if (bight & 0X80) rslt |= 1;
    bight <<= 1;
  }
  nbytes--;
  
  for (i = 0; i < nbytes; i++) {
    // these bytes must be UTF-8 trailing bytes
    bight = *s;
    if (!((bight & 0XC0) == 0X80)) {
      // Invalid UTF-8 sequence or missing byte(s)
      if (utf8_debug) printf("utf8_getchar: Error exit1\n");
      *sp = s;  
      return (int)UTF8_INVALID_CHAR;
    }
    bight &= 0X3F;  // get the six data bits
    rslt <<= 6;    // make room for the six data bits
    rslt |= bight;  // and OR them in
    s++;
  }   
  
  if (utf8_debug) printf("utf8_getchar: Raw MBC: 0X%X (%d), nbytes = %d\n", 
                         rslt, rslt, (nbytes + 1));
  if ((rslt >= 0XD800 && rslt <= 0XDFFF) 
      || (rslt == 0XFFFE) || (rslt == 0XFFFF)) {
    if (utf8_debug) printf("utf8_getchar: Illegal value: 0X%X (%d), nbytes = %d\n", 
                           rslt, rslt, (nbytes + 1));
    rslt = (int)UTF8_INVALID_CHAR;
  }
  
  *sp = s;
  //if (rslt >= unicode_tablesize) rslt = (int)UTF8_INVALID_CHAR;
  return rslt;
}


ucs_t utf8_peekchar(byte *ptr) {
  // Only a temporary pointer is advanced
  byte *tmp = ptr;
  return (utf8_getchar(&tmp));
}

byte *utf8_find_nthchar(byte *bytes, int n) {
  // Return a pointer to the first byte of the nth UTF-8 character
  // in bytes, numbering from 0 (i.e. if n<= 0, bytes will be returned.
  // If there are fewer than n characters in the string, bytes will be
  // returned.

  byte *r = bytes;
  int cnt = 0;
  if (n <= 0) return bytes;
  while (*r) {
    if (! (*r & 0X80)) { // ASCII
      if (++cnt > n) return r; 
    } else if ((*r & 0XC0) == 0XC0) { // UTF-8 first byte
      if (++cnt > n) return r; 
    }
    r++;
  }
  return bytes;
}

int utf8_strlen(byte *bytes) {
  // Return a count of UTF-8 characters in null-terminated bytes
  // Count one for every ASCII char and one for every UTF-8 first byte
  // ignore errors.

  byte *r = bytes;
  int cnt = 0;
  while (*r) {
    if (! (*r & 0X80)) cnt++;  // ASCII
    else if ((*r & 0XC0) == 0XC0) cnt++; // UTF-8 first byte
    r++;
  }
  return cnt;
}


int utf8_remove_invalid_chars(byte *bytes) {
  // Scan bytes and inplace replace any invalid UTF-8 sequences
  // with UTF8_INVALID_CHAR (currently space).
  // Return the length in bytes.  
  // Bytes is null-terminated before and after
  // Length cannot increase so inplace is fine.

  byte *r = bytes, *w = bytes;
  ucs_t uu;
  int len;

  while (*r && (uu = utf8_getchar(&r))) {
    len = ucs_to_utf8_nonull(uu, w);
    w += len;
  }
  *w = 0;
  return w - bytes;
}



int utf8_to_iso88591_inplace(byte *str, int map_controls) {
  // see section comment above
  byte *r=str, *w=str, c;
  int mbc;
  while ((c = *r)) {
    if (c > 0X7F) {
      if (0) printf("TRANSLATING\n");
      mbc = utf8_getchar(&r);
      // Note that the first 256 Unicode values correspond exactly to ISO859-1
      if (mbc < 256) c = (byte)mbc;
      else c = UTF8_INVALID_CHAR;
      if (map_controls && c >= 128 && c < 140) c = UTF8_INVALID_CHAR;  
      *w++ = c;
    } else {
      if (c == 0X7F) c = UTF8_INVALID_CHAR;
      else if (map_controls && c < 32 && c != 10) c = UTF8_INVALID_CHAR;
      *w = c;  
      r++;
      w++;
    }
  }
  *w = 0;
  return w-str;
}


int utf8_to_iso88591_inplace_len(byte *str, int map_controls, int len) {
  // see section comment above  
  byte *r=str, *w=str, c, *last = str + (len - 1);
  int mbc;
  while (r <= last)  {
    c = *r;
    if (c > 0X7F) {
      if (0) printf("TRANSLATING\n");
      mbc = utf8_getchar(&r);
      // Note that the first 256 Unicode values correspond exactly to ISO859-1
      if (mbc < 256) c = (byte) mbc;
      else c = UTF8_INVALID_CHAR;
      if (map_controls && c >= 128 && c < 140) c = UTF8_INVALID_CHAR;  
      *w++ = c;
    } else {
      if (c == 0X7F) c = UTF8_INVALID_CHAR;
      else if (map_controls && c < 32 && c != 10) c = UTF8_INVALID_CHAR;
      *w = c;  
      r++;
      w++;
    }
  }
  if (r > w) *w = 0;  // Null terminate if we're now shorter.
  return w-str;
}


static byte b1left[] = {0, 0, 0XC0, 0XE0, 0XF0, 0XF8, 0XFC},
  b1mask[] = {0, 0, 0X1F, 0XF, 0X7, 0X3, 0X1};

int ucs_to_utf8(ucs_t unicode, byte *buf) {
  // see section comment above
  // 
  int nbytes, b;
  u_int sixbit_mask = 0X3F;

  if (0) printf("unicode_to_utf8(%X) ", unicode);
  if (unicode < 0X80) {
    // 7 bit ASCII
    *buf = (byte) unicode;
    if (0) printf("[%X]\n", buf[0]);
    *(buf+1) = 0;
    return 1;
  }

  // UTF-8 needed
  // 2 bytes - 11 data bits, 3-bytes - 16 data bits, 4-bytes - 21 bits
  // 5-bytes - 26 bits, 6-bytes - 31 bits

  if (unicode < 2048) nbytes = 2;  
  else if ( unicode < 65536) nbytes = 3;
  else if ( unicode < 2097152) nbytes = 4;
  else if ( unicode < 1<<26) nbytes = 5;
  else if ( unicode < 1<<31) nbytes = 6;
  else {
    *buf = UTF8_INVALID_CHAR;
    *(buf+1) = 0;
    return 1;
  }
  
  // Work backwards from the null termination to the start.
  buf[nbytes] = 0;

  for (b = nbytes-1; b > 0; b--) {
    buf[b] = 0X80 | (unicode & sixbit_mask);
    unicode >>= 6;
  }
  
  // First byte is different
  buf[0] = b1left[nbytes] | (b1mask[nbytes] & unicode);
  return nbytes;
}

int ucs_to_utf8_nonull(ucs_t unicode, byte *buf) {
  // see section comment above
  // 
  int nbytes, b;
  u_int sixbit_mask = 0X3F;

  if (0) printf("unicode_to_utf8(%X) ", unicode);
  if (unicode < 0X80) {
    // 7 bit ASCII
    *buf = (byte) unicode;
    if (0) printf("[%X]\n", buf[0]);
    return 1;
  }

  // UTF-8 needed
  // 2 bytes - 11 data bits, 3-bytes - 16 data bits, 4-bytes - 21 bits
  // 5-bytes - 26 bits, 6-bytes - 31 bits

  if (unicode < 2048) nbytes = 2;  
  else if ( unicode < 65536) nbytes = 3;
  else if ( unicode < 2097152) nbytes = 4;
  else if ( unicode < 1<<26) nbytes = 5;
  else if ( unicode < 1<<31) nbytes = 6;
  else {
    *buf = UTF8_INVALID_CHAR;
    *(buf+1) = 0;
    return 1;
  }
  
  // No null termination.  Work backwards to the start.

  for (b = nbytes-1; b > 0; b--) {
    buf[b] = 0X80 | (unicode & sixbit_mask);
    unicode >>= 6;
  }
  
  // First byte is different
  buf[0] = b1left[nbytes] | (b1mask[nbytes] & unicode);
  return nbytes;
}

ucs_t ucs_from_hexseq(byte **hexbytes, int max_digits) {
  // read a series of up to max_digits hexadecimal bytes and convert to UTF-8
  // Advance hexbytes past the actual hex sequence.
  ucs_t rslt = 0;
  byte *h = *hexbytes;
  while (isxdigit(*h)) {
    rslt <<= 4;
    if (*h >= 'a') rslt += (*h - 'a' + 10);
    else if (*h >= 'A') rslt += (*h - 'A' + 10);
    else rslt += (*h - '0');
    h++;
    if (--max_digits <= 0) break;
  }
  if (0) printf("ucs_from_hexseq(%s) = %d\n", *hexbytes, rslt);
  *hexbytes = h;
  return rslt;
}


int utf8_from_url(byte *urlout, byte *urlin, int max_outlen) {
  // A URL such as urlin may include url-encoded characters which 
  // could represent UTF-8 characters.  I assume this is correct.
  // Copy from urlin to urlout replacing %xx.. sequences with equivalent
  // UTF-8.  Note that the output is guaranteed not to grow in length.
  // because the UTF-8 first byte stores some data bits (unlike the '%')
  // and because every additional byte in a % sequence delivers four data
  // bits whereas every additional UTF-8 byte delivers at least 5.

  // Accordingly, it is OK to call with urlin == urlout.

  if (0) printf("utf8_from_url(%s)\n", urlin);
  byte *r = urlin, *w = urlout, buf[7];
  int b, l = 0;
  ucs_t uc;
  if (urlout == NULL || max_outlen == 0) return -1;
  if (urlin == NULL  || *r == 0) {
    *w = 0;
    return 0;
  }
  while (*r && l < max_outlen) {
    if (*r != '%' && *r < 0X80) {
      *w++ = *r++;
      l++;
      continue;
    }

    // ----  Beyond this point we are dealing with either a % sequence
    // ----  or a non-ascii character
    
    if (*r >= 0X80) {
      // I don't think this is legal in a URL but humour it by assuming
      // ISO 8859-1
      uc = (ucs_t) *r++;
    } else {
      // Must be a % sequence.
      r++;
      uc = ucs_from_hexseq(&r, 2);
    } 
    b = ucs_to_utf8(uc, buf);
    if (0) printf("utf8_from_url: buf = '%s', b = %d\n", buf, b);
    if (b + l > max_outlen) {
      *w = 0;
      return l;
    } else {
      strcpy(w, buf);
      w += b;
      l += b;
    }
  }
  *w = 0;
  if (0) printf("URLOUT: %s\n", urlout);
  return l;
}

int utf8_encode(byte *dest, byte *src, int max_outlen) {
  // Input assumed to be in ISO8859-1 (for the moment)
  byte *r = src, *w = dest, buf[7];
  int b, l = 0;
  if (dest == NULL || max_outlen == 0) return -1;
  if (src == NULL  || *r == 0) {
    *w = 0;
    return 0;
  }
  while (*r && l < max_outlen) {
    if (*r < 0X80) {
      *w++ = *r;
      l++;
    } else {
      b = ucs_to_utf8((u_int)*r, buf);
      if (b + l > max_outlen) {
	*w = 0;
	return l;
      } else {
	strcpy(w, buf);
	w += b;
	l += b;
      }
    }
    r++;
  }
  *w = 0;
  return l;
}


int utf8_straxcmp(byte *str1, byte *str2, int nz, int deutsch) {
  // Like strcmp but works UTF-8 character by UTF-8 character, mapping accented
  // letters in each string to non-accented forms.  If nz is specified then
  // 'aa' appearing in the string is mapped to just 'a'. 
  // If deutsch is set then 'ue', 'oe' and 'ae' are mapped to u, o, and a resp.
  
  ucs_t ucs_rchar, ucs_schar;
  byte *r = str1, *s = str2;

  while (*r && *s) {
    // Get ucs_rchar, the normalised form of the next UTF-8 char in str1
    if (nz && *r == 'a' && *(r + 1) == 'a') {
      r+=2;
      ucs_rchar = 'a';
    } else if (deutsch && *(r + 1) == 'e' && (*r == 'u' || *r == 'o' || *r == 'a')) {
      ucs_rchar = *r;
      r+=2; 
    } else {
      ucs_rchar = utf8_getchar(&r);  // advances r
      ucs_rchar = ucs_mapax(ucs_rchar); 
    }

    // Now do the same for ucs_schar from str2
    if (nz && *s == 'a' && *(s + 1) == 'a') {
      s+=2;
      ucs_schar = 'a';
    } else if (deutsch && *(s + 1) == 'e' && (*s == 'u' || *s == 'o' || *s == 'a')) {
      ucs_schar = *s;
      s+=2; 
    } else {
      ucs_schar = utf8_getchar(&s);  // advances s
      ucs_schar = ucs_mapax(ucs_schar); 
    }

    if (0) 
      printf("Comparing Unicode %d with %d\n", ucs_rchar, ucs_schar);

    if (ucs_rchar < ucs_schar) return -1;
    if (ucs_rchar > ucs_schar) return 1;
  }
  
  if (*s) return -1;
  if (*r) return 1;
  return 0;
}

struct strax_test {
  byte *a, *b;
  int nz, deutsch, expected;
} strax_tests[] = {
  {"Zürich", "Zurich", 0, 0, 0},
  {"Zurich", "Zürich", 0, 0, 0},
  {"Zuerich", "Zürich", 0, 0, -1},
  {"Zuerich", "Zürich", 0, 1, 0},
  {"Zuerich", "Zurich", 0, 1, 0},
  {"Māori", "Maori", 0, 0, 0},
  {"Māori", "Maaori", 0, 0, 1},
  {"Māori", "Maaori", 1, 0, 0},
  {"Māori", "Maori", 1, 0, 0},
  {"Maaori", "Maori", 0, 0, -1},
  {"Maaori", "Maori", 1, 0, 0},
  {NULL, NULL, 0, 0, 0}
};
  


void test_straxcmp() {
  int i, r, failed = 0;
  for (i = 0; strax_tests[i].a != NULL; i++) {
    r = utf8_straxcmp(strax_tests[i].a, strax_tests[i].b,
                      strax_tests[i].nz, strax_tests[i].deutsch);
    if (r != strax_tests[i].expected) {
      printf("Test_straxcmp(%s, %s, %d, %d) --> %d, should have been %d\n",
             strax_tests[i].a, strax_tests[i].b,
             strax_tests[i].nz, strax_tests[i].deutsch, r, 
             strax_tests[i].expected);
      failed++;
    }
  }
  if (failed == 0) printf("Tests of utf8_straxcmp() all succeeded.\n");
}


int utf8_conflate_accents(byte *sout, byte *sin, int max_bytes) {
  // Copy UTF-8 string sin to sout, replacing accented letters with unaccented
  // forms.  sin is assumed to be null terminated.
  // At most max_bytes will be written to sout N, plus terminating NULL
  // Note that all current conversions reduce the
  // length of the string, so same size as sin should be good enough.

  // Return bytes_written.

  byte *r = sin, *w = sout, buf7[7];
  ucs_t ucs_rchar, ucs_wchar;
  int bytes_written = 0, u8wid;

  ucs_rchar = utf8_getchar(&r); // advances r
  
  // Each iteration of the following writes one UTF-8 char to s1
  while (ucs_rchar > 0 && bytes_written < max_bytes) {// l applies to s1 (output string)
    ucs_wchar = ucs_mapax(ucs_rchar);
    u8wid = ucs_to_utf8(ucs_wchar, buf7);	
    if (u8wid + bytes_written > max_bytes) break;
    strcpy(w, buf7);   // Annoying extra copy to avoid oflow.
    w += u8wid;
    bytes_written += u8wid;
    ucs_rchar = utf8_getchar(&r); // advances r
  }
  *w = 0;
  return bytes_written;
}


int utf8_conflate_accents_inline(byte *sin) {
  // Replace accented letters in sin with unaccented forms.  sin is assumed 
  // to be null terminated.
  // Note that all current conversions preserve or reduce the
  // length of the string, so inplace copying is OK.

  // Return bytes_written.

  byte *r = sin, *w = sin, buf7[7];
  ucs_t ucs_rchar, ucs_wchar;
  int bytes_written = 0, u8wid;

  ucs_rchar = utf8_getchar(&r); // advances r
  
  // Each iteration of the following writes one UTF-8 char to s1
  while (ucs_rchar > 0) {
    ucs_wchar = ucs_mapax(ucs_rchar);
    u8wid = ucs_to_utf8(ucs_wchar, buf7);	
    strncpy(w, buf7, u8wid);   // Annoying extra copy to avoid oflow.
    w += u8wid;
    bytes_written += u8wid;
    ucs_rchar = utf8_getchar(&r); // advances r
  }
  *w = 0;
  return bytes_written;
}



// -------- Functions dealing with UCS (Unicode) integers ---------------


// The following array documents the exceptions to the "rule" that all characters
// above 255 (0XFF) are indexable.   Each exception is present as a range from
// first non-indexable to last non-indexable.  Entries must be present in ascending
// order (except a special (0,0) last entry) and must not overlap.
// Note that this table is certainly NOT 100% complete!  The Unicode tables
// are littered with special marks and characters which would require a great
// deal of A) understanding and B) time to correctly reflect in this table.
// The below has been tuned a bit for Japanese.
// Further cases are no doubt needed for other specific languages.


static unicode_block ucs_exceptions[] = {
  {0X16EB,0X16ED},// Runic single, multiple and cross punctuation
  {0X2000,0X2BFF},// All sorts of punctuation, symbols, dingbats, emoticons etc.
  {0X3000,0X3020},// Unicode CJK punctuation and specials block
  {0X302A,0X3037},// Unicode CJK punctuation and specials block
  {0X303B,0X303F},// Unicode CJK punctuation and specials block
  {0X3099,0X30A0},// Unicode CJK punctuation and specials block
  // The following commented out as a result of Maki Maruyama's comments
  // -- may need further refinement
  //{0X30FB,0X30FF},// Unicode CJK punctuation and specials block
  {0XE000,0XE0FF},// Unicode supplemental punctuation block
  {0XFF00,0XFF0F},// Unicode half and full-width forms block + Unicode specials block
  {0XFF1A,0XFF20},// Unicode half and full-width forms block + Unicode specials block
  {0XFF3B,0XFF40},// Unicode half and full-width forms block + Unicode specials block
  {0XFF5B,0XFF64},// Unicode half and full-width forms block + Unicode specials block
  {0XFFE2,0XFFFF},// Unicode half and full-width forms block + Unicode specials block
  {0,0}
};

// The following array allows customers to specify that certain ASCII 
// punctuation symbols are actually indexeable in the same way as 
// letters and digits.  For example if the array (interpreted as a 
// string) contained backslash, equal sign and single quote then
// the sequence \\\\========'''''' would be treated as a single
// word.  
// Warning: Many of the ASCII punctuation characters are operators
// or are subject to special processing rules which are almost
// certainly not de-activated by listing them in this array.

byte extra_indexable_ascii_chars[MAX_EXTRA_IDX_CHARS + 1] = {0};
      
int ucs_indexable(ucs_t karacter) {
  // return 1 if the UCS value represented by karacter is indexable
  // return 0 otherwise.
  // Unicode version based on ucs_exceptions[] table above - see comment there.

  int i;
  u_char *eiacp = extra_indexable_ascii_chars;

  if (karacter < 0X100) {
    while (*eiacp 
           && (eiacp - extra_indexable_ascii_chars < MAX_EXTRA_IDX_CHARS)) {
      if (karacter == *eiacp) return 1; 
      eiacp++;
    }
    return(unicode_type[karacter] & UNI_INDEXABLE);
  }

  for (i = 0; ucs_exceptions[i].last != 0; i++) {
    if (karacter < ucs_exceptions[i].first) break;
    if (karacter <= ucs_exceptions[i].last) return 0;
  }
  
  if (0) {
    u_char buf7[7];
    ucs_to_utf8(karacter, buf7);
    printf("Character %s %d (%X) considered indexable\n", buf7, karacter, karacter);
  }
  return 1;  // New assumptions 30 Oct 2007
}


int ucs_isupper(ucs_t karacter) {
  // return 1 if the UCS value represented by karacter is upper case
  // return 0 otherwise.

  if (karacter >= unicode_casetablesize) return 0;   // New assumptions 30 Oct 2007
  if (unicode_tolower[karacter]) return 1;
  return 0;
}


int ucs_islower(ucs_t karacter) {
  // return 1 if the UCS value represented by karacter is lower case
  // return 0 otherwise.

  if (karacter >= unicode_casetablesize) return 1; // New assumptions 30 Oct 2007
  if (unicode_toupper[karacter]) return 1;
  return 0;
}

static unicode_block cjkt_blox[] = {
  // These entries assumed to be in ascending order
  {0X0E00, 0X0E7F}, //Thai
  {0X1100, 0X11FF}, //Hangul Jamo
  {0X1780, 0X17FF}, //Khmer
  {0X19E0, 0X19FF}, //Khmer Symbols
  {0X2E80, 0XA71F}, //CJK Radicals Supplement ... Modifier Tone Letters
  {0XAC00, 0XD7AF}, //Hangul Syllables
  {0XF900, 0XFAFF}, //CJK Compatibility Ideographs
  {0XFE30, 0XFE4F}, //CJK Compatibility Forms
  {0XFF66, 0XFF9D}, //Half-width katakana
  {0X20000, 0X2FA1F}, //CJK Unified Ideographs Extension B / CJK Compatibility Ideographs Supplement
  // Last entry assumed to have last == 0
  {0XFFFFF, 0}
};

static int cjkt_gramming_suppressed = 0;  // If set, the splitting of CJKT
                                          // sequences into bigrams and unigrams
                                          // will be suppressed.  We assume that
                                          // the CJKT text and/or queries has/have
                                          // been pre-segmented

void suppress_cjkt_gramming() {
  cjkt_gramming_suppressed = 1;
}


int cjkt_gramming_is_suppressed() {
  return cjkt_gramming_suppressed;
}



int ucs_isCJKT(ucs_t karacter) {
  // return 1 if the UCS value represented by karacter lies within a Unicode
  // code block reserved for CJKT characters (Chinese, Japanese, Korean, 
  // Thai and also Khmer)
  // return 0 otherwise.

  // This function always returns zero when bigram/unigram indexing is suppressed
  int i;

  if (cjkt_gramming_suppressed) return 0;  // When working with segmented Chinese text

  for (i = 0; cjkt_blox[i].last != 0; i++) {
    if (karacter < cjkt_blox[i].first) return 0;
    // Return 1 iff >= first and <= last
    if (karacter <= cjkt_blox[i].last) return 1;
  }
    
  return 0;
}


int ucs_isCJKT_noexempt (ucs_t karacter) {
  // return 1 if the UCS value represented by karacter lies within a Unicode
  // code block reserved for CJKT characters (Chinese, Japanese, Korean, 
  // Thai and also Khmer)
  // return 0 otherwise.

  // Unlike the previous function, it doesn't matter what mode we are in

  int i;

  for (i = 0; cjkt_blox[i].last != 0; i++) {
    if (karacter < cjkt_blox[i].first) return 0;
    // Return 1 iff >= first and <= last
    if (karacter <= cjkt_blox[i].last) return 1;
  }
    
  return 0;
}



int string_contains_CJKT(byte *string) {
  // Return 1 iff the UTF-8 string contains at least one CJKT character
  byte *current = string, *next = current;
  while (*current) {
    if (ucs_isCJKT(utf8_getchar(&next))) return 1;
    current = next;
  }
  return(0);
}

int string_contains_accent(byte *string) {
  // Return 1 iff the UTF-8 string contains at least one accented letter
  byte *current = string, *next = current;
  ucs_t karacter;
  while (*current) {
    karacter = utf8_getchar(&next);
    if (karacter < unicode_axtablesize) {
      if (unicode_axmap[karacter]) return 1;
    }
    current = next;
  }
  return(0);
}


int string_contains_non_ascii_non_indexable(byte *string) {
  // Return 1 iff the UTF-8 string contains at least one character
  // is non-ASCII and non-indexable
  ucs_t karacter;
  byte *current = string, *next = current;
  while (*current) {
    karacter = utf8_getchar(&next);
    if (karacter > 127 && !ucs_indexable(karacter)) return 1;
    current = next;
  }
  return(0);
}


int utf8_remove_non_indexable(byte *string) {
  // Replace all sequences of non-indexable characters with a single
  // space.   
  // At present this is only called for build_spelling_index
  // ** Special treatment of 
  //     '+' which is sometimes indexable. 
  //     '\'' which we want to appear in spelling suggestions
  //     '-' ditto.
  // Inplace.
  // Return new length.
  byte *rr, buf7[70], *w = string;
  int olen, spaceout = 1; // start with one to avoid leading spaces; 
  ucs_t sucs;

  if (string == NULL) return 0;
  if (string[0] == 0) return 0;
  
  rr = string;
  sucs = utf8_getchar(&rr);
  while (sucs) {
    if (ucs_indexable(sucs) || sucs == '\'' || sucs == '-' || (sucs == '+' && !spaceout)) {
      olen = ucs_to_utf8(sucs, buf7);
      strncpy(w, buf7, olen);
      w += olen;  
      spaceout = 0;
    } else {
      if (!spaceout) {
        *w++ = ' ';
        spaceout = 1;
      }
    }
    sucs = utf8_getchar(&rr);
  }  
  if (w > string && *(w-1) == ' ') w--;  // Avoid a trailing space.

  *w = 0;
  return (w - string);
}



int utf8_asciisize_winpunc(byte *string) {
  // Replace angled versions of single and double quotes and
  // dashes with hyphens in null-terminated string. Return count of 
  // characters replaced.
  // Inplace
  
  byte *rr, buf7[7], *w = string;
  int olen, changed = 0;
  ucs_t ucs;

  if (string == NULL) return 0;
  if (string[0] == 0) return 0;
  rr = string;
  ucs = utf8_getchar(&rr);
  while (ucs) {
    if (ucs == 0x2018 || ucs == 0x2019) {
      ucs = '\'';
      changed++;
    } else if (ucs == 0x201C || ucs == 0x201D) {
      ucs = '"';
      changed++;
    } else if (ucs == 0x2013 || ucs == 0x2014) {
      ucs = '-';
      changed++;
    }
      
    olen = ucs_to_utf8(ucs, buf7);
    strncpy(w, buf7, olen);
    w += olen;  
    ucs = utf8_getchar(&rr);
  }     
  *w = 0;
  return changed;
}

ucs_t ucs_tolower(ucs_t karacter) {
  // return the lower case version of karacter or karacter itself
  // if there is no known lower case version.

  // Unicode version!

  if (karacter >= unicode_casetablesize) return karacter;
  if (0) printf("ucs_tolower: karacter = %d, adder = %d, lc = %d\n", karacter,
                unicode_tolower[karacter], karacter + unicode_tolower[karacter]);

  return(karacter + unicode_tolower[karacter]);

}


ucs_t ucs_toupper(ucs_t karacter) {
  // return the upper case version of karacter or karacter itself
  // if there is no known upper case version.

  // Unicode version!

  if (karacter >= unicode_casetablesize) return karacter;
  if (0) printf("ucs_toupper: karacter = %d, adder = %d, lc = %d\n", karacter,
	 unicode_toupper[karacter], karacter + unicode_toupper[karacter]);
  
  return(karacter + unicode_toupper[karacter]);

}


ucs_t ucs_mapax(ucs_t karacter) {

  // Unicode version!
  
  if (karacter >= unicode_axtablesize) return karacter;
  return(karacter + unicode_axmap[karacter]);

}


// ---------------------------------------------------------------------

int utf8_lower(byte *utf8out, byte *utf8in, int maxbytes) {
  // Converts upper case letters in utf8str to lower.  Up to maximum of
  // maxbytes input.   Actually, maxbytes + 5 bytes could 
  // potentially be scanned if a UTF-8 sequence starts within the limit
  // but extends beyond it.  Hopefully this won't happen or won't matter
  // if it does.

  // In any case output is truncated at or before maxbytes + 1.  A partial
  // UTF-8 character is not written at the end of the buffer.

  // Caller's responsibility to ensure that utf8out is large enough.
  // i.e. maxbytes + 1

  // Assume that length will not increase due to case conversion. (Assumption 
  // checked by  utf8_check_case_bytes( ) when indexer starts.
  // Input scanning terminates on null or newline

  // utf8in may not be null terminated but utf8out will always be.


  // Return output length in bytes

  byte *r = utf8in, *rr, buf7[7], *w = utf8out;
  int ilen, olen;
  ucs_t ucsc;

  if (0) printf("utf8_lower('%s', %d)\n", utf8in, maxbytes);

  rr = r;
  if (r - utf8in >= maxbytes) return 0;  // In case maxbytes is zero
  ucsc = utf8_getchar(&rr);
  while (ucsc && ucsc != '\n') {
    ilen = rr - r;
    if (ucs_isupper(ucsc)) {
      ucsc = ucs_tolower(ucsc);
      olen = ucs_to_utf8(ucsc, buf7);
      if (w + olen > (utf8out + maxbytes)) break;  // No room 4 this char (plus null)
      strncpy(w, buf7, olen);
      w += olen;  
    } else {
      if (w + ilen > (utf8out + maxbytes)) break;  // No room 4 this char (plus null)
      strncpy(w, r, ilen);
      w += ilen;
    }
    r = rr;
    if (r - utf8in >= maxbytes) {
      *w = 0;
      return (w - utf8out);
    }
    ucsc = utf8_getchar(&rr);
  }
  *w = 0;
  if (w - utf8out > maxbytes) {
    printf("Error: utf8_lower catastrophe: %ld > %d\n", (long)(w - utf8out),
           maxbytes);
    exit(1);
  }
  return (w - utf8out);
}


int utf8_lower_inplace(byte *utf8str) {
  // Converts upper case letters in utf8str (assumed null-terminated) to 
  // lower, in place.  

  // return 0 on success and -1 on error.  Only error is that the lowercase
  // version uses more bytes which can't happen because this case is checked
  // by  utf8_check_case_bytes( ) when indexer starts.
  // If the impossible does happen, a valid utf8 string will be left
  // in utf8str but upper case letters will be unconverted from the point
  // of errror.

  byte *r = utf8str, *rr, buf7[7], *w = utf8str;
  int ilen, olen;
  ucs_t ucsc;

  rr = r;
  ucsc = utf8_getchar(&rr);
  while (ucsc) {
    ilen = rr - r;
    if (ucs_isupper(ucsc)) {
      ucsc = ucs_tolower(ucsc);
      olen = ucs_to_utf8(ucsc, buf7);
      if (olen > ilen) return -1;
      strncpy(w, buf7, olen);
      w += olen;  
    } else {
      strncpy(w, r, ilen);
      w += ilen;
    }
    r = rr;
   ucsc = utf8_getchar(&rr);
  }
  *w = 0;
  return 0;
}


case_mode_t utf8_determine_case(byte *ustring, int maxbytes) {
  // ustring may not be null terminated.  Stop either on null, newline or maxbytes.


  // Return case mode of ustring.

  byte *r = ustring, *rr;
  int lc_cnt = 0, uc_cnt = 0, pos = 0;
  ucs_t ucsc;
  case_mode_t casetype = CM_ALL_LOWER;

  rr = r;
  if (maxbytes == 0) return casetype;  // In case maxbytes is zero
  ucsc = utf8_getchar(&rr);
  while (ucsc && ucsc != '\n') {
    if (ucs_isupper(ucsc)) {
      uc_cnt++;
      if (pos == 0) casetype = CM_FIRSTCAP;
    } else lc_cnt++;
    r = rr;
    if (r - ustring >= maxbytes) break;
    ucsc = utf8_getchar(&rr);
    pos++;
  }
  if (uc_cnt == 0) return CM_ALL_LOWER;  
  if (uc_cnt == 1 && casetype == CM_FIRSTCAP) return CM_FIRSTCAP;
  if (lc_cnt > 1) return CM_MIXED;
  return CM_ALLCAPS;
}

int utf8_firstcap_words(byte *out, byte *in, int inlen, int lower_the_rest){
  // Convert the first inlen bytes of in into out (assumed large enough),
  // capitalising the first letter of each word. Leave the rest unchanged?

  // Stopwords are not capitalised unless they start the string, unless
  // never_capitalise_stops.

  // If lower_the_rest is set, subsequent letters will be lowercased

  byte *r = in, *rr, buf7[7], *w = out;
  int ilen, olen, firstletter = 1, wd = 0;
  ucs_t ucsc;

  rr = r;
  ucsc = utf8_getchar(&rr);
  while (ucsc && ucsc != '\n' && inlen > 0) {
    ilen = rr - r;
    inlen -= ilen;
    if (ucsc == ' ') {
      firstletter = 1;
      *w++ = ' ';
    } else {      
      if (firstletter) {
        int capitalise = ucs_islower(ucsc);  // Prima facie        
        if (capitalise) {
          ucsc = ucs_toupper(ucsc);
          olen = ucs_to_utf8(ucsc, buf7);
          strncpy(w, buf7, olen);
          w += olen; 
        } else {
          // Should we force lower case if it wasn't to start with?
          // Hmmm, not yet.
          strncpy(w, r, ilen);
          w += ilen;
        }
        wd++;
      } else {
        // It's not the first letter of a word
        if (lower_the_rest && ucs_isupper(ucsc)) {
          ucsc = ucs_tolower(ucsc);
          olen = ucs_to_utf8(ucsc, buf7);
          strncpy(w, buf7, olen);
          w += olen; 
        } else {
          strncpy(w, r, ilen);
          w += ilen;
        }
      }
      firstletter = 0;
    }
    r = rr;
    if (inlen > 0) ucsc = utf8_getchar(&rr);
  }
  *w = 0;
  return (w - out);
}


void utf8_check_case_bytes( ) {

  // Validate the assumption that case conversion never increases the length
  // of the UTF-8 representation.
  // Check whether there are characters whose UTF-8 representation
  // requires a different number of bytes in lower and upper case
  // forms.
  ucs_t in, out;
  int lin, lout, violations = 0, verbose = 0;
  u_char bufin[7], bufout[7];
  
  for (in = 0; in <unicode_casetablesize; in++) {
    ucs_to_utf8(in, bufin);
    lin = strlen(bufin);

    out = ucs_tolower(in);
    ucs_to_utf8(out, bufout);
    lout = strlen(bufout);
    if (lin != lout) {
      if (verbose)
        printf("UTF8 length diff for UCS %d(%s) converting to lower %d (diff = %d)\n",
               in, bufin, out, lout - lin);
      if (lout > lin) violations++;
    }

    out = ucs_toupper(in);
    ucs_to_utf8(out, bufout);
    lout = strlen(bufout);
    if (lin != lout) {
      if (verbose)
        printf("UTF8 length diff for UCS %d(%s) converting to upper %d (diff = %d)\n",
               in, bufin, out, lout - lin);
    }
    if (lout > lin) violations++;

  }

  if (violations) {
    printf("Error: Case_conversion length increased.  Assumption VIOLATED.\n");
    printf("   Edit common/utf8.c to find why.\n");
    exit(1);
  } 
}


byte *utf8_getword(byte **text, int *nchars) {
  // Get and return next indexable word out of text.  Update text pointer
  // to point at first byte of first non-indexable char after the word.
  // 1. skip non-indexable chars
  // 2. scan the word.
  // Used to stop on any low control character (<= 4) or NULL, but now
  // stop only on NULL.
  // Return pointer to static storage
  // Avoids conversion of UCS back to UTF-8

  ucs_t u1;
  byte *b1 = *text, *b0, *wdstart, *wdend;
  int indexable = 0, len;
  static byte outbuf[MAX_LINE + 5];

  outbuf[0] = 0;
  *nchars = 0;

  while (1) {
    b0 = b1;
    u1 = utf8_getchar(&b1);
    if (u1 == 0) {
      *text = b0;
      return outbuf;
    }
    indexable = ucs_indexable(u1);
    if (indexable) {
      wdstart = b0;
      break;
    }
  }

  wdend = wdstart;
  
  while (indexable) {
    (*nchars)++;
    b0 = b1;
    u1 = utf8_getchar(&b1);
    if (u1 == 0) {
      wdend = b0;
      break;
    }
    indexable = ucs_indexable(u1);
    if (!indexable) {
      wdend = b0;
      break;
    }
  }
  // At this point we are certain there is at least one indexable char
  len = wdend - wdstart;
  if (len > MAX_LINE) {
    *text = wdend;
    outbuf[0] = 0;
    return outbuf;  // Ignore excessively long word.
  }
  memcpy(outbuf, wdstart, len);
  outbuf[len] = 0;
  *text = wdend;
  return(outbuf);
}




byte *utf8_get_lc_word(byte **text) {
  // Get and return lowercase version of next indexable word out of text.  
  // Update text pointer to point at first byte of first non-indexable 
  // char after the word.
  // 1. skip non-indexable chars
  // 2. scan and lowercase the word.
  // Stop on any low control character (<= 4) or NULL
  // Return pointer to static storage
  // Avoids conversion of UCS back to UTF-8

  ucs_t u1, u2;
  int indexable = 0, len, full = 0;
  static byte outbuf[MAX_LINE + 5], buf7[7];
  byte *b1 = *text, *b0, *op = outbuf, *oend = op + MAX_LINE;

  outbuf[0] = 0;
  while (1) {
    b0 = b1;
    u1 = utf8_getchar(&b1);
    if (u1 <= 4) {
      *text = b0;
      return outbuf;
    }
    indexable = ucs_indexable(u1);
    if (indexable) {
      break;
    }
  }
  
  while (indexable) {
    u2 = ucs_tolower(u1);
    len = ucs_to_utf8(u2, buf7);
    if (op + len > oend) {
      *op = 0;
      full = 1;
    }
    if (!full) strcpy(op, buf7);
    op += len;
    b0 = b1;
    u1 = utf8_getchar(&b1);
    if (u1 <= 4) break;
    indexable = ucs_indexable(u1);
    if (!indexable) break;
  }
  *op = 0;
  *text = b0;
  return(outbuf);
}

int utf8_countwords(byte *text) {
  // Count the number of indexable words in text.  
  // Stop on any low control character (<= 4) or NULL

  ucs_t u1;
  byte *b1 = text;
  int indexable = 0, cnt = 0;

  while (1) {
    u1 = utf8_getchar(&b1);
    if (u1 <= 4) {
      return cnt;
    }
    indexable = ucs_indexable(u1);
    if (indexable) {
      cnt++;
      while (indexable) {
        u1 = utf8_getchar(&b1);
        if (u1 <= 4) {
         return cnt;
        }
        indexable = ucs_indexable(u1);
      }
    }
  }
  return cnt;  // Actually can't ever happen
}


int utf8_truncate_after_n_words(byte *text, int n) {
  // Cut off str after n (whitespace delimited) words
  // Return 0 iff it was not cut off.
  // n >= 1
  ucs_t u1;
  byte *b1 = text;
  int indexable = 0, cnt = 0;

  while (1) {
    u1 = utf8_getchar(&b1);
    if (u1 <= 4) {
      return 0;
    }
    indexable = ucs_indexable(u1);
    if (indexable) {
      cnt++;
      while (indexable) {
        u1 = utf8_getchar(&b1);
        if (u1 <= 4) {
          return 0;
        }
        indexable = ucs_indexable(u1);
      }
      if (cnt == n) {
        *b1 = 0;
        return 1;
      }
    }
  }
  return 0;  // Actually can't ever happen
}

byte *unicode_string_to_utf8_string(ucs_t *unicode_string) {
  // Given a zero-terminated array of unicode integers create a string 
  // of UTF-8 bytes in malloced storage. 
  // Callers responsibility to free
  byte *rslt, buf[7], *rip;
  int l = 0;
  ucs_t *up = unicode_string;
  

  while (*up) {
    up++;
    l++;
  }

  rslt = femalloc(l * 6 + 1, 1, "unicode_string_to_utf8_string");
  up = unicode_string;
  rip = rslt;
  while (*up) {
    if (0) printf("About to u2unonull(%u)\n", *up);
    l = ucs_to_utf8_nonull(*up, buf);
    strncpy(rip, buf, l);
    rip += l;
    up++;
  }
  *rip =0;
  return rslt;
}



ucs_t *utf8_string_to_unicode_string(byte *utf8_string) {
  // Given a zero-terminated array of UTF-8 bytes create an array  
  // of Unicode integers in malloced storage. 
  // Callers responsibility to free
  byte *bp = utf8_string;
  int l = strlen(bp);
  ucs_t *rslt, *up;

  rslt = femalloc(l + 1, sizeof(ucs_t), "utf8_string_to_unicode_string");
  up = rslt;
  while (*bp) {
    *up = utf8_getchar(&bp);
    if (0) printf("   Got %X(%u)\n", *up, *up);
    up++;
  }
  *up = 0;
  return rslt;

}


int utf8_cjkt_strip_spaces(byte *string) {
  // In-place removal of spaces following CJKT characters in string.
  // Application: A word segmenter has been run over Chinese text and
  // inserted spaces between words so that they can be indexed using
  // indexer_option -no_cjkt_grams.  But Chinese people don't like to 
  // read document summaries with such spaces.  This function can be
  // used to remove the spaces.  Note: only remove after a CJKT character
  // in case the text contains words or numbers in roman alphabet 
  // Return the length of the resulting string.  Zero in case of error.
  
  byte *rr, buf7[7], *w = string;
  int olen, stripped = 0;
  ucs_t ucs;

  if (string == NULL) return 0;
  if (0) printf("  <!-- utf8_cjkt_strip_spaces(%s) -->\n", string);
  if (string[0] == 0) return 0;
  rr = string;
  ucs = utf8_getchar(&rr);
  while (ucs) {
    if (ucs_isCJKT_noexempt(ucs)) {
      olen = ucs_to_utf8(ucs, buf7);
      strncpy(w, buf7, olen);
      w += olen; 
      // We've seen a CJKT character.  Skip over following spaces
      while (*rr && isspace(*rr)) {
	rr++;
	stripped++;
      }
      if (*rr) ucs = utf8_getchar(&rr);
      else ucs = 0;
    } else {
      olen = ucs_to_utf8(ucs, buf7);
      strncpy(w, buf7, olen);
      w += olen;  
      ucs = utf8_getchar(&rr);
    }
  }     
  *w = 0;
  if (0) printf("  <!-- utf8_cjkt_strip_spaces(%s) returning [%d spaces stripped]-->\n", 
		       string, stripped);
  return (w - string);
}


typedef struct {
  // UCS version
  u_char *ent;
  int lent;
  u_int equiv;  // Unicode value
} entity_t;



// ----------------------------------------------------------------------------
// The following table created by unicode_tables/entities/make_entity_tables.pl
static entity_t ents[] = {
  {"AElig", 5, 198},
  {"Aacute", 6, 193},
  {"Acirc", 5, 194},
  {"Agrave", 6, 192},
  {"Alpha", 5, 913},
  {"Aring", 5, 197},
  {"Atilde", 6, 195},
  {"Auml", 4, 196},
  {"Beta", 4, 914},
  {"Ccedil", 6, 199},
  {"Chi", 3, 935},
  {"Dagger", 6, 8225},
  {"Delta", 5, 916},
  {"ETH", 3, 208},
  {"Eacute", 6, 201},
  {"Ecirc", 5, 202},
  {"Egrave", 6, 200},
  {"Epsilon", 7, 917},
  {"Eta", 3, 919},
  {"Euml", 4, 203},
  {"Gamma", 5, 915},
  {"Iacute", 6, 205},
  {"Icirc", 5, 206},
  {"Igrave", 6, 204},
  {"Iota", 4, 921},
  {"Iuml", 4, 207},
  {"Kappa", 5, 922},
  {"Lambda", 6, 923},
  {"Mu", 2, 924},
  {"Ntilde", 6, 209},
  {"Nu", 2, 925},
  {"OElig", 5, 338},
  {"Oacute", 6, 211},
  {"Ocirc", 5, 212},
  {"Ograve", 6, 210},
  {"Omega", 5, 937},
  {"Omicron", 7, 927},
  {"Oslash", 6, 216},
  {"Otilde", 6, 213},
  {"Ouml", 4, 214},
  {"Phi", 3, 934},
  {"Pi", 2, 928},
  {"Prime", 5, 8243},
  {"Psi", 3, 936},
  {"Rho", 3, 929},
  {"Scaron", 6, 352},
  {"Sigma", 5, 931},
  {"THORN", 5, 222},
  {"Tau", 3, 932},
  {"Theta", 5, 920},
  {"Uacute", 6, 218},
  {"Ucirc", 5, 219},
  {"Ugrave", 6, 217},
  {"Upsilon", 7, 933},
  {"Uuml", 4, 220},
  {"Xi", 2, 926},
  {"Yacute", 6, 221},
  {"Yuml", 4, 376},
  {"Zeta", 4, 918},
  {"aacute", 6, 225},
  {"acirc", 5, 226},
  {"acute", 5, 180},
  {"aelig", 5, 230},
  {"agrave", 6, 224},
  {"alefsym", 7, 8501},
  {"alpha", 5, 945},
  {"amp", 3, 38},
  {"and", 3, 8743},
  {"ang", 3, 8736},
  {"apos", 4, 39},
  {"aring", 5, 229},
  {"asymp", 5, 8776},
  {"atilde", 6, 227},
  {"auml", 4, 228},
  {"bdquo", 5, 8222},
  {"beta", 4, 946},
  {"brvbar", 6, 166},
  {"bull", 4, 8226},
  {"cap", 3, 8745},
  {"ccedil", 6, 231},
  {"cedil", 5, 184},
  {"cent", 4, 162},
  {"chi", 3, 967},
  {"circ", 4, 710},
  {"clubs", 5, 9827},
  {"cong", 4, 8773},
  {"copy", 4, 169},
  {"crarr", 5, 8629},
  {"cup", 3, 8746},
  {"curren", 6, 164},
  {"dArr", 4, 8659},
  {"dagger", 6, 8224},
  {"darr", 4, 8595},
  {"deg", 3, 176},
  {"delta", 5, 948},
  {"diams", 5, 9830},
  {"divide", 6, 247},
  {"eacute", 6, 233},
  {"ecirc", 5, 234},
  {"egrave", 6, 232},
  {"empty", 5, 8709},
  {"emsp", 4, 8195},
  {"ensp", 4, 8194},
  {"epsilon", 7, 949},
  {"equiv", 5, 8801},
  {"eta", 3, 951},
  {"eth", 3, 240},
  {"euml", 4, 235},
  {"euro", 4, 8364},
  {"exist", 5, 8707},
  {"fnof", 4, 402},
  {"forall", 6, 8704},
  {"frac12", 6, 189},
  {"frac14", 6, 188},
  {"frac34", 6, 190},
  {"frasl", 5, 8260},
  {"gamma", 5, 947},
  {"ge", 2, 8805},
  {"gt", 2, 62},
  {"hArr", 4, 8660},
  {"harr", 4, 8596},
  {"hearts", 6, 9829},
  {"hellip", 6, 8230},
  {"iacute", 6, 237},
  {"icirc", 5, 238},
  {"iexcl", 5, 161},
  {"igrave", 6, 236},
  {"image", 5, 8465},
  {"infin", 5, 8734},
  {"int", 3, 8747},
  {"iota", 4, 953},
  {"iquest", 6, 191},
  {"isin", 4, 8712},
  {"iuml", 4, 239},
  {"kappa", 5, 954},
  {"lArr", 4, 8656},
  {"lambda", 6, 955},
  {"lang", 4, 9001},
  {"laquo", 5, 171},
  {"larr", 4, 8592},
  {"lceil", 5, 8968},
  {"ldquo", 5, 8220},
  {"le", 2, 8804},
  {"lfloor", 6, 8970},
  {"lowast", 6, 8727},
  {"loz", 3, 9674},
  {"lrm", 3, 8206},
  {"lsaquo", 6, 8249},
  {"lsquo", 5, 8216},
  {"lt", 2, 60},
  {"macr", 4, 175},
  {"mdash", 5, 8212},
  {"micro", 5, 181},
  {"middot", 6, 183},
  {"minus", 5, 8722},
  {"mu", 2, 956},
  {"nabla", 5, 8711},
  {"nbsp", 4, 160},
  {"ndash", 5, 8211},
  {"ne", 2, 8800},
  {"ni", 2, 8715},
  {"not", 3, 172},
  {"notin", 5, 8713},
  {"nsub", 4, 8836},
  {"ntilde", 6, 241},
  {"nu", 2, 957},
  {"oacute", 6, 243},
  {"ocirc", 5, 244},
  {"oelig", 5, 339},
  {"ograve", 6, 242},
  {"oline", 5, 8254},
  {"omega", 5, 969},
  {"omicron", 7, 959},
  {"oplus", 5, 8853},
  {"or", 2, 8744},
  {"ordf", 4, 170},
  {"ordm", 4, 186},
  {"oslash", 6, 248},
  {"otilde", 6, 245},
  {"otimes", 6, 8855},
  {"ouml", 4, 246},
  {"para", 4, 182},
  {"part", 4, 8706},
  {"permil", 6, 8240},
  {"perp", 4, 8869},
  {"phi", 3, 966},
  {"pi", 2, 960},
  {"piv", 3, 982},
  {"plusmn", 6, 177},
  {"pound", 5, 163},
  {"prime", 5, 8242},
  {"prod", 4, 8719},
  {"prop", 4, 8733},
  {"psi", 3, 968},
  {"quot", 4, 34},
  {"rArr", 4, 8658},
  {"radic", 5, 8730},
  {"rang", 4, 9002},
  {"raquo", 5, 187},
  {"rarr", 4, 8594},
  {"rceil", 5, 8969},
  {"rdquo", 5, 8221},
  {"real", 4, 8476},
  {"reg", 3, 174},
  {"rfloor", 6, 8971},
  {"rho", 3, 961},
  {"rlm", 3, 8207},
  {"rsaquo", 6, 8250},
  {"rsquo", 5, 8217},
  {"sbquo", 5, 8218},
  {"scaron", 6, 353},
  {"sdot", 4, 8901},
  {"sect", 4, 167},
  {"shy", 3, 173},
  {"sigma", 5, 963},
  {"sigmaf", 6, 962},
  {"sim", 3, 8764},
  {"spades", 6, 9824},
  {"sub", 3, 8834},
  {"sube", 4, 8838},
  {"sum", 3, 8721},
  {"sup", 3, 8835},
  {"sup1", 4, 185},
  {"sup2", 4, 178},
  {"sup3", 4, 179},
  {"supe", 4, 8839},
  {"szlig", 5, 223},
  {"tau", 3, 964},
  {"there4", 6, 8756},
  {"theta", 5, 952},
  {"thetasym", 8, 977},
  {"thinsp", 6, 8201},
  {"thorn", 5, 254},
  {"tilde", 5, 732},
  {"times", 5, 215},
  {"trade", 5, 8482},
  {"uArr", 4, 8657},
  {"uacute", 6, 250},
  {"uarr", 4, 8593},
  {"ucirc", 5, 251},
  {"ugrave", 6, 249},
  {"uml", 3, 168},
  {"upsih", 5, 978},
  {"upsilon", 7, 965},
  {"uuml", 4, 252},
  {"weierp", 6, 8472},
  {"xi", 2, 958},
  {"yacute", 6, 253},
  {"yen", 3, 165},
  {"yuml", 4, 255},
  {"zeta", 4, 950},
  {"zwj", 3, 8205},
  {"zwnj", 4, 8204},
};

static int ent_cnt = 253;
// ----------------------------------------------------------------------------


static int entcmp(entity_t *key, entity_t *candidate) {
  return strncmp(key->ent, candidate->ent, candidate->lent);
}


// See http://unicode.org/Public/MAPPINGS/VENDORS/MICSFT/WINDOWS/CP1252.TXT
// for Windows to Unicode mappings

static ucs_t unicode_equivalent_of_windows_punc[32] = {
  0x20AC,  // Euro sign
  0x20,    // Undefined
  0x201A,  // SINGLE LOW-9 QUOTATION MARK
  0x0192,  //LATIN SMALL LETTER F WITH HOOK

  0x201E,  //DOUBLE LOW-9 QUOTATION MARK
  0x2026,  //HORIZONTAL ELLIPSIS
  0x2020,  //DAGGER
  0x2021,  //DOUBLE DAGGER


  0x02C6,  //MODIFIER LETTER CIRCUMFLEX ACCENT
  0x2030,  //PER MILLE SIGN
  0x0160,  //LATIN CAPITAL LETTER S WITH CARON
  0x2039,  //SINGLE LEFT-POINTING ANGLE QUOTATION MARK

  0x0152,  //LATIN CAPITAL LIGATURE OE
  0x20,    //UNDEFINED
  0x017D,  //LATIN CAPITAL LETTER Z WITH CARON
  0x20,    //UNDEFINED


  0x20,    //UNDEFINED
  0x2018,  //LEFT SINGLE QUOTATION MARK
  0x2019,  //RIGHT SINGLE QUOTATION MARK
  0x201C,  //LEFT DOUBLE QUOTATION MARK

  0x201D,  //RIGHT DOUBLE QUOTATION MARK
  0x2022,  //BULLET
  0x2013,  //EN DASH
  0x2014,  //EM DASH


  0x02DC,  //SMALL TILDE
  0x2122,  //TRADE MARK SIGN
  0x0161,  //LATIN SMALL LETTER S WITH CARON
  0x203A,  //SINGLE RIGHT-POINTING ANGLE QUOTATION MARK

  0x0153,  //LATIN SMALL LIGATURE OE
  0x20,    //UNDEFINED
  0x017E,  //LATIN SMALL LETTER Z WITH CARON
  0x0178  //LATIN CAPITAL LETTER Y WITH DIAERESIS
};

ucs_t ucs_convert_entity(u_char *str, int *width) {
  /* str might be an SGML entity representing a UCS character.
     (A scanning routine has encountered an entity-introducer ('&')
     and has passed a pointer to the character immediately following
     it.)  *width gives the length of str.  If str is null-terminated,
     *width should be set to a large number.

     Returns the character code and the width of the entity string.  

     Width includes the optional trailing semicolon (if present).
     Three types of entity are recognised:
       a - # followed by up to 8 decimal digits - value defined by digits
       b - a word from the entities table above - value from table
       c - some other sequence of no more than MAX_ENT_LEN letters
           followed by a semicolon. - value zero, width non-zero

     If str matches none of these types, return value zero, and
     width zero.
  */

  u_char *p = str;
  ucs_t val = 0;
  int c, lent, l = *width, showlen = 7;
  entity_t target, *found;

  if (0) {
    printf("\n ==== ucs_convert_entity(%d) ====\n", *width);
  }

  *width = 0;  /* default to failure */
  if (*p == '#') {
    if (l < 3) return 0;  /* It's too short */
    p++;
    for (c = 0; c < 7; c++) {
      if (*p == ';') break;  // If entity is e.g. &#39;
      if (*p < '0' || *p > '9') return 0;  /* It's a dud */
      val *= 10;
      val += (*p - '0');
      p++;
    }
    if (l >= 3 && *p == ';') *width = p - str + 1;
    else *width = 4;

    if (val >= 0X80 && val <= 0X9F) {
      // Insert special code for those people who use numeric entities
      // in this range to represent Windows 1252 punctuation, (contrary
      // to HTML standard).  This even happens in documents which declare
      // themselves to be UTF-8.  e.g. http://www.slq.qld.gov.au/
      // as at 24 Nov 2008.
                                                                           
      // The range comprises the 32 characters from 128 to 159 decimal, inclusive

      // In Unicode, these characters are controls so it is fairly 
      // safe to substitute the Unicode equivalents of the punctuation
      // marks.
      if (0) printf("Windows entity replacement: %X -> %X\n",
                           val, unicode_equivalent_of_windows_punc[val - 0X80]);
      return unicode_equivalent_of_windows_punc[val - 0X80];
    } else  return val;
  } else {
    if (l < MIN_ENT_LEN) return 0;   /* It's too short */

    if (0) {
      printf("\nucs_convert_entity(");
      if (showlen > MAX_ENT_LEN) showlen = MAX_ENT_LEN;
      putchars(str, showlen);
      printf(")\n");
    }
    target.ent = p;  // doesn't matter what the other fields are.
    found = bsearch(&target, ents, ent_cnt, sizeof(entity_t), 
		    (int (*)())entcmp);
   
    if (found) {

      val = found->equiv;
      lent = found->lent;
      if (*(p + lent) == ';') *width = lent + 1;
      else *width = lent;
      if (0) {
        printf("convert_entity: '");
        if (showlen > MAX_ENT_LEN) showlen = MAX_ENT_LEN;
        putchars(str, showlen);
        printf("' -> '");
        printf("' val = %u [%d]\n", val, *width);
      }
      return val;
    }
    if (0) printf("WE DIDN't FIND A MATCH\n");
    /* If we get here, we didn't find a match - look for some unknown
       entity name (up to MAX_ENT_LEN chars) followed by a semicolon. 
       Later refinement -  stop scanning the entity as soon as we
       find a null or a non-letter.

    */
    if (l > MAX_ENT_LEN) l = MAX_ENT_LEN;
    for (c = 0; c < l; c++) {
      if (str[c] == 0) return 0;
      if (str[c] == ';') {
        *width = c + 1;
        return 0;
      } else if (!isalpha(str[c])) return 0;
    }
    /* If we get here, it doesn't seem to be an entity of any kind. */
    return 0;   
  }
}



int replace_all_entities(byte *str, int strip_controls) {
  /* Scan null-terminated str and replace all recognized entities with their
     UTF-8 equivalent.

     *** Unrecognised entities are not replaced ***

     Inplace copying is fine - result must be no longer than the original.

     Note that apart from special handling of entities, copying is 
     byte for byte, doesn't matter whether they're parts of multibyte
     characters or not..

     Return the new string length.

     If strip_controls != 0 entities mapping to codes less than 32 decimal
     are dropped.

  */
  byte *r = str, *w = str, buf7[7];
  int ewid, u8wid;
  ucs_t e;

  if (str == NULL) {return 0;}
  if (0) printf("Replace_all_entities(%s)\n", str);

  while (*r) {
    if (*r == '&') {
      ewid = MAX_ENT_LEN;
      e = ucs_convert_entity(r+1, &ewid);
      if (ewid && e) {
        if (!strip_controls || e >= 32) {
          u8wid = ucs_to_utf8(e, buf7);	
          if (u8wid > ewid + 1) {
            printf(
                    "Error: Failed assertion - entity(%s) width < UTF-8 width\n",
                    r);
            return 0;
          } 
          strcpy(w, buf7);   // Annoying extra copy to avoid oflow.
          w += u8wid;
        }
        r += ewid + 1;  // Advancew read pointer even if ctrl-char stripped
        if (0) printf("replace_all_entities: %s, char = %d, ewid = %d\n", 
                      r, e, ewid);
      } else *w++ = *r++;  // Just copy over the &
    } else *w++ = *r++;  // Just copy over the byte.
  }
  *w = 0;
  if (0) printf("\nReplace_all_entities: result is '%s' (%ld)\n", str, (long) (w - str));
  return w - str;
}


