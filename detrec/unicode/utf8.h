/* Copyright (C) Funnelback Pty Ltd */
/*****************************************************************************/
/*                                                                           */
/*                   utf8.h  (David Hawking 07 Feb 2006)                     */
/*                                                                           */
/*****************************************************************************/

#define UTF8_INVALID_CHAR ' '  // Needs to be non-indexable, non-punctuation

#define MAX_ENT_LEN 7  // Max length of entities. Including neither amp nor semicolon
#define MIN_ENT_LEN 2

typedef struct {
  ucs_t first, last;
} unicode_block;


int utf8_to_iso88591_inplace(byte *str, int map_controls);

int utf8_to_iso88591_inplace_len(byte *str, int map_controls, int len);

ucs_t ucs_convert_entity(u_char *str, int *width);

int ucs_to_utf8(u_int unicode, byte *buf);

int ucs_to_utf8_nonull(u_int unicode, byte *buf);

int utf8_encode(byte *dest, byte *src, int max_outlen);

ucs_t ucs_from_hexseq(byte **hexbytes, int max_digits);

int utf8_from_url(byte *urlout, byte *urlin, int max_outlen);

ucs_t utf8_getchar(byte **sp);

ucs_t utf8_peekchar(byte *sp);

int utf8_strlen(byte *bytes);

int utf8_remove_invalid_chars(byte *bytes);

int utf8_remove_non_indexable(byte *string);

byte *utf8_find_nthchar(byte *bytes, int n);

int utf8_straxcmp(byte *str1, byte *str2, int nz, int deutsch);

void test_straxcmp();

int utf8_conflate_accents(byte *sout, byte *sin, int max_bytes);

int utf8_conflate_accents_inline(byte *sin);

#define MAX_EXTRA_IDX_CHARS 10

extern byte extra_indexable_ascii_chars[];

int ucs_indexable(ucs_t karacter);

int ucs_isupper(ucs_t karacter);

int ucs_islower(ucs_t karacter);

#define minCJKT 0X0E00

void suppress_cjkt_gramming();

int cjkt_gramming_is_suppressed();

int ucs_isCJKT(ucs_t karacter);

int ucs_isCJKT_noexempt (ucs_t karacter);

int string_contains_CJKT(byte *string);

int string_contains_accent(byte *string);

int string_contains_non_ascii_non_indexable(byte *string);

int utf8_asciisize_winpunc(byte *string);

ucs_t ucs_tolower(ucs_t karacter);

ucs_t ucs_toupper(ucs_t karacter);

ucs_t ucs_mapax(ucs_t karacter);

int utf8_lower(byte *utf8out, byte *utf8in, int maxbytes);

int utf8_lower_inplace(byte *utf8str);

case_mode_t utf8_determine_case(byte *ustring, int maxbytes);

int utf8_firstcap_words(byte *out, byte *in, int inlen, int lower_the_rest);

void utf8_check_case_bytes( );

byte *utf8_getword(byte **text, int *nchars);

byte *utf8_get_lc_word(byte **text);

int utf8_countwords(byte *text);

int utf8_truncate_after_n_words(byte *str, int n);

byte *unicode_string_to_utf8_string(ucs_t *unicode_string);

ucs_t *utf8_string_to_unicode_string(byte *utf8_string);

int utf8_cjkt_strip_spaces(byte *string);
