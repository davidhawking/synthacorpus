/* Copyright (C) Funnelback Pty Ltd */
/*****************************************************************************/
/*                                                                           */
/*             utf8_mapping.c  (David Hawking 03 Apr 2012)                   */
/*                                                                           */
/*****************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>

#include "../detrec.h"
#include "utf8.h"
#include "chinese_init.h"
#include "utf8_mapping.h"


ucs_t map_ucs(ucs_t inchar, unimap_t *mapping, int num_mappings) {
  // Look up inchar in mapping.  If found return the RHS of the 
  // the mapping, otherwise return inchar.

  // On error return inchar

  unimap_t tmap, *found;

  if (mapping == NULL || num_mappings <= 0) 
    return inchar;

  tmap.lhs = inchar;
  found = (unimap_t *) bsearch(&tmap, mapping, num_mappings,
                                 sizeof(unimap_t), (int (*)())unimap_cmp);
 
  if (found == NULL) {
    if (0) printf("  <!-- map_ucs: %X not affected by mapping -->\n",
                         inchar);
    return inchar;
  } else {
    if (0) printf("  <!-- map_ucs: %X mapped to %X -->\n",
                         inchar, found->rhs);
    return found->rhs;
  }
}







byte *utf8_mapchars(byte *input, unimap_t *mapping, int num_mappings,
  int outsize) {
  // Use the conversion table mapping to map each of the UTF-8 characters in 
  // input which appear in the left hand side of a mapping.  It's possible 
  // for the output to be longer than the input, so output is in storage 
  // malloced to be outsize + 1.  
  // input assumed null-terminated. Output will always be null-terminated.
  // On error, return NULL.
  // Caller's responsibility to estimate outsize (though the length is
  // checked) and to free the result

  int outcnt, chlen;
  byte *rslt, *bin, *bout, bufout[7];
  unimap_t tmap, *found;


  if (mapping == NULL || num_mappings <= 0 || outsize < 1) return NULL;

  outcnt = 0;
  
  rslt = femalloc(outsize + 1, 1, "cv2simplified");
  bout = rslt;
  bin = input;
  while (*bin) {
    tmap.lhs = utf8_getchar(&bin);
    found = (unimap_t *) bsearch(&tmap, mapping, num_mappings,
                                 sizeof(unimap_t), (int (*)())unimap_cmp);
 
    if (found == NULL) {
      // just output the original character
      ucs_to_utf8(tmap.lhs, bufout);
    } else {
      // output the conversion
      ucs_to_utf8(found->rhs, bufout);
    }

    // Copy the characters but only if there's room.
    chlen = strlen(bufout);
    if (outcnt + chlen <= outsize) {
      strcpy(bout, bufout);
      bout += chlen;
      *bout = 0;
    } else {
      *bout = 0;
      break;
    }
  }
  return rslt;
}

static ucs_t test_string[] =  {0x346F, 65, 66,  0x4330, 0x2A2FF, 67, 0x29944, 0},
  shouldbe_string[] =  {0x3454, 65, 66, 0x26219, 0x2A38D, 67, 0x29A0B, 0};




void test_utf8_mapping() {
  // Takes an array of u_ints representing a mixture of Traditional
  // unicode points and ascii characters:
  //   1. convert it to UTF-8
  //   2. Use utf8_mapchars() to map the traditional chars
  //      into their simplified equivalents.
  //   3. Convert the mapped UTF-8 string back into an array
  //      of u_ints representing the mix of ASCII and simplified.
  //   4. Check that the new array contains what it should do.

  byte *orig8, *mapped;
  ucs_t *converted, *sbp, *cp;
  int cnt = 0, errors = 0;

  orig8 = unicode_string_to_utf8_string(test_string);
  mapped = utf8_mapchars(orig8, to_simplified, num_simp_chinese_mappings,
                         2000);
  converted = utf8_string_to_unicode_string(mapped);
  sbp = shouldbe_string;
  cp = converted;
  while (*sbp) {
    if (*sbp != *cp) {
      printf("  <!-- Error: in byte %d -- %X v %X -->\n", cnt, *cp, *sbp);
      errors++;
      break;
    }
    cp++;
    sbp++;
    cnt++;
  }

  free(orig8);
  free(mapped);
  free(converted);

  if (errors) printf("  <!-- test_utf8_mapping() - FAILED -->\n");
  else printf("  <!-- test_utf8_mapping() - PASSED (%d unicode vals) -->\n", cnt);
}
