/* Copyright (C) Funnelback Pty Ltd */
/*****************************************************************************/
/*                                                                           */
/*               utf8_mapping.h  (David Hawking 03 Apr 2012)                 */
/*                                                                           */
/*****************************************************************************/

extern ucs_t map_ucs(ucs_t inchar, unimap_t *mapping, int num_mappings);

extern byte *utf8_mapchars(byte *input, unimap_t *mapping, int num_mappings,
                           int outsize);
 
extern void test_utf8_mapping();

