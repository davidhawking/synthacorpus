/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*	                                            		      */
/*                             index/warcfile.c                       */
/*	                                            		      */
/**********************************************************************/

/*
  All the code for indexing WARCfiles used to be in scan.c but 
  for maintainability/understandability reasons it's now here.

*/ 

#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h> 
#include <fcntl.h>
#include <stdlib.h>

#include "detrec.h"
#include "zlib/zlib.h"
#include "pcre/pcre.h"
#include "scan.h"
#include "extractor.h"

typedef enum {WARC_RESPONSE, WARC_RESOURCE,
              WARC_OTHER} warctype_t;

static long int warc_content_length, warc_unco_content_length;
static int warc_header_bytes, warc_expect_http_header, warc_show_next_line = 0;
static warctype_t warc_record_type;
static u_char warc_format[81];
static u_char warc_url[MAX_URL_LEN + 1];
static u_char *warc_access_locks = NULL;


#define WARC_MAX_HDR_LEN 10000

static void clear_warc_globals() {
  warc_content_length = 0;
  warc_unco_content_length = 0;
  warc_header_bytes  = 0;
  warc_expect_http_header = 0;
  warc_record_type = WARC_OTHER;
  warc_format[0] = 0; 
  warc_url[0] = 0;
  if (warc_access_locks) {
      free(warc_access_locks);
      warc_access_locks = NULL;
  }
}


static void print_warc_globals() {
  printf("WARC/%s record: length: %ld, type: %d, http hdr: %d, url: '%s'\n", 
  warc_format, warc_content_length, (int)warc_record_type, warc_expect_http_header, 
  warc_url);
}


int format_message_shown = 0;

static void process_warc_header_line(u_char *line, int line_len) {
  /*  ClueWeb Example
WARC/0.18
WARC-Type: response
WARC-Target-URI: http://00000-nrt-realestate.homepagestartup.com/
WARC-Warcinfo-ID: 993d3969-9643-4934-b1c6-68d4dbe55b83
WARC-Date: 2009-03-65T08:43:19-0800
WARC-Record-ID: <urn:uuid:67f7cabd-146c-41cf-bd01-04f5fa7d5229>
WARC-TREC-ID: clueweb09-en0000-00-00000
Content-Type: application/http;msgtype=response
WARC-Identified-Payload-Type:
Content-Length: 16558
   */

  /* UK2006 example
warc/0.9 10757 response http://www.mattenltd.co.uk/ 20060920234350 message/http uuid:c6f7927d-aaea-4e53-b121-c4a594218d8a
BUbiNG-guessed-charset: iso-8859-1
BUbiNG-content-digest: eceb3de18182b34aedab72fb9a823464
 */

  int l;
  u_char *p, *q, *w;
  if (warc_show_next_line) 
    printf("process_warc_header_line(%d, %s)\n", line_len, line);
  warc_show_next_line = 0;
  if (!strncasecmp(line, "warc", 4)) {
    // =========================  Lines beginning with WARC or warc============
    if (line[4] == '/') {
      // ------------------------------  Line starting with WARC format -------------
      trec_docno[0] = 0;
      p = line + 5;
      while (*p > ' ') p++;      
      l = p - line - 5;
      if (l > 80) {
        fprintf(stderr, "Error Exit: WARC format string impossibly long: '%s'\n", line);
        exit(1);
      }
      q = line + 5;
      w = warc_format;
      while (q < p) *w++ = *q++;  
      *w = 0;
      if (!strcmp(warc_format, "0.9")) {
        // .....WARC0.9 All headers on a single line -- as in UK2006/7 .......
        
        // Start with the content-length
        p = line + 9;
        warc_content_length = strtol((const char *)p, (char **)&q, 10);

        // Then the record type
        p = q;
        while (isspace(*p)) p++;
        if (!strncasecmp(p, "response", 8)) {
          warc_record_type = WARC_RESPONSE;
          warc_expect_http_header = 1;
          // Now grab the URL after skipping whitespace
          p += 8;
          while (isspace(*p)) p++;
          q = p;
          while (*q > ' ') q++;
          l = q - p;
          if (l > MAX_URL_LEN) {
            printf("Warning: WARC url '%s' too long.  > %d\n", line, 
            MAX_URL_LEN);
             strcpy(warc_url, "http://url.too.long/");
          } else {
            strncpy(warc_url, p, l);
            warc_url[l] = 0;
          }
        } else {
          warc_record_type = WARC_OTHER;
        }
        // ...................................................................
      } else if (strcmp(warc_format, "0.18")) {
	if (format_message_shown < 1) {
	  printf("\n\nWarning: WARC format '%s' not currently supported. We may come to grief.\n"
		 "  --- further warnings suppressed.\n\n", warc_format);
	  format_message_shown++;
	}
      }
    } else if (line[4] == '-') {
      // ---------------------------------  Other WARC lines ----------------------
      // Currently only interested in:
      // WARC-Type: response
      // WARC-Target-URI: http://00000-nrt-realestate.homepagestartup.com/
      p = line + 5;
      if (!strncasecmp(p, "Type: ", 6)) {
        p += 6;
        while (isspace(*p)) p++;
        
        if (!strncasecmp(p, "response", 8)) 
          warc_record_type = WARC_RESPONSE;
        else if (!strncasecmp(p, "resource", 8))
          warc_record_type = WARC_RESOURCE;
        // Will have to add code to distinguish and deal with redirects
        else warc_record_type = WARC_OTHER;
      } else if (!strncasecmp(p, "Target-URI: ", 12)) {
        p += 12;
        while (isspace(*p)) p++;
        
        q = p;
        while (*q > ' ') q++;
        l = q - p;
        if (l > MAX_URL_LEN) {
          printf("Error: WARC url '%s' too long.  > %d\n", line, MAX_URL_LEN);
           strcpy(warc_url, "http://url.too.long/");
        } else {
          url_decode( warc_url, p, MAX_URL_LEN, &l);
          //warc_url[l] = 0;  not needed, url_decode null terminates output
        }
      } else if (!strncasecmp(p, "TREC-ID: ", 9)) {
        p += 9;
        while (isspace(*p)) p++;
        q = p;
        while (*q > ' ') q++;
        l = q - p;
        if (l > MAX_DOCNAME_LEN) l = MAX_DOCNAME_LEN;
        strncpy(trec_docno, p, l);
        trec_docno[l] = 0;
        //if (0) printf("TREC-ID: %s\n", trec_docno);
      }
      // --------------------------------------------------------------------------
    }
  } else if (!strncasecmp(line, "Content-", 8)) {
    // ================================  Lines beginning with Content- ============
    // Currently only interested in:
    // Content-Length: 16558
    // Content-Type: application/http;msgtype=response
    p = line + 8;
    if (!strncasecmp(p, "Length: ", 8)) {
      p += 8;
      warc_content_length = strtol(p, NULL, 10);
    } else if (!strncasecmp(p, "Type: ", 6)) {
      p += 6;
      while (isspace(*p)) p++;
      if (!strncasecmp(p, "application/http", 16)) {
        if (0) printf("Found application/http - Ignore it\n");
      }
    }
    // ============================================================================

#if 0
  } else if (!strncasecmp(line, "Compression-Mode: ", 18)) {
    /*
      From Cliff Henderson - JIRA: FUN-....
      in my warc files the content of records will be compressed if it has a
      "Compression-Mode" field with a value of contentonly. Such records
      should also have an "Uncompressed-Content-Length" field containing the
      original uncompressed length. The "Content-Length" field will always
      contain the length of the content present in the warc file so other
      warc file programs can skip over the compressed content. Thought: is
      there a mime type to denote such content is also gzip compressed? if
      so then could also adjust the content type for compatibility with
      other programs. Will look into it.
      
      Other valuses for "Compression-Mode" are:
      
      * none no compression
      * all full gzip compression of entire record (not currently used)
      
      Record content is assumed to be uncompressed if it has no
      "Compression-Mode" field.
      
      Every record should be checked since my routines will not compress
      content that is uncompressible (eg jpeg, zip) or too small (can result
      in larger "compressed" content than the original).

     */
    p = line + 18;
    if (!strncasecmp(p, "contentonly", 11)) {
      if (0) printf("WARC: This record is gzipped\n");
    }
    // ============================================================================
#endif

  } else if (!strncasecmp(line, "Uncompressed-Content-Length: ", 29)) {
    p = line + 29;
    warc_unco_content_length = strtol(p, NULL, 10);

    if (0) printf("WARC: This gzipped content expands to %ld bytes\n", 
                  warc_unco_content_length);
  }    
  warc_show_next_line = 0;

  // Just quietly ignore unrecognized header lines.
}


static off_t process_warc_content_block(u_char *cpp, off_t warcoff, FILE *OUTFILE) {
  // We now have a WARC content block which starts at chamberpot 
  // and is of length cpp - chamber.  Our job is to decompress it
  // if necessary and then set up to call process_doc()

  // We assume that the block is compressed if warc_unco_content_length
  // is non-zero and different from warc_content_length

  // warcoff is the offset within the [uncompressed] warcfile of the
  // start of the current warc record.

  int uncolen = 0, dnwib = 0;
  off_t data_indexed = 0;

  if (0) printf("process_warc_content_block %ld - %ld\n", 
                       warc_unco_content_length,
                       warc_content_length);          

  if (warc_unco_content_length > 0 
      &&  warc_unco_content_length != warc_content_length) {
    // Need to decompress
    byte *tmpbuf;
    z_stream zs;
    int rslt;

    if (warc_unco_content_length > CHAMBER_SIZE) {
      printf("\nError: Uncompressed WARC content block (%ld) too large for chamber(%d).\n"
             "Skipping.\n", warc_unco_content_length, CHAMBER_SIZE);
      return 0;
    }

    // Check the GZIP header ID and compression method 
    if (chamberpot[0] != 0x1F || chamberpot[1] != 0x8B || chamberpot[2] != 8)  {
      printf("\nError: Chamber doesn't start with a gzip header.  First bytes are %X, %X, %X\n", 
             chamberpot[0], chamberpot[1], chamberpot[2]);
      return 0;
    }


    tmpbuf = (byte *) femalloc(warc_unco_content_length + 10, 1, 
                               "process_warc_content_block()");

    zs.next_in = chamberpot;
    zs.avail_in = cpp - chamberpot;
    zs.next_out = tmpbuf;
    zs.avail_out = warc_unco_content_length;
    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = Z_NULL;
      
    rslt = inflateInit2(&zs, 31);  // Should handle gzip and zlib headers
    if (rslt != Z_OK) {
      printf("Error: inflateInit2() failed in process_warc_content_block()\n"
             "Version is %s sizeof(z_stream) =  %lu\n", ZLIB_VERSION,
             (u_long)sizeof(z_stream));
      if (rslt == Z_DATA_ERROR) printf("DATA_ERROR\n");
      else if (rslt == Z_MEM_ERROR) printf("MEM_ERROR\n");
      else if (rslt == Z_STREAM_ERROR) printf("STREAM_ERROR\n");
      else if (rslt == Z_VERSION_ERROR) printf("VERSION_ERROR\n");
      else printf("OTHER_ERROR %d\n", rslt);
      return 0;
    }

    rslt = inflate(&zs, Z_FINISH); // Z_FINISH: Process entire input in one call
    if (rslt != Z_STREAM_END) {
      printf("Error: inflate() failed in process_warc_content_block().\n"
             "Code was: %d\n", rslt);
      return 0;
    }
    
    rslt = inflateEnd(&zs);
    if (rslt != Z_OK) {
      printf("Error: inflateEnd() failed in process_warc_content_block().\n"
             "Code was: %d\n", rslt);
      return 0;
    }

    
    // Inflation succeeded.  Copy the uncompressed data over the compressed 
    // stuff and adjust cpp
    memcpy(chamberpot, tmpbuf, warc_unco_content_length);
    cpp = chamberpot + warc_unco_content_length;
    free(tmpbuf);
  }

  doc_starts[0] = chamberpot;
  doc_starts[1] = cpp;
  conversion_buf = cpp + 1;
  uncolen = cpp - chamberpot + 1;
  cbsize = CHAMBER_SIZE - uncolen;
  data_indexed += uncolen;
  if (0) printf("WARC: about to call process_doc, uncolen = %d\n", uncolen);
  if (warc_record_type == WARC_RESPONSE)// Call to process_doc() -------->
    process_doc(0, warc_url, warc_access_locks, 0, 0, HDR_YES, dnwib, warcoff, OUTFILE); 
  else if (warc_record_type == WARC_RESOURCE)  
    process_doc(0, warc_url, warc_access_locks, 0, 0, HDR_POSSIBLE, dnwib, warcoff, OUTFILE); 
  else 
    process_doc(0, warc_url, warc_access_locks, 0, 0, HDR_NO, dnwib, warcoff, OUTFILE); 
    
  dnwib++;
  return data_indexed;
}


typedef enum {SCANNING_A_HEADER,
              HEADER_CR_SEEN, 
              SCANNING_AN_HTTP_HEADER,
              HTTP_HEADER_CR_SEEN,
              ACCUMULATING_RECORD,
              END_OF_BLOCK,
              ERROR_RECOVERY} warcstate_t;

  // -------------- WARCfile Examples:  UK-2006/7 ---------------------------

  /*
warc/0.9 10757 response http://www.mattenltd.co.uk/ 20060920234350 message/http uuid:c6f7927d-aaea-4e53-b121-c4a594218d8a
BUbiNG-guessed-charset: iso-8859-1
BUbiNG-content-digest: eceb3de18182b34aedab72fb9a823464

HTTP/1.1 200 OK
content-type: text/html
accept-ranges: bytes
connection: close
server: Apache/1.3.33 (Unix) mod_auth_passthrough/1.8 mod_log_bytes/1.2 mod_bwlimited/1.4 FrontPage/5.0.2.2635 mod_ssl/2.8.22 OpenSSL/0.9.7a PHP-CGI/0.1b
content-length: 10123
last-modified: Thu, 13 Oct 2005 22:41:57 GMT
etag: "4f1df5-278b-434ee2b5"
date: Wed, 26 Apr 2006 13:11:05 GMT
ubi-http-equiv-charset: iso-8859-1

<html>
...
</html>


warc/0.9 60166 response http://www.bedroomdoors.co.uk/ 20060920234350 message/http uuid:ab896c57-ee03-44c5-b2e3-5f14c71aae4d


WARC/0.18 is very different.  See http://archive-access.sourceforge.net/warc/

---------------------------- ClueWeb09  example: ---------------------------

WARC/0.18
WARC-Type: warcinfo
WARC-Date: 2009-03-65T08:43:19-0800
WARC-Record-ID: <urn:uuid:993d3969-9643-4934-b1c6-68d4dbe55b83>
Content-Type: application/warc-fields
Content-Length: 219

software: Nutch 1.0-dev (modified for clueweb09)
isPartOf: clueweb09-en
description: clueweb09 crawl with WARC output
format: WARC file version 0.18
conformsTo: http://www.archive.org/documents/WarcFileFormat-0.18.html

WARC/0.18
WARC-Type: response
WARC-Target-URI: http://00000-nrt-realestate.homepagestartup.com/
WARC-Warcinfo-ID: 993d3969-9643-4934-b1c6-68d4dbe55b83
WARC-Date: 2009-03-65T08:43:19-0800
WARC-Record-ID: <urn:uuid:67f7cabd-146c-41cf-bd01-04f5fa7d5229>
WARC-TREC-ID: clueweb09-en0000-00-00000
Content-Type: application/http;msgtype=response
WARC-Identified-Payload-Type:
Content-Length: 16558

HTTP/1.1 200 OK
...
-----------------------------------------------------------------------------
  */


#define ZBUFSZ 1000000
off_t process_warcfile(u_char *filepath, FILE *OUTFILE) {
  // This is a generalised version of process_uk2006() which was 
  // based on Tim Jones's splitting code (C++), modified to 
  // remove all the directory and file creation and to extract and
  // index a document at a time.

  // Each uncompressed document is copied into the decompression chamber,
  // ready for character set conversion then indexing.

  // The finite state machine starts in SCANNING_A_HEADER state
  // in which it scans a line, copying characters into chamberpot,
  // up to the CR when it switches into HEADER_CR_SEEN state.  
  // This state scans the next character.
  // If it is LF and the line is empty, state changes either 
  // into ACCUMULATING_RECORD or SCANNING_AN_HTTP_HEADER depending upon
  // whether previous headers told us that this was a Response record
  // of content-type application/http, otherwise the non-empty line
  // is passed to process_warc_header().

  // In ACCUMULATING_RECORD state the specified number of characters 
  // are read into chamberpot and process_doc() is called.

  // In SCANNING_AN_HTTP_HEADER state, each HTTP header line is processed
  // by process_http_header(), using similar state transitions to
  // those made during processing WARC headers.

  // Each header line ends with CRLF.  In warc/0.9 there is only one
  // actual header line.
  // Warc headers end with an empty line (CRLF).  
  // Warc record content blocks and one header line at a time are stored in chamberpot
  // (not simultaneously).

  // Annoyingly, ClueWeb09 doesn't seem to use CRLFs, just LF. So let's
  // simplify the logic and just ignore CR when it appears.

  // Annoyingly, the length given in a WARC/0.9 record includes the headers
  // as well as the block (but not the two CRLFs after the block), whereas
  // in WARC/0.18 it's only the length of the block.

  gzFile UNCO = NULL;
  u_char *cpp = chamberpot, error_reset[] = "\nwarc/";
  int i, err, red = 0, error_reset_index = 0; 
  u_char *inbuf = femalloc(1, ZBUFSZ, "process_warcfile");
  u_int record_len = 0;
  warcstate_t state = END_OF_BLOCK;  
  off_t total_red = 0, total_data_indexed = 0, warcoff = -1, warcrecstart = -1;

 
  UNCO = gzopen(filepath, "rb");
  if (UNCO == NULL) {
    printf("Error: WARC: Couldn't open %s via zlib\n\n", filepath);
    free(inbuf);
    return (off_t) 0;
  }


  warc_flag = 1; // For the benefit of process_docheader.
  clear_warc_globals();

  do {  // --- loop to read entire archive one buffer at a time
    red = gzread(UNCO, inbuf, ZBUFSZ);
    total_red += red;

    for (i = 0; i < red; i++) {  // --- loop to process characters in buffer
      warcoff++;
      switch(state) {
				case ACCUMULATING_RECORD:
					if(record_len > 1) {
            *cpp++ = inbuf[i];
						record_len--;
					} else {
            *cpp++ = inbuf[i];
            *cpp = 0;
            // ------- At this point, we have a full warc record.  It
            // starts at chamberpot and is of length cpp - chamberpot
            if (0) {
              printf("\nInput char '%c'(%d). Chamber starts with '",
              inbuf[i], inbuf[i]);
              putchars(chamberpot, 30);
              printf("'\n  and ends with '");
              putchars(cpp - 30, 30);
              printf("'\n");
              printf("Length was %ld\n", (long) (cpp - chamberpot));
            }

            // if (warc_record_type == WARC_RESPONSE && warc_http_code[0] == '2') {
            if (warc_record_type == WARC_RESPONSE) {
              total_data_indexed += process_warc_content_block(cpp, warcrecstart, OUTFILE);  
            }

            if (warc_record_type == WARC_RESOURCE) {
              total_data_indexed += process_warc_content_block(cpp, warcrecstart, OUTFILE);
            }

            state = END_OF_BLOCK;
					}
					break;
				case END_OF_BLOCK:
          if (0) printf("Got to END_OF_BLOCK Char is '%c'(%d)\n",inbuf[i],
          inbuf[i]);
          // Just skip over whitespace, newlines between the end of the480
	 
          // block and the start of the next warc record.
          if (!isspace(inbuf[i])) {
            if (tolower(inbuf[i]) != 'w') {
              printf("Error: parsing of WARC records is out of step.\n");
              state = ERROR_RECOVERY;
              error_reset_index = 0;
              if (inbuf[i] != '\n') error_reset_index = 1;
              break;
            }
            cpp = chamberpot;
            clear_warc_globals();
            warcrecstart = warcoff;
            state = SCANNING_A_HEADER;
            *cpp++ = inbuf[i];
            warc_header_bytes++;
          }
					break;
        case ERROR_RECOVERY:
          // Keep scanning until we've seen the full error_reset string
          // i.e. "\nwarc/\"
          if (tolower(inbuf[i]) == error_reset[error_reset_index]) {
            if (error_reset_index  >= 5) {
              // Found the complete reset string, copy the warc part
              // into chamberpot etc and change to SCANNING_A_HEADER state.
              clear_warc_globals();
              state = SCANNING_A_HEADER;
              strcpy(chamberpot, error_reset + 1); // Don't copy linefeed
              cpp = chamberpot + 5;
              printf("Error recovery: found a warc header line\n");
              warc_show_next_line = 1;
              warc_header_bytes += 5;
            } else error_reset_index++;
          } else error_reset_index = 0;

					break;
				case SCANNING_A_HEADER:
          // Just copy the characters from current line into chamberpot
          // until we encounter a LF.
          if (0) printf("Got to SCANNING_A_HEADER Char is '%c'(%d)\n",inbuf[i],
          inbuf[i]);
          warc_header_bytes++;
          if (inbuf[i] == '\012') {
            // End of a WARC header line
            *cpp = 0;
            if (cpp - chamberpot < 1) {
              // Empty header line
              record_len = warc_content_length;
              if (0) 
                printf("SCANNING_A_HEADER: Found empty line.  Content len: %d expect: %d\n",
                record_len, warc_expect_http_header);
              if (!strcmp(warc_format, "0.9")) record_len -= warc_header_bytes;
              if (warc_expect_http_header) state = SCANNING_AN_HTTP_HEADER;
              else {
                if (record_len > CHAMBER_SIZE) {
                  printf("\nWarning:  overlong (%ld bytes) document will be truncated to %ld.\n  (Further error messages will  no doubt ensue.)\n", (long) record_len,
                  (long) CHAMBER_SIZE);
                  record_len = CHAMBER_SIZE;
                }
                state = ACCUMULATING_RECORD;
                if (0) print_warc_globals();
                if (0)
                  printf("\n ** About to accumulate block of %d bytes **\n", 
                  record_len);
              }
            } else {           
              process_warc_header_line(chamberpot, cpp - chamberpot);
              state = SCANNING_A_HEADER;
            }
            cpp = chamberpot;
          } else if (inbuf[i] != '\015') {
            *cpp++ = inbuf[i];
          }
          break;
				case SCANNING_AN_HTTP_HEADER:
          // Just copy the characters from current line into chamberpot
          // until we encounter a LF
          // Reduce the record_len by one for each character scanned.
          record_len--;
          if (inbuf[i] == '\012') {
            // End of an HTTP header line
            *cpp = 0;
            if (cpp - chamberpot < 1) {
              // Empty header line
              if (0) print_warc_globals();
              if (0)
                printf("\n *** About to accumulate block of %d bytes ***\n", 
                record_len);
              // There may be no content other than the HTTP header
              if (record_len <= 0) {
                state = SCANNING_A_HEADER;
                clear_warc_globals();
              } else {
                if (record_len > CHAMBER_SIZE) {
                  printf("\nWarning:  overlong (%ld bytes) document will be truncated to %ld.\n  (Further error messages will  no doubt ensue.)\n", (long) record_len,
                  (long) CHAMBER_SIZE);
                  record_len = CHAMBER_SIZE;
                }
                state = ACCUMULATING_RECORD;
              }
            } else {           
              process_http_header_line(chamberpot, cpp - chamberpot);
              state = SCANNING_AN_HTTP_HEADER;
            }
            cpp = chamberpot;
          } else if (inbuf[i] != '\015') {
            *cpp++ = inbuf[i];
          }
          break;
				default:
					printf("Error: WARC: Finite state machine derailed.\n");
			}
		}
	} while (red > 0);

  err = gzclose(UNCO);
  if (err) {
    printf("Error: WARC: couldn't [gz]close %s\n", filepath);
  }
  free(inbuf);
  return(total_data_indexed);  // Doesn't include WARC or HTTP headers,
                               // just the data passed to process_doc()
}


