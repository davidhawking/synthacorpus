/* Copyright (C) Funnelback Pty Ltd */
/**********************************************************************/
/*	                                            		      */
/*                          index/warcfile.h                          */
/*	                                            		      */
/**********************************************************************/

/*
   Functions for indexing and otherwise manipulating warcfiles
                        David Hawking 11 May 2010
*/ 

extern off_t process_warcfile(u_char *filepath, FILE *OUTFILE);

extern void warc_tidyup();
