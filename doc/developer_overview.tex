\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{url}
\usepackage{color}
\usepackage[dvipsnames]{xcolor}
\usepackage{listings}

\newcommand{\red}[1]{{\color{red}{\emph{#1}}}}
\newcommand{\projectName}{\texttt{SynthaCorpus}}
\newcommand{\projectRoot}{\texttt{.../SynthaCorpus}}

\title{Developer Overview of \projectName}
\author{Microsoft, subsequently updated and extended by David Hawking}

\begin{document}
\maketitle{}
The \projectName{}project home is at
\url{https://bitbucket.org/davidhawking/synthaCorpus}.  To use it:

\begin{verbatim}
git clone "https://bitbucket.org/davidhawking/synthaCorpus"
\end{verbatim}


The \projectName{} project is written in C11 and perl 5.  Development
was originally carried out under cygwin on Windows 10, using gcc version
5.4.0 and perl 5.22.  Since 2018, development has continued under
MacOS, using the LLVM compiler.   We don't expect major problems porting to
other environments.  However, because large data sizes lead to heavy
memory demands, there is a requirement that the executables will
be built for and run on a 64-bit architecture.

In the following it is assumed that the root of the cloned project directory
structure is at \texttt{SynthaCorpus}.  All references to directory paths are relative to
that.

\section{Using \projectName}

Please see the companion User Overview document.



\section{Licenses, Authorship and Copyright}
Here is a list of third-party open-source code distributed as part of
the \projectName{} project.

\begin{description}
\item [Project Gutenberg books] - the directory
  \texttt{ProjectGutenberg} contains 49 popular books in text
  format, downloaded from \url{http://www.gutenberg.org/}.  These
  are books which are no longer subject to copyright.  They
  are provided as a small test set to illustrate and verify the
  operation of project \projectName.
\item [Fowler Noll Vo hash function] - this is a hashing function
  which we have consistently found to be fast and effective.  The code
  is at \texttt{src/imported/Fowler-Noll-Vo-hash}.  Both
  \texttt{fnv.c} and \texttt{fnv.h} in that directory include a
  licence allowing free distribution.  See \url{http://www.isthe.com/chongo/tech/comp/fnv/}.
\item [Tiny Mersenne Twister random number generator] - this is
  a fast random number generator with very good properties.  We
  chose to use the tiny version (still with excellent
  properties), to allow the possibility of corpus generation
  being built into an indexer being studied.  The code is at
  \texttt{src/imported/TinyMT\_cutdown}.  The file
  \texttt{readme.html} contains the license.
  See
  \url{http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/TINYMT/index.html}
\item [detrec] - this is a text scanning and format conversion
  system designed to convert TREC, HTML, SGML, XML and other files including
  content in a variety of character sets, into a simplified TREC
  format with all text content converted into the UTF-8 character
  encoding.  Copyright is held by Funnelback Pty Ltd
  (\url{https://funnelback.com/}
  who have agreed
  to it being distributed under the MIT open-source licence.
\end{description}

\projectName{} was originally developed by Microsoft Corporation and open-sourced
at \url{https://github.com/Microsoft/SynthaCorpus}.  The principal
author at Microsoft was David Hawking.  Since retiring from paid work, he has continued
 to work on the project and, for administrative convenience, has set
 up the new repository.

 Accordingly, copyright in the current SynthaCorpus code is a mixture of
 Microsoft Corporation and David Hawking.

 \section{Simple TREC format}
 The program \texttt{detrec} mentioned in the previous section
 converts a variety of document corpus formats into a \texttt{.trec}
 file which is in pseudo-XML format, consisting of a concatenation of
 \texttt{DOC} elements, each containing a \texttt{DOCNO} element and a
 \texttt{TEXT} element.  The content of the latter is encoded in
 UTF-8.

 Because of character set conversion, and removal of almost all
 mark-up, this format is very well suited to efficiency
 experimentation. To use TREC you may need to develop a converter from whatever other
format.  To assist in this process, you may wish to look at
\texttt{convertPGtoTREC.c}, the provided  converter from
Project Gutenberg format into TREC. Tools for checking the validity
of a TREC file and for counting documents are also provided:
\texttt{checkTRECFile.exe} and
\texttt{countDocsInCorpus.exe}

 This format is recommended for use with \projectName{} although other
 formats are supported. 
    
 \section{Simple Text ARChive (STARC) format}
 
A STARC is a UTF-8 text file consisting of a
concatenation of records.   Three types of records are defined: Document
(D), Header (H), and Trailer (T). D records are the most important.  H
and T records are entirely optional but can be used to contain
metadata about a D record.  SynthaCorpus extracts properties
from D records only.  When generating, it puts a document
identifier into an H record preceding the D record containing the
generated text.

A STARC record starts with a space, then a decimal length $L$
represented in ASCII digits, then a single character specifying the record
type (H, D, or T), another space, followed by $L$ bytes of the actual
document content.  This form of representation allows arbitrary
content including control characters and NULs.

Here is an example STARC containing four records.  The dollar signs are not
part of the archive; they are included to unambigously mark the beginning and end of
the STARC.  \\r represents RETURN and \\n represents LINEFEED.
\begin{verbatim}
\$ 4H Doc1 19D A quick brown fox\r\n 4H Doc2 22D Jumped over the lazy\r\n$
\end{verbatim}

To use STARC you may need to develop a converter from whatever other
format.  To assist in this process, you may wish to look at
\texttt{convertPGtoSTARC.c}, the provided  converter from
Project Gutenberg format into STARC. Tools for checking the validity
of a STARC file and for counting documents are also provided:
\texttt{checkSTARCFile.exe} and
\texttt{countDocsInCorpus.exe}


\section{Project dependencies}
Successful building and operation of the \projectName~project requires
installation of the following packages.  Version numbers shown are the
ones used.  Other versions may be suitable.

\begin{itemize}
\item \texttt{gcc 5.4.0} and \texttt{make 4.2.1} 
\item \texttt{perl 5.22}
  \item \texttt{gnuplot 5.0} -- for displaying extracted corpus properties
  \item \texttt{pdflatex 2.9.4902} -- for compiling documentation (if needed)
    \item \texttt{acroread 2017 version} OR other PDF viewer -- for viewing documentation
      and plots.
\end{itemize}



\section{Building executables and documents}

\subsection{With gcc}
As noted in the User Overview, you can build the gcc executables by
running the Exerciser script.  If you want to do it manually,

\begin{verbatim}
cd src
make cleanest
make
\end{verbatim}

builds all the executables.  Currently, the executables
remain in \texttt{src}.

\subsection{Building on Windows}
We expect that \projectName{} should build without major problems on
Windows, using gcc under Cygwin or under the Windows 10 Bash shell.

Another promising avenue we have not yet explored is Visual Studio Code, a free,
multi-platform, open-source development environment distributed by
Microsoft at \url{https://code.visualstudio.com/}.


\subsection{Testing the system once built}
The exerciser script (\texttt{src/exerciser.pl})runs each of the main components and can be used
as a basic sanity test.  For a more thorough test add the '\texttt{thorough}'
option to the \texttt{exerciser.pl} command line.

More thorough testing of corpus emulation can be achieved with:
\begin{verbatim}
cd src
perl exploreEmulationOptions.pl PG
\end{verbatim}

That runs \texttt{src/emulateARealCorpus.pl} with ten different
combinations of options, using the tiny Project Gutenberg extract as
the base corpus in order to
avoid excessive runtime.


\subsection{Rebuilding PDF documents}
In the uncommon case that you want to rebuild the documentation PDFs, you will need
to have a TeX distribution installed plus a few additional packages,
easily obtained via CTAN.

\begin{verbatim}
cd doc
rm *.pdf
pdflatex user_overview
pdflatex developer_overview
pdflatex how_to_synthesize_a_corpus
\end{verbatim}

\section{Notes on coding}

Project \projectName{} has been developed in the course of scientific
research rather than engineered to commercial standards.  In many
cases, but not always, there is quite extensive internal documentation
at the head of modules or critical functions.
Unfortunately, there is inconsistency in variable
naming. The process of moving from the use\_of\_underscores to
``camelCase'' is regrettably incomplete.

Be warned that the principal author is highly enamoured of the
\texttt{unless} clause in perl.  Most scripts contain many examples
of:
\begin{verbatim}
die "message"
   unless <condition>;
\end{verbatim}

The C programs make very extensive use of \texttt{dahash} hash tables,
defined in \texttt{src/utils/dahash.[ch]}.  These hash tables avoid
the use of buckets.  Their size is constrained to be a power of two
and collisions are handled by relatively prime rehash.  Once a table
reaches a specified percentage of capacity, the table size is doubled
behind the scenes.  The hash function is the Fowler Noll Vo one.

Example code for using the \texttt{dahash} system is in
\texttt{src/utils/dahashDemo.c}.


SynthaCorpus also defines a mechanism for dynamic arrays --
\texttt{src/utils/dynamicArrays.[ch]}.  Accessing an array element
beyond allocated storage, causes the array to grow using a
configurable strategy.

C programs make extensive use of memory mapping through wrappers in
\texttt{src/utils/general.[ch]} which use either the
Windows style \texttt{CreateFileMapping()/MapViewOfFile()} (used if the
WIN64 symbol is defined) or the Unix interface \texttt{mmap()}.

All programs assume the use of UTF-8.  We recommend the use of the
iconv library \url{https://www.gnu.org/software/libiconv/} for
converting other character sets into UTF-8.  Functions for dealing
with UTF-8 are in \texttt{src/characterSetHandling/unicode.[ch]}.
Also in that directory is a perl script for updating Unicode tables
from the database at \url{http://unicode.org/}.


\section{Layout of Project Directories}

The project root directory contains the following files and directories:

\begin{description}
\item[\texttt{doc}] - Contains the LaTeX source and PDFs of project documentation.
\item[\texttt{Experiments}] - This is the directory under which all
  the output produced by scripts and executables will be stored.  If
  it doesn't exist the main perl scripts will create it.
\item[\texttt{LICENCE.TXT}] - See beginning of this document.
\item[\texttt{ProjectGutenberg}] - Text versions of 49 books used to
  illustrate and validate the executables in this project.
\item[\texttt{src}] - All the perl and C sources.  Currently
  executables are built in or under this directory.
\item[\texttt{bookScripts}] - A forthcoming Morgan and Claypool book ``Simulating
  Information Retrieval Test Collections'' is based on experimentation
  using \projectName{}.  This directory contains a number of scripts
  used to generate tables and figures in the book.
\item[\texttt{miscScripts}] - Miscellaneous scripts not considered
  part of the main \projectName{} project.
\end{description}

\section{Desirable future developments}

\begin{enumerate}
  \item Code tidyup - variable names, internal documentation, module
    structure.  [Boring but useful]
  \item Piecewise growth models - Currently scaling up from a
    small corpus assumes the use of only a simple linear model for
    word frequency distribution.  [A challenge and very useful
    contribution.]
  \item Extending the supported term dependency models to include
    term repetition within a document, and term co-occurrence as
    well as n-grams. [A challenge and very useful
    contribution.]
  \item Efficiency monitoring and improvement, particularly
    reducing memory demands without exploding runtime.  [Could
    increase scope of application.]
  \item Reduce memory demands of \verb|corpusGenerator.exe| by
    using more memory-efficient representations of Markov
    transition matrices.  [Could
    increase scope of application.]
  \end{enumerate}


\end{document}
