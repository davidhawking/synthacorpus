#/usr/bin/perl -w

print "Generating a LaTeX table showing the memory requirements of a Markov matrix\n\n";

$cellBytes = 8;  # Sizeof(double)
@kVals = (0, 1, 2, 3, 4, 5, 6);


for $k (@kVals) { print "& $k"; }
print "\\\\\n";

for $as (4, 27, 100) {   # Three different alphabet sizes
  print $as;
  $power = 1;
  for $k (@kVals) { # Seven different values of k, the Markov order
    $power *= $as;
    $megaBytes = $power * $cellBytes / 1024 / 1024;
    print "& ", sprintf("%.3f", $megaBytes);
  }
  print "\\\\\n";
}


print "Generating a LaTeX table showing Hash Table sizes\n\n";
$maxTermLen = 15;
$entryBytes = $maxTermLen + 1 + 4;   # MAX_TERM_LEN + NUL + sizeof(int);
 
@vSizes = (100000, 1000000, 10000000, 100000000, 1000000000);

print "Vocab. words";
for $vs (@vSizes) { print " &", $vs;}
print "\\\\\nHashtable MB";

$MB = 1024 * 1024;   # Assume a minimum of 1MB
$pow2 = 1;
for $vs (@vSizes) {
  while ($pow2 * $MB <= $vs) {$pow2 *= 2;}
  if ($vs > 0.9 * $pow2 * $MB) { $pow2 *= 2;}
  $htReq = $pow2 * $entryBytes;
  print "& $htReq";
}

print "\\\\\nVocab list MB";
for $vs (@vSizes) {
  $vlMB = $vs * ($maxTermLen + 1) / $MB;
  print "& ", sprintf("%.3f", $vlMB);
}
print "\\\\\n";
