\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{url}
\usepackage{color}
\usepackage[dvipsnames]{xcolor}
\usepackage{listings}

\newcommand{\red}[1]{{\color{red}{\emph{#1}}}}
\newcommand{\projectName}{\texttt{SynthaCorpus}}

\title{User Overview of \projectName}
\author{Microsoft, subsequently updated and extended by David Hawking}

\begin{document}
\maketitle{}


In its simplest form a text corpus consists of a set of documents,
each containing text, comprising a sequence of words\footnote{In CJKT
  languages, such as Chinese, there is no explicit segmentation into
  words. \projectName{} doesn't yet attempt to deal with CJKT text.}.
A synthetic corpus has the same form, but the sequence of words in a
document may be randomly chosen, and the representation of the words
may be made up.

Like real corpora, synthetic corpora have observable properties such
as total number of word occurrences, vocabulary size, term frequency
distribution, document length distribution, letter frequency
distribution, word length distribution and so on.

Real corpora have other properties such as internal structure and
external linking which are not currently modelled by \projectName.

There are a number of scenarios in which \projectName{} may be useful:
\begin{enumerate}
  \item when you need to work with a private corpus you're
    not entitled to see,
  \item when someone wants to replicate results
    you obtained on a corpus you can't share  for reasons of
    confidentiality, or size,
  \item when you want to realistically scale up a corpus
  \item when you want to engineer an artificial corpus with 
    specific properties, e.g. to stress test an IR system,
\end{enumerate}

\section{Getting going with \projectName}

\subsection{Obtaining \projectName}

Assuming you are working with a Unix style shell, e.g. on Linux or MacOSx,
under Cygwin, or under Ubuntu Bash for Windows 10 ....

\begin{verbatim}
git clone "https://bitbucket.org/davidhawking/synthaCorpus"
\end{verbatim}


\subsection{Prerequisites}

To use the system you need to build executables from their sources.
For this you need to have installed \texttt{gcc} and
\texttt{make}.

To use the system you will also need to have \texttt{perl 5}
installed.  Finally, to generate PDF plots (optional) and view them you will
need to have installed the \texttt{gnuplot} package and a PDF viewer
such as \texttt{acrobat reader}, \texttt{okular}, \texttt{preview}, or
a browser such as \texttt{edge}.

\subsection{Building with gcc / make}

If you have \texttt{gcc}, \texttt{make}, \texttt{perl} and
\texttt{gnuplot} installed and in your path you can just type:

\begin{verbatim}
cd SynthaCorpus/src    # Change to the src directory of the cloned repository
perl exerciser.pl
\end{verbatim}

The Exerciser script builds the executables from scratch and then runs
most of the tools, starting with a corpus derived from some Project
Gutenberg books.

\section{Other documentation}
A companion document \url{developer_overview.pdf} gives more detail on 
building and using the system.  It also provides
information for developers who wish to understand how things work, or
to contribute enhancements or extensions to the project.  That
document lists some useful extensions which contributors may wish to
work on.

A set of HOWTOs in this directory is planned to  provide detailed
information on each of the main components of the system.  Currently
only \url{how_to_synthesize_a_corpus.pdf} which describes
\verb|corpusGenerator.exe| has been written.

The mathematics and science behind synthesizing a corpus with
specified properties will be described in a forthcoming Morgan and
Claypool book, ``Simulating Information Retrieval Test Collections''.


\section{Tools provided}

At the most fundamental level, the \projectName~ project provides executables for:
\begin{enumerate}
\item Extracting the properties of a text corpus. (\texttt{corpusPropertyExtractor.exe})
\item Generating a synthetic corpus with specified properties. (\texttt{corpusGenerator.exe})
\item Generating a set of known item queries from a text corpus. (\texttt{queryGenerator.exe})
\item Converting a query log for a base text corpus into a 
  version suitable for running against an emulation of the base. (\texttt{queryLogEmulator.exe})
\end{enumerate}

It also provides perl scripts to achieve:
\begin{description}
\item [Emulation of a corpus.] i.e. Extract the properties of a base
  corpus, then use the extracted properties to generate a mimic corpus.
  (\texttt{emulateARealCorpus.pl})
\item [Corpus sampling]. i.e. taking samples of a base corpus and
  extracting their properties.  Sample sizes range from 1\% to 100\%.
  Plotfiles are generated showing how the properties change with
  increasing sample size.  A growth model is also generated, allowing
  for potential scaling up.  The script also allows for temporal growth
  scenarios. (\texttt{samplingExperiments.pl})
\item [Scaling up a corpus]. i.e. generating a corpus many times
  larger than a real one, which has properties which might be expected
  of a larger version of the real corpus. (\texttt{scaleUpASample.pl})
\end{description}


\section{Corpus formats}
Currently the executables and scripts described above expect text to
be encoded in UTF-8 and stored in a single file recorded in one or
other of three formats:
\begin{description}
  \item[Simple TREC] Each document is represented as a \texttt{DOC}
    element, containing only a \texttt{DOCNO} element and a
    \texttt{TEXT} element, with content in UTF-8.
  \item[TSV] Each document (with TABs and newlines removed) is
    represented by a single record.  The text of the document is in
    column 1, and a (numeric) static score is expected to be in column
    2. Other columns of metadata may optionally be present.
  \item[STARC] Simple Text Archive format allows documents to contain
    control characters, by prepending each record with an
    ASCII-encoded length.  The STARC format is specified in the
    Developer Overview.
\end{description}



\section{Limitations on size}
The size of corpus which can be generated, or whose properties can be
extracted, is limited by the amount of RAM you have.   Hash tables to
record all the n-grams in a corpus tend to be very large, and the programs in
\projectName~{} are written to favour simplicity of coding at the expense of
memory demands.  Start with small corpora as in the example in the
next section, and gradually work up.  If your RAM is insufficient your
CPU utilisation percentage will eventually drop, and progress may slow
dramatically.


\section{Usage}

In order to use \projectName{} you will need to make a directory under
which all \projectName{} experimental data will live.   You will also
need to reference it using the environment variable
\$SC\_EXPERIMENTS\_ROOT.

You then run the
\texttt{exerciser.pl} script) which create a TREC file from the Project Gutenberg documents,
emulates it, generates sets of known item queries for the base corpus
and also the emulated one, runs sampling experiments on the base
corpus, scales up a 1\% sample and compares the scaled-up sample with
the original.   Finally it runs other corpus generators such as
Caesar, Nomenclator, and MarkovGenerator.

Once \texttt{exerciser.pl} script has completed successfully, you
should be up and running with \projectName{}.   Check out the code
in \texttt{exerciser.pl} for usage examples, or run executables or
perl scripts without options to see a usage message for it.

 
\end{document}
