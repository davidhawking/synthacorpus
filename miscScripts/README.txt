** miscScripts directory **

David.Hawking@acm.org  14 Apr 2020

Scripts in this directory are unlikely to be useful without modification to other than the author.

They are included in case they may serve as the basis for something more solid.
