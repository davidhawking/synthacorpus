#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Jensen-Shannon divergences lie in 0 - 1, but can contributions from individual
# probability pairs be negative?   I don't think so but let's check

$negs = 0;

for ($q = 0.01; $q < 1.0; $q += 0.01) {
  for ($i = 0; $i < 1000000; $i++) {
    $p = rand(1.0 - $q);
    $m = ($p + $q) / 2.0;
    $k1 = $p * log($p / $m) / 2.0;
    #print " ------ k1 = $k1\n" if $k1 < 0;
    $k2 = $q * log($q / $m) / 2.0;
    $k = $k1 + $k2;
    if ($k < 0) {
      print "Negative JSD contribution $k for p = $p, q = $q, k1 = $k1, k2 = $k2\n\n";
    $negs++;
    }
  }  
  print "$negs negs out of 1 million random trials for q = $q.\n";

}
  
