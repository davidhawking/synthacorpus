#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# Measure and compare query processing effectiveness between a base and many different emulated corpora,
# using the ATIRE retrieval system

use Time::HiRes qw(time);
$|++;

$CNAME = "ap";
$rankMethod = "tfidf";
$meanQueryLength = 3;
$numQueries = 1000;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

die "This script requires the installation of third-party 
retrieval software, located under a directory identified by the environment 
variable SC_THIRD_PARTY_SW. If you use Bash you may need something like:   

     export SC_THIRD_PARTY_SW=/opt/local/

"
  unless defined($ENV{SC_THIRD_PARTY_SW});

$softwareDir = $ENV{SC_THIRD_PARTY_SW};


$IXCMD_AT = "$softwareDir/atire-caa2f2598c19/bin/index -sa -rtrec -findex $INDEX";
$QPCMD_AT = "$softwareDire/atire-caa2f2598c19/bin/atire -QN:d -sa -l1000 -findex $INDEX -R$rankMethod";

# The emulation method is described by
#     "<method name> % <stem>"
# It is assumed that the documents will be in stem.trec, the queries in stem.topics and the qrels in stem.qrels

@methods = (
	    "Base % $expRoot/Base/$CNAME",
	    "Copy % $expRoot/Cp/$CNAME",
	    "Caesar0 % $expRoot/Caesar/${CNAME}0",
	    "Caesar1 % $expRoot/Caesar/$CNAME",
	    "Nomenclator % $expRoot/Nomenclator/$CNAME",
	    "SAC_longDocs % $expRoot/Engineered/LongDocs/simpleWords_dluniform_ind/$CNAME",
	    "SAC_uniformLen % $expRoot/Engineered/Linear/simpleWords_dluniform_ind/$CNAME",
	    "SAC_unifLenTinyV % $expRoot/Engineered/TinyVocab/simpleWords_dluniform_ind/$CNAME",
	    "SAC_basic % $expRoot/Emulation/Linear/simpleWords_dlnormal_ind/$CNAME",
	    "SAC_basic1 % $expRoot/Emulation/Linear/simpleWords_dlhisto_ind/$CNAME",
	    "SAC_basic2 % $expRoot/Emulation/Linear/simpleWords_dlhisto_ngrams3/$CNAME",
	    "SAC_advanced % $expRoot/Emulation/Piecewise/markov-5e_dlhisto_ngrams3/$CNAME",
	   );



for $rankMethod ("BM25", "tfidf") {
  for $meanQueryLength (2.9, 5.8,  8.7) {
    foreach $m (@methods) {
      ($method, $stem) = split / % /, $m;
      $corpus = "$stem.trec";
      $topics = "$stem.topics";
      $qrels = "$stem.qrels";
      
      
      print "  ------ $method   $corpus   $topics   $qrels ------\n";
      
      print "Generating $numQueries of length $meanQueryLength for $CNAME\n";
      system("./queryGenerator.exe corpusFileName=$corpus propertiesStem=$stem -numQueries=$numQueries -meanQueryLength=$meanQueryLength");
      die "queryGeneration failed\n" if $?;
      
      print "Indexing $method ($CNAME): ";
      $startTime = time();
      system("$IXCMD_AT $corpus > wipeMeIndex.log");
      die " indexing error\n" if $?;
      $timeDiff = time() - $startTime;
      print sprintf("%.3f\n", $timeDiff);
      
      print "Running queries on $corpus Index: ";
      $startTime = time();
      system("$QPCMD_AT -q $topics -a $qrels| tail -25 > QP.summary");
      die " querying error\n" if $?;  
      $timeDiff = time() - $startTime;
      print sprintf("%.3f\n", $timeDiff);
      $procLine = `egrep '^Processed [0-9]+ topics' QP.summary`;
      if ($procLine =~ /^Processed ([0-9]+) topics \(([0-9]+) evaluated\)/) {
	$topicsProcessed = $1;
	$topicsEvaluated = $2;
	$MAPLine = `egrep '^MAP: ' QP.summary`;
	$MAPLine =~ /^MAP:\s+([0-9.]+)/;
	$MAP = $1;
	$baseMAP = $MAP if $method eq "Base";
	$pLine = sprintf("%s  %18s: %s  %s  %s  %s %s\n", $rankMethod, $method, $topicsProcessed, 
			 $topicsEvaluated, getMeanQueryLength($topics), $MAP, starRating($baseMAP, $MAP));
      } else {
	die "Error in evaluation output.   Examine QP.summary\n";
      }
      print $pLine;
      push @pLines, $pLine;		   
    }
  }
}
    
print "\n\n\n @pLines\n";
    

exit(0);

# ----------------------------------------------------------

sub getMeanQueryLength {
  my $topicFile = shift;
  die "Can't open $topicFile\n" unless open T, $topicFile;
  my $wdCnt = 0;
  my $titleCnt = 0;
  while (<T>) {
    if (/<title>\s+(.*)\n/) {
      my $query = $1;
      my @q = split /\s+/, $query;
      $titleCnt++;
      $wdCnt += ($#q + 1);
    }
  }
  close(T);
  return sprintf("%.3f", $wdCnt / $titleCnt);
}
      



sub starRating {
  $m1 = shift;  # This is the base
  $m2 = shift;
  $perc = $m2 * 100 / $m1;
  if ($perc > 100) {$percDiff = $perc - 100;}
  else {$percDiff = 100 - $perc};

  if ($percDiff <= 2) {return "*****";}
  elsif ($percDiff <= 5) {return " ****";}
  elsif ($percDiff <= 10) {return "  ***";}
  elsif ($percDiff <= 20) {return "   **";}
  elsif ($percDiff <= 50) {return "    *";}
  else {return "     ";}
}
