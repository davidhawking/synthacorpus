#! /usr/bin/perl -w

# Copyright (c) David Hawking.   All rights reserved.
# Licensed under the MIT license.

# Measure and compare indexing and query processing times between a base and several emulated corpora.  Repeat the measurements
# R times and take the median of results.
# Uses the ATIRE retrieval system

use Time::HiRes qw(time);
$|++;

$R = 15;    # Number of iterations.  Should be odd
$CNAME = "ap";
$rankMethod = "BM25";

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

die "This script requires the installation of third-party 
retrieval software, located under a directory identified by the environment 
variable SC_THIRD_PARTY_SW. If you use Bash you may need something like:   

     export SC_THIRD_PARTY_SW=/opt/local/

"
  unless defined($ENV{SC_THIRD_PARTY_SW});

$softwareDir = $ENV{SC_THIRD_PARTY_SW};




$TRECDIR = "$ENV{HOME}/Research/QuerySets/AdHoc";
die "$TRECDIR doesn't exist\n"
  unless -d $TRECDIR;

$BASE = "$expRoot/Base/$CNAME.trec";
$BASE_QUERIES = "$TRECDIR/51-100";
$BASE_QUERIES = "$expRoot/Base/$CNAME.topics"; 

$IXCMD_AT = "$softwareDir/atire-caa2f2598c19/bin/index -sa -rtrec -findex $INDEX";
$QPCMD_AT = "$softwareDIR/atire-caa2f2598c19/bin/atire -QN:d -sa -l1000 -findex $INDEX -R$rankMethod"";


# The parameters needed to evaluate an emulation method are encoded in a " % " separated string:
# "<method name> % <document set> % <topic set> % <qrels file>"

@methods = (
	    "Base % $expRoot/Base/$CNAME",
	    "Copy % $expRoot/Cp/$CNAME",
	    "Caesar0 % $expRoot/Caesar/${CNAME}0",
	    "Caesar1 % $expRoot/Caesar/$CNAME",
	    "Nomenclator % $expRoot/Nomenclator/$CNAME",
	    "SAC_longDocs % $expRoot/Engineered/LongDocs/simpleWords_dluniform_ind/$CNAME",
	    "SAC_uniformLen % $expRoot/Engineered/Linear/simpleWords_dluniform_ind/$CNAME",
	    "SAC_unifLenTinyV % $expRoot/Engineered/TinyVocab/simpleWords_dluniform_ind/$CNAME",
	    "SAC_basic % $expRoot/Emulation/Linear/simpleWords_dlnormal_ind/$CNAME",
	    "SAC_basic1 % $expRoot/Emulation/Linear/simpleWords_dlhisto_ind/$CNAME",
	    "SAC_basic2 % $expRoot/Emulation/Linear/simpleWords_dlhisto_ngrams3/$CNAME",
	    "SAC_advanced % $expRoot/Emulation/Piecewise/markov-5e_dlhisto_ngrams3/$CNAME",
	   );


# First establish the baseline
print "\n\n------------------------------------------\n";
for ($i = 1; $i <= $R; $i++) {
  print "[$i/$R] Indexing Base ($CNAME): ";
  $startTime = time();
  system("$IXCMD_AT $BASE > wipeMeBaseIndex.log");
  die " indexing error\n" if $?;
  $timeDiff = time() - $startTime;
  push @baseITimes, $timeDiff;
  print sprintf("%.3f\n", $timeDiff);

  print "[$i/$R] Running queries on Base Index: ";
  $startTime = time();
  system("$QPCMD_AT -q $BASE_QUERIES > wipeMeBaseQP.log");
  die " querying error\n" if $?;
  $timeDiff = time() - $startTime;
  push @baseQPTimes, $timeDiff;
  print sprintf("%.3f\n", $timeDiff);
}

@BI = sort {$a <=> $b}  @baseITimes;
@BQ = sort {$a <=> $b}  @baseQPTimes;

print "------------------------------------------\n\n";

$outputA =
  "\n         Emulation method    Base_time    Emul_time  Rating
-----------------------------------------------------------\n";
$outputB = "";

# Now run each of the methods and compare to the base

foreach $m (@methods) {
  undef @emuITimes;
  undef @emuQPTimes;
  ($method, $stem) = split / % /, $m;
  $corpus = "$stem.trec";
  $topics = "$stem.topics";
  $qrels = "$stem.qrels";
  

  print "  ------ $method   $corpus   $topics   $qrels ------\n";
  
  for ($i = 1; $i <= $R; $i++) {
    print "[$i/$R] Indexing $method: ";
    $startTime = time();
    system("$IXCMD_AT $corpus > WipeMeEmuIndex.log");
    die " indexing error\n" if $?;
    $timeDiff = time() - $startTime;
    push @emuITimes, $timeDiff;
    print sprintf("%.3f\n", $timeDiff);
  
    print "[$i/$R] Running queries on $method Index: ";
    $startTime = time();
    system("$QPCMD_AT -q $topics > wipeMeEmuQP.log");
    die "querying error\n" if $?;
    $timeDiff = time() - $startTime;
    push @emuQPTimes, $timeDiff;
    print sprintf("%.3f\n", $timeDiff);
  }
  

  @EI = sort {$a <=> $b}  @emuITimes;
  @EQ = sort {$a <=> $b}  @emuQPTimes;

  $MP = int($R/2);   #  Assume $R is odd.

  $outputA .= sprintf("%25s   %10.3f   %10.3f   ", "Indexing[$method]:", $BI[$MP], $EI[$MP]);
  $outputA .= starRating($BI[$MP], $EI[$MP]),;
  $outputA .= "\n";
  $outputB .= sprintf("%25s   %10.3f   %10.3f   ", "Querying[$method]:", $BQ[$MP], $EQ[$MP]);
  $outputB .= starRating($BQ[$MP], $EQ[$MP]);
  $outputB .= "   $rankMethod\n";

  print $outputA, "\n", $outputB, "\n";  
}

exit(0);

# ----------------------------------------------------------

sub starRating {
  $m1 = shift;  # This is the base
  $m2 = shift;
  $perc = $m2 * 100 / $m1;
  if ($perc > 100) {$percDiff = $perc - 100;}
  else {$percDiff = 100 - $perc};

  if ($percDiff <= 2) {return "*****";}
  elsif ($percDiff <= 5) {return " ****";}
  elsif ($percDiff <= 10) {return "  ***";}
  elsif ($percDiff <= 20) {return "   **";}
  elsif ($percDiff <= 50) {return "    *";}
  else {return "     ";}
}
