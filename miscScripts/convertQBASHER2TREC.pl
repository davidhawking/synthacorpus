#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.


$|++;

die "Usage: $0 <infile> <outfile>

     - infile is in TSV format where the document text is in column 1 and the other columns are ignored
     - outfile will be written in simple TREC format

" unless $#ARGV == 1;

die "Can't write to $ARGV[1]\n" unless open O, ">$ARGV[1]";
die "Can't read $ARGV[0]\n" unless open I, $ARGV[0];

$docno = 0;

while (<I>) {
  chomp;
  next if (/^\s*$/);
  @f = split /\t/;
  print O "<DOC>
<DOCNO> ", sprintf("QB%09d", ++$docno), "</DOCNO>
<TEXT>
$f[0]
</TEXT>
</DOC>
";
  print "     $docno\n" if $docno % 500000 == 0;
}

close(O);
close(I);

print "Documents written: $docno\n\n";

exit(0);

