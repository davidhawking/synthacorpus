#! /usr/local/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Read the two natural language query files used in the TREC8 Web Track, and output each non-blank
# query as a document in cut down TREC format.

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";

$TREC8dir = "/opt/local/SonyZ/Webtrack/t8";
die "Error: Directory $TREC8dir not found.\n"
  unless -e $TREC8dir;
mkdir $baseDir unless -e $baseDir;

$outFile = "$baseDir/t8nlqs.trec";

@nlqFiles = ("$TREC8dir/AV.nlqs",
	     "$TREC8dir/EM.nlqs");

die "Can't write to $outFile\n"
  unless open O, ">$outFile";

$outCount = 0;

foreach $nlq (@nlqFiles) {
  die "Can't read from $nlq\n"
    unless open I, $nlq;
  while (<I>) {
    next if /^\s*$/;  # skip blank lines if any
    chomp;
    s@<.*>@@g;  # remove all XML-like tags
    s@<|>@@g;   # and spurious angle brackets.
    print O "<DOC>
<DOCNO> NLQ-$outCount </DOCNO>
<TEXT>
$_
</TEXT>
</DOC>
";
    $outCount++;
  }
  close(I);
}
close(O);

print "$outFile written with $outCount documents.  Now checking for validity.\n";

$cmd = "../src/checkTRECFile.exe $outFile";
system($cmd);
print "\n";
exit(0);
