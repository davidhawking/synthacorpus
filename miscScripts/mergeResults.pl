#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.

# Given two input directories and an output directory, combine the data in main_experiment.results.*
# in each of the input directories into files of the same name in the output directory.

die "Usage: $0 <input dir 1>  <input dir 2> <output dir>\n" unless $#ARGV == 2;

for ($a = 0; $a < 3; $a++) {
  die "$ARGV[$a] isn't a directory" unless -d $ARGV[$a];
}		   	 

foreach $ifile1 (glob "$ARGV[0]/main_experiment.results.*") {
  ($suff) = ($ifile1 =~ /results\.([a-z0-9]+)/i);
  die "Can't open $ifile1\n" unless open ONE, $ifile1;
  $ifile2 = "$ARGV[1]/main_experiment.results.$suff";
  die "Can't open $ifile2\n" unless open TWO, $ifile2;
  $ofile = "$ARGV[2]/main_experiment.results.$suff";
  die "Output file $ofile already exists.  Delete it yourself if you don't want it.\n" if -e $ofile;
  die "Can't write to $ofile\n" unless open OUT, ">$ofile";

  # Copy ifile1 to ofile in full  (retain the initial comment line)
  while (<ONE>) {
    print OUT $_;
  }
  close(ONE);

  #Copy ifile2 to ofile minus the comment
  while (<TWO>) {
    next if /^\s*#/;
    print OUT $_;
  }
  close(TWO);
  close(OUT);

  print "    $ofile written.\n";
}

print "\n";
exit(0);
  
