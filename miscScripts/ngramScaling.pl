#! /usr/bin/perl - w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Run the corpusPropertyExtractor over the wt10g corpus multiple times,
# increasing the limit on the number of postings processed, to allow estimation
# of RAM requirements for increasing corpus sizes.

$|++;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$corpus = "wt10g";


$baseCmd = "../src/corpusPropertyExtractor.exe inputFileName=$expRoot/Base/$corpus.tsv outputStem=$expRoot/Base/wt10g -ngramObsThresh=10 -maxNgramWords=4 -ngramPostFilterFraction=0.75 -maxPostings=";


print "#Postings    #Distinct_Ngrams    #HashBits\n";
foreach $millionPostings (10, 20, 30, 50, 100, 200, 300, 500, 1000) {
  $numPostings = $millionPostings * 1000000;
  system($baseCmd."$numPostings > /dev/null");
  die "Rats!\n" if $?;
  die "Can't read summary file\n" unless  open Q, "$expRoot/Base/wt10g_summary.txt";
  while (<Q>) {
    if (/total_postings=([0-9]+)/) {
      $postings = $1;
    } elsif (/entries_in_ngrams_hash=([0-9]+)/) {
      $distinctNgrams= $1;
    }
  }
  close(Q);

  # Estimate required hashbits.
  $bits = int(log($distinctNgrams) / log(2)) + 1;

  print "$postings     $distinctNgrams   $bits\n";
}

exit(0);

       
