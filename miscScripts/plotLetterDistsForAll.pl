#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.


die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$emuRoot = "$expRoot/Emulation";
$baseDir = "$expRoot/Base";



foreach $dFile ((glob "$emuRoot/*/*/*_letters_ranked.dat"), (glob "$emuRoot/*/*_letters_ranked.dat")) {
  ($file) = $dFile =~ m@.*/([^/]+)$@;
  next if $file =~ /PG_/;   # PG is only for playing and testing.
  $baseFile = "$baseDir/$file";
  if (-r $baseFile) {
    print "Comparing $dFile with $baseFile\n";
    $plotcmds = $dFile;
    $plotcmds =~ s/letters_ranked.dat/base_v_mimic_letters_plot.cmds/;
    $plotpdf = $dFile;
    $plotpdf =~ s/letters_ranked.dat/base_v_mimic_letters.pdf/;
    
    $plotpdf2 = $plotpdf;
    $plotpdf2 =~ s/letters/letter_pairs/;
    $baseFile2 = $baseFile;
    $baseFile2 =~ s/letters/letter_pairs/;
    $dFile2 = $dFile;
    $dFile2 =~ s/letters/letter_pairs/;
    die "Can't open $plotcmds\n"
      unless open PC, ">$plotcmds";
    print PC "
set terminal pdf
set size ratio 1
set style line 1 linewidth 4
set style line 2 linewidth 4
set style line 3 linewidth 4
set pointsize 3
set xlabel \"Letter rank\"
set ylabel \"Letter probability in corpus\"
set output \"$plotpdf\"
plot '$baseFile' using 1:7 title 'Base' pt 7 ps 0.5 with linespoints, '$dFile' using 1:7 title 'Mimic' pt 7 ps 0.25 with linespoints

set xlabel \"Letter-pair rank\"
set ylabel \"Letter-pair probability in corpus\"
set output \"$plotpdf2\"
plot '$baseFile2' using 1:3 title 'Base' pt 7 ps 0.5 with linespoints, '$dFile2' using 1:2 title 'Mimic' pt 7 ps 0.25 with linespoints

";
    close(PC);

    $gcmd = "gnuplot $plotcmds\n";
    system($gcmd);
    die "$gcmd failed\n" if $?;

    print "Plot commands in $plotcmds. To view the PDF use:

    acroread $plotpdf
    acroread $plotpdf2

";
    
  } else {
    warn "Base file $baseFile not found.  Skipping\n";
  }

}
