#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.

# Rerun the base vs. mimic JSD calculations for all the emulations

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$emuRoot = "$expRoot/Emulation";
$base = "$expRoot/Base";
$count = 0; 
foreach $trecFile ((glob "$emuRoot/*/*.trec"), (glob "$emuRoot/*/*/*.trec")) {
  $count++;
  ($dir, $corpus) = $trecFile =~ m@(.*)/([^/]+).trec@;
  $cmd1 = "./computeKLDsForEmuBase.exe $corpus $dir";
  print "Sub[$count]: $cmd1\n";
  $outfile = "$dir/${corpus}_KLKS.txt";
  runit($cmd1);

  $f1 = "$base/${corpus}_letters_ranked.dat";
  $f2 = "$dir/${corpus}_letters_ranked.dat";
  if (-r $f1 && -r $f2) {
    $count++;
    $cmd2 = "./computeKLDKSForTwoDats.exe $f1 $f2 3";
    print "Sub[$count]: L $cmd1\n";
    runit("$cmd2 | grep JSD= >> $outfile");
  } else {
    print "\nSkipping letters for $corpus\n\n";
  }
  
  $f1 = "$base/${corpus}_letter_pairs_ranked.dat";
  $f2 = "$dir/${corpus}_letter_pairs_ranked.dat";
  if (-r $f1 && -r $f2) {
    $count++;
    $cmd3 = "./computeKLDKSForTwoDats.exe $f1 $f2 3";
    print "Sub[$count]: LP $cmd1\n";
    runit("$cmd3 | grep JSD= >> $outfile");
  } else {
    print "\nSkipping letter pairs for $corpus\n\n";
  }

}


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
