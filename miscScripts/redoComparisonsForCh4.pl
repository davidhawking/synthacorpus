#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

use File::Copy;
# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
 
$perl = $^X;
$perl =~ s@\\@/@g;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$emuRoot = "$expRoot/Emulation";
$imageRoot = "$ENV{HOME}/GIT/SynthaCorpus/BitBucket/Imagefiles";

die "Directory $imageRoot doesn't exist\n" unless -e $imageRoot;


@corpora = (
	    "t8nlqs", "ap", "fr", "patents", "alladhoc", "wt10g"
	   );  

$subDir = "Piecewise/tnum_dlhisto_ind";
@fileTypes = ("unigrams", "bigrams", "ngrams", "repetitions", "distinct_terms");

foreach $corpus (@corpora) {
  unlink "$emuRoot/$subDir/${corpus}_summary.txt";
  $cmd = "$perl compareBaseWithMimic.pl $corpus $subDir 2 2";
  runit($cmd);
  # Copy the relevant PDFs to the applicable image directory
  $imageDir = "$imageRoot/$subDir";
  foreach $ft (@fileTypes) {
    $file = "$emuRoot/$subDir/${corpus}_base_v_mimic_$ft.pdf";
    copy($file, $imageDir);
    print "\n       $file copied to $imageDir\n";
  }
}


exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
    
