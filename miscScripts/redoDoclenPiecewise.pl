#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.


$pdfs = "acroread";

$baseDir = "/opt/local/BigData/Experiments/Base";
foreach $baseCorpusFile (glob "$baseDir/*.trec") {
  ($corpus) = $baseCorpusFile =~ /\/([a-z0-9]+)\.trec/i;
  if (-r "$baseDir/${corpus}_docLenHist.tsv") {
    $cmd = "perl doclen_piecewise_modeling.pl $corpus $baseDir";
    print $cmd, "\n";
    system($cmd);
    $pdfs .= " $baseDir/${corpus}_dlsegs_fitting.pdf"
      unless $?;
  } else {
    print "\n\nNo doclen histo for $corpus\n\n";
  }
}

print $pdfs, "\n";
exit(0);
