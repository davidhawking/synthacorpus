#! /usr/bin/perl -w

# Copyright (c) David Hawking, 2019. All rights reserved.
# Licensed under the MIT license.

# Try to simulate the distribution of word lengths for the markov-0e word string generator, given the
# probability of encountering an EOW as observed on the ap corpus.

# This should be a straightforward, monotonically decreasing geometric distribution, EXCEPT for the
# fact that there can only be 26 words of length one, 676 of length 2 and so on.


$vocabSize = 308037;
$EOWProb = 0.1708;
$power = 26;
$conditionalProb = 1;  # the probability of having got to this length

for ($l = 1; $l <= 15; $l++) {
  $prob = $conditionalProb * $EOWProb;
  $probLimit = $power / $vocabSize;  # The limit imposed by constraints of length
  if ($probLimit < $prob) {
    $prob = $probLimit;
    $conditionalProb *= (1 - $prob);
  } 
  $conditionalProb *= (1 - $prob);
  if ($l == 15) {print sprintf("%3d  %.6f\n", $l, $prob + $conditionalProb);}
  else {print sprintf("%3d  %.6f\n", $l, $prob);}
  $power *= 26;
}
