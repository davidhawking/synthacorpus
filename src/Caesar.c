// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                                     Caesar.c                                     //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Takes a base corpus C of documents in STARC, TSV or TREC format and encodes all the
// ASCII letters in the content using Julius Caesar's substitution cipher.


#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>

#include "definitions.h"
#include "characterSetHandling/unicode.h"
#include "utils/general.h"
#include "Caesar.h"
#include "utils/argParser.h"

static void initialiseGlobals(globals_t *globals) {
  globals->startTime = what_time_is_it();
}

static char *outDoc = NULL;
static int oDocNum = 0;




static int encodeOneDoc(globals_t *globals, char *docText, size_t docLen, u_char *outType, int diff) {
  int i;
  char *outP = NULL;
  size_t len;
  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.
  
  if (0) printf("Encode_one_doc()\n");
  if (!strcmp(outType, "TREC")) fprintf(globals->cipherOut,"<DOC>\n<DOCNO> CAESR%d-%06d </DOCNO>\n<TEXT>", diff, oDocNum++);
  if (outDoc == NULL) outDoc = cmalloc(MAX_DOC_LEN + 1, "outDoc", FALSE);
  outP = outDoc;
  if (docLen > MAX_DOC_LEN) {
    printf("Warning:  Document truncated from %zd to %d\n", docLen, MAX_DOC_LEN);
    docLen = MAX_DOC_LEN;
  }
  if ((docText[docLen] & 0xC0) == 0x80) {
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }

  for (i = 0; i < docLen; i++) {
    int c = docText[i], d = c;
    if (c >= '0' && c <= '9') {
      d = (((c - '0') + diff) % 10) + '0';
    } else if (c >= 'A' && c <= 'Z') {
      d = (((c - 'A') + diff) % 26) + 'A';
    } else if (c >= 'a' && c <= 'z') {
      d = (((c - 'a') + diff) % 26) + 'a';
    } 
    *outP++ = d;
  }

    

  *outP = 0;
  len = outP - outDoc;
  if (!strcmp(outType, "STARC")) fprintf(globals->cipherOut, " %zdD %s", len, outDoc);
  else if (!strcmp(outType, "TREC")) fprintf(globals->cipherOut, "%s</TEXT>\n</DOC>\n", outDoc);
  else fprintf(globals->cipherOut, "%s\n", outDoc);
  return len;
}


static void processTSVFormat(globals_t *globals, int diff) {
  // Input is assumed to be in TSV format with an arbitrary (positive) number of
  // columns (including one column), in which the document text is in
  // column one and the other columns are ignored.   (All LFs and TABs are assumed
  // to have been removed from the document.)
  char *lineStart, *p, *inputEnd;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  lineStart = globals->inputInMemory;
  globals->numDocs = 0; 
  p = lineStart;
  while (p <= inputEnd) {
    // Find the length of column 1, then process the doc.
    while (p <= inputEnd  && *p >= ' ') p++;  // Terminate with any ASCII control char
    docLen = p - lineStart;
    encodeOneDoc(globals, lineStart, docLen, "TSV", diff);
    // Now skip to end of line (LF) or end of input.
    while (p <= inputEnd  && *p != '\n') p++;  // Terminate with LineFeed only
    p++;
    lineStart = p;
  }
}


static void processTRECFormat(globals_t *globals, int diff) {
  // Input is assumed to be in highly simplified TREC format, such as that produced by
  // the detrec program.  Documents are assumed to be DOC elements, each starting with
  // a DOCNO element and with content inside a TEXT element.  It is also assumed that
  // the content contains no other sub-elements and no stray angle brackets.
  // Ideally, the text is just words separated by spaces.
  char *docStart, *p, *q, *inputEnd;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  globals->numDocs = 0;
  while (p <= inputEnd) {
    if (0) printf("Looking for <TEXT>\n");
    q = mlstrstr(p, "<TEXT>", inputEnd - p - 6, 0, 0);
    if (q == NULL) break;   // Presumably the end of the file. ... or wrong format file.
    docStart = q + 6;
    if (0) printf("Looking for </TEXT>\n");
    q = mlstrstr(docStart, "</TEXT>", inputEnd - docStart - 7, 0, 0);
    if (q == NULL) break;   // Presumably file is incorrectly formatted.    
    docLen = q - docStart;
    encodeOneDoc(globals, docStart, docLen, "TREC", diff);
    p = q + 7;  // Move to char after </TEXT>
  }
}




static void processSTARCFormat(globals_t *globals, int diff) {
  // Input is assumed to be records in in a very simple <STARC
  // header><content> format. The STARC header begins and ends with a
  // single ASCII space.  Following the leading space is the content
  // length in decimal bytes, represented as an ASCII string,
  // immediately followed by a letter indicating the type of record.
  // Record types are H - header, D - document, or T - Trailer.  The
  // decimal length is expressed in bytes and is represented in
  // ASCII. If the length is L, then there are L bytes of document
  // content following the whitespace character which terminates the
  // length representation.  For example: " 13D ABCDEFGHIJKLM 4D ABCD"
  // contains two documents, the first of 13 bytes and the second of 4
  // bytes.
  //
  // Although this representation is hard to view with editors and
  // simple text display tools, it completely avoids the problems with
  // TSV and other formats which rely on delimiters, that it's very
  // complicated to deal with documents which contain the delimiters.
  //
  // This function skips H and T records.
  // If encode is TRUE, each D record is passed to encodeOneDoc()
  // otherwise to processOneDoc().
  
  size_t docLen;
  char *docStart, *p, *q, *inputEnd;
  byte recordType;

  globals->numDocs = 0; 
  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  while (p <= inputEnd) {
    // Decimal length should be encoded in ASCII at the start of this doc.
    if (*p != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     p - globals->inputInMemory);
      exit(1);

    }
    errno = 0;
    docLen = strtol(p, &q, 10);  // Making an assumption here that the number isn't terminated by EOF
    if (errno) {
      fprintf(stderr, "processSTARCFile: Error %d in strtol() at offset %zd\n",
	     errno, p - globals->inputInMemory);
      exit(1);
    }
    if (docLen <= 0) {
      printf("processSTARCFile: Zero or negative docLen %zd at offset %zd\n",
	     docLen, p - globals->inputInMemory);
      exit(1);
    }

    recordType = *q;    
    if (recordType != 'H' && recordType != 'D' && recordType != 'T') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);
    }
    q++;
    if (*q != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't end with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);

    }
    
    docStart = q + 1;  // Skip the trailing space.
    if (0) printf(" ---- Encountered %c record ---- \n", recordType);
    if (recordType == 'D') {
      encodeOneDoc(globals, docStart, docLen, "STARC", diff);
   }
    p = docStart + docLen;  // Should point to the first digit of the next length, or EOF
  }
}

int main(int argc, char **argv) {
  int error_code, CaesarDiff;
  globals_t globals;
  char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
    
  setvbuf(stdout, NULL, _IONBF, 0);
  
  if (argc != 4) {
    printf("Usage: %s <name of plainTextFile> <Caesar diff> <name of output file>\n"
	   " - Note that the output format will always be the same as the input format, regardless\n"
	   " - of the name of the output file.\n",argv[0]);
    exit(1);
  }

  CaesarDiff = atoi(argv[2]);
  printf("Going to shift ASCII letters and digits by %d\n", CaesarDiff);

  // Make sure we can write to the output file
  globals.cipherOut = fopen(argv[3], "wb");
  if (globals.cipherOut == NULL) {
    printf("Error: Can't open %s for writing.\n", argv[3]);
    exit(1);
  }
  printf("Output file %s opened for writing.\n", argv[3]);
  
  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);
  initialiseGlobals(&globals);   // Includes the hashtables as well as scalar values
  printf("Globals initialised\n");

 
  // Memory map the whole input file
  if (! exists(argv[1], "")) {
    printf("Error: Input file %s doesn't exist.\n", argv[1]);
    exit(1);
  }
  
  globals.inputInMemory = (char *)mmap_all_of(argv[1], &(globals.inputSize),
					       FALSE, &(globals.inputFH), &(globals.inputMH),
					       &error_code);
  if (globals.inputInMemory == NULL ) {
    printf("Error: mmap_all_of(%s) failed with code %d\n", argv[1], error_code);
    exit(1);
  }

  madvise(globals.inputInMemory, globals.inputSize, MADV_SEQUENTIAL);

  printf("Input mapped.\n");


  // Perform the encryption

  if (tailstr(argv[1], ".tsv") != NULL || tailstr(argv[1], ".TSV") != NULL) {
    processTSVFormat(&globals, CaesarDiff);
  } else if (tailstr(argv[1], ".trec") != NULL || tailstr(argv[1], ".TREC") != NULL) {
    processTRECFormat(&globals, CaesarDiff);
  } else {
    processSTARCFormat(&globals, CaesarDiff);
  }
  

  unmmap_all_of(globals.inputInMemory, globals.inputFH, globals.inputMH, globals.inputSize);
  fclose(globals.cipherOut);
  printf("\n\nEncoded output in %s\n", argv[3]);
}
