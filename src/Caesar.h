// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

typedef struct {
  double startTime;
  long long vocabSize, numDocs, numEmptyDocs, totalPostings, longestPostingsListLength;
  wordCounter_t ngramFreqFilterThresh;
  char *inputInMemory;
  byte *cipherInMemory;
  byte **cipherVocabLines;
  int cipherVocabLineCount;
  size_t inputSize, cipherSize;
  CROSS_PLATFORM_FILE_HANDLE inputFH, cipherFH;
  HANDLE inputMH, cipherMH;
  FILE *cipherOut;
 } globals_t;
