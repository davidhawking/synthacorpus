// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                                CaesarQueries.c                                   //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  // A simple filter which leaves intact SGML tags, e.g. <top> and performs Caesar substition on
  // ASCII letters and digits in the rest.
  int c, d, state = 0, diff;   // States:  0 - copying, 1 - Scanning a tag, 2 - Scanning a <num> Number: line
  unsigned char tagBuf[2000], *tagP;
  if (argc != 2) {
    printf("Usage: %s <Caesar difference>\n", argv[0]);
    exit(1);
  }

  diff = atoi(argv[1]);
  
  c = getchar();
  while (c != EOF) {
    if (state == 0) {
      if (c == '<') {
	tagP = tagBuf;
	*tagP++ = c;
	state = 1;
      } else {
	d = c;  // until proven otherwise
	if (c >= '0' && c <= '9') {
	  d = (((c - '0') + diff) % 10) + '0';
	} else if (c >= 'A' && c <= 'Z') {
	  d = (((c - 'A') + diff) % 26) + 'A';
	} else if (c >= 'a' && c <= 'z') {
	  d = (((c - 'a') + diff) % 26) + 'a';
	}
	putchar(d);
      }
    } else if (state == 1) {
      if (c == '>') {
	// Have to handle stupid things in TREC queries like <num> Number: 21.  Need to treat Number:
	*tagP++ = c;
	if (!strncmp(tagBuf, "<num>", 5) || !strncmp(tagBuf, "<desc>", 6) || !strncmp(tagBuf, "<narr>", 6)) state = 2;
	else {
	  *tagP = 0;
	  printf("%s", tagBuf);
	  state = 0;
	}
      }
      *tagP++ = c;
    } else {
      // Scanning the Number:  Description: Narrativ: thing.  Up to : or Newline
      if (c == ':' || c == '\n') {
	*tagP++ = c;
	*tagP = 0;
	printf("%s", tagBuf);
	state = 0;
      } else {
	*tagP++ = c;
      }
    }	
    c = getchar();
  }
}
