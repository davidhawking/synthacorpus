// Copyright David Hawking. All rights reserved.
// Licensed under the MIT license.

// Table of command line argument definitions for the Markov document
// generator.  The functions in argParser.c operate
// on this array.

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "MarkovGenerator.h"
#include "utils/argParser.h"

arg_t args[] = {
  { "inFile", ASTRING, (void *)&(params.inFileName), "This is the file of text comprising the corpus on which the Markov model will be trained.  Formats supported: TREC, TSV, or STARC."},
  { "outFile", ASTRING, (void *)&(params.outFileName), "The name of the file to which the generated corpus will be written, always in simple TREC format."},
  //  { "numHeadTerms", AINT, (void *)&(params.numHeadTerms), "To dramatically reduce run time, the successor lists for head terms may be maintained using efficient structures."},
  { "hashBits", AINT, (void *)&(params.hashBits), "The initial size (2**hashBits) of the hash table representing the transition matrix. Will double as required."},
  { "randomSeed", AINTLL, (void *)&(params.randomSeed), "Seed for the random generator.  If not given (i.e. -1), a seed based on date/time will be used."},
  { "postingsScaleFactor", AFLOAT, (void *)&(params.postingsScaleFactor), "Controls how many postings to generate relative to the training corpus."},
  { "useOrderZero", ABOOL, (void *)&(params.useOrderZero), "If TRUE, corpus will be gnerated from a context free model."},
  { "verbose", ABOOL, (void *)&(params.verbose), "If TRUE, more progress information may be displayed."},
 
  { "", AEOL, NULL, "" }
  };


void initialiseParams() {
  params.inFileName = NULL;
  params.outFileName = NULL;
  params.numHeadTerms = 0;
  params.hashBits = 20;  // Enough for a vocab size of almost 1 million.  Will double if not enough
  params.randomSeed = -1;  // Represents undefined
  params.postingsScaleFactor = 1;  // Default is emulation
  params.useOrderZero = FALSE;
  params.verbose = FALSE;
}



