// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                               MarkovGenerator.c                                  //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Takes a base corpus C of documents in STARC, TSV or TREC format and builds an
// order one Markov model, then generates a random output corpus using the transition
// probabilities from the model.
//
// We need two scans of the input.  First to build the vocab table so as to be able to
// represent words by their word-ids (ranks).
//
// The transition matrix is represented using a hash table to represent the context word.
// Each entry in the hash table includes a total occurrence count plus a pointer to a
// linked list in which each entry has the word-id of the following word and a count
// of the the number of times this word is preceded by the context word.   The list
// is single linked but uses a move-to-front heuristic to encourage more frequently
// occurring words to positions early in the list.
//
// The use of linked lists makes both the building of the transition matrix and the
// generation of the emulated corpus unacceptably slow.   As a first step to improving
// that, the lists of successors for head terms are now maintained as hashtables during
// building of the matrix, and as cumulative probability arrays during generation.
// Still slow but much faster than before.

#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "characterSetHandling/unicode.h"
#include "utils/general.h"
#include "utils/argParser.h"
#include "MarkovGenArgTable.h"
#include "MarkovGenerator.h"
#include "imported/TinyMT_cutdown/tinymt64.h"
#include "utils/randomNumbers.h"
#include "shuffling.h"


static char docCopy[MAX_DOC_LEN + 1], *docWords[MAX_DOC_WORDS];

static u_char textEOD[] = "@!EOD!@";


globals_t globals;

params_t params;

static void initialiseGlobals(globals_t *globals) {
  globals->startTime = what_time_is_it();
  globals->numDocs = 0;
  globals->numEmptyDocs = 0;
  globals->totalPostingsIn = 0;

  // Set up global hash with room for overall occurrence frequency, rank in Vocab
  // and a pointer to a list of following words.
  globals->gMarkovHash =
    dahash_create((u_char *)"globalVocab", params.hashBits, MAX_WORD_LEN, sizeof(MarkovEntry_t),
		  (double)0.9, FALSE);
  globals->vocabRanking = NULL;
  globals->llStorage = NULL;
  globals->llNextFree = NULL;
  globals->firstWordIds = NULL;
  globals->memUseBySubsidiaryHashes = 0;
  globals->vocabOut = 0;
  globals->wordsUsed = NULL;
  globals->numHeadTerms = DFLT_HEAD_TERMS;
  globals->MarkovOrder = 1;  // Unless we get a useOrderZero option.
  globals->debug = 0;
}


static void llPack(u_int termId, u_int freq, u_ll nextP, byte *where) {
  // write the bytes of termid, freq and nextP in the appropriate widths starting at where
  int b;
  byte x, *w = where;
  for (b = 0; b < llRankBytes; b++) {
    x = termId & 0xFF;
    *w++ = x;
    termId >>= 8;
  }
  for (b = 0; b < llFreqBytes; b++) {
    x = freq & 0xFF;
    *w++ = x;
    freq >>= 8;
  }
  for (b = 0; b < llNextPBytes; b++) {
    x = nextP & 0xFF;
    *w++ = x;
    nextP >>= 8;
  }
}


static void llUnpack(u_int *termId, u_int *freq, u_ll *nextP, byte *where) {
  // Extract the bytes representing termID and nextP
  int b;
  byte *w = where;
  *termId = 0;
  *freq = 0;
  *nextP = 0;
  for (b = llRankBytes - 1; b >= 0;  b--) {
    *termId <<= 8;
    *termId |= w[b];
  }
  w += llRankBytes;

  for (b = llFreqBytes - 1; b >= 0;  b--) {
    *freq <<= 8;
    *freq |= w[b];
  }
  w += llFreqBytes;
  
  for (b = llNextPBytes - 1; b >= 0; b--) {
    *nextP <<= 8;
    *nextP |= w[b];
  }
}


static void llTestPackUnpack() {
  byte array[100];
  u_int termId, freq;
  u_ll nextP;
  printf("Testing linked list pack/unpack ");
  llPack(1717, 38, 872505, array);
  llUnpack(&termId, &freq, &nextP, array);
  if (termId != 1717 || freq != 38 || nextP != 872505) {
    printf(" -- test FAILED.  %u should have been 1717.  %u should have been 38.  %llu should have been 872505\n",
	   termId, freq, nextP);
    exit(1);
  } else printf(" -- test PASSED\n");
}


/* static void checkHashTable(byte *term, dahash_table_t *ht) { */
/*   // One of the subsidiary hashtables (the one for 'home') is having its */
/*   // first 13 entries over-written.  This just checks every time something is */
/*   // inserted that the keys are all zero. */
/*   off_t htOff; */
/*   int e, nonEmpties = 0; */
/*   byte *htEntry; */
  
/*   htOff = 0; */
/*   for (e = 0; e < ht->capacity; e++) { */
/*     htEntry = ((byte *)(ht->table)) + htOff; */
/*     if (htEntry[0]) {    // Entry is used if first byte of key is non-zero */
/*       nonEmpties++; */
/*     } */
/*     htOff += ht->entry_size; */
/*   } */

/*   if (nonEmpties != ht->entries_used) { */
/*     printf("\nAlarm! Alarm! hash table '%s' is corrupted.  %d vs %zu / %zu\n", */
/* 	   ht->name, nonEmpties, ht->entries_used, ht->capacity); */
/*     globals.debug = 2; */
/*   } */
/* } */

/* static void checkAllHashTables(dahash_table_t *mht) { */
/*   off_t htOff; */
/*   int e, nonEmpties = 0; */
/*   byte *htEntry; */
/*   dahash_table_t *sht; */
/*   MarkovEntry_t *MHV;  */
/*   // Check the main hash table first */
/*   htOff = 0; */
/*   for (e = 0; e < mht->capacity; e++) { */
/*     htEntry = ((byte *)(mht->table)) + htOff; */
/*     if (htEntry[0]) {    // Entry is used if first byte of key is non-zero */
/*       nonEmpties++; */
/*     } */
/*     htOff += mht->entry_size; */
/*   } */

/*   if (nonEmpties != mht->entries_used) { */
/*     printf("\nAlarm! Alarm! Alarm! Main hash table '%s' is corrupted.  %d vs %zu / %zu\n", */
/* 	   mht->name, nonEmpties, mht->entries_used, mht->capacity); */
/*     globals.debug = 2; */
/*     exit(1); */
/*   } */

/*   printf("checkAllHashTables(%s) Main table is OK.\n", mht->name); */

/*   // Now check all of the subsidiaries */

/*   htOff = 0; */
/*   for (e = 0; e < mht->capacity; e++) { */
/*     htEntry = ((byte *)(mht->table)) + htOff; */
/*     if (htEntry[0]) {    // Entry is used if first byte of key is non-zero */
/*       MHV = (MarkovEntry_t *)(htEntry + mht->key_size); */
/*       if (MHV->rank <= globals.numHeadTerms) { */
/* 	sht = (dahash_table_t *)(MHV->first); */
/* 	checkHashTable(htEntry, sht); */
/*       } */
/*     } */
/*     htOff += mht->entry_size; */
/*   } */

/*   printf("checkAllHashTables(%s) complete.\n\n", mht->name); */
/* } */


static int llAppend(u_int termId, MarkovEntry_t *ME) {
  //
  byte *LE0, *LE1;
  u_int TI0, TI1, freq0, freq1;
  u_ll next0, next1, newNext;
  int elementCount;
  int tracing = globals.debug;


  if (tracing) printf("Appending term %u to hashtable entry for %s\n",
	 termId, (byte *)ME - globals.gMarkovHash->key_size);
  
  if (ME->first == NULL) {
    // There is no linked list here.
    if (tracing) printf("llAppend() - creating new list for term %d\n", termId);
    llPack(termId, 1, llNULL, globals.llNextFree);
    ME->first = globals.llNextFree;
    globals.llNextFree += llEltSize;
    if (globals.llNextFree >= globals.llStorageLimit) {
      printf("Catastrophe1: Globals.llStorage overflow!\n");
      exit(1);
    }
    return 0;   // ------------------->
  } else {
    // There is a linked list.  Scan it for termid.
    LE0 = ME->first;
    LE1 = NULL;
    llUnpack(&TI0, &freq0, &next0, LE0);
    if (tracing) printf(" ----- %d, %d, %llX\n", TI0, freq0, next0);
    if (TI0 == termId) {
      // Matching element is the first in the list.  Increment count and leave where it is
      if (tracing) printf("llAppend() - incrementing frequency of first list element for term %d\n", termId);
      llPack(TI0, freq0 + 1, next0, LE0);
      return 0;   // ------------------->
    }
    if (next0 == llNULL) {
      // There's only one element in the list and it wasn't the one we want.  Insert new elt at head.
      if (tracing) printf("llAppend() - inserting list element in list of one for term %d\n", termId);
      newNext = (ME->first - globals.llStorage) / llEltSize;
      llPack(termId, 1, newNext, globals.llNextFree);
      ME->first = globals.llNextFree;
      globals.llNextFree += llEltSize;
      if (globals.llNextFree >= globals.llStorageLimit) {
	printf("Catastrophe2: Globals.llStorage overflow!\n");
	exit(1);
      }
      return 0;   // ------------------->	
    }

    
    // List continues.  Prepare to scan it.
    LE1 = globals.llStorage + next0 * llEltSize;
    elementCount = 2;
    
    while (1) {
      // We've already considered the element corresponding to LE0, now look at LE1
      llUnpack(&TI1, &freq1, &next1, LE1);
      if (0) printf("llAppend() - Looping through element %d for term %d\n",
		      elementCount, termId);
      if (TI1 == termId) {
	if (tracing) printf("llAppend(%s) - incrementing frequency of element %d for term %d\n",
			    (byte *)ME - globals.gMarkovHash->key_size, elementCount, termId);
	// We've found the list element corresponding to termID, increase the frequency and
	// move to front.  That means:
	// 0. Change LE0->next to LE1->next
	// 1. Change LE1->next to point to what ME->first used to.
	// 2. Change ME->first to point to LE1
	llUnpack(&TI0, &freq0, &next0, LE0);   // Step 0
	llPack(TI0, freq0, next1, LE0);
	llPack(TI1, freq1 + 1, (ME->first - globals.llStorage) / llEltSize, LE1); // Update freq and Step 1
	ME->first = LE1;  // Step 2	
	return 0;   // ------------------->
      }

      if (next1 == llNULL) {
	// Element corresponding to termId not present. Insert new list element at the head of the list.
	if (tracing) printf("llAppend(%s) - reached end of list of %d elements for term %d\n",
			    (byte *)ME - globals.gMarkovHash->key_size, elementCount, termId);
	newNext = (ME->first - globals.llStorage) / llEltSize;
	llPack(termId, 1, newNext, globals.llNextFree);
	ME->first = globals.llNextFree;
	globals.llNextFree += llEltSize;
	if (globals.llNextFree >= globals.llStorageLimit) {
	  printf("Catastrophe2: Globals.llStorage overflow!\n");
	  exit(1);
	}
	return 0;   // ------------------->	
      }

      
      // Move to the next element of the list  -- We know next1 is not llNULL
      LE0 = LE1;
      LE1 = globals.llStorage + next1 * llEltSize;
      elementCount++;
    }
  }
  return 0;
}


static void llShowList(byte *htEntry) {
  // For debugging purposes, print the term corresponding to ME, and each element of
  // its list.
  llPointer p;
  u_int tid, f;
  long long n;
  MarkovEntry_t *ME;
  
  ME = (MarkovEntry_t *)(htEntry + globals.gMarkovHash->key_size);
  printf("llShowList(%s): Rank: %d; Total frequency: %d; List: ",
	 htEntry, ME->rank, ME->freq);
  p = ME->first;
  while (1) {
    llUnpack(&tid, &f, &n, p);
    printf("(%s, %d) ", globals.vocabRanking[tid -1], f);
    if (n == llNULL) break;  // ---->
    p = globals.llStorage + n * llEltSize;
  }
  printf("\n");
}


static u_int getRank(u_char *word) {
  // Look up word in globals.gMarkovHash and return the rank member
  MarkovEntry_t *ME;
  ME = (MarkovEntry_t *)dahash_lookup(globals.gMarkovHash, word, 0);
  
  if (ME == NULL) {
    printf("Error: Can't happen: Word %s supplied to getRank not found in hash\n",
	   word);
    exit(1);
  }
  if (0) printf("getRank(%s) = %d\n", word, ME->rank);
 
  return ME->rank;
}


static int hashInsert(byte *term, MarkovEntry_t *ME) {
  MarkovHash_t *MHP;
  byte *parentTerm;
  parentTerm = (byte *)ME - globals.gMarkovHash->key_size;
  if (ME->first == NULL) {
    // First occurrence of a successor - create the hash table
    ME->first = (byte *)dahash_create(parentTerm, 8, MAX_WORD_LEN,
				   sizeof(MarkovHash_t), 0.9, 0);
    if (ME->first == NULL) {
      printf("Error: hashInsert(): ahash_create() failed\n");
      exit(1);
    } else if (0) {
      printf("hashInsert(): Created subsidiary hash '%s' for rank %d\n", 
	     ((dahash_table_t *)(ME->first))->name, ME->rank);
    }
    globals.memUseBySubsidiaryHashes += dahash_memory_used((dahash_table_t *)(ME->first));
    MHP = (MarkovHash_t *)dahash_lookup((dahash_table_t *)(ME->first), term, 1);
    MHP->freq = 1;
    if (!strcmp(term, textEOD)) MHP->rank = EOD;
    else MHP->rank = getRank(term);
    if (0) printf("Successors, starting with %s, of term %d are to be stored in hash tables rather than linked lists.\n",
		  term, ME->rank);
  } else {
    if (globals.debug) {
      printf("   Inserting '%s' into hash table for '%s' with %zu out of %zu entries\n", term, parentTerm,
	     ((dahash_table_t *)(ME->first))->entries_used, ((dahash_table_t *)(ME->first))->capacity);
      if (!strcmp(term, "cimarron")) ((dahash_table_t *)(ME->first))->verbose = 1;
      MHP = (MarkovHash_t *)dahash_lookup((dahash_table_t *)(ME->first), term, 0);
      if (MHP != NULL) {
	printf("Frequency prior to addition is %d\n", MHP->freq);
      } else {
	printf("Adding new entry for %s\n", term);
      }
    }
    
    MHP = (MarkovHash_t *)dahash_lookup((dahash_table_t *)(ME->first), term, 1);
    MHP->freq++;
    if (!strcmp(term, textEOD)) MHP->rank = EOD;
    else MHP->rank = getRank(term);
  }
  //if (!strcmp(parentTerm, "home") && ((dahash_table_t *)(ME->first))->capacity > 2000) checkFirst13Entries(term, (dahash_table_t *)(ME->first));
  return 0;
}


static void convertHashesToArrays() {
  // Only does this for the head terms (if defined)
  int t, u, e, *countP, itemsTransferred;
  byte *newArray, *htEntry;
  MarkovEntry_t *ME;
  dahash_table_t *ht;
  off_t htOff = 0;
  probStruct_t *cumProbs;
  MarkovHash_t *MH;
  double prob, cumProb = 0, totFreq;
  long long sumOfFreqs;
  
  for (t = globals.numHeadTerms; t > 0; t--) {
    // Look up the t-th most popular word in the global hash
    ME = (MarkovEntry_t *)dahash_lookup(globals.gMarkovHash, globals.vocabRanking[t - 1], 0);
    if (ME == NULL) {
      printf("Error: convertHashesToArrays() - lookup failed for '%s'\n", globals.vocabRanking[t - 1]);
      exit(1);
    }
    totFreq = (double)(ME->freq);
    ht = (dahash_table_t *)(ME->first);  // Get a pointer to the subsidiary hash table
    if (0) printf("Transferring %zu items from hash %s\n", ht->entries_used, ht->name);
    newArray = (byte *)cmalloc(ht->entries_used * sizeof(probStruct_t) + sizeof(int), "convertHashesToArrays", 0);
    countP = (int *)newArray;   // First 4 bytes of newArray are a count of how many probstructs are in the rest of it.
    *countP = ht->entries_used;
    cumProbs = (probStruct_t *)(newArray + sizeof(int));
    cumProb = 0;
    itemsTransferred = 0;
    sumOfFreqs = 0;
    u = 0; // Index of next entry to go in cumProbs
    htOff = 0;
    for (e = 0; e < ht->capacity; e++) {
      htEntry = ((byte *)(ht->table)) + htOff;
      if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
	MH = (MarkovHash_t *)(htEntry + ht->key_size);
	sumOfFreqs += MH->freq;
	itemsTransferred++;
	if (MH->freq <= 0 || MH->freq > ME->freq) {
	  printf("\nError: Bad frequency %d in subsidiary hash for term rank %d (%s).  ME->freq = %d\n",
		 MH->freq, t, globals.vocabRanking[t - 1], ME->freq);
	  printf("Subsidiary hash (%s) used %zu / %zu entries.\n",
		 ht->name, ht->entries_used, ht->capacity);
	  printf("Main hash (%s) uses %zu / %zu entries.\n",
		 globals.gMarkovHash->name, globals.gMarkovHash->entries_used, globals.gMarkovHash->capacity);
	  exit(1);
	}
	prob = (double)(MH->freq) / totFreq;
	if (prob > 1.0 || prob < 0.0) {
	  printf("Probability out of range: %.6f\n", prob);
	  printf("%3d: Prob calculation: %.0f / %.0f => %.6f\n", t, (double)(MH->freq), totFreq, prob);
	  printf("Subsidiary hash (%s) used %zu / %zu entries.\n",
		 ht->name, ht->entries_used, ht->capacity);
	  printf("Main hash (%s) uses %zu / %zu entries.\n",
		 globals.gMarkovHash->name, globals.gMarkovHash->entries_used, globals.gMarkovHash->capacity);
	  exit(1);
	}
	cumProb += prob;
	cumProbs[u].rank = MH->rank;
	cumProbs[u++].cumProb = cumProb;
      }
      htOff += ht->entry_size;
    }

    if (0) printf("Items transferred: %d / %zu  Total frequencies: %lld / %d\n",
		  itemsTransferred, ht->entries_used, sumOfFreqs, ME->freq);

    if ((size_t)itemsTransferred != ht->entries_used) {
      printf("Error: wrong number of items transferred. %d vs. %zu for term %d\n",
	     itemsTransferred, ht->entries_used, t);
      exit(1);
    }

    if (cumProbs[u - 1].cumProb > 1.0001  || cumProbs[u - 1].cumProb < 0.9999) {
      printf("Error in cumulative probabilities.  Last cumProb for term %d: %.6f\n", 
	     t, cumProbs[u - 1].cumProb);
      printf(" Total frequency for this term was %.0f. sumOfFreqs = %lld\n", totFreq, sumOfFreqs);
      for (e = 0; e < ht->capacity; e++) {
	htEntry = ((byte *)(ht->table)) + htOff;
	if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
	  MH = (MarkovHash_t *)(htEntry + ht->key_size);
	  sumOfFreqs += MH->freq;
	  printf(" -- %d --\n", MH->freq);
	}
      }

      exit(1);
    }

    // Destroy the small hash
    // Note that the entries in the arrray will be in random order.  Perhaps they could be sorted in some way
    // to speed things up?
    dahash_destroy((dahash_table_t **)&(ME->first));
    ME->first = newArray;
  }  // End of loop over head words whose successors are recorded as hashes.
  if (0) printf("convertHashesToArrays() -- Finished!\n");
}


static int buildMarkovModel(globals_t *globals, char *docText, size_t docLen, int docNum) {
  // This function adds all the information from a single document represented by docText
  // to the Markov transition matrix (at this stage containing frequencies rather than
  // transition probabilities.)
  int numWords, w;
  u_int nextWordId;
  MarkovEntry_t *hashValueP;
  u_char *nextWord;
  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.

  if (docLen > MAX_DOC_LEN) docLen = MAX_DOC_LEN;
  if ((docText[docLen] & 0xC0) == 0x80) {
    if (0) printf("Document is being truncated\n");
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80 && docLen > 0);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }

  // Copy document text so we can write on it
  memcpy(docCopy, docText, docLen);
  docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
  if (globals->debug) {
    printf("buildMarkovModel: len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n",
	   docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	   globals->inputSize - 1);
    put_n_chars(docCopy, 100);
    printf("\n\n\n");
  }

  numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							MAX_DOC_WORDS, MAX_WORD_LEN,
							TRUE,  // case-fold line before splitting
							FALSE, // remove accents before splitting
							FALSE,  // Perform some heuristic substitutions 
							FALSE   // True if words must contain an ASCII alnum
							);

  if (numWords <= 0) {
    if (docNum > globals->numDocs) {
      printf("Error: bad index to firstWordIds. (Regression to old error).\n");
      exit(1);
    }
    globals->firstWordIds[docNum] = 1;  // Choose most frequent word.  Needed to avoid segfaults
    return 0;  // ----------------------------->
  }

  if (docNum > globals->numDocs) {
    printf("Error: bad index to firstWordIds. (Regression to old error).\n");
    exit(1);
  }
  globals->firstWordIds[docNum] = getRank(docWords[0]);

  for (w = 0; w < numWords; w++) {
    if (w == (numWords - 1)) {
      nextWord = textEOD;
      nextWordId = EOD;
    } else {
      nextWord = docWords[w + 1];
      nextWordId = getRank(docWords[w + 1]);
    }

     // dahash_lookup() returns a pointer to the first byte of the value part of the entry.
    hashValueP = (MarkovEntry_t *)dahash_lookup(globals->gMarkovHash, docWords[w], 0);
    if (hashValueP == NULL) {
      printf("Error: buildMarkovModel(): lookup of %s in gMarkovHash failed\n", docWords[w]);
      exit(1);
    }

    if (globals->debug && w != (numWords - 1))  
      printf("Appending %s termid %u to entry for %s\n",
	     docWords[w + 1], nextWordId, (byte *)hashValueP - globals->gMarkovHash->key_size);

    if (globals->numHeadTerms >= hashValueP->rank) {  // ranks start from 1.
      if (globals->debug) printf(" %d: hashInserting '%s'\n", w, nextWord);
      hashInsert(nextWord, hashValueP);
    } else {
      if (globals->debug) printf(" %d: llAppending '%s'\n", w, nextWord);
      llAppend(nextWordId, hashValueP);
    }
  }

  return 0;
}


static int vCmp(const void *i, const void *j) {
  // i and j are pointers to pointers to entries in globals.gMarkovHash
  byte **ip = (byte **)i, **jp = (byte **)j;
  byte *htEntryi, *htEntryj;
  MarkovEntry_t *wci, *wcj;

  htEntryi = *ip;
  wci = (MarkovEntry_t *) (htEntryi + globals.gMarkovHash->key_size);
  htEntryj = *jp;
  wcj = (MarkovEntry_t *) (htEntryj + globals.gMarkovHash->key_size);
  if (wcj->freq > wci->freq) return 1;
  if (wci->freq > wcj->freq) return -1;
  return strcmp(htEntryi, htEntryj);
}

static void makeVocabRanking(globals_t *globals) {
  // Having accumulated the entire vocabulary in globals->gMarkovHash,
  // we make a permutation array globals->vocabRanking and sort it
  // by descending frequency of occurrence.  We then traverse the
  // ranking represented by the permutation array and insert the term
  // rank into the rank member of the relevant entry in globals->gMarkovHash.
  // That done, we can free the memory used by globals->vocabRanking
  int e, v = 0;
  byte *htEntry;
  off_t htOff = 0;
  MarkovEntry_t *ME;

  globals->vocabSize = globals->gMarkovHash->entries_used;
  globals->vocabRanking = (byte **)cmalloc(globals->vocabSize * sizeof(byte *),
					  "vocabRanking", 0);
  
  for (e = 0; e < globals->gMarkovHash->capacity; e++) {
    htEntry = ((byte *)(globals->gMarkovHash->table)) + htOff;
    if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
      globals->vocabRanking[v++] = htEntry;   // Term at rank one, will be in element 0 of the array
    }
    htOff += globals->gMarkovHash->entry_size;
  }

  // Each entry in globals->vocabRanking is a pointer to the key of
  // an entry in globals->gMarkovHash.    Sort the vocabRanking in
  // descending order of frequency.

  qsort(globals->vocabRanking, globals->vocabSize, sizeof(byte *), vCmp);

  if (globals->debug) {
    printf("Ranked vocab\n============\n");
    for (v = 0; v < 20; v++) {
      htEntry = globals->vocabRanking[v];
      printf("%6d: %20s\t%9d\n", v + 1, htEntry, (wordCounter_t)
	     *(wordCounter_t *)(htEntry + globals->gMarkovHash->key_size));
    }
  }

  // Now transfer the ranks into the .rank part of the MarkovEntry_t's
  // and see if globals->numHeadTerms needs to be reduced

  printf("\nHeadTerms was %d, ", globals->numHeadTerms);
  for (v = 0; v < globals->vocabSize; v++) {
    ME = (MarkovEntry_t *)(globals->vocabRanking[v] + globals->gMarkovHash->key_size);
    ME->rank = v + 1;
    if (v < globals->numHeadTerms && ME->freq < MIN_HEAD_TERM_FREQ)
      globals->numHeadTerms = v;
  }
  printf("is now %d\n", globals->numHeadTerms);
}


static int buildVocab(globals_t *globals, char *docText, size_t docLen,
		      u_char *format) {
  int numWords, w;
  wordCounter_t *counter, tf;
  MarkovEntry_t *ME;
  dahash_table_t *lVocab = NULL;
  char *htEntry;
  off_t htOff;
  long long e;
  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.

  if (0) printf("Process_one_doc()\n");
  if (docLen > MAX_DOC_LEN) docLen = MAX_DOC_LEN;
  if ((docText[docLen] & 0xC0) == 0x80) {
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80 && docLen > 0);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }
  // Copy document text so we can write on it
  memcpy(docCopy, docText, docLen);
  docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
  if (0) {
    printf("buildVocab(): len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n",
	   docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	   globals->inputSize - 1);
    put_n_chars(docCopy, 100);
    printf("\n\n\n");
  }
  numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							MAX_DOC_WORDS, MAX_WORD_LEN,
							TRUE,  // case-fold line before splitting
							FALSE, // remove accents before splitting
							FALSE,  // Perform some heuristic substitutions 
							FALSE   // must contain ASCII alnum
							);

  if (numWords <= 0) {
    globals->numEmptyDocs++;
    return 0;  // ----------------------------->
  }

    // Create a local vocab hash table  (10 bits cos most docs have few distinct words).
  // Just count up the frequency of each distinct word
  lVocab = dahash_create("localVocab", 10, MAX_WORD_LEN, sizeof(wordCounter_t),
			 (double)0.9, FALSE);

  
 for (w = 0; w < numWords; w++) {
    if (0) printf("Word %d: %s\n", w, docWords[w]);
    counter = (wordCounter_t *)dahash_lookup(lVocab, docWords[w], 1);   // 1 means add the key if it's not already there.
     (*counter)++;
     if (*counter >= WCMAX) {
       printf("\n\nError: buildVocab(local words): Counter overflow.  Recompile with -DVERY_LARGE_CORPUS\n");
       exit(1);
     }
  }

  // Now transfer the data from the local hash to the global one.
  htOff = 0;
  for (e = 0; e < lVocab->capacity; e++) {
    htEntry = ((char *)(lVocab->table)) + htOff;
    if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
      if (0) printf("transferring entry %lld/%zd\n", e, lVocab->capacity);
      tf = *((wordCounter_t *)(htEntry + lVocab->key_size));
      ME = (MarkovEntry_t *)dahash_lookup(globals->gMarkovHash, htEntry, 1);   
      ME->freq += tf;  // 
      if (ME->freq >= WCMAX) {
	printf("\n\nError: buildVocab(global words tf): Counter overflow.  Recompile with -DVERY_LARGE_CORPUS\n");
	exit(1);
      }    
    }
    htOff += lVocab->entry_size;
  }

  // Finally free the space from the local hash
  dahash_destroy(&lVocab);

  globals->totalPostingsIn += numWords;
  return numWords;
}


// The following three processXXXFormat functions are copied from Nomenclator.c
// ... which may have copied them from other modules in this directory.
// Note that they each version calls different functions and that the Nomenclator
// version and this one support two passes over the input.

static void processTSVFormat(globals_t *globals, BOOL phase1) {
  // Input is assumed to be in TSV format with an arbitrary (positive) number of
  // columns (including one column), in which the document text is in
  // column one and the other columns are ignored.   (All LFs and TABs are assumed
  // to have been removed from the document.)
  char *lineStart, *p, *inputEnd;
  long long printerval = 1000;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  lineStart = globals->inputInMemory;
  globals->numDocs = 0; 
  p = lineStart;
  while (p <= inputEnd) {
    // Find the length of column 1, then process the doc.
    while (p <= inputEnd  && *p >= ' ') p++;  // Terminate with any ASCII control char
    docLen = p - lineStart;
    if (phase1) {
      buildVocab(globals, lineStart, docLen, "TSV");
    } else {
      if (0) printf("About to process a doc.\n");
      buildMarkovModel(globals, lineStart, docLen, globals->numDocs);
      globals->numDocs++;
      if (globals->numDocs % printerval == 0) {
	printf("   --- %lld docs scanned @ %.3f msec per record --- postings thus far: %lld\n",
	       globals->numDocs,
	       (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
	       globals->totalPostingsIn);
	if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    }
    // Now skip to end of line (LF) or end of input.
    while (p <= inputEnd  && *p != '\n') p++;  // Terminate with LineFeed only
    p++;
    lineStart = p;
  }
}


static void processTRECFormat(globals_t *globals, BOOL phase1) {
  // Input is assumed to be in highly simplified TREC format, such as that produced by
  // the detrec program.  Documents are assumed to be DOC elements, each starting with
  // a DOCNO element.  It is also assumed that the content contains no other sub-elements
  // and no stray angle brackets.  Ideally, the text is just words separated by spaces.
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 1;
  size_t docLen;
  double startTime = what_time_is_it();

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  globals->numDocs = 0;
  while (p <= inputEnd) {
    if (0) printf("Looking for <TEXT>\n");
    q = mlstrstr(p, "<TEXT>", inputEnd - p - 6, 0, 0);
    if (q == NULL) break;   // Presumably the end of the file. ... or wrong format file.
    docStart = q + 6;
    if (0) printf("Looking for </TEXT>\n");
    q = mlstrstr(docStart, "</TEXT>", inputEnd - docStart - 7, 0, 0);
    if (q == NULL) break;   // Presumably file is incorrectly formatted.    
    docLen = q - docStart;
    if (phase1) {
      buildVocab(globals, docStart, docLen, "TREC");
    } else {
      buildMarkovModel(globals, docStart, docLen, globals->numDocs);
    }
    globals->numDocs++;
    if (globals->numDocs % printerval == 0) {
      if (phase1) printf("   buildVocab");
      else printf("   buildMarkovModel");
      printf(" --- %lld docs scanned @ %.3f msec per record",
	     globals->numDocs,
	     (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs);
      if (phase1) printf(" --- postings thus far: %lld\n", globals->totalPostingsIn);
      else printf("\n");
      if (printerval < 10000) printerval *= 10;
    }
    p = q + 7;  // Move to char after </TEXT>
  }
  if (phase1) printf("   Vocab built");
  else printf("   MarkovModel built");
  printf(" --- %lld docs scanned @ %.3f msec per record --- total postings: %lld\n\n",
	 globals->numDocs,
	 (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs,
	 globals->totalPostingsIn);

}




static void processSTARCFormat(globals_t *globals, BOOL phase1) {
  // Input is assumed to be docs in in a very simple <STARC
  // header><content> format. The STARC header begins and ends with a
  // single ASCII space.  Following the leading space is the content
  // length in decimal bytes, represented as an ASCII string,
  // immediately followed by a letter indicating the type of record.
  // Record types are H - header, D - document, or T - Trailer.  The
  // decimal length is expressed in bytes and is represented in
  // ASCII. If the length is L, then there are L bytes of document
  // content following the whitespace character which terminates the
  // length representation.  For example: " 13D ABCDEFGHIJKLM 4D ABCD"
  // contains two documents, the first of 13 bytes and the second of 4
  // bytes.
  //
  // Although this representation is hard to view with editors and
  // simple text display tools, it completely avoids the problems with
  // TSV and other formats which rely on delimiters, that it's very
  // complicated to deal with documents which contain the delimiters.
  //
  // This function skips H and T docs.
  // If phase1 is TRUE, each D record is passed to buildVocab()
  // otherwise to buildMarkovModel().
  
  size_t docLen;
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 10;
  byte recordType;

  globals->numDocs = 0; 
  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  while (p <= inputEnd) {
    // Decimal length should be encoded in ASCII at the start of this doc.
    if (*p != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     p - globals->inputInMemory);
      exit(1);

    }
    errno = 0;
    docLen = strtol(p, &q, 10);  // Making an assumption here that the number isn't terminated by EOF
    if (errno) {
      fprintf(stderr, "processSTARCFile: Error %d in strtol() at offset %zd\n",
	     errno, p - globals->inputInMemory);
      exit(1);
    }
    if (docLen <= 0) {
      printf("processSTARCFile: Zero or negative docLen %zd at offset %zd\n",
	     docLen, p - globals->inputInMemory);
      exit(1);
    }

    recordType = *q;    
    if (recordType != 'H' && recordType != 'D' && recordType != 'T') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);
    }
    q++;
    if (*q != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't end with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);

    }
    
    docStart = q + 1;  // Skip the trailing space.
    if (0) printf(" ---- Encountered %c record ---- \n", recordType);
    if (recordType == 'D') {
      if (phase1) {
	buildVocab(globals, docStart, docLen, "STARC");
      } else {
	if (0) printf("About to process a doc.\n");
	buildMarkovModel(globals, docStart, docLen, globals->numDocs);
	globals->numDocs++;
	if (globals->numDocs % printerval == 0) {
	  printf("   --- %lld docs scanned @ %.3f msec per record--- Postings thus far: %lld\n",
		 globals->numDocs,
		 (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
		 globals->totalPostingsIn);
	  if (globals->numDocs % (printerval * 10) == 0) printerval *= 10;
	}
      }
    }
    p = docStart + docLen;  // Should point to the first digit of the next length, or EOF
  }
}


static u_int chooseRandomWord(int param) {
  double r = rand_val(0);
  r *= r * r;  // Bias toward low fractions.
  return (int)floor(r * (double)param + 0.5) + 1;
}

static probStruct_t *rand_cumdist_bsearch2(probStruct_t *cumProbs, int numProbs) {
  // Slightly modified from utils/randomNumbers.c to handle structs rather than doubles
  // CumProbs[] is an array 0 .. numProbs - 1 representing a cumulative
  // probability distribution.  It is assumed that cumProbs[numProbs - 1] = 1.0
  // and that the values in cumProbs are in ascending order;
  //
  // Returns an integer in 0 .. numProbs - 1
  double uniRand = rand_val(0);  // Uniform in range 0 -1
  int u = numProbs - 1, l = 0, m, counter = 0;

  if (numProbs < 3) {
    if (numProbs == 0) {
      printf("Error: rand_cumdist_bsearch2() called with %d elements\n", numProbs);
      exit(1);
    }
    // Special case when there are only 1 or 2 elements
    if (uniRand > cumProbs[0].cumProb) return cumProbs + 1;
    else return cumProbs;
  }
  if (0) printf("rand_cumdist_bsearch2() called with %d elements\n", numProbs);
  do {
    m = (u + l) / 2;
    if (uniRand > cumProbs[m].cumProb) {
      l = m + 1;
    } else {
      if ((uniRand == cumProbs[m].cumProb)
          || (m == 0) || (m == u) || (l > m)
          || (uniRand > cumProbs[m -1].cumProb)) {
	return cumProbs + m;   // ----------------------->
      } else {
        u = m;
      }
    }
  } while (++counter < 1000);

  printf("Error: rand_cumdist_bsearch2() looping. m = %d, l = %d, u = %d, uniRand = %.5f\n",
         m, l, u, uniRand);
  exit(1);
}
  



static int chooseNextWord(MarkovEntry_t *ME, double probRandom) {
  // Return the termId of the next word to emit or -1 if the list is empty.
  // With probability probRandom pick a random word from the background model,
  // otherwise pick randomly from the list of successors for the word
  // represented by ME.
  
  double target, dTotFreq = (double)(ME->freq);
  llPointer current, successor;
  u_int freq0, freq1, cumFreq0 = 0, cumFreq1 = 0, TI0, TI1;
  long long next0, next1;

  if (0) printf("Welcome to CNW\n");

  if (rand_val(0) <= probRandom) return chooseRandomWord(globals.vocabSize - 1);
  if (ME->first == NULL) return -1;   // ----------------->
    
  target = floor(rand_val(0) * dTotFreq + 0.5);
  if (0) printf("CNW: target %.0f\n", target);

  if (globals.numHeadTerms >= ME->rank) {
    // ************* HEAD TERM -- ARRAY *************************
    int count;
    probStruct_t *cPArray, *chosen;
    if (0) printf("Finding successor to head term %d using array rather than linked list\n",
		  ME->rank);
    // The storage pointed to by ME->first consists of an integer count followed by
    // count probStructs, representing termIds with cumulative probabilities.
    count = *((int *)ME->first);
    cPArray = (probStruct_t *)(ME->first + sizeof(int));
    chosen = rand_cumdist_bsearch2(cPArray, count);
    if (0) printf("Array choice: termid %d\n", chosen->rank);
    TI0 = chosen->rank;
    return TI0;
  } else {
    // ************ OTHER TERM -- LINKED LIST *******************
    current = ME->first;
    llUnpack(&TI0, &freq0, &next0, current);
    cumFreq0 += freq0;
    while (next0 != llNULL) {
      successor = globals.llStorage + next0 * llEltSize;
      llUnpack(&TI1, &freq1, &next1, successor);
      cumFreq1 += freq1;
      if (0) printf("  CNW: testing %.0f against %u and %u\n", target, cumFreq0, cumFreq1);
      if (target >= (double)cumFreq0 && target < (double)cumFreq1) {
	return TI0;    // ------------------------>
      }
      current = successor;
      next0 = next1;
      TI0 = TI1;
      freq0 = freq1;
      cumFreq0 = cumFreq1;
    }
    // Reached the end of the list.  Return the last item
    return TI0;    // ---------------------->
  }
}

int chooseFirstWordInDoc() {
  // The firstWordIds array shows the termId of the first word in every document. It
  // probably contains repetitions.
  long long r = random_long_long(0, globals.numDocs - 1);
  int rezo;
  rezo = globals.firstWordIds[r];
  if (0) printf("Choosing %lld-th element of globals.firstWordIds -> %d(%s)\n",
		       r, rezo, globals.vocabRanking[rezo - 1]);
  return rezo;
}


static void generateCorpusFromOrderZero() {
  long long postingsOut = 0, numDocs = 0;
  MarkovEntry_t *ME;
  double startTime = what_time_is_it(), *cumVocab = NULL, prob, cumProb = 0.0, PincludingEOD;
  int printerval = 1, i, tRank;
  BOOL firstWordInDoc = TRUE;

  if (params.verbose) setvbuf(globals.corpusOut, NULL, _IONBF, 0);

  PincludingEOD = (double)(globals.totalPostingsIn + globals.numDocs);  // Counting EODs as postings.
  // Create cumulative probability array from vocabRanking
  cumVocab = (double *)cmalloc((globals.vocabSize + 1)* sizeof(double), "cumVocab", 0);
  prob = (double)(globals.numDocs) / PincludingEOD;
  cumProb = prob;
  cumVocab[0] = cumProb;   // Probability of end-of-document
  for (i = 1; i <= globals.vocabSize; i++) {
    ME = (MarkovEntry_t *)(globals.vocabRanking[i - 1] + globals.gMarkovHash->key_size);
    prob = (double)(ME->freq) / (double)PincludingEOD;
    cumProb += prob;
    cumVocab[i] = cumProb;
  }

  fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);
  postingsOut = 0;
  while (postingsOut < globals.desiredTotalPostings) {
    tRank = rand_cumdist_bsearch(cumVocab, globals.vocabSize + 1);
    globals.wordsUsed[tRank] = 1;  // Set a flag to say we've emitted at least one of this word (or EOD).
    if (tRank == 0) {
      // End of document, start a new one.
      fprintf(globals.corpusOut, "</TEXT>\n</DOC>\n");
      if (numDocs % printerval == 0) {
	printf("   Markov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	       numDocs, postingsOut, globals.totalPostingsIn, (double)postingsOut
	       / 1000000.0 / (what_time_is_it() - startTime));
	if (printerval < 10000) printerval *= 10;
      }

      if (postingsOut < PincludingEOD)
	fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++); //Unless it's last
      firstWordInDoc = TRUE;
    } else {
      fprintf(globals.corpusOut, "%s ", globals.vocabRanking[tRank - 1]);
      postingsOut++;
      firstWordInDoc = FALSE;
    }
  }
     
  if (!firstWordInDoc)
    fprintf(globals.corpusOut, "</TEXT>\n</DOC>\n");
  

  printf("   Markov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	 numDocs, postingsOut, globals.totalPostingsIn, (double)postingsOut
	 / 1000000.0 / (what_time_is_it() - startTime));
  
  fprintf(globals.corpusOut, "</TEXT>\n</DOC>\n");
  globals.postingsOut = postingsOut;
  globals.docsOut = numDocs;
  printf("\n\nMG(%d): Corpus generated in %.3f sec.  \n"
	 "MG(%d): Rate of generation (ignoring training): %.3f Mpostings/sec.\n",
	 globals.MarkovOrder, what_time_is_it() - startTime,
	 globals.MarkovOrder, (double)globals.totalPostingsIn / 1000000.0 / (what_time_is_it() - startTime));
}


static void generateCorpusFromModel() {
  long long postingsOut = 0, numDocs = 0;
  int TI0, TI1;
  MarkovEntry_t *ME;
  double startTime = what_time_is_it();
  int printerval = 1;

  if (params.verbose) setvbuf(globals.corpusOut, NULL, _IONBF, 0);
  fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);
  
  TI0 = chooseFirstWordInDoc();
  TI1 = -1;
  fprintf(globals.corpusOut, "%s ", globals.vocabRanking[TI0 - 1]);
  postingsOut++;
  while (postingsOut < globals.desiredTotalPostings) {
    ME = (MarkovEntry_t *)(globals.vocabRanking[TI0 - 1] + globals.gMarkovHash->key_size);
    TI1 = chooseNextWord(ME, 0.001);
    if (TI1 < 0) {
      if (0) printf("  choosing random word after CNW returned -1: ");
      TI1 = chooseRandomWord(globals.vocabSize - 1);
      if (0) printf("%d\n", TI1);
    }
    
    if (TI1 == EOD) {
      fprintf(globals.corpusOut, "\n</TEXT>\n</DOC>\n");
      fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);
      if (numDocs % printerval == 0) {

	printf("   Markov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	       numDocs, postingsOut, globals.desiredTotalPostings, (double)postingsOut
	       / 1000000.0 / (what_time_is_it() - startTime));
	if (printerval < 10000) printerval *= 10;
      }
      TI0  = chooseFirstWordInDoc();  // Start new document with a random word
      fprintf(globals.corpusOut, "%s ", globals.vocabRanking[TI0 - 1]);
      globals.wordsUsed[TI0] = 1;  // Set a flag to say we've emitted at least one of this word (or EOD).
      postingsOut++;
    } else {  
      fprintf(globals.corpusOut, "%s ", globals.vocabRanking[TI1 - 1]);
      globals.wordsUsed[TI1] = 1;  // Set a flag to say we've emitted at least one of this word (or EOD).
      postingsOut++;
      TI0 = TI1;
    }
  }
  printf("   Markov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	 numDocs, postingsOut, globals.desiredTotalPostings, (double)postingsOut
	 / 1000000.0 / (what_time_is_it() - startTime));

  fprintf(globals.corpusOut, "</TEXT>\n</DOC>\n");
  globals.postingsOut = postingsOut;
  globals.docsOut = numDocs;
  printf("\n\nMG(%d): Corpus generated in %.3f sec.  \n"
	 "MG(%d): Rate of generation (ignoring training): %.3f Mpostings/sec.\n",
	 globals.MarkovOrder, what_time_is_it() - startTime,
	 globals.MarkovOrder, (double)globals.desiredTotalPostings / 1000000.0 / (what_time_is_it() - startTime));

}


static void printSummary(globals_t *globals) {  
  printf("docs=%lld # Including %lld zero-length\n", globals->numDocs, globals->numEmptyDocs);
  printf("vocab_size=%zd\n", globals->gMarkovHash->entries_used);
  //printf("longest_list=%lld\n", globals->longestPostingsListLength);
  printf("total_postings=%lld\n", globals->postingsOut);
}

static void printUsage(char *progName, arg_t *args) {
  printf("Usage: %s inFile=<blah> outFile=<blah> [ <arg=val> ...]\n\n", progName);
  print_args(TEXT, args, "Default");
  exit(1);
}


int main(int argc, char **argv) {
  int error_code,  a;
  u_char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
  char *ignore;
  double startTime, veryStart, MB, totalMB = 0;
  long long tRank;
 

  veryStart = what_time_is_it();
  
  setvbuf(stdout, NULL, _IONBF, 0);

  if (sizeof(u_char *) != 8) {
    printf("\n\nWarning: Pointers are only 4-byte. It is recommended that %s should be compiled for 64 bit.\n\n", argv[0]);
  }



  initialiseParams();
  if (argc < 3) printUsage(argv[0], args);

  llTestPackUnpack();
  
  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);

  printf("Sizeof(size_t) = %zu, Sizeof(off_t) = %zu, Sizeof(byte *) = %zu\n",
	 sizeof(size_t), sizeof(off_t), sizeof(byte *));

  if (params.verbose) printf("Params initialised\n");
  initialiseGlobals(&globals);   // Includes the hashtables as well as scalar values
  if (params.verbose) printf("Globals initialised\n");
  for (a = 1; a < argc; a++) {
    assign_one_arg(argv[a], (arg_t *)(&args), &ignore);
  }
  
  if (params.randomSeed == -1) params.randomSeed = (u_ll)fmod(veryStart * 1000000.0, 1000000.0);
  rand_val((u_ll)params.randomSeed);  // Seed the random generator.
  printf("Random number generator seed: %llu\n", params.randomSeed);

  // Make sure we can write to the output file
  globals.corpusOut = fopen(params.outFileName, "wb");
  if (globals.corpusOut == NULL) {
    printf("Error: Can't open %s for writing.\n", params.outFileName);
    printUsage(argv[0], args);
  }
  printf("Output file %s opened for writing.\n", params.outFileName);
  

  // Memory map the whole input file
  if (! exists(params.inFileName, "")) {
    printf("Error: Input file %s doesn't exist.\n", params.inFileName);
    printUsage(argv[0], args);
  }

  startTime = what_time_is_it();
  globals.inputInMemory = (char *)mmap_all_of(params.inFileName, &(globals.inputSize),
					       FALSE, &(globals.inputFH), &(globals.inputMH),
					       &error_code);
  if (globals.inputInMemory == NULL ) {
    printf("Error: mmap_all_of(%s) failed with code %d\n", params.inFileName, error_code);
    exit(1);
  }

  madvise(globals.inputInMemory, globals.inputSize, MADV_SEQUENTIAL);

  printf("Input mapped.\n");


  // Pass 1:  Build the vocabulary hash table for the input corpus 

  if (tailstr(params.inFileName, ".tsv") != NULL || tailstr(params.inFileName, ".TSV") != NULL) {
    processTSVFormat(&globals, TRUE);
   } else if (tailstr(params.inFileName, ".trec") != NULL || tailstr(params.inFileName, ".TREC") != NULL) {
    processTRECFormat(&globals, TRUE);
  } else {
    processSTARCFormat(&globals, TRUE);
  }
     

  printf("Building vocabulary table took %.3f seconds.  Here are some statistics: \n",
	 what_time_is_it() - startTime);
  printSummary(&globals);
  dahash_print_key_stats(globals.gMarkovHash, TRUE, "after all docs scanned for buildVocab()");


  makeVocabRanking(&globals);

  if (globals.numHeadTerms > 0) {
    printf("\nThe %d most frequently occurring words will store their successors in hashtables\n",
	   globals.numHeadTerms);
  }


  // WordsUsed allows us to keep track of how many words from input vocab are actually emitted.
  // +1 in size is to allow for EOD symbol
  globals.wordsUsed = (byte *)cmalloc(globals.vocabSize + 1, "wordsUsed", 0);
  memset(globals.wordsUsed, 0, globals.vocabSize + 1);


  startTime = what_time_is_it();
  if (params.useOrderZero) {
     printf("\n\nOrder Zero:  No need to build transition table. \n\n");
     globals.MarkovOrder = 0;
  } else {  // WBMarkov1
    // Allocate sufficient storage for the linked lists.  There will be one needed for
    // every posting, because each word in the corpus is followed by either a word or
    // EOD (end of document), represented by maxRank + 1;
    
    if (globals.totalPostingsIn > llMaxElts) {
      printf("Error: Insufficient bytes allowed for pointers to accommodate this\n"
	     "corpus.  You need to increase llNextPBytes and llMaxElts in\n"
	     "MarkovGenerator.h, and remake.\n");
      exit(1);
    }
    globals.llStorage = (llPointer)cmalloc((globals.totalPostingsIn + 10) * llEltSize, "llStorage", 0);
    globals.llNextFree = globals.llStorage;
    globals.llStorageLimit = globals.llStorage + (globals.totalPostingsIn + 10) * llEltSize;
    // Make an array of the ids of the first word in every input doc.
    printf("Creating globals.firstWordIds with %lld elements.\n", globals.numDocs);
    globals.firstWordIds = (int *)cmalloc(globals.numDocs * sizeof(int), "firstWordIds", 0);
  

    // Pass 2:  Build the Markov transition table.
    if (tailstr(params.inFileName, ".tsv") != NULL || tailstr(params.inFileName, ".TSV") != NULL) {
      processTSVFormat(&globals, FALSE);
    } else if (tailstr(params.inFileName, ".trec") != NULL || tailstr(params.inFileName, ".TREC") != NULL) {
      processTRECFormat(&globals, FALSE);
    } else {
      processSTARCFormat(&globals, FALSE);
    }
    
    if (globals.numHeadTerms > 0) convertHashesToArrays();
    

    printf("MG(%d): Markov transition table built in %.3f sec.\n"
	   "MG(%d): Overall time to train the model: %.3f sec.\n",
	   globals.MarkovOrder, what_time_is_it() - startTime,  globals.MarkovOrder, what_time_is_it() - veryStart);

    if (0) llShowList(globals.vocabRanking[100]);
  }
  
  // Phase 3: Run the model to produce the outputCorpus, but first set the desired
  // number of postings to generate

  globals.desiredTotalPostings = (long long) ((double) globals.totalPostingsIn * params.postingsScaleFactor + 0.5);
  printf("Total postings In = %lld,  will try to generate %lld due to scale factor %.2f\n",
	 globals.totalPostingsIn, globals.desiredTotalPostings, params.postingsScaleFactor);

  if (params.useOrderZero) generateCorpusFromOrderZero();
  else generateCorpusFromModel();

  // Count the size of the vocabulary of the emitted text.
  for (tRank = 1; tRank <= globals.vocabSize; tRank++) {
    if (globals.wordsUsed[tRank]) globals.vocabOut++;
  }
  
  MB =  (double)dahash_memory_used(globals.gMarkovHash) / 1048576.0;
  totalMB += MB;
  printf("\nMemory used by main Markov hash table: %.1fMB\n", MB);
  if (!params.useOrderZero) {
    MB = (double)(globals.llStorageLimit - globals.llStorage)  / 1048576.0;
    totalMB += MB;
    printf("Memory allocated for linked list elements: %.1fMB\n", MB);
    
    MB = (double)globals.memUseBySubsidiaryHashes  / 1048576.0;
    totalMB += MB;
    printf("Memory allocated for subsidiary hashtables: %.1fMB\n", MB);
    
    MB = (double)(globals.numDocs * sizeof(int))  / 1048576.0;
    totalMB += MB;
    printf("Memory allocated for first word Ids: %.1fMB\n", MB);
  }
  
  MB = (double)(globals.vocabSize * sizeof(byte *))  / 1048576.0;
  totalMB += MB;
  printf("Memory allocated for vocab Ranking: %.1fMB\n", MB);
  
  
  printf("===============================================\n\n"
	 "MG(%d): Total memory used (ignoring mmapped input file): %.3fMB approx.\n",
	 globals.MarkovOrder, totalMB);

  dahash_destroy(&(globals.gMarkovHash));
  unmmap_all_of(globals.inputInMemory, globals.inputFH, globals.inputMH, globals.inputSize);
  free(globals.firstWordIds);
  globals.firstWordIds = NULL;
  free(globals.vocabRanking);
  globals.vocabRanking = NULL;
  fclose(globals.corpusOut);
#ifdef __clang__
  printf("MG(%d): Version: %s\n", globals.MarkovOrder, __TIMESTAMP__);
#endif
  if (!params.useOrderZero) 
    printf("MG(%d): headTerms: %d\n", globals.MarkovOrder, globals.numHeadTerms);

  printf("MG(%d): Overall runtime: %.3f sec.\n"
	 "MG(%d): Input: %s\n"
	 "MG(%d): Output: %s\n"
	 "MG(%d): docs: %lld v. %lld\n"
	 "MG(%d): vocab_size: %lld v. %lld\n"
	 "MG(%d): total_postings: %lld out v. %lld in  (due to scaleFactor %.2f)\n"
	 "MG(%d): randomSeed: %lld\n\n",
	 globals.MarkovOrder, what_time_is_it() - veryStart,
	 globals.MarkovOrder, params.inFileName,
	 globals.MarkovOrder, params.outFileName,
	 globals.MarkovOrder, globals.docsOut, globals.numDocs,
	 globals.MarkovOrder, globals.vocabOut, globals.vocabSize,
	 globals.MarkovOrder, globals.postingsOut, globals.totalPostingsIn, params.postingsScaleFactor,
	 globals.MarkovOrder,params.randomSeed);

 
}
