// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

// Linked list elements are represented in compact form with llRankBytes bytes allowed for the
// rank of the word allowing at least two billion distinct words, and llNextPBytes allowed for
// the pointer (index within the globals.llStorage array.)
#define llRankBytes 3
#define llFreqBytes 4
#define llNextPBytes 5
#define llMaxElts 0XFFFFFFFFFE  // 2**llNextPBytes - 2
#define llEltSize 12  // = llRankBytes + llNextPBytes
#define llNULL 0XFFFFFFFFFF  // 2**llNextPBytes - 1

#define EOD 0XFFFFFF  // 2**llRankBytes - 1
#define MIN_HEAD_TERM_FREQ 50     
#define DFLT_HEAD_TERMS 50000    // The default number of head terms whose successors will be processed using
                                 // hash tables rather than linked lists.  This number will reduce if the
                                 // frequency of the DFLT_HEAD_TERMS-th word is less than MIN_HEAD_TERM_FREQ.

typedef byte *llPointer;

typedef struct {   // This is the format of entries in the linked lists hanging off gMarkovHash entries, and the entries in the main hash table
  wordCounter_t freq;
  u_int rank;
  llPointer first;
} MarkovEntry_t;

typedef struct {  // This is the format of values of entries in the hashtables hanging off gMarkovHash entries.
  wordCounter_t freq;
  u_int rank;
} MarkovHash_t;

typedef struct { // Format of entries in the array of successors to a term, AFTER conversion from hash table.
  u_int rank;
  double cumProb;
} probStruct_t;



typedef struct {
  double startTime;
  long long vocabSize, numDocs, numEmptyDocs, totalPostingsIn, desiredTotalPostings,
    longestPostingsListLength, postingsOut, docsOut, vocabOut;
  dahash_table_t *gMarkovHash;
  byte **vocabRanking;  // Pointers to entries in gMarkovHash
  byte *wordsUsed;   // Array of word ranks initialised to zero, set to one if this rank is output
  wordCounter_t ngramFreqFilterThresh;
  char *inputInMemory;
  byte *corpusInMemory;
  size_t inputSize;
  CROSS_PLATFORM_FILE_HANDLE inputFH;
  HANDLE inputMH;
  FILE *corpusOut;
  llPointer llStorage;
  byte *llNextFree, *llStorageLimit;
  int *firstWordIds;  // The term Ids of the first words in each input doc.
  int numHeadTerms;
  long long memUseBySubsidiaryHashes;
  int MarkovOrder;
  int debug;
 } globals_t;


typedef struct {
  u_char *inFileName;
  u_char *outFileName;
  int numHeadTerms;
  int hashBits;
  long long randomSeed;
  double postingsScaleFactor;  // Ratio of output postings to input postings
  BOOL useOrderZero;
  BOOL verbose;
} params_t;

extern params_t params;
