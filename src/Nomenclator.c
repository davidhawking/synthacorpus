// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                                  Nomenclator.c                                   //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Takes a base corpus C of documents in STARC, TSV or TREC format and a list of words,
// at least as long as the vocabulary list for C and creates a one-to-one mapping from
// words in the vocabulary list V to words in the first |V| positions of the word list.
// It then uses the mapping to make an encoded corpus E from C.
//
// Two scans of the input data are required, one to build up a hashtable of the
// vocabulary, and the second to do the actual encoding.


#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "characterSetHandling/unicode.h"
#include "utils/general.h"
#include "Nomenclator.h"
#include "utils/argParser.h"

static char docCopy[MAX_DOC_LEN + 1], *docWords[MAX_DOC_WORDS];

static void initialiseGlobals(globals_t *globals) {
  globals->startTime = what_time_is_it();
  globals->numDocs = 0;
  globals->numEmptyDocs = 0;
  globals->totalPostings = 0;

  // Set up global vocab hash with room for both df and overall occurrence frequency
  // and with an initial capacity of around 15 million  (90% of 2**24)
  globals->gVocabHash =
    dahash_create((u_char *)"globalVocab", 24, MAX_WORD_LEN, sizeof(wordCounter_t),
		  (double)0.9, FALSE);
}


static int processOneDoc(globals_t *globals, char *docText, size_t docLen) {
  int numWords, w;
  wordCounter_t *counter, tf;
  dahash_table_t *lVocab = NULL;
  char *htEntry;
  off_t htOff;
  long long e;
  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.

  if (0) printf("Process_one_doc()\n");
  if (docLen > MAX_DOC_LEN) docLen = MAX_DOC_LEN;
  if ((docText[docLen] & 0xC0) == 0x80) {
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }
  // Copy document text so we can write on it
  memcpy(docCopy, docText, docLen);
  docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
  if (0) {
    printf("processOneDoc: len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n",
	   docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	   globals->inputSize - 1);
    put_n_chars(docCopy, 100);
    printf("\n\n\n");
  }
  numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							MAX_DOC_WORDS, MAX_WORD_LEN,
							TRUE,  // case-fold line before splitting
							FALSE, // before splitting
							FALSE,  // Perform some heuristic substitutions 
							FALSE
							);

  if (numWords <= 0) {
    globals->numEmptyDocs++;
    return 0;  // ----------------------------->
  }
  
  // Create a local vocab hash table  (10 bits cos most docs have few distinct words).
  // Just count up the frequency of each distinct word
  lVocab = dahash_create("localVocab", 10, MAX_WORD_LEN, sizeof(wordCounter_t),
			 (double)0.9, FALSE);
  if (0) printf("Putting %d words into local hash.\n", numWords);
  for (w = 0; w < numWords; w++) {
    if (0) printf("Word %d: %s\n", w, docWords[w]);
    counter = (wordCounter_t *)dahash_lookup(lVocab, docWords[w], 1);   // 1 means add the key if it's not already there.
     (*counter)++;
     if (*counter >= WCMAX) {
       printf("\n\nError: processOneDoc(local words): Counter overflow.  Recompile with -DVERY_LARGE_CORPUS\n");
       exit(1);
     }
  }

  // Now transfer the data from the local hash to the global one.
  htOff = 0;
  for (e = 0; e < lVocab->capacity; e++) {
    htEntry = ((char *)(lVocab->table)) + htOff;
    if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
      if (0) printf("transferring entry %lld/%zd\n", e, lVocab->capacity);
      tf = *((wordCounter_t *)(htEntry + lVocab->key_size));
      counter = (wordCounter_t *)dahash_lookup(globals->gVocabHash, htEntry, 1);   
      (*counter)+= tf;  // First counter is total occurrence frequency
      if (*counter >= WCMAX) {
	printf("\n\nError: processOneDoc(global words tf): Counter overflow.  Recompile with -DVERY_LARGE_CORPUS\n");
	exit(1);
      }    
    }
    htOff += lVocab->entry_size;
  }

  // Finally free the space from the local hash
  dahash_destroy(&lVocab);

  globals->totalPostings += numWords;
  return numWords;
}


static u_char *outDoc = NULL;
static int oDocNum = 0;
static int truncatedDocCount = 0;
static long long randomChoicesMade = 0, encodeCalledCount = 0, emptyOutCount = 0;

static int encodeOneDoc(globals_t *globals, char *docText, size_t docLen, u_char *outType) {
  int numWords, w;
  wordCounter_t *codeNumP, codeNum;
  u_char *outP = NULL, *wordP;
  size_t lengthRemaining = MAX_DOC_LEN, len;
  BOOL randomChoice;
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.
  
  if (0) printf("Encode_one_doc()\n");
  encodeCalledCount++;
  if (!strcmp(outType, "TREC")) fprintf(globals->cipherOut,"<DOC>\n<DOCNO> Nomen-%d </DOCNO>\n<TEXT>\n", oDocNum++);
  if (outDoc == NULL) outDoc = cmalloc(MAX_DOC_LEN + 1, "outDoc", FALSE);
  outP = outDoc;
  if (docLen > MAX_DOC_LEN) {
    docLen = MAX_DOC_LEN;
    truncatedDocCount++;
  }
  if ((docText[docLen] & 0xC0) == 0x80) {
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }
  // Copy document text so we can write on it
  memcpy(docCopy, docText, docLen);
  docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
  if (0) {
    printf("encodeOneDoc: len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n//%s//",
	   docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	   globals->inputSize - 1, docCopy);
    printf("\n\n\n");
  }
  numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							MAX_DOC_WORDS, MAX_WORD_LEN,
							TRUE,  // case-fold line before splitting
							FALSE, // before splitting
							FALSE,  // Perform some heuristic substitutions 
							FALSE
							);

  /* if (numWords <= 0) { */
  /*   printf("\n\n"); */
  /*   return 0;  // -----------------------------> */
  /* } */
  if (numWords > 0) {
    for (w = 0; w < numWords; w++) {
      codeNumP = (wordCounter_t *)dahash_lookup(globals->gVocabHash, docWords[w], 0);
      if (codeNumP == NULL) {
	printf("Lookup of '%s' in gVocabHash failed\n", docWords[w]);
	exit(1);
      }
      codeNum = *codeNumP;
      if (0) printf("Word %d: %s -> %d", w, docWords[w], codeNum);
      if (w > 0) {
	if (lengthRemaining-- < 2) break;   // length remaining in output buffer that is.
	*outP++ = ' ';
      }
      
      if (codeNum < 0) codeNum = 0;
      randomChoice = FALSE;
      if (codeNum >= globals->cipherVocabLineCount) {
	// We've encountered a codeNum too high for the input vocabulary supplied.
	// So we pick a random valid word.
	codeNum = rand() % globals->cipherVocabLineCount;
	randomChoice = TRUE;
	randomChoicesMade++;
      } 
      wordP = globals->cipherVocabLines[codeNum];
      
      // --------------------------  FOr Debugging Only -------------------------------
      // wordP = docWords[w];   // ---------------------- Just ouput the input.
      // --------------------------  FOr Debugging Only -------------------------------
      
      if (1 && randomChoice) printf("Random choice: ");
      while (lengthRemaining-- > 1 && *wordP > ' ') {
	*outP++ = *wordP;
	if (1 && randomChoice) putchar(*wordP);
	wordP++;
      }
    if (1 && randomChoice) printf("\n");

    
    
    }
  } else emptyOutCount++;

  *outP++ = '\n';
  *outP = 0;
  len = outP - outDoc;
  if (!strcmp(outType, "STARC")) fprintf(globals->cipherOut, " %zdD %s", len, outDoc);
  else if (!strcmp(outType, "TREC")) fprintf(globals->cipherOut, "%s\n</TEXT>\n</DOC>\n", outDoc);
  else fprintf(globals->cipherOut, "%s\n", outDoc);
  return len;
}


static void processTSVFormat(globals_t *globals, BOOL encode) {
  // Input is assumed to be in TSV format with an arbitrary (positive) number of
  // columns (including one column), in which the document text is in
  // column one and the other columns are ignored.   (All LFs and TABs are assumed
  // to have been removed from the document.)
  char *lineStart, *p, *inputEnd;
  long long printerval = 1000;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  lineStart = globals->inputInMemory;
  globals->numDocs = 0; 
  p = lineStart;
  while (p <= inputEnd) {
    // Find the length of column 1, then process the doc.
    while (p <= inputEnd  && *p >= ' ') p++;  // Terminate with any ASCII control char
    docLen = p - lineStart;
    if (encode) {
      encodeOneDoc(globals, lineStart, docLen, "TSV");
    } else {
      globals->numDocs++;
      if (0) printf("About to process a doc.\n");
      processOneDoc(globals, lineStart, docLen);
      if (globals->numDocs % printerval == 0) {
	printf("   --- %lld records scanned @ %.3f msec per record --- postings thus far: %lld\n",
	       globals->numDocs,
	       (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
	       globals->totalPostings);
	if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    }
    // Now skip to end of line (LF) or end of input.
    while (p <= inputEnd  && *p != '\n') p++;  // Terminate with LineFeed only
    p++;
    lineStart = p;
  }
}


static void processTRECFormat(globals_t *globals, BOOL encode) {
  // Input is assumed to be in highly simplified TREC format, such as that produced by
  // the detrec program.  Documents are assumed to be DOC elements, each starting with
  // a DOCNO element.  It is also assumed that the content contains no other sub-elements
  // and no stray angle brackets.  Ideally, the text is just words separated by spaces.
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 1;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  globals->numDocs = 0;
  while (p <= inputEnd) {
    if (0) printf("Looking for <TEXT\n");
    q = mlstrstr(p, "<TEXT>", inputEnd - p - 6, 0, 0);
    if (q == NULL) break;   // Presumably the end of the file. ... or wrong format file.
    docStart = q + 6;
    if (0) printf("Looking for </TEXT>\n");
    q = mlstrstr(docStart, "</TEXT>", inputEnd - docStart - 7, 0, 0);
    if (q == NULL) break;   // Presumably file is incorrectly formatted.    
    docLen = q - docStart;
    if (encode) {
      encodeOneDoc(globals, docStart, docLen, "TREC");
      globals->numDocs++;
    } else {
      globals->numDocs++;
      if (0) printf("About to process one doc, of len %zd\n", docLen);
      processOneDoc(globals, docStart, docLen);
      if (globals->numDocs % printerval == 0) {
	printf("   --- %lld records scanned @ %.3f msec per record --- postings thus far: %lld\n",
	       globals->numDocs,
	       (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
	       globals->totalPostings);
	if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    }
    p = q + 7;  // Move to char after </TEXT>
  }
  printf("Number of docs scanned:  %lld\n", globals->numDocs);
}




static void processSTARCFormat(globals_t *globals, BOOL encode) {
  // Input is assumed to be records in in a very simple <STARC
  // header><content> format. The STARC header begins and ends with a
  // single ASCII space.  Following the leading space is the content
  // length in decimal bytes, represented as an ASCII string,
  // immediately followed by a letter indicating the type of record.
  // Record types are H - header, D - document, or T - Trailer.  The
  // decimal length is expressed in bytes and is represented in
  // ASCII. If the length is L, then there are L bytes of document
  // content following the whitespace character which terminates the
  // length representation.  For example: " 13D ABCDEFGHIJKLM 4D ABCD"
  // contains two documents, the first of 13 bytes and the second of 4
  // bytes.
  //
  // Although this representation is hard to view with editors and
  // simple text display tools, it completely avoids the problems with
  // TSV and other formats which rely on delimiters, that it's very
  // complicated to deal with documents which contain the delimiters.
  //
  // This function skips H and T records.
  // If encode is TRUE, each D record is passed to encodeOneDoc()
  // otherwise to processOneDoc().
  
  size_t docLen;
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 10;
  byte recordType;

  globals->numDocs = 0; 
  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  while (p <= inputEnd) {
    // Decimal length should be encoded in ASCII at the start of this doc.
    if (*p != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     p - globals->inputInMemory);
      exit(1);

    }
    errno = 0;
    docLen = strtol(p, &q, 10);  // Making an assumption here that the number isn't terminated by EOF
    if (errno) {
      fprintf(stderr, "processSTARCFile: Error %d in strtol() at offset %zd\n",
	     errno, p - globals->inputInMemory);
      exit(1);
    }
    if (docLen <= 0) {
      printf("processSTARCFile: Zero or negative docLen %zd at offset %zd\n",
	     docLen, p - globals->inputInMemory);
      exit(1);
    }

    recordType = *q;    
    if (recordType != 'H' && recordType != 'D' && recordType != 'T') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);
    }
    q++;
    if (*q != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't end with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);

    }
    
    docStart = q + 1;  // Skip the trailing space.
    if (0) printf(" ---- Encountered %c record ---- \n", recordType);
    if (recordType == 'D') {
      if (encode) {
	encodeOneDoc(globals, docStart, docLen, "STARC");
      } else {
	globals->numDocs++;
	if (0) printf("About to process a doc.\n");
	processOneDoc(globals, docStart, docLen);
	if (globals->numDocs % printerval == 0) {
	  printf("   --- %lld records scanned @ %.3f msec per record--- Postings thus far: %lld\n",
		 globals->numDocs,
		 (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
		 globals->totalPostings);
	  if (globals->numDocs % (printerval * 10) == 0) printerval *= 10;
	}
      }
    }
    p = docStart + docLen;  // Should point to the first digit of the next length, or EOF
  }
}

static void printSummary(globals_t *globals) {  
  globals->numDocs -= globals->numEmptyDocs;  // Don't count zero-length documents.
  printf("docs=%lld  # Excluding zero-length\n", globals->numDocs);
  printf("vocab_size=%zd\n", globals->gVocabHash->entries_used);
  //printf("longest_list=%lld\n", globals->longestPostingsListLength);
  printf("total_postings=%lld\n", globals->totalPostings);
}


static void randomlyMapWords(globals_t *globals) {
  // Since words are effectively randomly placed in the hash table, we just linearly
  // scan the table and assign each word encountered to the next in a sequence of
  // integers from 0 to the vocabulary size minus 1.
  // If the cypherVocab is too small we make up a series of new words $NoMeN$0,  $NoMeN$1, ...
  long long e;
  u_char *htEntry, *vocabP, vWord[1001], *vWordP;
  off_t htOff;
  wordCounter_t wNum = 0, codeNum;

  srand(5);  // Synchronise with the encode function
  htOff = 0;
  for (e = 0; e < globals->gVocabHash->capacity; e++) {
    htEntry = ((char *)(globals->gVocabHash->table)) + htOff;
    if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
      if (0) printf("About to randomMap %s to cipher word %d\n", htEntry, wNum);
      *((wordCounter_t *)(htEntry + globals->gVocabHash->key_size)) = wNum;
      // The file containing the encryption is usually a TSV file with word then frequencies
      // so copy just the word part.
      codeNum = wNum;
      if (codeNum >= globals->cipherVocabLineCount) {
       // Sychronise with encode function
       // We've encountered a codeNum too high for the input vocabulary supplied.
       // So we pick a random valid word.
       codeNum = rand() % globals->cipherVocabLineCount;
      } 

      vocabP = globals->cipherVocabLines[codeNum];
      vWordP = vWord;
      while (*vocabP > ' ' && (vWordP - vWord) < 1000) *vWordP++ = *vocabP++;
      *vWordP = 0;
      fprintf(globals->mappingOut, "%s\t%s\n", htEntry, vWord);
      wNum++;
    }
    htOff += globals->gVocabHash->entry_size;
  }
}


int main(int argc, char **argv) {
  FILE *cipherVocab;
  int error_code;
  globals_t globals;
  char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
    
  setvbuf(stdout, NULL, _IONBF, 0);
  
  if (argc != 5) {
    printf("Usage: %s <name of plainTextFile> <name of cipherVocab file> <name of output file> <name of file to save mapping>\n"
	   " - Note that the output format will always be the same as the input format, regardless\n"
	   " - of the name of the output file.\n",argv[0]);
    exit(1);
  }

  cipherVocab = fopen(argv[2], "r");
  if (cipherVocab == NULL) {
    printf("Error: Can't open %s\n", argv[2]);
  }

  // Make sure we can write to the output file
  globals.cipherOut = fopen(argv[3], "wb");
  if (globals.cipherOut == NULL) {
    printf("Error: Can't open %s for writing.\n", argv[3]);
    exit(1);
  }
  printf("Output file %s opened for writing.\n", argv[3]);

  // Make sure we can write to the output file
  globals.mappingOut = fopen(argv[4], "wb");
  if (globals.mappingOut == NULL) {
    printf("Error: Can't open %s for writing.\n", argv[4]);
    exit(1);
  }
  printf("Encryption table will be saved to %s.\n", argv[4]);

  
  
  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);
  initialiseGlobals(&globals);   // Includes the hashtables as well as scalar values
  printf("Globals initialised\n");

 
  // Memory map the whole input file
  if (! exists(argv[1], "")) {
    printf("Error: Input file %s doesn't exist.\n", argv[1]);
    exit(1);
  }
  
  globals.inputInMemory = (char *)mmap_all_of(argv[1], &(globals.inputSize),
					       FALSE, &(globals.inputFH), &(globals.inputMH),
					       &error_code);
  if (globals.inputInMemory == NULL ) {
    printf("Error: mmap_all_of(%s) failed with code %d\n", argv[1], error_code);
    exit(1);
  }

  madvise(globals.inputInMemory, globals.inputSize, MADV_SEQUENTIAL);

  printf("Input mapped.\n");


  // Pass 1:  Build the vocabulary hash table for the corpus 

  if (tailstr(argv[1], ".tsv") != NULL || tailstr(argv[1], ".TSV") != NULL) {
    processTSVFormat(&globals, FALSE);
   } else if (tailstr(argv[1], ".trec") != NULL || tailstr(argv[1], ".TREC") != NULL) {
    processTRECFormat(&globals, FALSE);
  } else {
    processSTARCFormat(&globals, FALSE);
  }
     

  printf("Scanning of input finished.  Here are some statistics: \n");
  printSummary(&globals);
  dahash_print_key_stats(globals.gVocabHash, TRUE, "after all docs scanned for the first time");

  // Load the cipherVocab
  
  globals.cipherVocabLines = load_all_lines_from_textfile(argv[2], &(globals.cipherVocabLineCount),
							  &(globals.cipherFH), &(globals.cipherMH),
							  &(globals.cipherInMemory), &(globals.cipherSize));
  globals.vocabSize = globals.gVocabHash->entries_used;
  printf("Corpus vocabulary size: %lld\n", globals.vocabSize);
  printf("Cipher vocabulary has %d lines\n", globals.cipherVocabLineCount);

  if (globals.cipherVocabLineCount < globals.vocabSize) {
    printf("\n\nWarning: The number of lines in the cipher Vocabulary file %s\n"
	   "         is less than the vocabulary of the plain text.\n",
	   argv[2]);
    printf(" - Each occurrence of %lld different plain text words will be replaced by a different\n"
	   " - cipher word.\n\n\n",  globals.vocabSize - globals.cipherVocabLineCount);
  }
  
  randomlyMapWords(&globals);
  printf("Random mapping of words complete. About to encrypt the text.\n");

  // Pass 2:  Perform the encryption
  srand(5);  // Synchronise with random mapping function.
  if (tailstr(argv[1], ".tsv") != NULL || tailstr(argv[1], ".TSV") != NULL) {
    processTSVFormat(&globals, TRUE);
  } else if (tailstr(argv[1], ".trec") != NULL || tailstr(argv[1], ".TREC") != NULL) {
    processTRECFormat(&globals, TRUE);
  } else {
    processSTARCFormat(&globals, TRUE);
  }
  

  unload_all_lines_from_textfile(globals.cipherFH, globals.cipherMH, &(globals.cipherVocabLines),
				 &(globals.cipherInMemory), globals.cipherSize);

  fclose(cipherVocab);
  dahash_destroy(&(globals.gVocabHash));
  unmmap_all_of(globals.inputInMemory, globals.inputFH, globals.inputMH, globals.inputSize);
  fclose(globals.cipherOut);
  fclose(globals.mappingOut);

  printf("Encode called %lld times.\n", encodeCalledCount);
  printf("%lld empty documents were emitted.\n", emptyOutCount);
  printf("Encoded output in %s.  Encryption table saved in %s\n", argv[3], argv[4]);
  if (truncatedDocCount > 0) {
    printf("\n\nWarning: Corpus was not fully encrypted due to document truncation.\n"
	   "   %d documents were truncated to the %d maximum length.\n\n",
	   truncatedDocCount, MAX_DOC_LEN);
    sleep(2);
  }

  if (randomChoicesMade > 0) {
    printf("\n\nWarning: Vocabulary size of output will be smaller than input due to %lld random choices.\n"
	   "   cypher vocabulary must have been too small.\n\n",
	   randomChoicesMade);
    sleep(2);
  }

    
}
