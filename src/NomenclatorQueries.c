// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                             NomenclatorQueries.c                                 //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "definitions.h"
#include "utils/general.h"
#include "characterSetHandling/unicode.h"


unsigned char lineBuf[2000], *lineP;
byte **mapLines, *mapInMem;
CROSS_PLATFORM_FILE_HANDLE H;
HANDLE MH;
size_t sighs;
int mapLineCount;
  

static int mapCmp(const void *ip, const  void *jp) {
  u_char *i = *((u_char**)ip), *j = *((u_char**)jp);
  while (*i > ' ' && *j > ' ') {
    if (*i > *j) {
      return 1;
    } else if (*j > *i) {
      return -1;
    }
    i++;
    j++;
  }
  if (*i <= ' ') {
    if (*j <= ' ') {
      return 0;
    }
    return -1;
  }
  return 1;
}

byte *words[MAX_DOC_WORDS];

void encodeAndPrintLine(u_char *line) {
  // Use that function to split the line into words.  Then, for each word, look it up in the sorted
  // mapping list using bsearch() and print the translation.
  int w, numWords;
  u_char **foundMap, *f;

  numWords = utf8_split_line_into_null_terminated_words(line, strlen(line), (byte **)(&words),
                                                        MAX_DOC_WORDS, MAX_WORD_LEN,
                                                        TRUE,  // case-fold line before splitting
                                                        FALSE,  // remove_accents before splitting
                                                        FALSE,  // Perform some heuristic substitutions 
                                                        FALSE   // words_must_have_an_ASCII_alnum
                                                        );

  for (w = 0; w < numWords; w++) {
    if (0) printf("Looking up '%s'\n", words[w]); 
    if (w != 0) putchar(' ');
    foundMap = bsearch(words + w, mapLines, mapLineCount, sizeof(u_char *), mapCmp);
    if (foundMap == NULL) {
      if (1) fprintf(stderr, "Word '%s' not found!\n", words[w]);
      printf("%s", words[w]);  // Print the unencoded word.
    } else {
      f = *foundMap;
      while (*f > ' ') f++;  // Skip over column 1
      f++;   // and the tab
      while (*f > ' ') {
	putchar(*f++);
      }     
    }
  }
  printf("\n");
}
  

int main(int argc, char **argv) {
  // A simple line-by-line filter which leaves intact SGML tags, e.g. <top> and performs Nomenclator substitution
  // on the non-boilerplate words.  Designed to encrypt queries from TREC ad hoc as per
  // ~/Research/QuerySets/AdHoc/51-100
  char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
  
  if (argc != 2) {
    printf("Usage: %s <Nomenclator mapping file>\n", argv[0]);
    exit(1);
  }

  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);

  mapLines = load_all_lines_from_textfile(argv[1], &mapLineCount, &H, &MH, &mapInMem, &sighs);
  printf("Mapping has %d lines.\n", mapLineCount);
  
  qsort(mapLines, mapLineCount, sizeof(u_char *), mapCmp);
  
  while (fgets(lineBuf, 2000, stdin) != NULL) {
    if (!strncmp(lineBuf, "<top>", 5) || !strncmp(lineBuf, "</top>", 6)
	|| !strncmp(lineBuf, "<num>", 5) || !strncmp(lineBuf, "<desc>", 6)  || !strncmp(lineBuf, "<narr>", 6)) {
      // Nothing on these lines needs to be encoded
      printf("%s", lineBuf);
    } else if (!strncmp(lineBuf, "<title> ", 8)) encodeAndPrintLine(lineBuf + 8);
    else encodeAndPrintLine(lineBuf);
  }


  unload_all_lines_from_textfile(H, MH, &mapLines, &mapInMem, sighs);

}
