#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Given the name of a corpus and base and emulation directories, calculate the degree of overlap
# between the emulation and base in terms of vocabulary and sentences.   E.g. calculate what proportion
# of words in the emulated vocabulary were present in that of the base.

die "Usage: $0 <corpusName> <baseDir> <emulationDir\n"
  unless $#ARGV == 2;

$corpus = $ARGV[0];
$baseDir = $ARGV[1];
$emuDir = $ARGV[2];

$baseFile = "$baseDir/$corpus.trec";
die "Can't read $baseFile\n" unless -r $baseFile;
$emuFile = "$emuDir/$corpus.trec";
die "Can't read $emuFile\n" unless -r $emuFile;

runit("grep -v '<' $baseFile | sort -u > $baseDir/$corpus.ust");  # A sorted file of unique text-only lines
runit("awk -F\\t '{print \$1}' $baseDir/${corpus}_vocab.tsv > $baseDir/$corpus.wdlist");

runit("grep -v '<' $emuFile | sort -u > $emuDir/$corpus.ust");  # A sorted file of unique text-only lines
runit("awk -F\\t '{print \$1}' $emuDir/${corpus}_vocab.tsv > $emuDir/$corpus.wdlist");


# Now measure the overlap of texts and vocab words
$tlines = `wc -l $emuDir/$corpus.ust`;
die "wc -l $emuDir/$corpus.ust failed\n" if $?;
$tlines =~ /([0-9]+)/;
$tlines = $1;
$vlines = `wc -l $emuDir/$corpus.wdlist`;
die "wc -l $emuDir/$corpus.wdlist failed\n" if $?;
$vlines =~ /([0-9]+)/;
$vlines = $1;
$toverlap = `comm -12 $emuDir/$corpus.ust $baseDir/$corpus.ust | wc -l`;
die "First comm failed\n" if $?;
chomp($toverlap);
#runit("comm -12 $emuDir/$corpus.ust $baseDir/$corpus.ust");

$voverlap = `comm -12 $emuDir/$corpus.wdlist $baseDir/$corpus.wdlist | wc -l`;
die "Second comm failed\n" if $?;
chomp($voverlap);

$bmsumfile = "$emuDir/${corpus}_base_v_mimic_summary.txt";
die "Can't append to $bmsumfile\n" unless open BMS, ">>$bmsumfile";
print BMS "Text overlap: $toverlap / $tlines = ", sprintf("%.2f%%\n", 100.0  * $toverlap / $tlines);
print BMS "Vocab overlap: $voverlap / $vlines = ", sprintf("%.2f%%\n", 100.0  * $voverlap / $vlines);
close(BMS);
print "Text overlap: $toverlap / $tlines = ", sprintf("%.2f%%\n", 100.0  * $toverlap / $tlines);
print "Vocab overlap: $voverlap / $vlines = ", sprintf("%.2f%%\n", 100.0  * $voverlap / $vlines);

print "\nOverlap results stored in summary file.  To see them:

     cat $emuDir/${corpus}_base_v_mimic_summary.txt

To see the sentences which overlapped:

     comm -12 $emuDir/$corpus.ust $baseDir/$corpus.ust

";

exit(0);

#----------------------------------------------------------------------------------
sub runit {
  my $cmd = shift;
  print $cmd, "\n";
  system $cmd;
  die "Command $cmd failed\n"  if $?;
}
