// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                           calculateVocabHashRate.c                               //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Given a list of words (a vocab.tsv file will do), create the smallest hash table
// capable of holding all the words, then load the words into the hashtable and
// report statistics about collision rates etc.


#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "characterSetHandling/unicode.h"
#include "utils/general.h"
#include "Nomenclator.h"
#include "utils/argParser.h"

int main(int argc, char **argv) {
  int vocabSize, bits, w, *valP;
  long long capacity;
  CROSS_PLATFORM_FILE_HANDLE H;
  HANDLE MH;
  byte *fileInMem, **lines, *p, *q, word[MAX_WORD_LEN + 1];
  size_t fileSize;
  dahash_table_t *vHash;
  double startTime, hashingTime;
  
  if (argc < 2) {
    printf("Usage: %s <vocabFile>\n", argv[0]);
    exit(1);
  }
  
  
  lines = load_all_lines_from_textfile(argv[1], &vocabSize, &H, &MH, &fileInMem, &fileSize);

  bits = dahash_calculate_bits(vocabSize, 0.9);
  capacity = 1 << bits;

  vHash = dahash_create("vocabHash", bits, MAX_WORD_LEN, sizeof(int),
			      (double)0.9, FALSE);
  startTime = what_time_is_it();
  for (w = 0; w < vocabSize; w++) {
    p = lines[w];
    q = word;
    while (*p > ' ') {
      *q++ = *p++;
    }    
    *q = 0;
    
    valP = (int *)dahash_lookup(vHash, word, 1);
    *valP = 1;
  }
  hashingTime = what_time_is_it() - startTime;
  
  printf("#vocabSize\tcapacity\tbits\tcollisions\tmax_collision_chain\thashingTime\tpercentFull\tcollisionRatio\n"
	 "%d\t%lld\t%d\t%lld\t%d\t%.4f\t%.4f\t%.4f\n", vocabSize, capacity, bits, vHash->collisions,
	 vHash->max_collision_chain, hashingTime, (double)vocabSize * 100 / (double)capacity, (double)vHash->collisions / (double)vocabSize);

  dahash_destroy(&vHash);

  unload_all_lines_from_textfile(H, MH, &lines, &fileInMem, fileSize);

  exit(0);
}
