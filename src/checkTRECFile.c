// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#ifdef WIN64
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#endif

#include "definitions.h"
#include "utils/general.h"



static void print_usage(char *progname) {
  printf("Usage: %s <TRECFile>\n", progname);
  exit(1);
}


static long long checkTRECInMem(byte *fileInMem, size_t fileSize) {
  // Check that the structure of the TREC file makes sense.
  byte *p = fileInMem, *end = fileInMem + fileSize;
  long long validDocsRead = 0;
  while (p < end) {
    while (p < end && *p <= ' ') p++; // skip whitespace before <DOC>
    if (p >= end) break;  // - - - - - - - - - - - - - - >  This is OK
    
    if (*p != '<') {
      printf("Error at byte %zd after %lld valid docs read: Non-whitespace before <DOC>\n",
	     p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }
    if (strncmp(++p, "DOC>", 4)) {
      printf("Error at byte %zd after %lld valid docs read: First tag is not <DOC>\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }

    p += 4;
    while (p < end && *p <= ' ') p++; // skip whitespace before <DOC>
    if (p > end) {
      printf("Error at byte %zd after %lld valid docs read: EOF at start of DOC element.\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }
      
    if (*p != '<') {
      printf("Error at byte %zd after %lld valid docs read: Non-whitespace before <DOCNO>\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }
    if (strncmp(++p, "DOCNO>", 6)) {
      printf("Error at byte %zd after %lld valid docs read: First tag is not <DOCNO>\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }

    p += 6;
    while (p < end && *p != '<') p++; // skip to next tag, assuming there is a useful docID
    if (strncmp(++p, "/DOCNO>", 7)) {
      printf("Error at byte %zd after %lld valid docs read: Next tag should be </DOCNO>\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }

    p += 7;
    while (p < end && *p <= ' ') p++; // skip whitespace before <TEXT>
    if (*p != '<') {
      printf("Error at byte %zd after %lld valid docs read: Non-whitespace between </DOCNO> and </TEXT>\n",
	     p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }
    if (strncmp(++p, "TEXT>", 5)) {
      printf("Error at byte %zd after %lld valid docs read: Tag after </DOCNO> should be <TEXT>\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }
    
    p += 5;
    while (p < end && *p != '<') p++; // skip to next tag, assuming there is a useful doc content
    if (strncmp(++p, "/TEXT>", 6)) {
      printf("Error at byte %zd after %lld valid docs read: Next tag should be </TEXT>\n", p - fileInMem, validDocsRead);
      printf("Context: \n");
      put_n_chars(p - 100, 200);
      printf("\n");
      exit(1);  // ------------------------------->
    }
    
    p += 6;
    while (p < end && *p <= ' ') p++; // skip whitespace before </DOC>
    if (*p != '<') {
      printf("Error at byte %zd after %lld valid docs read: Non-whitespace between </TEXT> and </DOC>\n",
	     p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }
    if (strncmp(++p, "/DOC>", 5)) {
      printf("Error at byte %zd after %lld valid docs read: Tag after </TEXT> should be </DOC>\n", p - fileInMem, validDocsRead);
      exit(1);  // ------------------------------->
    }

    p += 5;
    validDocsRead++;
    if (validDocsRead % 10000 == 0) printf("  -- %lld docs --\n", validDocsRead);
  }  // And round we go again.

  return validDocsRead;
}


int main(int argc, char *argv[]) {
  byte *fileInMem;
  size_t fileSize;
  CROSS_PLATFORM_FILE_HANDLE fh;
  HANDLE h;
  int errorCode;
  double startTime;
  long long validDocsRead;


  setvbuf(stdout, NULL, _IONBF, 0);
  if (sizeof(u_char *) != 8) {
    printf("\n\nWarning: Pointers are only 4-byte. It is recommended that SynthaCorpus should be compiled for 64 bit.\n\n");
  }



  if (argc != 2) print_usage(argv[0]);

  startTime = what_time_is_it();

  fileInMem = (byte *)mmap_all_of((u_char *)argv[1], &fileSize, FALSE, &fh, &h,
				  &errorCode);
  if (fileInMem == NULL) {
    printf("Error:  Failed to mmap %s, error_code was %d\n", argv[1], errorCode);
    exit(1);
  }

  validDocsRead = checkTRECInMem(fileInMem, fileSize);

  unmmap_all_of(fileInMem, fh, h, fileSize);

  printf("Check of %s completed in %.3f sec. with no errors found.  Valid documents: %lld.\n",
	 argv[1], what_time_is_it() - startTime, validDocsRead);
  return 0;
}

