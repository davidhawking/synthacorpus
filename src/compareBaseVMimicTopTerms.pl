#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.

use File::Copy "cp";
use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
    $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

print "Being run in directory: $cwd\n";

die "Usage: $0 <corpusName> <emulationDir> <k> [<alt. baseDir>]
    - emulation_dir can be an absolute path or a path relative to the Experiments/Emulation directory
    - alternate baseDir can be an absolute path or a path relative to the Experiments/ directory
"
  unless $#ARGV >= 2;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});


$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$baseDir = "$expRoot/Base";

$corpusName = $ARGV[0];
if ($ARGV[1] =~ /^([a-zA-Z]:)?\//) {
  # Path is absolute
  $emuDir = $ARGV[1];
} else {
  # Path is relative
  $emuDir = "$expRoot/Emulation/$ARGV[1]";
}

$k = $ARGV[2];

if ($#ARGV >= 3) {
  # Alternate base directory has been specified
  if ($ARGV[3] =~ /^([a-zA-Z]:)?\//) {
    # Path is absolute
    $baseDir = $ARGV[3];
  } else {
    # Path is relative
    $baseDir = "$expRoot/$ARGV[3]";
  }
}


die "Directory $baseDir doesn't exist\n" unless -d $baseDir;

die "Directory $emuDir doesn't exist\n" unless -d $emuDir;


$vocabBase = "$baseDir/${corpusName}_vocab_by_freq.tsv";
$vocabMimic = "$emuDir/${corpusName}_vocab_by_freq.tsv";


die "Failed to read $vocabBase\n"
    unless -r $vocabBase;
die "Failed to read $vocabMimic\n"
    unless -r $vocabMimic;



$comparison_file = "$emuDir/${corpusName}_base_v_mimic_top_${k}_terms.txt";

die "VBASE" unless open VBASE, $vocabBase;
die "VMIMIC" unless open VMIMIC, $vocabMimic;
die "Can't write to $comparison_file\n" unless open CF, ">$comparison_file";

print CF "# Comparison of top $k most frequent terms, base v. mimic for $corpus_name
#                Base                 Mimic
# -----------------------------------------
";

for ($l = 0; $l < $k; $l++) {
    $vb = <VBASE>;
    $vm = <VMIMIC>;
    $vb =~ s/\t.*\n//;
    $vm =~ s/\t.*\n//;
    print CF sprintf("%20s  %20s\n", $vb, $vm);
}

close VBASE;
close VMIMIC;
close CF;

print "To compare popular words in the base and synthetic corpora: 

      cat $comparison_file

";

exit(0);


# -------------------------------------------------------


