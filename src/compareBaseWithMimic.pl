#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.

# Produces a stack of base_v_mimic comparison plots and summary files for
# a specified corpus and emulation directory.   A lot of the code was
# originally in emulateACorpus.pl, but I split it out to allow it to be
# used more generally

$|++;

$perl = $^X;
$perl =~ s@\\@/@g;


use File::Copy "cp";
use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
  $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}


die "Usage: $0 <corpusName> <emulationDir> <minNgram> <maxNgram> [-force] [-simplified]
    - emulationDir can be an absolute path or a path relative to the Experiments/Emulation directory
    - To avoid confusion, must explicitly specify range of ngram properties to extract.
    - If -force is given, corpusPropertyExtractor.exe will be run even if _summary.txt exists.
    - If -simplifed is given, the comparison will be with the corpus in Simplified rather than Base.
\n"
  unless $#ARGV >= 3;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});


$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$corpusName = $ARGV[0];
if ($ARGV[1] =~ /^([a-zA-Z]:)?\//) {
  # Path is absolute
  $emuDir = $ARGV[1];
} else {
  # Path is relative
  $emuDir = "$expRoot/Emulation/$ARGV[1]";
}

$minNgram = $ARGV[2];
die "Bad value $minNgram for minNgram (1 - 5)\n" unless $minNgram > 0 && $minNgram <= 5;
$maxNgram = $ARGV[3];
die "Bad value $maxNgram for maxNgram ($minNgram - 5)\n" unless $maxNgram < 6;
if ($minNgram > $maxNgram) {
  warn "Setting minNgram to maxNgram\n";
  $minNgram = $maxNgram;
}

$reExtract = 0;
$baseDir = "$expRoot/Base";
for ($a = 4; $a <= $#ARGV; $a++) {
  $reExtract = 1 if ($ARGV[$a] =~ /force/i);
  $baseDir = "$expRoot/Simplified" if ($ARGV[$a] =~ /simplified/i);
}

die "Directory $baseDir doesn't exist\n" unless -d $baseDir;
die "Directory $emuDir doesn't exist\n" unless -d $emuDir;

print "\nComparing emulation of $corpus in $emuDir with baseline in $baseDir\n\n";


foreach $suffix (".trec", ".TREC", ".starc", ".STARC", ".tsv", ".TSV") {
    $cf = "$baseDir/$corpusName$suffix";
    if (-r $cf) {
	$baseCorpusFile = $cf;
	$fileType = $suffix;
	last;   # Give preference to TREC over STARC over TSV.
    }
}

die "No base corpus file (STARC, TSV, or TREC) found\n" unless defined $baseCorpusFile;
die "Base corpus file $baseCorpusFile not found\n" unless -r $baseCorpusFile;


$emuCorpusFile = "$emuDir/$corpusName$fileType";
die "Emulated corpus file $emuCorpusFile (assumed same format as base) not found\n"
  unless -r $emuCorpusFile;

# The executables and scripts we need
$extractor = check_exe("../src/corpusPropertyExtractor.exe");
$lsqcmd = check_exe("../src/lsqfitGeneral.pl");
$doclenCmd = check_exe("../src/doclenPiecewiseModeling.pl");
$plotDoclengths = check_exe("../src/plotDocLengths.pl");
$plotWdlengths = check_exe("../src/plotWdLengths.pl");
$compare_top_terms = check_exe("../src/compareBaseVMimicTopTerms.pl");
$computeKL = check_exe("../src/computeDivergencesForEmuBase.exe");
$letterExtractor = check_exe("../src/extractLetterFrequenciesFromVocab.exe");
$plotter = `which gnuplot 2>&1`;
chomp($plotter);
die "Gnuplot not found\n"
  if $plotter =~/^which: no/;

$plotter = "gnuplot";   # There were problems with using a path (included spaces or punctuation.)

$baseSumryfile = "$baseDir/${corpusName}_summary.txt";
$emuSumryfile = "$emuDir/${corpusName}_summary.txt";

print "Base summaryfile: '$baseSumryfile'
Emu summaryfile: '$emuSumryfile'
";


## Has corpusPropertyExtractor been run on both Base and Emu?
# These options should match those in emulateARealCorpus.pl
$propertyExtractorOptions = " -separatelyReportBigrams=TRUE -minNgramWords=$minNgram -maxNgramWords=$maxNgram -ngramObsThresh=10 -ngramHashBits=25 -zScoreCriterion=3 -ngramStringBytes=39";  

if (-f "$baseSumryfile" && !$reExtract) {
    printf("Relying on properties already extracted from the base corpus\n");
} else {
  # Run the corpusPropertyExtractor on the base corpus
  $cmd = "$extractor inputFileName='$baseCorpusFile' outputStem='$baseDir'/$corpusName $propertyExtractorOptions";
  print $cmd, "\n";
  
  $start_time = time();
  $code = system($cmd);
  die "Base corpus property extraction failed with code $code. Cmd was:\n   $cmd\n"
    if ($code);
  $end_time = time();
  print sprintf("Time for base property extraction: %.2f\n", $end_time - $start_time);

  $cmd = "$letterExtractor $corpusName '$baseDir'\n";
  print $cmd, "\n";
  $start_time = time();
  $code = system($cmd);
  die "Base letter extraction failed with code $code. Cmd was:\n   $cmd\n"
    if ($code);
  $end_time = time();
  print sprintf("Time for base letter extraction: %.2f\nNow measure compressibility with gzip ...\n", $end_time - $start_time);

  $start_time = time();
  $gzipRatio = get_compression_ratio($baseCorpusFile, $baseDir, $corpusName);
  $end_time = time();
  print sprintf("Time for base gzip: %.2f\n", $end_time - $start_time);
  $size_on_disk = -s $baseCorpusFile;
  # Append gzip_ratio and size_on_disk to the summary file
  die "Can't append to $baseSumryfile\n"
    unless open SF, ">>$baseSumryfile";
  print SF "gzip_ratio=", sprintf("%.4f\n", $gzipRatio);
  print SF "size_on_disk=$size_on_disk\n";
  close SF;
}

    
if (-f $emuSumryfile  && !$reExtract) {
  printf("Relying on properties already extracted from the emulated corpus\n");
} else {
  # Run the corpusPropertyExtractor on the emulated corpus 
  $cmd = "$extractor inputFileName='$emuCorpusFile' outputStem='$emuDir'/$corpusName $propertyExtractorOptions";
  print $cmd, "\n";
  
  $start_time = time();
  $code = system($cmd);
  die "Emulated corpus property extraction failed with code $code. Cmd was:\n   $cmd\n"
    if ($code);
  $end_time = time();
  print sprintf("Time for emulated property extraction: %.2f\n", $end_time - $start_time);

  $cmd = "$letterExtractor $corpusName '$emuDir'\n";
  print $cmd, "\n";
  $start_time = time();
  $code = system($cmd);
  die "Emulated letter extraction failed with code $code. Cmd was:\n   $cmd\n"
    if ($code);
  $end_time = time();
  print sprintf("Time for emulated letter extraction: %.2f\nNow gzipping ...\n", $end_time - $start_time);

  $start_time = time();
  $gzipRatio = get_compression_ratio($emuCorpusFile, $emuDir, $corpusName);
  $end_time = time();
  print sprintf("Time for Emu gzip: %.2f\n", $end_time - $start_time);

  $size_on_disk = -s $emuCorpusFile;
  
  # Append gzip-ratio and size_on_disk to the summary file
  die "Can't append to $emuSumryfile\n"
    unless open SF, ">>$emuSumryfile";
  print SF "gzip_ratio=", sprintf("%.4f\n", $gzipRatio);
  print SF "size_on_disk=$size_on_disk\n";
  close SF;
}
    

# Extract some stuff from the the base summary file  - Format: '<attribute>=<value>'
die "Can't open $baseSumryfile\n"
    unless open SF, $baseSumryfile;
while (<SF>) {
  next if /^\s*#/;  # skip comments
  if (/(\S+)=(\S+)/) {
    $base{$1} = $2;
  }
}
close(SF);


# Extract the same stuff from the the Emu summary file  - Format: '<attribute>=<value>'
die "Can't open $emuSumryfile\n"
    unless open SF, $emuSumryfile;
while (<SF>) {
    next if /^\s*#/;  # skip comments
    if (/(\S+)=(\S+)/) {
	$mimic{$1} = $2;
    }
}
close(SF);


# Extract values for piecewise model of mimic document lengths

$cmd = "$doclenCmd $corpusName '$emuDir'\n";
print $cmd;
$start_time = time();
$use_gamma_instead = 0;
$rez = `$cmd`;
if ($?) {
  warn "Error: $doclenCmd failed to find good piecewise options\n";
  $use_gamma_instead = 1;
} else {
  if ($rez =~ /(synth_dl_segments=\"[0-9.:,;]+\")/s) {
    $mimic_dl_segs_option = $1;
    print "DL Piecewise Segs: $mimic_dl_segs_option\n";
  } else {
    warn "Match failed: $doclenCmd failed to find good piecewise options. Output was: '$rez'\n";
    $use_gamma_instead = 1;
  }
}

if ($use_gamma_instead) {
  $mimic_dl_segs_option = "-synth_doclen_gamma_shape=$mimic{doclen_gamma_shape} -synth_doclen_gamma_scale=$mimic{doclen_gamma_scale}";
  print "DL Piecewise Segs: Using gamma instead: $mimic_dl_segs_option\n";
}

$end_time = time();
print ETL sprintf("\t%.2f", $end_time - $start_time);

#  --------------------------------------------------------------------------------------

    
# --------------------- slope fitting and plot element generation for BASE -------------------------------
    
# Use lsqcmd to compute alpha for unigrams -- In this case, plots include linear and quadratic fit
$cmd = "$lsqcmd '$baseDir'/${corpusName}_vocab -plotLinearFit -plotQuadraticFit\n";
$lsqout = `$cmd`;
die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

    print $lsqout;


if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
    $plot_elt_real = $1;
    $plot_elt_real =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
    $base{alpha} = $1;
    $plot_elt_real =~ s/Linear fit/Fitted base/;
} else {
    die "Error: Unable to extract stuff from base corpus $lsqcmd output\n";
}


if (-r "$baseDir/${corpusName}_bigrams.plot") {
  # Use lsqcmd to compute alpha for bigrams
  $cmd = "$lsqcmd '$baseDir'/${corpusName}_bigrams\n";
    $lsqout = `$cmd`;
  die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;
  
  print $lsqout;

  if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
    $plot_elt_real_bg = $1;
    $plot_elt_real_bg =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
    $base{alpha_bigrams} = $1;
    $plot_elt_real_bg =~ s/Linear fit/Fitted real/;
  } else {
    die "Error: Unable to extract stuff from base corpus bigrams $lsqcmd output\n";
  }
} else {
  print "There is no bigrams plot file for Base.\n";
}


if (-r "$baseDir/${corpusName}_repetitions.plot") {
  # Use lsqcmd to compute alpha for repetitions
  $cmd = "$lsqcmd '$baseDir'/${corpusName}_repetitions\n";
  $lsqout = `$cmd`;
  die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;
  
  print $lsqout;
  
  
  if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
    $plot_elt_real_re = $1;
    $plot_elt_real_re =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
    $base{alpha_repetitions} = $1;
    $plot_elt_real_re =~ s/Linear fit/Fitted real/;
  } else {
    die "Error: Unable to extract stuff from base corpus repetitions $lsqcmd output\n";
  }
} else {
  print "There is no repetitions plot file for Base.\n";
}
  
  
  if (-e "$baseDir/${corpusName}_coocs.plot") {
    # Use lsqcmd to compute alpha for cooccurs (Only if the cooccurs files are available)
    $cmd = "$lsqcmd '$baseDir'/${corpusName}_cooccurs\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;
    
    print $lsqout;
    
    
    if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
      $plot_elt_real_co = $1;
      $plot_elt_real_co =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
      $base{alpha_cooccurrences} = $1;
      $plot_elt_real_co =~ s/Linear fit/Fitted real/;
    } else {
      die "Error: Unable to extract stuff from base corpus cooccurs $lsqcmd output\n";
    }
  }



    

# --------------------- slope fitting and plot element generation for EMU -------------------------------

 # Use lsqcmd to compute alpha for unigrams -- In this case, plots include linear and quadratic fit
$cmd = "$lsqcmd '$emuDir'/${corpusName}_vocab -plotLinearFit -plotQuadraticFit\n";
$lsqout = `$cmd`;
die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

print $lsqout;


if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
    $plot_elt_real = $1;
    $plot_elt_real =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
    $mimic{alpha} = $1;
    $plot_elt_real =~ s/Linear fit/Fitted base/;
} else {
    die "Error: Unable to extract stuff from emulated corpus $lsqcmd output\n";
}

    
if (-r "$emuDir/${corpusName}_bigrams.plot") {
    # Use lsqcmd to compute alpha for bigrams
    $cmd = "$lsqcmd '$emuDir'/${corpusName}_bigrams\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

    print $lsqout;


    if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
      $plot_elt_mimic_bg = $1;
      $plot_elt_mimic_bg =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
      $mimic{alpha_bigrams} = $1;
      $plot_elt_mimic_bg =~ s/Linear fit/Fitted real/;
    } else {
      die "Error: Unable to extract stuff from emulated corpus bigrams $lsqcmd output\n";
    }
  } else {
    print "There is no bigrams plot file for emulated $corpusName.\n";
  }


  if (-r "$emuDir/${corpusName}_repetitions.plot") {
    # Use lsqcmd to compute alpha for repetitions
    $cmd = "$lsqcmd '$emuDir'/${corpusName}_repetitions\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;
    
    print $lsqout;
    
    
    if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
      $plot_elt_mimic_re = $1;
      $plot_elt_mimic_re =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
      $mimic{alpha_repetitions} = $1;
      $plot_elt_mimic_re =~ s/Linear fit/Fitted real/;
    } else {
      die "Error: Unable to extract stuff from emulated corpus repetitions $lsqcmd output\n";
    }
  } else {
    print "There is no repetitions plot file for emulated $corpusName.\n";
  }
  
  
  if (-r "$emuDir/${corpusName}_coocs.plot") {
    # Use lsqcmd to compute alpha for cooccurs (Only if the cooccurs files are available)
    $cmd = "$lsqcmd '$emuDir'/${corpusName}_cooccurs\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;
    
    print $lsqout;
    
    
    if ($lsqout =~ /,(.*?\s[\-0-9.]+\s*\*x\s*title "Linear fit")/s){
      $plot_elt_mimic_co = $1;
      $plot_elt_mimic_co =~ /([\-0-9.]+)\s*\*x\s*title "Linear fit"/;
      $mimic{alpha_cooccurrences} = $1;
      $plot_elt_mimic_co =~ s/Linear fit/Fitted real/;
    } else {
      die "Error: Unable to extract stuff from emulated corpus cooccurs $lsqcmd output\n";
    }
  } else {
    print "There is no coocs plot file for emulated $corpusName.\n";
  }

   
# --------------  Compare term frequency distributions of various kinds for EMU ----------------------   

# Create a plotfile to compare the unigram and bigram frequency distributions
# in various ways.
    $pcfile = "$emuDir/${corpusName}_compare_tfds_plot.cmds";
    die "Can't open $pcfile for writing\n" unless open P, ">$pcfile";

    print P "
set terminal pdf
set size ratio 1
set xlabel \"Log10(rank)\"
set ylabel \"Log10(frequency)\"
set style line 1 linewidth 4
set style line 2 linewidth 4
set style line 3 linewidth 4
set pointsize 3
";

if (-e "${emuDir}/${corpusName}_cooccurs.plot") {
    print P "
set output \"$emuDir/${corpusName}_compare_tfds.pdf\"
plot \"${emuDir}/${corpusName}_vocab.plot\" title \"Unigrams (Base)\"  pt 7 ps 0.25, \"${emuDir}/${corpusName}_bigrams.plot\" title \"Bigrams (Base)\"  pt 7 ps 0.25, \"${emuDir}/${corpusName}_cooccurs.plot\" title \"Cooccurrences (Base)\"  pt 7 ps 0.25, \"${emuDir}/${corpusName}_repetitions.plot\" title \"Repeated terms (Base)\"  pt 7 ps 0.25 
";
} elsif (-e "${emuDir}/${corpusName}_bigrams.plot" && -e "${emuDir}/${corpusName}_repetitions.plot") {
     print P "
set output \"$emuDir/${corpusName}_compare_tfds.pdf\"
plot \"${emuDir}/${corpusName}_vocab.plot\" title \"Unigrams (Base)\"  pt 7 ps 0.25, \"${emuDir}/${corpusName}_bigrams.plot\" title \"Bigrams (Base)\"  pt 7 ps 0.25, \"${emuDir}/${corpusName}_repetitions.plot\" title \"Repeated terms (Base)\"  pt 7 ps 0.25 
";
}  

close P;

if (defined($plotter)) {
    `$plotter '$pcfile' > /dev/null 2>&1`;
    die "$plotter failed with code $? for $pcfile!\n" if $?;
}





die "Can't write to $emuDir/${corpusName}_base_v_mimic_summary.txt\n"
    unless open RVM, ">$emuDir/${corpusName}_base_v_mimic_summary.txt";

print RVM "\n                          Base v. Mimic";
print RVM "\n===============================================================\n";

foreach $k (sort keys %base) {
    $perc = "NA";
    if (defined($mimic{$k})) {
	$mim = $mimic{$k};
	$perc = sprintf("%+.1f%%", 100.0 * ($mimic{$k} - $base{$k}) / $base{$k})
	    unless !defined($base{$k}) || $base{$k} == 0;
    }
    else {$mim = "UNDEF";}
    print RVM sprintf("%27s", $k), "  $base{$k} v. $mim   ($perc)\n";
}

close(RVM);

#...............................................................................#
#         Now plot the term frequency distributions etc. for base and mimic     #
#...............................................................................#

$pcfile = "$emuDir/${corpusName}_base_v_mimic_plot.cmds";
die "Can't open $pcfile for writing\n" unless open P, ">$pcfile";

print P "
set terminal pdf
set size ratio 1
set xlabel \"Log10(rank)\"
set ylabel \"Log10(frequency)\"
set style line 1 linewidth 4
set style line 2 linewidth 4
set style line 3 linewidth 4
set pointsize 3
";
print P "
set output \"$emuDir/${corpusName}_base_v_mimic_unigrams.pdf\"
plot \"${baseDir}/${corpusName}_vocab.plot\" title \"Base\" pt 7 ps 0.4, \"$emuDir/${corpusName}_vocab.plot\" title \" $syntyp mimic\" pt 7 ps 0.17
";

if (-r "$emuDir/${corpusName}_bigrams.plot") {
  print P "
set output \"$emuDir/${corpusName}_base_v_mimic_bigrams.pdf\"
plot \"${baseDir}/${corpusName}_bigrams.plot\" title \"Base\" pt 7 ps 0.4, \"$emuDir/${corpusName}_bigrams.plot\" title \"$syntyp mimic\" pt 7 ps 0.17

";
}

if (-r "$emuDir/${corpusName}_ngrams.plot") {
  print P "
set output \"$emuDir/${corpusName}_base_v_mimic_ngrams.pdf\"
plot \"${baseDir}/${corpusName}_ngrams.plot\" title \"Base\" pt 7 ps 0.4, \"$emuDir/${corpusName}_ngrams.plot\" title \"$syntyp mimic\" pt 7 ps 0.17

";
}


if (-r "$emuDir/${corpusName}_repetitions.plot") {
  print P "
set output \"$emuDir/${corpusName}_base_v_mimic_repetitions.pdf\"
plot \"${baseDir}/${corpusName}_repetitions.plot\" title \"Base\" pt 7 ps 0.4, \"$emuDir/${corpusName}_repetitions.plot\" title \"$syntyp mimic\" pt 7 ps 0.17

";
}


    print P "
set ylabel \"Distinct words per doc.\"
set xlabel \"Word occurrences per doc.\"
set output \"$emuDir/${corpusName}_base_v_mimic_distinct_terms.pdf\"
plot \"${baseDir}/${corpusName}_termRatios.tsv\" title \"Base\" pt 7 ps 0.4, \"$emuDir/${corpusName}_termRatios.tsv\" title \"$syntyp mimic\" pt 7 ps 0.17

";


if (-e "$emuDir/coocs.plot") {
    print P "
set xlabel \"Log10(rank)\"
set ylabel \"Log10(frequency)\"
set output \"${experimentDir}/$syntyp/${lbl}/${corpusName}_base_v_mimic_cooccurs.pdf\"
plot \"${baseDir}/${corpusName}_cooccurs.plot\" title \"Base\" pt 7 ps 0.4, \"$emuDir/${corpusName}_cooccurs.plot\" title \"$syntyp mimic\" pt 7 ps 0.17

";
}

close P;

if (defined($plotter) && -e $pcfile) {
    $rslt = `$plotter '$pcfile' > /dev/null 2>&1`;
    die "$plotter failed with code $? for $pcfile!\n$rslt\n" if $?;
} else {
    warn "\n\n$0: Warning: gnuplot not found.  PDFs of graphs will not be generated.\n\n";
}

    

#...........................................................................#
#         Now plot the document length distributions for base and mimic     #
#...........................................................................#

$cmd = "$plotDoclengths $corpusName '$emuDir' $baseDir\n";
print $cmd, "\n";
$out = `$cmd`;
warn "Command failed with code $?.\nCommand was $cmd.\n" if $?;


#...........................................................................#
#         Now plot the word length distributions for base and mimic     #
#...........................................................................#

$cmd = "$plotWdlengths $corpusName '$emuDir' $baseDir\n";
print $cmd, "\n";
$out = `$cmd`;
warn "Command failed with code $?.\nCommand was $cmd.\n" if $?;

#...........................................................................#
#      Now calculate KL divergences and KS statistics for mimic v. base     #
#...........................................................................#

$cmd = "$computeKL $corpusName '$emuDir' $baseDir\n";
print $cmd, "\n";
$out = `$cmd`;
warn "Command failed with code $?.\nCommand was $cmd.\n" if $?;


    

# --------------------------Comparing lists of head terms, base v. mimic ------------------------------

$cmd = "$compare_top_terms $corpusName '$emuDir' 25 $baseDir";
$out = `$cmd`;
print $out;   # Just a message saying how to print the comparison file
die "$cmd failed with code $?.\nCommand was $cmd.\n" if $?;

print "To see base v. mimic statistics: 

       cat '${emuDir}'/${corpusName}_base_v_mimic_summary.txt

";


  
if (defined($plotter)) {
    print  "To see comparison term-frequency-distributions for unigrams, bigrams, cooccurrences and repetitions:

       acroread '$emuDir'/${corpusName}_compare_tfds.pdf

To see fitting plots for the base collection:

";
    foreach $t ("vocab", "bigrams", "repetitions") {
	print "      acroread '${baseDir}/'${corpusName}_${t}_lsqfit.pdf\n";
    }
    print "      acroread '${baseDir}'/${corpusName}_cooccurs_lsqfit.pdf\n" 
	if -e "${baseDir}/${corpusName}_cooccurs_lsqfit.pdf";
    print "\n";

    print "To see base v. mimic comparisons for word length and doc length distributions:

      acroread '${emuDir}'/${corpusName}_base_v_mimic_wdlens.pdf
      acroread '${emuDir}'/${corpusName}_base_v_mimic_wdfreqs.pdf
      acroread '${emuDir}'/${corpusName}_base_v_mimic_doclens.pdf

";

    print "To see KL divergences and KS statistics for various distributions:

      cat '${emuDir}'/${corpusName}_KLKS.txt

";

    print "To see base v. mimic comparisons for unigrams, bigrams, ngrams, cooccurrences and repetitions (if created):
 
";


    foreach $t ("unigrams", "bigrams", "ngrams", "repetitions", "distinct_terms") {
	print "      acroread '${emuDir}'/${corpusName}_base_v_mimic_$t.pdf\n";
    }
    print "      acroread '${emuDir}'/${corpusName}_base_v_mimic_cooccurs.pdf\n" 
	if -e "${emuDir}/${corpusName}_base_v_mimic_cooccurs.pdf";

    print "\n";

} else {
    print "\nNote: production of PDF plots suppressed because $plotter not found.\n\n";
}




# -----------------------------------------------------------------------------------
sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}



sub get_compression_ratio {
    my $file = shift;
    my $dir = shift;   
    my $corpusName = shift;
    my $tmpFile = "$dir/$corpusName.gz";  # Choose a path which reduces risk of clash during
                                          # multiple simultaneous runs.
    my $cmd = "gzip -c '$file' > '$tmpFile'";
    my $code = `$cmd`;
    if ($code) {
	warn "Command $cmd failed with code $code\n";
	return "N/A";
    }

#    my $gzo = `gzip -l $tmpFile`;  # Produced wrong answers for a multi-gigabyte file
#    if ($?) {
#	warn "gzip -l failed with code $?\n";
#	unlink "$tmpFile";
#	return "N/A";
#    }
# 
    #    $gzo =~ /([0-9]+)\s+([0-9]+)\s+([0-9.]+%)/s;

    my $uncoSize = -s $file;
    my $coSize = -s $tmpFile;
    
    $ratio = sprintf("%.3f", $uncoSize / $coSize);
    
    print "


$tmpFile: compression ratio (uncompressed size : compressed size) is $ratio



";
    unlink $tmpFile;
    return $ratio;
}
