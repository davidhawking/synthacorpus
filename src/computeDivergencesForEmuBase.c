// Copyright (c) David Hawking, 2019. All rights reserved.
// Licensed under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "definitions.h"
#include "utils/general.h"
#include "divergence.h"


int *readTSVintoArrayOfFreqs(u_char *fileName, int *lineCount) {
  byte **lines, *file_in_mem;
  CROSS_PLATFORM_FILE_HANDLE H;
  HANDLE MH;
  size_t sighs;
  int *rslt, i, count, L = 0;
  u_char *p;
 
  lines = load_all_lines_from_textfile(fileName, &count, &H, &MH, &file_in_mem, &sighs);
  rslt = cmalloc(count * sizeof(int), fileName, 0);
  for (i = 0; i < count; i++) {
    p = lines[i];
    while (*p && isspace(*p)) p++;
    if (*p == '#') continue;   // -------------->
    while (*p && *p != '\t') p++;
    p++;
    // p should now be positioned at the ASCII repn of the frequency in this TSV table
    rslt[L++] = strtol(p, NULL, 10);
  }

  unload_all_lines_from_textfile(H, MH, &lines, &file_in_mem, sighs);

  *lineCount = L;
  
  if (0) printf("\nRead %d frequencies from %s\n", *lineCount, fileName);
  return rslt;
}


#define STEMSZ 1000

int main(int argc, char **argv) {
  u_char baseFile[STEMSZ + 100], emuFile[STEMSZ + 100], baseStem[STEMSZ + 100], emuStem[STEMSZ + 100],
    outFile[STEMSZ + 100];
  u_char *files[] = {
    "_vocab_by_freq.tsv",
    "_bigrams_by_freq.tsv",
    "_ngrams_by_freq.tsv",
    "_repetitions_by_freq.tsv",
    "_docLenHist.tsv",
    "_vocab_wdlens.tsv",
    NULL};
  u_char *expRoot = NULL;
  int i, *P, *Q, pCount, qCount, baseCode, emuCode;
  double KLD, KSD, JSD, BD;
  FILE *KLKS = NULL;
  size_t baseSize, emuSize;
    
  if (argc < 3) {
    printf("Usage: %s <corpusName> <subDirectoryOfExperiments/Emulation> [<baseDir>]\n\n"
	   "   - Must be run in SynthaCorpus/src directory\n"
	   "   - subDirectory may be expressed as an absolute path\n"
	   "   - baseDir must be absolute or relative to CWD (lazy I know).\n\n", argv[0]);
    exit(1);
  }

  // We want to Emulation-vs-Base divergence scores for each of the files listed in files[] above

  if (strlen(argv[1]) + strlen(argv[2]) > STEMSZ) {
    printf("Error: Length of emulationDirectory + corpusName too large > %d.  Recompile with larger STEMSZ\n",
	   STEMSZ);
    exit(1);
  }

  expRoot = getenv("SC_EXPERIMENTS_ROOT");
  if (expRoot == NULL) {
    printf("Error: Environment variable $SC_EXPERIMENTS_ROOT must be defined.\n");
    exit(1);
  }

  if ((isalpha(argv[2][0]) && argv[2][1] == ':' && argv[2][2] == '/') || argv[2][0] == '/') {
    // A windows absolute path C:/blah
    // or a Unix absolute path /blah
    strcpy(emuStem, argv[2]);
  } else {
    // Relative path
    strcpy(emuStem, expRoot);
    strcat(emuStem, "/Emulation/");
    strcat(emuStem, argv[2]);
  }

  strcat(emuStem, "/");
  strcat(emuStem, argv[1]);

  if (argc > 3) {
    strcpy(baseStem, argv[3]);
    strcat(baseStem, "/");
    strcat(baseStem, argv[1]);
  } else {
    strcpy(baseStem, expRoot);
    strcat(baseStem, "/Base/");
    strcat(baseStem, argv[1]);
  }

  strcpy(outFile, emuStem);
  strcat(outFile, "_KLKS.txt");
  KLKS = fopen(outFile, "wb");
  if (KLKS == NULL) {
    printf("Error:  Can't write to %s\n", outFile);
    exit(1);
  }
	 

  printf("Base Stem: %s\nEmu Stem: %s\n\n", baseStem, emuStem);
  printf("computeKLDSForEmuBase.exe: Output in %s\n", outFile);

  for (i = 0; files[i] != NULL; i++) {
    strcpy(emuFile, emuStem);
    strcat(emuFile, files[i]);
    strcpy(baseFile, baseStem);
    strcat(baseFile, files[i]);

    if (!exists(baseFile, "") || !exists(emuFile, "")) {
      printf("Skipping KLD and KS calculation for %s.  Base or Emu not found.\n", files[i]);
      continue;  // ------------->
    }

    baseSize = get_filesize(baseFile, 0, &baseCode);
    emuSize = get_filesize(emuFile, 0, &emuCode);
    if (baseCode || emuCode || baseSize < 1 || emuSize < 1) {
      printf("Skipping KLD and KS calculation for %s.  Base or Emu empty or error.\n", files[i]);
      continue;  // ------------->
    }
      
      

    P = readTSVintoArrayOfFreqs(baseFile, &pCount);
    Q = readTSVintoArrayOfFreqs(emuFile, &qCount);

    if (1) printf("Calculating divergences(%s(%d), %s(%d))\n\n", baseFile, pCount, emuFile, qCount);

    calculateDivergencesFromHistos(P, Q, pCount, qCount, &KLD, &JSD, &BD, &KSD);
    fprintf(KLKS, "\n%s/%s: JSD=%.4f   KLD=%.4f BD=%.4f KSD=%.4f.  \n\n",
	    argv[1], files[i], JSD, KLD, BD, KSD);

    free(P);
    free(Q);
  }
  fclose(KLKS);
}
