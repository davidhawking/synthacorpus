// Copyright (c) David Hawking, 2019. All rights reserved.
// Licensed under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "definitions.h"
#include "utils/general.h"
#include "divergence.h"


static double *readDATintoArrayOfProbs(u_char *fileName, int *lineCount, int column) {
  byte **lines, *file_in_mem;
  CROSS_PLATFORM_FILE_HANDLE H;
  HANDLE MH;
  size_t sighs;
  int i, count, L = 0, c, debug = 0;
  double *rslt = NULL;
  u_char *p;
 
  lines = load_all_lines_from_textfile(fileName, &count, &H, &MH, &file_in_mem, &sighs);
  rslt = (double *)cmalloc(count * sizeof(double), fileName, 0);
  for (i = 0; i < count; i++) {
    p = lines[i];
    while (*p && isspace(*p)) p++;
    if (*p == '#') continue;   // -------------->
    for (c = 1; c < column; c++) {  // Skip column - 1 columns
      // Skip non-whitespaces and then whitespaces
      while (*p && !isspace(*p)) p++;
      while (*p && isspace(*p)) p++;
    }
    // p should now be positioned at the ASCII repn of the probability in this DAT table
    rslt[L++] = strtod(p, NULL);    // assumed to be a probability
    if (debug) printf("Read value %.5f from column %d of %s\n", rslt[L-1], column, fileName);
  }

  unload_all_lines_from_textfile(H, MH, &lines, &file_in_mem, sighs);

  *lineCount = L;
  
  if (debug) printf("\nRead %d probabilities from %s\n", *lineCount, fileName);
  return rslt;
}


#define STEMSZ 1000

int main(int argc, char **argv) {
  int pCount, qCount, errorCode1, errorCode2, column = 0;
  double KLD, KSD, JSD, BD, *P, *Q;
  size_t size1, size2;
    
  if (argc < 4) {
    printf("Usage: %s <DAT1> <DAT2> <c>\n\n"
	   "   - Compare distribution represented by the c-th column of DAT2 against same col of reference DAT1.\n"
	   "   - Columns number from 1.\n"
	   "   - In this program, the columns are separated by arbitrary whitespace (stupid decision!)\n"
	   "   - Also, the data values are doubles (probabilities) rather than int frequencies\n",
	   argv[0]);
    exit(1);
  }



  if (!exists(argv[1], "") || !exists(argv[2], "")) {
    printf("Can't do divergence calculation.  One of the DAT files doens't exist.\n");
    exit(1);
  }

  column = strtol(argv[3], NULL, 10);
  if (column < 1 || column > 100) {
    printf("Error: Comparison column must lie between 1 and 100 (numbering from 1)\n");
    exit(1);
  }
  
  size1 = get_filesize(argv[1], 0, &errorCode1);
  size2 = get_filesize(argv[2], 0, &errorCode2);
  if (errorCode1 ||errorCode2 || size1 < 1 || size2 < 1) {
    printf("Can't do divergence calculation.  One of the DAT files is empty or gives error.\n");
    exit(1);
  }
  
  
  
  P = readDATintoArrayOfProbs(argv[1], &pCount, column);
  Q = readDATintoArrayOfProbs(argv[2], &qCount, column);
  
  if (0) printf("Calculating divergences(%s(%d), %s(%d))\n\n",argv[1], pCount, argv[2], qCount);
  calculateDivergences(P, Q, pCount, qCount, &KLD, &JSD, &BD, &KSD);
  printf("\nJSD=%.4f   KLD=%.4f BD=%.4f KSD=%.4f %s(%d items), %s(%d items).  \n\n",
	 JSD, KLD, BD, KSD, argv[1], pCount, argv[2], qCount);
  
  free(P);
  free(Q);
}

