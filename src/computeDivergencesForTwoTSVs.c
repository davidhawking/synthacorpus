// Copyright (c) David Hawking, 2019. All rights reserved.
// Licensed under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "definitions.h"
#include "utils/general.h"
#include "divergence.h"


int *readTSVintoArrayOfFreqs(u_char *fileName, int *lineCount) {
  byte **lines, *file_in_mem;
  CROSS_PLATFORM_FILE_HANDLE H;
  HANDLE MH;
  size_t sighs;
  int *rslt, i, count, L = 0;
  u_char *p;
 
  lines = load_all_lines_from_textfile(fileName, &count, &H, &MH, &file_in_mem, &sighs);
  rslt = cmalloc(count * sizeof(int), fileName, 0);
  for (i = 0; i < count; i++) {
    p = lines[i];
    while (*p && isspace(*p)) p++;
    if (*p == '#') continue;   // -------------->
    while (*p && *p != '\t') p++;
    p++;
    // p should now be positioned at the ASCII repn of the frequency in this TSV table
    rslt[L++] = strtol(p, NULL, 10);
  }

  unload_all_lines_from_textfile(H, MH, &lines, &file_in_mem, sighs);

  *lineCount = L;
  
  if (0) printf("\nRead %d frequencies from %s\n", *lineCount, fileName);
  return rslt;
}


#define STEMSZ 1000

int main(int argc, char **argv) {
  int *P, *Q, pCount, qCount, errorCode1, errorCode2;
  double KLD, KSD, JSD, BD;
  size_t size1, size2;
    
  if (argc < 3) {
    printf("Usage: %s <TSV1> <TSV2>\n\n"
	   "   - Compare distribution represented by TSV2 against reference TSV1.\n", argv[0]);
    exit(1);
  }



  if (!exists(argv[1], "") || !exists(argv[2], "")) {
    printf("Can't do KLD and KS calculation.  One of the TSV files doens't exist.\n");
    exit(1);
  }
  
  size1 = get_filesize(argv[1], 0, &errorCode1);
  size2 = get_filesize(argv[2], 0, &errorCode2);
  if (errorCode1 ||errorCode2 || size1 < 1 || size2 < 1) {
    printf("Can't do KLD and KS calculation.  One of the TSV files is empty or gives error.\n");
    exit(1);
  }
  
  
  
  P = readTSVintoArrayOfFreqs(argv[1], &pCount);
  Q = readTSVintoArrayOfFreqs(argv[2], &qCount);
  
  if (1) printf("Calculating divergences(%s(%d), %s(%d))\n\n",argv[1], pCount, argv[2], qCount);

  calculateDivergencesFromHistos(P, Q, pCount, qCount, &KLD, &JSD, &BD, &KSD);
  printf("\nJSD=%.4f   KLD=%.4f BD=%.4f KSD=%.4f %s(%d items), %s(%d items).  \n\n",
	 JSD, KLD, BD, KSD, argv[1], pCount, argv[2], qCount);
  
  free(P);
    free(Q);
}

