// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license.

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifdef WIN64  // For the definition of _O_BINARY in _setmode
#include <io.h>
#include <fcntl.h>
#endif

#include "definitions.h"
#include "utils/general.h"


static  byte **PGLines = NULL, *PGFileInMemory, *oneBeyondEnd, *afterBOM;
static int chapterCount = 0;

static void emitOneChapter(int chapStart, int chapEnd, char *fname, int chapterNumber) {
  printf("<DOC>\n<DOCNO>%s_Chap%d</DOCNO>\n<TEXT>\n", fname, chapterNumber);
  put_n_chars(PGLines[chapStart], PGLines[chapEnd] - PGLines[chapStart]);
  printf("</TEXT>\n</DOC>\n");
  chapterCount++;
}


static void emitChapterByChapter(int endOfHeader, int startOfTrailer, char *fname) {
  // The content of the book starts at line endOfHeader and ends the line before
  // line startOfTrailer.  Chapters are assumed to start with line endOfHeader or
  // with a line starting with Chapter, CHAPTER, or ****
  int l, chapterNumber = 0;
  int chapStart = endOfHeader;

  for (l = endOfHeader; l < startOfTrailer; l++) {
    if (strstr_within_line(PGLines[l], "Chapter") == (char *)PGLines[l]
	|| strstr_within_line(PGLines[l], "CHAPTER") == (char *)PGLines[l]
	|| strstr_within_line(PGLines[l], "****") == (char *)PGLines[l]) {
      if (l > chapStart) {
	chapterNumber++;
	emitOneChapter(chapStart, l, fname, chapterNumber);
	chapStart = l;
      }
    }
  }

  // Output the last chapter
  if (startOfTrailer > (chapStart + 1)) {
    chapterNumber++;
    emitOneChapter(chapStart, l, fname, chapterNumber);
  }
}


void print_usage(char *progname) {
  printf("Usage: %s <Project_Gutenberg_Textfile> ...\n", progname);
  printf("\n - converts a list of PG files into a single file in simple\n"
	 "TREC format.  Each document in the output is represented by an\n"
	 "SGML-style DOC element, which starts with a DOCNO element giving\n"
	 "the document identifier.  The DOCNO element is immediately followed\n"  
	 "by a TEXT element containing all the content.  For example:\n"
	 "<DOC>\n"
	 "<DOCNO>Wuthering_Heights_Ch1</DOCNO>\n"
	 "<TEXT>\n"
	 " ... content ...\n"
	 "</TEXT>\n"
	 "</DOC>.\n\n"
	 "By default, Project Gutenberg headers and trailers are not output, and \n"
	 "books are broken up into chapters.  Each chapter results in a TREC\n"
	 "document.  Output is to stdout.\n"
	 "\n"
	 "All files are assumed to be in the UTF-8 character set.\n");
  exit(1);
}


int main(int argc, char **argv) {
  int f, l, numLines, endOfHeader, startOfTrailer;
  HANDLE mh;
  CROSS_PLATFORM_FILE_HANDLE h;
  size_t PGFileSize;
  BOOL breakIntoChapters = TRUE;
  
  test_strstr_within_line();
  
  if (argc < 2) print_usage(argv[0]);

#ifdef WIN64
  _setmode(1,_O_BINARY);  // ON Windows we need to force stdout to binary mode otherwise we get double CRs
#endif

  for (f = 1; f <argc; f++) { // Loop over the input files
    // Make an array of pointers to all the lines in the file
    fprintf(stderr, "File %d: %s\n", f, argv[f]);
    PGLines = load_all_lines_from_textfile(argv[f], &numLines, &h,
                                           &mh, &PGFileInMemory,
                                           &PGFileSize);
    afterBOM = PGFileInMemory;
    if (*PGFileInMemory == 0xEF
	&& *(PGFileInMemory + 1) == 0xBB
	&& *(PGFileInMemory + 2) == 0xBF) {
      afterBOM += 3;
      PGLines[0] += 3;
    }
    
    oneBeyondEnd = PGFileInMemory + PGFileSize;
    endOfHeader = -1;
    startOfTrailer = -1;
    // Find the boundaries of the three sections of the file
    for (l = 0; l <numLines; l++) {
      if (strstr_within_line(PGLines[l], "PROJECT GUTENBERG EBOOK") != NULL) {
	if (strstr_within_line(PGLines[l], "START") != NULL) 
	  endOfHeader = l + 1;
	else if (strstr_within_line(PGLines[l], "END") != NULL)
	  startOfTrailer = l;
      }
    }

    if (endOfHeader == -1 || startOfTrailer == -1) {
      fprintf(stderr,"Error: didn't find header or trailer line in %s. Skipping.\n",
	    argv[f]);
    } else {
      if (breakIntoChapters) {
	emitChapterByChapter(endOfHeader, startOfTrailer, argv[f]);
      }  else {
	// Output the entire content part of the file as a single book 
	printf("<DOC>\n<DOCNO>%s</DOCNO>\n<TEXT>\n", argv[f]);
	put_n_chars(PGLines[endOfHeader], PGLines[startOfTrailer] - PGLines[endOfHeader]);
	printf("\n</TEXT>\n</DOC>\n");
      }

      
    }

    unload_all_lines_from_textfile(h, mh, &PGLines, &PGFileInMemory,
                                           PGFileSize);
    
  }
  fprintf(stderr, "Normal exit. Input Files: %d.  Output Chapters: %d\n",
	  argc - 1, chapterCount);
}
