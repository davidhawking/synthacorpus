// Copyright (c) David Hawking, 2019. All rights reserved.
// Licensed under the MIT license.

// A library for calculating different divergence/distance measures between two arrays
// P and Q of probabilities.  The measures are:
//
//    Kullback-Leibler Divergence  KLD
//    Jensen-Shannon Divergence    JSD  -- Requires computation of a mean array
//    Bhattacharyya Distance       BD
//    Kolmogorov-Smirnov Distance  KSD
//
// Unlike a previous version for KLD, no smoothing is applied.  Zero or missing is zero.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "definitions.h"
#include "utils/general.h"
#include "divergence.h"

int calculateDivergences(double *P, double *Q, int pCount, int qCount,
			   double *KLD, double *JSD, double *BD, double *KSD) {
  // Calculate all four types of divergence from the two probability arrays P and Q
  // Return non-zero on error
  
  int i, count = pCount;
  double kld = 0, jsd = 0,
    cumProbP = 0, cumProbQ = 0,  probDiff, maxCumProbDiff = 0, // for KSD
    bc = 0,  // for BD
    m, jsd1, jsd2,  // for JSD
    p, q;
  *KLD = 0.0; *JSD = 0;  *BD = 0;  *KSD = 0;  // Initialise in case of error exit
  
  if (count < qCount) count = qCount;   // max(pCount, qCount)
  if (count < 3) {
    printf("Error: calculate_divergences():  Too little data\n");
    return 1;
  }

  for (i = 0; i < count; i++) {
    // Cope with missing elements.  A missing element is considered zero.
    if (i >= pCount) p = 0;
    else p = P[i];
    if (i >= qCount) q = 0;
    else q = Q[i];

    // Check for out-of-range probabilities
    if (p < 0 || p > 1 || q < 0 || q > 1) {
      printf("Error: calculate_divergences(): Array element %d not a probability\n", i);
      return 2;
    }
    
    // --- KLD ---
    if (p > 0 && q > 0) kld += p * log2(p / q);  

    // --- JSD ---
    m = (p + q) / 2;
    jsd1 = 0;  jsd2 = 0;
    if (p > 0 && m > 0) jsd1 += p * log2(p / m);
    if (q > 0 && m > 0) jsd2 += q * log2(q / m);
    jsd += (jsd1 + jsd2) / 2;

    // --- BD ---
    bc += sqrt(p * q);
    
    // --- KSD ---
    cumProbP += p;
    cumProbQ += q;
    
    if (cumProbP > 1.0001 || cumProbQ > 1.0001) {
      printf("Error: calculate_divergences(): cumulative probabilities %.4f, %.4f exceed 1.0001 after %d!\n",
	     cumProbP, cumProbQ, i);
      return(3);
    }
    probDiff = fabs(cumProbP - cumProbQ);
    if (probDiff > maxCumProbDiff) maxCumProbDiff = probDiff;
  }

  // Set up the return variables
  *KLD = kld;
  *JSD = jsd;
  *BD = -log(bc);
  *KSD = maxCumProbDiff;
  return 0;
}


int calculateDivergencesFromHistos(int *iP, int *iQ, int pCount, int qCount,
			   double *KLD, double *JSD, double *BD, double *KS) {
  // Calculate all four types of divergence from the two histogram arrays iP and iQ,
  // by converting them to probability arrays.
  // Return non-zero on error

  double sum, *P, *Q;
  int i, r;

  if (pCount < 3 || qCount < 3) {
    printf("Error: calculateDivergencesFromHistos():  Too few elements\n");
    return 11;
  }

  // First convert P array
  P = (double *)cmalloc(pCount * sizeof(double), "P", 0);
  sum = 0;
  for (i = 0; i < pCount; i++) {
    P[i] = (double)iP[i];
    sum += P[i];
  }
  for (i = 0; i < pCount; i++) {
    P[i] /= sum;
  }

  // Next convert Q array
  Q = (double *)cmalloc(qCount * sizeof(double), "Q", 0);
  sum = 0;
  for (i = 0; i < qCount; i++) {
    Q[i] = (double)iQ[i];
    sum += Q[i];
  }
  for (i = 0; i < qCount; i++) {
    Q[i] /= sum;
  }

  r = calculateDivergences(P, Q, pCount, qCount,
			   KLD, JSD, BD, KS);

  free(P);
  free(Q);

  return r;
}

#ifdef TEST

static void dTest(int *I, int *J, int iCount, int jCount) {
  double KLD, JSD, BD, KSD;
  int r;
  calculateDivergencesFromHistos(I, I, iCount, iCount, &KLD, &JSD, &BD, &KSD);
  printf("H1 vs. H1: KLD = %.4f, JSD = %.4f, BD = %.4f, KSD = %.4f\n", KLD, JSD, BD, KSD);
  calculateDivergencesFromHistos(J, J, jCount, jCount, &KLD, &JSD, &BD, &KSD);
  printf("H2 vs. H2: KLD = %.4f, JSD = %.4f, BD = %.4f, KSD = %.4f\n", KLD, JSD, BD, KSD);
  calculateDivergencesFromHistos(I, J, iCount, jCount, &KLD, &JSD, &BD, &KSD);
  printf("H1 vs. H2: KLD = %.4f, JSD = %.4f, BD = %.4f, KSD = %.4f\n", KLD, JSD, BD, KSD);
  calculateDivergencesFromHistos(J, I, jCount, iCount, &KLD, &JSD, &BD, &KSD);
  printf("H2 vs. H1: KLD = %.4f, JSD = %.4f, BD = %.4f, KSD = %.4f\n", KLD, JSD, BD, KSD);
}


int main(int argc, char **argv) {
  int A[10] = {5,3,7,2,9,8,0,1,7,7},
    B[10] = {1,2,3,4,5,6,7,8,9,0},
    P[3] = {36,48,16},
    Q[3] = {33333, 33333, 33333},
    X[6] = {1000000, 0, 1000000, 0, 1000000, 0},
    Y[6] = {0, 1000000, 0, 1000000, 0, 1000000},
    Z[9] = {0, 1000000, 0, 1000000, 0, 1000000, 0, 0, 0};
  double KLD, KS;
  int i;
  
  printf("Array A: ");
  for (i = 0; i < 10; i++) printf("%d, ", A[i]);
  printf("\n\n");
  
  printf("Array B: ");
  for (i = 0; i < 10; i++) printf("%d, ", B[i]);
  printf("\n\n");

  dTest(A, B, 10 ,10);

  printf("\n\nArray P: ");
  for (i = 0; i < 3; i++) printf("%d, ", P[i]);
  printf("\n\n");
  
  printf("Array Q: ");
  for (i = 0; i < 3; i++) printf("%d, ", Q[i]);
  printf("\n\n");
  
  dTest(P, Q, 3, 3);

  printf("\n\nArray X: ");
  for (i = 0; i < 6; i++) printf("%d, ", X[i]);
  printf("\n\n");
  
  printf("Array Y: ");
  for (i = 0; i < 6; i++) printf("%d, ", Y[i]);
  printf("\n\n");
  
  dTest(X, Y, 6, 6);
  
  printf("\n\nArray Y: ");
  for (i = 0; i < 6; i++) printf("%d, ", Y[i]);
  printf("\n\n");
  
  printf("Array Z: ");
  for (i = 0; i < 9; i++) printf("%d, ", Z[i]);
  printf("\n\n");

  dTest(Y, Z, 6, 9);
 
}

#endif
