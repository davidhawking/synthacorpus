// Copyright (c) David Hawking, 2019. All rights reserved.
// Licensed under the MIT license.

int calculateDivergences(double *P, double *Q, int pCount, int qCount,
			   double *KLD, double *JSD, double *BD, double *KSD);

int calculateDivergencesFromHistos(int *iP, int *iQ, int pCount, int qCount,
			   double *KLD, double *JSD, double *BD, double *KSD);
