#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.

$|++;

# Read a .doclenhist file and determine how to model the document length
# distribution using a piecewise approach.   Output is a value string for
# the -synth_dl_segments option to corpusGenerator.exe and a plot illustrating
# the accuracy of the model.

$numBucks = 100;
$bucketingThresh = 4 * $numBucks;  # Bucket if the range of lengths is larger than this,
                                   # otherwise use the raw histogram.

# If there are a lot of distinct lengths, then we bucket to smooth the distribution.
# Originally, the buckets corresponded to a linear distribution of the horizontal
# axis (lengths) but some corpora have a very long and sparse tail, leading to poor
# bucketing performance.   Now we are trialing the use of buckets containing an
# approximately equal number of documents.


die "Usage: $0 <corpusName> <Dir>
    Dir can be an absolute path or a path relative to the Experiments/Emulation directory.
    If absolute it can refer to the base directory."
  unless $#ARGV >= 1;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  If the
nominated directory doesn't exist, this script will attempt to create it.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$corpusName = $ARGV[0];
if ($ARGV[1] =~ /^([a-zA-Z]:)?\//) {
  # Path is absolute
  $Dir = $ARGV[1];
} else {
  # Path is relative
  $Dir = "$expRoot/Emulation/$ARGV[1]";
}

die "Directory $Dir doesn't exist\n" unless -d $Dir;

$plotter = `which gnuplot 2>&1`;
chomp($plotter);
if ($plotter =~/^which: no/) {
  undef $plotter;
} else {
  $plotter = "gnuplot";   # Rely on it being in our path
}
$xaxisMax = 3000;

$threshPercOfMax = 2;  # If the maximum deviation from a segment is less than this,
                         # the segment will not be split

$q = 0;

$dlhfile = "$Dir/${corpusName}_docLenHist.tsv";

die "Can't open $dlhfile"
  unless open DLH, $dlhfile;

# Read the _summary.txt file to find the total number of non-empty documents in the corpus.
# This allows us to set the bucket size.
$sumryFile .= "$Dir/${corpusName}_summary.txt";
$docLine = `head -1 '$sumryFile'`;
die "Head of $sumryFile failed\n"  if $?;
$docLine =~ /docs=([0-9]+)\s/;
$numDox = $1;
$buckSize = $numDox / $numBucks;

die "Not sensible to try to model a corpus with fewer than 100 documents.  This one has $numDox\n"
  unless $numDox >= 100;


$cumFreq[0] = 0;
$lines = 0;
$minLen = 9999999999;
$maxLen = 0;
$maxFreq = 0;

$b = 0;  # bucket number
$nobs[$b] = 0;    # Number of distinct lengths in this bucket
$freq[$b] = 0;    # Total number of docs in this bucket
$lenSum[$b] = 0;  # Sum of the lenghts of all the docs in this bucket.

while (<DLH>) {
  next if /^\s*\#/;
  if (/^\s*([1-9][0-9]*)\s+([0-9]+)/) {   # Make sure to consider only lines with '<length>\t<freq>'
    $l = $1;
    if ($2 < 1) {
      # This line has a zero frequency -- it's not usual to include such lines
      next;    # -------------------->
    } 
   
    $rfreq[$l] = $2;  # record the histogram point.

    # Record statistics
    $cumFreq += $2;
    $maxFreq = $2 if $maxFreq < $2;
    $lines++;
    # Test for 
    $minLen = $l if $minLen > $l;
    $maxLen = $l if $maxLen < $l;

    # Deal with bucketing.
    $nobs[$b]++;
    $freq[$b] += $rfreq[$l];
    
    $lenSum[$b] += $l * $rfreq[$l];
    if ($freq[$b] >= $buckSize) {
      # Tip over to the next bucket
      $meanLen[$b] = sprintf("%.2f", $lenSum[$b] / $freq[$b]);
      $meanFreq[$b] = sprintf("%.2f", $freq[$b] / $nobs[$b]);
      $b++;
      $nobs[$b] = 0;
      $freq[$b] = 0;
      $lenSum[$b] = 0;
    }
  }
}
close(DLH);

print "\nTotal frequency: $cumFreq
Length range: $minLen -- $maxLen
No. distinct non-zero lengths: $lines

";

if ($xaxisMax > $maxLen) {$xaxisMax = $maxLen};

# Tidy up bucketing
if ($nobs[$b] > 0) {
  $meanLen[$b] = sprintf("%.2f", $lenSum[$b] / $freq[$b]);
  $meanFreq[$b] = sprintf("%.2f", $freq[$b] / $nobs[$b]);
  $numBucks = $b + 1;   # May be less than the original value due to some buckets being over full
} else {
  $numBucks = $b;
}
  
# Are we going to use bucketing?
if ($maxLen - $minLen < $bucketingThresh) {
  # --- No. --- Pretend that the raw data are buckets
  $b = 0;
  for ($i = $minLen; $i <= $maxLen; $i++) {
    if (defined($rfreq[$i])) {
      $meanLen[$b] = $i;
      $meanFreq[$b] = $rfreq[$i];
      $b++;
    }
  }
  $numBucks  = $b;
  $usingBuckets = 0;
  print "Not using bucketing:  Number of 'buckets': $numBucks\n\n";
} else {
  # --- Yes. ----
  $usingBuckets = 1;

  print "Bucketing:  Number of buckets: $numBucks\n\n";
}

for ($b = 0; $b < $numBucks; $b++) {
    print "Bucket $b:  $meanLen[$b] ", sprintf("%.1f\n", $meanFreq[$b]);
}




# Now do the segment splitting and write the results 
$split_thresh = $threshPercOfMax * $maxFreq / 100; 

$dlSegments = "$Dir/${corpusName}_segments.dat";

my @segBoundaries;   # Each element contains a bucket number
$q = 0;

segsplit(0, $numBucks - 1);   # Fills in the @segBoundaries array and sets $q to its number of elements.
$segBoundaries[$q++] = $numBucks -1;  # segsplit doesn't fill this in

print "\nSegment splitting finished: $q segments\n\n";

for ($i = 0; $i < $q; $i++) {
  print "   $i  $meanLen[$segBoundaries[$i]]\n";
}

createCGOptionStringAndPlotDataFile();

# Now do the plotting

$pcfile = "$Dir/${corpusName}_dlsegs_fitting_plot.cmds";
die "Can't open $pcfile for writing\n" unless open P, ">$pcfile";

print P "
set terminal pdf
set size ratio 1
set ylabel \"Frequency\"
set xlabel \"Document length\"
set style line 1 linewidth 4
set style line 2 linewidth 4
set pointsize 3
";
    print P "
set output \"$Dir/${corpusName}_dlsegs_fitting.pdf\"
plot [0:$xaxisMax]\"$dlhfile\" pt 7 ps 0.4  title \"${corpusName}\", \"$dlSegments\" with lines ls 2 title \"Piecewise($threshPercOfMax% of max freq): $q segments\"
";

close P;

if (defined($plotter)) {
  `$plotter '$pcfile' > /dev/null 2>&1`;
  if ($?) {
    warn "Gnuplot failed with code $? for 'gnuplot $pcfile!'

$Dir/${corpusName}_dlsegs_fitting.pdf will not be generated.\n\n";
  } else {
    print "To see quality of fitting:

    acroread '$Dir'/${corpusName}_dlsegs_fitting.pdf\n\n";
  }
} else {
    die "\n\n$0: Warning: gnuplot not found.  $Dir/${corpusName}_dlsegs_fitting.pdf will not be generated.\n\n";
}


exit(0);


# ---------------------------------------------------------------------------------

# segsplit() is a recursive function which takes a segment represented by l
# and r, where l and r are bucket indexes, and splits it at the point of 
# maximum deviation from the line connecting (meanLen(l), meanFreq(l)) and (meanLen(r), meanFreq(r)),
# provided that that deviation exceeds a threshold.

sub segsplit {
  my $l = shift;
  my $r = shift;
  
  return if ($l > ($r - 1));
  
  my $slope = ($meanFreq[$r] - $meanFreq[$l]) / ($meanLen[$r] - $meanLen[$l]);
  my $up;
  my $down;
  my $diff;
  
  print "Attempting to split segment from ($meanLen[$l], $meanFreq[$l]) to ($meanLen[$r], $meanFreq[$r]). Slope: ",
    sprintf("%.4f\n",$slope);
  
  my $ixofmaxdiff = 0;
  my $maxdiff = 0;
  # Loop over all the points between l and r, looking for the maximum deviation
  # from the linear expectation.
  for (my $i = $l + 1;  $i < $r; $i++) {
    next if (! defined($meanFreq[$i]));
    my $expected = $meanFreq[$l] + $slope * ($meanLen[$i] - $meanLen[$l]);
    my $actual = $meanFreq[$i];
    
    # This is a complicated way of calculating the absolute value of the difference.
    # while avoiding some problem when down is less than 1   Huh??
    if ($expected > $actual) {
      $up = $expected;
      $down = $actual;
    } else {
      $up = $actual;
      $down = $expected;
    }
    if ($down < 1) {
      $diff = 0.0;
    } else {
      $diff = $up - $down;
    }
    
    #print "Point $i: act: $actual; exp: $expected; diff: $diff\n";
    if ($diff > $maxdiff) {
      $maxdiff = $diff;
      $ixofmaxdiff = $i;
    }
  }
  
  if ($maxdiff > $split_thresh) {
    #print "Max diff is $maxdiff, for length $ixofmaxdiff\n";
    print "   splitting ...\n";
    segsplit($l, $ixofmaxdiff);
    segsplit($ixofmaxdiff, $r);
  } else {
    $segBoundaries[$q++] = $l;
  }
}


sub createCGOptionStringAndPlotDataFile {
  # The @segBoundaries array (with $q elements) contains a list of bucket indexes. They
  # should be in increasing order.  The elements of arrays @meanLen and @meanFreq correspond
  my @segLen;
  my @segCumProb;
  my @plotFreq;
  my $si = 0;  # Index into the final segment arrays segLen and segCumProb;
  my $optionString = "-synth_dl_segments=";

  # If we're bucketing, we probably need to add an element corresponding to minLen
  if ($meanLen[$segBoundaries[0]] != $minLen) {
    $segLen[$si] = $minLen;
    $segCumProb[$si] = 0.0;
    $plotFreq[$si] = $rfreq[$minLen];
    $si++;
    print " .. added a segment for minLen $minLen\n";
  }

  # Now loop through the raw frequencies and calculate the cumulative probability up
  # to and including the mean length of each bucket listed in @segBoundaries
  my $cumFreq = 0;
  my $sbi = 0; 
  for (my $r = $minLen; $r <= $maxLen; $r++) {
    if (defined($rfreq[$r])) {
      $cumFreq += $rfreq[$r];
      if ($r >= $meanLen[$segBoundaries[$sbi]]) {
	# We've come to a segment boundary
	print "Seg boundary found:  $sbi, $si r = $r,  meanLen[sbi] = $meanLen[$segBoundaries[$sbi]]\n";
	$segLen[$si] = $meanLen[$segBoundaries[$sbi]];
	$segCumProb[$si] = sprintf("%.10f", $cumFreq / $numDox);
	$plotFreq[$si] = $meanFreq[$segBoundaries[$sbi]];
	$si++;
	$sbi++;
	last if $sbi >= $q;
      }
    }
  }

  # If we're bucketing, we probably need to add an element corresponding to maxLen
  if ($meanLen[$segBoundaries[$sbi - 1]] != $maxLen) {
    $segLen[$si] = $maxLen;
    $segCumProb[$si] = 1.0;
    $plotFreq[$si] = $rfreq[$maxLen];
    $si++;
    print " .. added a segment for maxLen $maxLen\n";
  }

  
  # Now fill in the option string from the segLen and segCumProb arrays
  $optionString .= "\"$si:";
  for (my $i = 0; $i < $si; $i++) {
    $optionString .= "$segLen[$i],$segCumProb[$i];";
  }
  $optionString .= "\"";

  print "The corpusGenerator option is: $optionString\n\n";

  # ----------------------------------------------------------------
  # Now write the segments in the right format in the plot datafile.  Each
  # segment is in the form of x0 y0 on one line, x1 y1 on the next and then
  # a blank line.   The y values must be actual frequencies not cumulative
  # probabilities.
  
   die "Can't write to $dlSegments" unless open DLQ, ">$dlSegments";
  for (my $i = 0; $i < ($si - 1); $i++) {
    print DLQ "$segLen[$i] $plotFreq[$i]
$segLen[$i+1] $plotFreq[$i+1]

";
  }
  close(DLQ);
}
