#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.


# 1. Run corpusPropertyExtractor.exe
#     - this creates a whole bunch of files which can be used for plotting
#       and analysis as well as for generating the parameters needed in
#       step 2
# 2. generateACorpus.exe is documented in ../doc/how_to_synthesize_a_corpus.pdf


# 
# Notes on modeling the term frequency distribution:
# --------------------------------------------------
# 1. Singleton terms are treated separately.  We count how many there are.
# 2. Terms at the head are also treated separately.   For several 
#    corpora, the first few terms show substantial deviations from either
#    a linear or a quadratic fit (in log-log space).  Therefore, we calculate
#    the term occurrence percentages of the 10 most frequent terms.  
# 3. The non-singleton, non-head part of the term frequency distribution
#    is best modeled in piecewise linear fashion.  We assume 10 pieces.
# 4. Naive least squares best fitting results in domination by the tail terms
#    and poor fit toward the head.  Therefore we do the fitting based on 
#    a subset of the data which ensures that datapoints are only picked if
#    they are more than a threshold X distance from the previous picked point.
#    Adamic uses bucketing -- our method is similar and may have some advantage.

# Scripts are now called via quotemeta($^X) in case the perl we want is not in /usr/bin/
#  (see check_exe())
#


# Versions after 05 Jan 2016 do all the work in corpus specific sub-directories, (A) to
# allow this script to be run simultaneously on multiple corpora, and (B) to
# declutter.

$perl = $^X;
$perl =~ s@\\@/@g;

use File::Copy "cp";
use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
  $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}
# ------------------------------ Time logging -----------------------------
# [David Hawking 26 Mar 2019] - Added code to record times taken by the major operations in the emulation.
# The times are recorded as lines in the $timeLog file.  Each line is tagged with the date and the
# emulation parameters and contains seven times.  The file is written in TSV format.
use Time::HiRes qw(time);

$overall_start_time = time();

$markov_defaults = "-markov_lambda=0 -markov_model_word_lens=TRUE -markov_use_vocab_probs=FALSE -markov_full_backoff=FALSE -markov_assign_reps_by_rank=TRUE -markov_favour_pronouncable=TRUE";


$|++;

die "Usage: $0 <corpus_name> <tf_model> <doc_length_model> <term_repn_model> <dependence_model> [-dependencies=neither|both|base|mimic] [-force]
    <corpus_name> is a name (e.g. TREC-AP) not a path.  We expect to find a single file called
          <corpus_name>.tsv, <corpus_name>.trec, or <corpus_name>.starc in the Base sub-directory of $SC_EXPERIMENT_ROOT
    <tf_model> ::= Piecewise|Linear|Copy|Uniform|BASELINE
          If Piecewise we'll use a 3-segment term-frequency model with 10 headpoints, 10 linear
          segments in the middle and an explicit count of singletons.  If Linear we'll approximate
          the whole thing as pure Zipf.  If Copy we'll copy the exact term frequency distribution
          from the base corpus.  BASELINE is like Copy but sampling with replacement.
    <doc_length_model> ::= dlnormal|dlsegs|dlhisto|dluniform|dlgamma
          If dlhisto or dlsegs is given, the necessary data will be taken from the base corpus.
          Recommended: dlgamma
    <term_repn_model> ::= tnum|base26|base26s|bubble_babble|simpleWords|from_tsv|markov-9e?|markov-9e?c?
          The Markov order is specified by the single digit, represented by '9'.
          If present, the 'e' specifies use of the end-of-word symbol.  Otherwise
          a random length will be generated for each word and it will be cut off there.
          The Markov model will be trained on the base corpus.
          If from_tsv is given, the vocab will be that of the base corpus.
          Recommended: from_tsv if appropriate or markov-5e
    <dependence_model> ::= ind|ngrams[2-5]|bursts|coocs|fulldep
          Currently, only ind(ependent) and ngramsX are implemented.  Ind means that words are 
          generated completely independently of each other.  Fulldep means ngrams + bursts 
          + coocs.  Dependence models are only applied if the relevant files, i.e ngrams.termids,
          bursts.termids, coocs.termids, are available for the base corpus. 

    The default value for dependencies is both.  This means that corpusPropertyExtractor will
    extract term dependency data for both the base and the mimic corpus.   That may be memory 
    intensive and very time consuming. Other values can be used to suppress that computation 
    for either or both of the base and mimic corpus. 

    -force forces reextraction of base properties even if a summary.txt file exists.
\n"

    unless $#ARGV >= 4 ;

$corpusName = $ARGV[0];

die "\nUnfortunately, SynthaCorpus runs into trouble when there is punctuation
in directory paths or the corpusName.  Note that the directory path may now contain spaces
but the corpusName may not.

Your current working directory, or the corpusName you have
specified, contains at least one such character.  Please reinstall synthaCorpus in a new place which doesn't
have that problem or rename the corpus file to overcome this difficulty.  The only characters 
permitted are letters, numbers, period, hyphen, underscore and (in the current working directory
only) colons and slashes of either persuasion.\n\n
CWD: $cwd
corpusName: $corpusName

"
    if ($cwd =~ /[^ :a-z0-9\-_\/\.\\]/i) || ($corpusName =~  /[^a-z0-9\-_\.]/i);  #Allow spaces in CWD but not corpusName


print "\n*** $0 @ARGV\n\n";

$propertyExtractorOptions = " -separatelyReportBigrams=TRUE -ngramObsThresh=10 -ngramHashBits=25 -zScoreCriterion=3 -ngramStringBytes=39";  # (must be the same for base and mimic corpora)

$dependencies = "both";
$forcePropertyExtraction = 0;

for ($a = 1; $a <=$#ARGV; $a++) {
  if ($ARGV[$a] eq "dlnormal") { $dl_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "dlsegs") { $dl_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "dlhisto") { $dl_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "dluniform") { $dl_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "dlgamma") { $dl_model = $ARGV[$a];}
  
  elsif ($ARGV[$a] eq "tnum") { $tr_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "base26") { $tr_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "base26s") { $tr_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "bubble_babble") { $tr_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "simpleWords") { $tr_model = $ARGV[$a];}
  elsif ($ARGV[$a] eq "from_tsv") { $tr_model = $ARGV[$a];}
  # [David Hawking 26 March 2019] Limited Markov value to 5
  elsif ($ARGV[$a] =~ /^markov-[0-5]/) { $tr_model = $ARGV[$a];}
  
  elsif ($ARGV[$a] eq "Piecewise") {$syntyp = "Piecewise";}
  elsif ($ARGV[$a] eq "Linear") {$syntyp = "Linear";}
  elsif ($ARGV[$a] eq "Copy") {$syntyp = "Copy";}
  elsif ($ARGV[$a] eq "Uniform") {$syntyp = "Uniform";}
  elsif ($ARGV[$a] eq "BASELINE") {$syntyp = "BASELINE";}
  
  elsif ($ARGV[$a] eq "ind") {
    $dep_model = "ind";
    $ngram_degree = 1;
    $propertyExtractorOptions .= " -minNgramWords=2 -maxNgramWords=3";  # Extract n-grams but don't generate them.
  }
  elsif ($ARGV[$a] =~ /^ngrams([2-5])$/) {
    $ngram_degree = $1;
    $dep_model = "ngrams$ngram_degree";
    $propertyExtractorOptions .= " -maxNgramWords=$ngram_degree";
  } elsif ($ARGV[$a] eq "bursts") {$dep_model = "bursts";}
  elsif ($ARGV[$a] eq "coocs") {$dep_model = "coocs";}
  elsif ($ARGV[$a] eq "fulldep") {$dep_model = "fulldep";}
  
  elsif ($ARGV[$a] =~ /^-?dependencies=(.*)/) { 
    $dependencies = $1;    
  } elsif ($ARGV[$a] =~ /^-force/) { 
    $forcePropertyExtraction = 1;   
  } else {
    # [David Hawking 26 March 2019] - Provide more explanation about invalid Markov and ngram values
    if ($ARGV[$a] =~ /^markov-/) {
      die "Unacceptable argument to markov.  Max supported value is 5\n";
    } elsif ($ARGV[$a] =~ /^ngrams/) {
      die "Unacceptable argument to ngrams-.  Max supported value is 5\n";
    } else {
      die "Unrecognized argument $ARGV[$a]\n";
    }
  }
}

die "Invalid argument $dependencies for -dependencies\n"
    unless $dependencies =~ /^(both|neither|base|mimic)$/;

die "No document length model specified\n"
    unless defined($dl_model);

die "No term representation model specified\n"
    unless defined($tr_model);

die "No term frequency distribution model specified\n"
    unless defined($syntyp);

die "No term dependence model specified\n"
  unless defined($dep_model);

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$experimentRoot = $ENV{SC_EXPERIMENTS_ROOT};
  
$baseDir = "$experimentRoot/Base";
die "$baseDir must exist and must contain the corpus file to be emulated\n"
    unless -d $baseDir;

foreach $suffix (".trec", ".TREC", ".starc", ".STARC", ".tsv", ".TSV") {
    $cf = "$baseDir/$corpusName$suffix";
    if (-r $cf) {
	$corpusFile = $cf;
	$fileType = $suffix;
	last;   # Give preference to TREC over STARC over TSV.
    }
}
    
die "Corpus file not at either $baseDir/$corpusName.tsv/TSV, $baseDir/$corpusName.trec/TREC, or $baseDir/$corpusName.starc/STARC\n"
	unless defined($corpusFile);


$emuMethod = "${tr_model}_${dl_model}_${dep_model}";

$experimentDir = "${experimentRoot}/Emulation";
if (! -e $experimentDir) {
    die "Can't mkdir $experimentDir\n"
	unless mkdir $experimentDir;
}
mkdir "$experimentDir/$syntyp" unless -d "$experimentDir/$syntyp";  
$emulationDir = "$experimentDir/$syntyp/$emuMethod";
mkdir $emulationDir unless -d $emulationDir;  # A directory in which to do all our work  

$timeLog = "$experimentDir/emulationTimeLog.tsv";

print "
------------------------- Settings Summary ----------------------------
Output directory: $emulationDir
Term frequency model: $syntyp
Document length model: $dl_model
Term representation model: $tr_model
Term dependence model: $dep_model
Corpus name: $corpusName
Output format: $fileType
propertyExtractor options: $propertyExtractorOptions
dependencies: $dependencies
-----------------------------------------------------------------------

    ";

sleep(3);  # Give people an instant to read the settings.


# The executables and scripts we need
$extractor = check_exe("../src/corpusPropertyExtractor.exe");
$generator = check_exe("../src/corpusGenerator.exe");
$doclenCmd = check_exe("../src/doclenPiecewiseModeling.pl");
$compareBaseWithMimic = check_exe("../src/compareBaseWithMimic.pl");


$fit_thresh = 0.02;

if ($syntyp eq "Piecewise") {
    $head_terms = 10; 
    $middle_segs = 10;
} else {
    $head_terms = 0; 
    $middle_segs = 1;
}


if (! -e $timeLog) {
  die "Can't open $timeLog\n"
    unless open ETL, ">$timeLog";
  print ETL "Start date\tName\tDL_model\tTR_model\tTF_model\t$dependencies\t$dep_model\tT_gzip_base\tT_P_Ext_base\tT_DL_base\tT_generate\tT_gzip_emu\tT_P_Ext_emu\tT_DL_emu\tT_total\n";
} else {
  die "Can't append to $timeLog\n"
    unless open ETL, ">>$timeLog";
}

$start_date = localtime;
print ETL "$start_date\t$corpusName\t$dl_model\t$tr_model\t$syntyp\t$dependencies\t$dep_model";

if (-e "$baseDir/${corpusName}_summary.txt"  && !$forcePropertyExtraction) {
  print "\nSkipping base property extraction because $baseDir/${corpusName}_summary.txt exists.\n\n";
} else {
  # Run the corpusPropertyExtractor on the base corpus
  $cmd = "$extractor inputFileName='$corpusFile' outputStem='$baseDir'/$corpusName $propertyExtractorOptions";
  $cmd .= " ignoreDependencies=TRUE"
    if ($dependencies =~/^(neither|mimic)$/);
  print $cmd, "\n";
  
  $start_time = time();
  $code = system($cmd);
  die "Corpus property extraction failed with code $code. Cmd was:\n   $cmd\n"
    if ($code);
  $end_time = time();
  print ETL sprintf("\t%.2f", $end_time - $start_time);
}
  
  
# Extract some stuff from the the summary file  - Format: '<attribute>=<value>'
$sumryfile = "$baseDir/${corpusName}_summary.txt";
die "Can't open base summary file $sumryfile\n"
  unless open S, $sumryfile;
while (<S>) {
  next if /^\s*#/;  # skip comments
  if (/(\S+)=(\S+)/) {
    $base{$1} = $2;
  }
}
close(S);


# Extract values for piecewise model of document lengths
$cmd = "$doclenCmd $corpusName '$baseDir'\n";
print $cmd;
$start_time = time();
$use_gamma_instead = 0;
$rez = `$cmd`;
if ($?) {
  warn "Error: $doclenCmd failed to find good piecewise options\n";
  $use_gamma_instead = 1;
} else {
  if ($rez =~ /(synth_dl_segments=\"[0-9.:,;]+\")/s) {
    $base_dl_segs_option = $1;
    print "DL Piecewise Segs: $base_dl_segs_option\n";
  } else {
    warn "Match failed: $doclenCmd failed to find good piecewise options. Output was: '$rez'\n";
    $use_gamma_instead = 1;
  }
}

if ($use_gamma_instead) {
  $base_dl_segs_option = "-synth_doclen_gamma_shape=$base{doclen_gamma_shape} -synth_doclen_gamma_scale=$base{doclen_gamma_scale}";
  print "DL Piecewise Segs: Using gamma instead: $base_dl_segs_option\n";
}
$end_time = time();
print ETL sprintf("\t%.2f", $end_time - $start_time);


print "


-----------------------------------------------------------------------
        We've done everything needed for the base corpus.
-----------------------------------------------------------------------


";


sleep(3);  # Let that sink in for a moment

####################################################################################################
#                                         Do the emulation                                         #
####################################################################################################

$dlMean = $base{doclen_mean};
$dlStdev = $base{doclen_stdev};

# Read in synthesis options from the Base .tfd file
die "Can't open ${baseDir}/${corpusName}_vocab.tfd\n"
    unless open TFD, "${baseDir}/${corpusName}_vocab.tfd";
while (<TFD>) {
    chomp;
    if (m@-*(.*)=([^#]*)@) {  # Ignore hyphens.  Ignore trailing comments
	$attr = $1;
	$val = $2;
	$options{$attr} = $val;
    }
}
close(TFD);

foreach $k (keys %options) {
    print "\$options{$k} = $options{$k}\n";
}


if ($dl_model eq "dlnormal") {
    $doclenopts = "synth_doc_length=$dlMean synth_doc_length_stdev=$dlStdev";
} elsif ($dl_model eq "dlgamma") {  
    $doclenopts = "synth_dl_gamma_shape=$base{doclen_gamma_shape} synth_dl_gamma_scale=$base{doclen_gamma_scale}";
} elsif ($dl_model eq "dlsegs") {
    $doclenopts = $base_dl_segs_option;
} elsif ($dl_model eq "dlhisto") {
    $doclenopts = "synth_dl_read_histo='${baseDir}'/${corpusName}_docLenHist.tsv";
}

$termrepopts = "-synth_term_repn_method=$tr_model";
if ($tr_model eq "from_tsv" || $tr_model eq "base26s" || $syntyp eq "Copy") {
    $termrepopts .= " -synth_input_vocab='${baseDir}'/${corpusName}_vocab_by_freq.tsv";
} elsif ($tr_model =~ /^markov-/) {
    $termrepopts .= " -synth_input_vocab='${baseDir}'/${corpusName}_vocab_by_freq.tsv ";
    $termrepopts .= $markov_defaults;
}


$generator_cmd = "$generator $doclenopts $termrepopts synth_postings=$options{synth_postings} synth_vocab_size=$options{synth_vocab_size} file_synth_docs='$emulationDir'/$corpusName$fileType";


# ...................... set up options for term dependence .....................
if (($dep_model eq "fulldep" || $dep_model =~ /ngrams/) && (-r "${baseDir}/${corpusName}_ngrams.termids")) {
    print "\nGoing to attempt ${ngram_degree}-gram emulation using '${baseDir}'/${corpusName}_ngrams.termids\n\n";
    $generator_cmd .= " -synth_input_ngrams='${baseDir}'/${corpusName}_ngrams.termids";
}

# The following two are in advance of implementation ....
if (($dep_model eq "fulldep" || $dep_model eq "bursts") && (-r "${baseDir}/${corpusName}_bursts.termids")) {
    print "\nGoing to attempt n-gram emulation using '${baseDir}'/${corpusName}_bursts.termids\n\n";
    $generator_cmd .= " -synth_input_ngrams='${baseDir}'/${corpusName}_bursts.termids";
}

if (($dep_model eq "fulldep" || $dep_model eq "coocs") && (-r "${baseDir}/${corpusName}_coocs.termids")) {
    print "\nGoing to attempt n-gram emulation using '${baseDir}'/${corpusName}_coocs.termids\n\n";
    $generator_cmd .= " -synth_input_ngrams='${baseDir}'/${corpusName}_coocs.termids";
}
# ...............................................................................

undef %mimic;


#..................................................................................................#
#        generate with those characteristics in subdirectory $syntyp/$emuMethod                  #
#..................................................................................................#

# ------------------- Generating the corpus --------------------------------
$cmd = $generator_cmd;
if ($syntyp eq "Piecewise") {
  $cmd .= " -zipf_tail_perc=$options{zipf_tail_perc} -head_term_percentages=$options{head_term_percentages} -zipf_middle_pieces=$options{zipf_middle_pieces}";
} elsif ($syntyp eq "Linear") {
  $cmd .= " -zipf_tail_perc=$options{zipf_tail_perc} -zipf_alpha=$options{zipf_alpha}";
} elsif ($syntyp eq "Uniform") {
  $cmd .= " -zipf_alpha=0";
} elsif ($syntyp eq "Copy") {
  $cmd .= " -tfd_use_base_vocab=TRUE";
} elsif ($syntyp eq "BASELINE") {
  $cmd .= " -tfd_use_base_vocab=TRUE -use_baseline_lang_model=TRUE";
}


die "Can't open $emulationDir/${corpusName}_gencorp.cmd\n" 
    unless open QC, ">$emulationDir/${corpusName}_gencorp.cmd";
print QC $cmd, "\n";
close QC;

$cmd .=  " > '$emulationDir'/${corpusName}_gencorp.log\n";

print $cmd;
$start_time = time();
$code = system($cmd);
die "Corpus generation failed with code $code.\nCmd was $cmd\n" if $code;
$end_time = time();
print ETL sprintf("\t%.2f", $end_time - $start_time);

print "

--------------------------- $syntyp --------------------------

Emulation finished for $syntyp/$emuMethod.  

";


print "-----------------------------------------------------------------


";


$sumryFile = "$emulationDir/${corpusName}_summary.txt";
print "We're now going to extract properties and compare with the base corpus
using the compareBaseWithMimic.pl script.   To ensure that it doesn't rely on
properties files lying around from a previous emulation, we must remove the
summary file $sumryFile.

";

unlink $sumryFile;




# ============================ All analysis and comparison from here on ==============================

$maxNgramWords = 3;
if ($ngram_degree > $maxNgramWords) {$maxNgramWords = $ngram_degree;}

$cmd = "$compareBaseWithMimic $corpusName '$emulationDir' 2 $maxNgramWords";
print $cmd, "\n";
$code = system($cmd);
die "Command $cmd failed with code $?\n" if $?;


$end_time = time();
$total_time = sprintf("%.2f sec.", $end_time - $overall_start_time);

print ETL " $total_time\n";  # Close the line
close(ETL);      # Close the file

print "\nSuccessful exit from $0.   Overall elapsed time: ", $total_time, "\n";
exit 0;





#----------------------------------------------------------------------

sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}



sub get_compression_ratio {
    my $file = shift;
    my $dir = shift;   
    my $corpusName = shift;
    my $tmpFile = "$dir/$corpusName.gz";  # Choose a path which reduces risk of clash during
                                          # multiple simultaneous runs.
    my $cmd = "gzip -c $file > $tmpFile";
    my $code = `$cmd`;
    if ($code) {
	warn "Command $cmd failed with code $code\n";
	return "N/A";
    }

#    my $gzo = `gzip -l $tmpFile`;  # Produced wrong answers for a multi-gigabyte file
#    if ($?) {
#	warn "gzip -l failed with code $?\n";
#	unlink "$tmpFile";
#	return "N/A";
#    }
# 
    #    $gzo =~ /([0-9]+)\s+([0-9]+)\s+([0-9.]+%)/s;

    my $uncoSize = -s $file;
    my $coSize = -s $tmpFile;
    
    $ratio = sprintf("%.3f", $uncoSize / $coSize);
    
    print "


$tmpFile: compression ratio (uncompressed size : compressed size) is $ratio



";
    unlink $tmpFile;
    return $ratio;
}
