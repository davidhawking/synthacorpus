#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.

use File::Copy;
# Scripts are now called via quotemeta($^X) in case the perl we want is not in /usr/bin/
$perl = $^X;

use Cwd;
$cwd = getcwd;
if ($cwd =~ m@/cygdrive@) {
    $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

die "\nUnfortunately, SynthaCorpus runs into trouble when there is punctuation
in the directory path.  Note that the directory path may now contain spaces.
Your current working directory contains at least one problem character.  Please reinstall 
synthaCorpus in a new place which doesn't have that problem to overcome this difficulty.  
The only characters permitted are letters, numbers, period, hyphen, underscore, space, 
colons, and slashes of either persuasion.\n\n
CWD: $cwd
corpusName: $corpusName

"
    if ($cwd =~ /[^ :a-z0-9\-_\/\.\\]/i) || ($corpusName =~  /[^a-z0-9\-_\.]/i);  #Allow spaces in CWD but not corpusName



$|++;

if ($#ARGV >= 0) {
  for ($a = 0; $a <= $#ARGV; $a++) {  
    if ($ARGV[$a] =~/thorough/i) {
      $thorough = "yes";
    } else {
      die "Error: $0: Unrecognized argument $ARVG[$a]\n";
    }
  }
}

$arch = `arch`;
if ($? == 0) {
  print "  -------- Architecture is $arch\n";
  if ($arch =~ /^arm/i && !defined($ENV{CPU_IS_ARM})) {
    print "
You seem to be running on an ARM CPU.  Please set the environment variable \$CPU_IS_ARM
and run this script again.   Use 'SETENV CPU_IS_ARM=1' on command line or in .bashrc etc.

";    
    exit(0);
  }
}

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  If the
nominated directory doesn't exist, this script will attempt to create it.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$exptRoot = $ENV{SC_EXPERIMENTS_ROOT};
  
$plotter = `which gnuplot`;

die "A number of SynthaCorpus scripts rely on gnuplot to generate plots. You need to 
install that e.g. from https://sourceforge.net/projects/gnuplot/files/gnuplot/ and include
the relevant directory in your $PATH.  You'll also need a PDF viewer, either Adobe Acrobat 
Reader, Apple Preview or Okular to view the results.

" unless $plotter =~ /gnuplot/;

$plotter = "gnuplot";

mkdir $exptRoot
	unless -e $exptRoot;

mkdir "$exptRoot/Base" unless -e "$exptRoot/Base";

run("make cleaner");
run("make");

#$converter = check_exe("convertPGtoSTARC.exe");
$converter = check_exe("convertPGtoTREC.exe");
#$detrec = check_exe("../detrec/detrec.exe");
$checker = check_exe("checkTRECFile.exe");
$queryGen = check_exe("queryGenerator.exe");
$queryLogEmulator = check_exe("queryLogEmulator.exe");
$sampler = check_exe("samplingExperiments.pl");
$scaler = check_exe("scaleUpASample.pl");
$caesar = check_exe("Caesar.exe");
$nomenclator = check_exe("Nomenclator.exe");
$markovGen = check_exe("MarkovGenerator.exe");

$step = 1;
$emulations = 0;

print "\n\n\nStep $step:   Removing PG.* files from $exptRoot/Base\n";  $step++;
run("/bin/rm -f $exptRoot/Base/PG*");  # Query generation can be affected by presence of files from an earlier run

if (-e "../projectGutenberg/PG.trec") {
  print "\n\n\nStep $step:   Found PG.trec in distribution.  Copying it to $exptRoot/Base\n"; $step++;
  copy("../projectGutenberg/PG.trec", "$exptRoot/Base/PG.trec");
  die "Copy of PG.trec failed\n" if $?;
} else {
  print "\n\n\nStep $step:   PG.trec not found. Convert Project Gutenberg (PG) files into a raw TREC file\n";  $step++;
  run("$converter ../ProjectGutenberg/*.txt > $exptRoot/Base/PG.raw");

  print "\n\n\nStep $step:   Attempt to build detrec.  If this fails you may need to configure/make in ../detrec/pcre \n";  $step++;
  run("make detrec");
  
  print "\n\n\nStep $step:   Run detrec.exe to remove excess markup\n";  $step++;
  run("$detrec $exptRoot/Base/PG.trec $exptRoot/Base/PG.raw");
}


print "\n\n\nStep $step:   Check the format of the TREC file\n";  $step++;
run("$checker '$exptRoot/Base/PG.trec'");



# If specified, run a series of PG emulations
if (defined($thorough)) {
    print "\n\n\nStep $step:   Running a thorough series of emulations ...\n";  $step++;
    run("$perl ./emulateARealCorpus.pl PG Copy from_tsv dlhisto ngrams2");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG BASELINE from_tsv dlhisto ngrams2");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Uniform from_tsv dlhisto ngrams2");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Linear base26 dlhisto ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Linear base26 dlhisto ind -dependencies=neither");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Linear base26 dlhisto ind -dependencies=base");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Linear base26 dlhisto ind -dependencies=mimic");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Linear base26 dlhisto ind -dependencies=both");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise tnum dlsegs ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise base26 dlhisto ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise base26 dlnormal ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise markov-0 dlhisto ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise markov-0e dlhisto ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise markov-4 dlhisto ind");  $emulations++;
    run("$perl ./emulateARealCorpus.pl PG Piecewise markov-4e dlhisto ngrams3");  $emulations++;
} else {
    print "Skipping the thorough tests ...\n";
    sleep 3;
}

print "\n\n\nStep $step:   Always do at least one emulation of PG\n";  $step++;
run("$perl ./emulateARealCorpus.pl PG Piecewise markov-4e dlhisto ngrams3");  $emulations++;

print "\n\n\nStep $step:    Run the Azzopardi-et-al inspired query generator on both the Base corpus and the always-done emulation of it\n"; $step++;
run("$queryGen corpusFileName='${exptRoot}'/Base/PG.trec propertiesStem='${exptRoot}'/Base/PG  -numQueries=1000");
run("$queryGen corpusFileName='${exptRoot}'/Emulation/Piecewise/markov-4e_dlhisto_ngrams3/PG.trec propertiesStem='${exptRoot}'/Emulation/Piecewise/markov-4e_dlhisto_ngrams3/PG  -numQueries=1000");

print "\n\n\nStep $step:    Now use the Azzopardi-et-al queries for Base as though they were a query log and emulate it.\n"; $step++;
copy_first_column("${exptRoot}/Base/PG.q", "${exptRoot}/Base/PG.qlog");
run("$queryLogEmulator baseStem='${exptRoot}'/Base/PG emuStem='${exptRoot}'/Emulation/Piecewise/markov-4e_dlhisto_ngrams3/PG");

print "\n\n\nStep $step:     Run the corpus sampling script to build up a growth model\n";  $step++;
run("$sampler PG");

mkdir "$exptRoot/Scalingup" unless -e "$exptRoot/Scalingup";
die "Mkdir $exptRoot/Scalingup failed\n" if $?;
mkdir "$exptRoot/Scalingup/CS-1-100" unless -e "$exptRoot/Scalingup/CS-1-100";
die "Mkdir $exptRoot/Scalingup/CS-1-100 failed\n" if $?;

print "\n\nRemoving PG.* files from $exptRoot/Scalingup/CS-1-100\n";
run("/bin/rm -f '$exptRoot'/Scalingup/CS-1-100/PG*");  # Query generation can be affected by presence of files from an earlier run

print "\n\n\nStep $step:     Use the growth model to scale up a small sample of PG by two orders of magnitude.\n";  $step++;
run("$scaler PG '$exptRoot'/Sampling/Sample/scaling_model_for_PG_1.txt 100 CS-1-100 '$exptRoot'/Sampling/Sample/1% Linear markov-4e dlnormal ind -compareWithBase");
run("$queryGen corpusFileName='${exptRoot}'/Scalingup/CS-1-100/PG.trec propertiesStem='${exptRoot}'/Scalingup/CS-1-100/PG  -numQueries=1000");

mkdir "$exptRoot/Emulation/Caesar13" unless -e "$exptRoot/Emulation/Caesar13";
die "Mkdir $exptRoot/Emulation/Caesar13 failed\n" if $?;
print "\n\n\nStep $step:     Check out the Caesar encrypter.\n";  $step++;
run("$caesar '$exptRoot'/Base/PG.trec 13 '$exptRoot'/Emulation/Caesar13/PG.trec");

mkdir "$exptRoot/Emulation/Nomenclator" unless -e "$exptRoot/Emulation/Nomenclator";
die "Mkdir $exptRoot/Emulation/Nomnclator failed\n" if $?;
print "\n\n\nStep $step:     Check out the Nomenclator encrypter.\n";  $step++;
run("$nomenclator '$exptRoot'/Base/PG.trec '$exptRoot'/Emulation/Piecewise/markov-4e_dlhisto_ngrams3/PG_vocab.tsv '$exptRoot'/Emulation/Nomenclator/PG.trec '$exptRoot'/Emulation/Nomenclator/PG_mapping.tsv");

mkdir "$exptRoot/Emulation/WBMarkov1" unless -e "$exptRoot/Emulation/WBMarkov1";
die "Mkdir $exptRoot/Emulation/WBMarkov1 failed\n" if $?;
print "\n\n\nStep $step:     Check out the Markov Generator.\n";  $step++;
run("$markovGen inFile='$exptRoot'/Base/PG.trec outFile='$exptRoot'/Emulation/WBMarkov1/PG.trec");



print "

    Brilliant!  The exerciser script has finished normally.  $emulations emulations were run. :-)

";  




    exit(0);

# ---------------------------------------------



sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}




sub run {
    my $cmd = shift;
    print $cmd, "\n";
    my $code = system($cmd);
    die "Command $cmd failed with code $code\n"
	if ($code);
}


sub copy_first_column {
    $src = shift;
    $dest = shift;

    # Copy first column (i.e. up to first TAB) of $src to  $dest
    # Don't copy if the field is empty
    # Copy the whole line if there's no TAB

    die "Can't read $src\n"
	unless open SRC, $src;
    die "Can't write to $dest\n"
	unless open DEST, ">$dest";

    while (<SRC>) {
	if (/([^\t]+)(\t|\r|\m)/) {
	    $tocopy = $1;
	    print DEST $tocopy, "\n"
		unless length($tocopy) <= 0;
	}
    }
    close(SRC);
    close(DEST);
}
