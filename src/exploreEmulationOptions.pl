#! /usr/bin/perl - w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

$perl = $^X;
$perl =~ s@\\@/@g;


# Run the emulateARealCorpus.pl script with a combination of options for a specified corpus.

die "Usage: perl $0 <corpus_name>\n"
  unless $#ARGV == 0;

$corpusName = $ARGV[0];

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};


# Test if we are running on an ARM processor.  First check environment, then run arch
if (defined($ENV{CPU_IS_ARM})) {
  $runningOnSmallSystem = 1;
} else {
  $tmp = `arch`;
  if ($tmp =~ /^arm/i) {
    $runningOnSmallSystem = 1;
  } else {
    $runningOnSmallSystem = 0;
  }
}

if ($runningOnSmallSystem) {
  print "\nHave detected that we are running on a small system. Tests using large 
corpora or memory-intensive modes may fail.\n\n";
  sleep(2);
}

die "Emulation 1 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Copy from_tsv dlhisto ind");

die "Emulation 2 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Linear tnum dlsegs ngrams2");

die "Emulation 3 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise base26 dlnormal ngrams2");

die "Emulation 4 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise base26s dlhisto ngrams3");

die "Emulation 5 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise simpleWords dlhisto ngrams3");

die "Emulation 6 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise markov-3e dlhisto ngrams4");

die "Emulation 7 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise markov-1 dlhisto ngrams4");

die "Emulation 8 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise markov-3e dlhisto ngrams5");

die "Emulation 9 failed\n"
  if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise markov-3e dlhisto ngrams5");

if ($runningOnSmallSystem) {
  print "\nSkipping test with markov-5e because we are running on a small system\n\n";
} else {
  die "Emulation 10 failed\n"
    if system("$perl ./emulateARealCorpus.pl $corpusName Piecewise markov-5e dlhisto ngrams5");
}


print "

Times taken by each of the phases of each emulation should be logged in:

    $expRoot/Emulation/emulationTimeLog.tsv

";

exit(0);
