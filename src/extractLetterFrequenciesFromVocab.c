// Copyright (c) David Hawking. All rights reserved.
// Licensed under the MIT license.

// Read a vocab file and produce all sorts of letter frequency histogram information.
// Ignore words containing characters outside the 'a' - 'z' alphabet.
//
// Assume word is in column 1, and raw frequency in column 2.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "definitions.h"
#include "utils/general.h"

static int dblCmp(const void *ip, const void *jp) {
  double i = *(double *)ip, j = *(double *)jp;
  if (i < j) return 1;
  if (i > j) return -1;
  return 0;
}


static void generate_plots(u_char *filePathRoot) {
  FILE *cmds;
  size_t fpRootLen = strlen(filePathRoot);
  u_char plotFileName[1000], pdfFileName[1000], datFileName[1000],
    cmd[1000];
  int error;
  if (fpRootLen > 950) {
    printf("Error: File path name too long (> 950): %s\n", filePathRoot);
    exit(1);
  }
  strncpy(plotFileName, filePathRoot, fpRootLen + 1);
  strncpy(pdfFileName, filePathRoot, fpRootLen + 1);
  strncpy(datFileName, filePathRoot, fpRootLen + 1);
  strcpy(plotFileName + fpRootLen, "letters_plot.cmds");
  strcpy(pdfFileName + fpRootLen, "letters.pdf");
  strcpy(datFileName + fpRootLen, "letters_ranked.dat");   // Max length of suffix is 18, allow 50.

  printf("Generate_plots: %s\n%s\n%s\n", plotFileName, pdfFileName, datFileName);
  
  cmds = fopen(plotFileName, "wb");
  
  fprintf(cmds, "set terminal pdf\n"
	  "set output '%s'\n"
	  "set size ratio 1\n"
	  "set logscale xy\n"
	  "set xlabel \"Log(rank)\"\n"
	  "set ylabel \"Log(probability)\"\n"
	  "set style line 7 linewidth 3\n"
	  "set pointsize 3\n"
	  "plot [1:30] '%s' using 1:7 title 'corpus' pt 7 ps 0.4, '%s' using 1:6 title \"vocab\" pt 7 ps 0.4\n",
	  pdfFileName, datFileName, datFileName);
  fclose(cmds);
  strcpy(cmd, "/usr/local/bin/gnuplot ");
  strcat(cmd, plotFileName);
  error = system(cmd);
  if (error) {
    printf("Warning: Command '%s' failed.\n", cmd);
  } else {
    printf("Plot commands in: %s\n", plotFileName);
    printf("PDF graph in: %s\n\n", pdfFileName);
  }
}



int main (int argc, char**argv) {
  byte *file_in_mem = NULL, **lines = NULL;
  int l, i, line_count;
  CROSS_PLATFORM_FILE_HANDLE H;
  HANDLE MH;
  size_t sighs, len, pathRootLen;
  BOOL nonAlphabetic;
  u_char *p, *vfile_name, *ofile_name;
  long long freq;
  double sumFirstV = 0, sumFirstC = 0, sumLastV = 0, sumLastC = 0, sumV = 0, sumC = 0, 
    histoFirstLetterV[26] = {0},
    histoFirstLetterC[26] = {0},
    histoLastLetterV[26] = {0},
    histoLastLetterC[26] = {0},
    histoLetterV[26] = {0},
    histoLetterC[26] = {0}
    ;

  FILE *out = stdout;

  if (argc != 3) {
    printf("Usage: %s <corpus> <directory>\n"
	   "  - reads from directory/corpus_vocab.tsv\n"
	   "  - writes directory/corpus_letters_raw.dat, and\n"
	   "    directory/corpus_letters_ranked.dat\n\n", argv[0]);
    exit(1);
  }

  pathRootLen = strlen(argv[1]) + strlen(argv[2]) + 2;  // Including slash and underscore
  
  vfile_name = cmalloc(pathRootLen + 100, "V File Name", 0);
  ofile_name = cmalloc(pathRootLen + 100, "O File Name", 0);
  
  strcpy(vfile_name, argv[2]);
  strcat(vfile_name, "/");
  strcat(vfile_name, argv[1]);
  strcat(vfile_name, "_vocab.tsv");

  if (!exists(vfile_name, "")) {
    printf("Error: file %s doesn't exist\n", vfile_name);
  }

    
  if (1) printf("Extracting letter frequencies from words in  %s\n", vfile_name);

  lines = load_all_lines_from_textfile(vfile_name, &line_count,
                                           &H, &MH, &file_in_mem, &sighs);

  for (l = 0; l < line_count; l++) {
    nonAlphabetic = FALSE;
    p = lines[l];
    while (*p && !isspace(*p)) {
      if (*p < 'a'  || *p > 'z') {
	nonAlphabetic = TRUE;
	break;   // ---->
      }
      p++;
    }
    if (nonAlphabetic) continue;  //-------->  Ignore words with non-alphabetics
    len = p - lines[l];
    freq = strtol(p, NULL, 10);

    // We have an all-letter word -- accumulate some statistics.
    // Accumulate raw data, we'll sort later to get the ranked versions
    p = lines[l];
    i = *p - 'a';
    histoFirstLetterV[i]++;  sumFirstV++;
    histoFirstLetterC[i] += freq;  sumFirstC += freq;
    while (*p && !isspace(*p)) {
      histoLetterV[i]++;   sumV++;
      histoLetterC[i] += freq;   sumC += freq;
      p++;
      i = *p - 'a';
    } 

    p = lines[l];
    i = p[len -1] - 'a';
    histoLastLetterV[i]++;   sumLastV++;
    histoLastLetterC[i] += freq;   sumLastC += freq;
  }
  
  unload_all_lines_from_textfile(H, MH, &lines, &file_in_mem, sighs);

  // Convert frequencies to probabilities
  for (i = 0; i < 26; i++) {
    histoFirstLetterV[i] /= sumFirstV;
    histoFirstLetterC[i] /= sumFirstC;
    histoLetterV[i] /= sumV;
    histoLetterC[i] /= sumC;
    histoLastLetterV[i] /= sumLastV;
    histoLastLetterC[i] /= sumLastC; 
  }
 
  // Now write the data files
  strncpy(ofile_name, vfile_name, pathRootLen);  // up to the underscore
  strcpy(ofile_name + pathRootLen, "letters_raw.dat");
  out = fopen(ofile_name, "wb");
  if (out == NULL) {
    printf("Error: can't write to %s\n", ofile_name);
    exit(1);
  }
  printf("Output in %s\n", ofile_name);
  fprintf(out, "\n#Raw statistics from %s.\n", vfile_name);

  fprintf(out, "#Letter   FirstVocab FirstCorp  LastVocab  LastCorp  AnyVocab   AnyCorp\n");
  for (i = 0; i < 26; i++) {
    fprintf(out, "%5c      %9.7f %9.7f %9.7f %9.7f %9.7f %9.7f\n",
	    'a' + i, histoFirstLetterV[i], histoFirstLetterC[i], 
	    histoLastLetterV[i], histoLastLetterC[i],
	    histoLetterV[i], histoLetterC[i]);
  }

  fclose(out);
  
  strcpy(ofile_name + pathRootLen, "letters_ranked.dat");
  out = fopen(ofile_name, "wb");
  if (out == NULL) {
    printf("Error: can't write to %s\n", ofile_name);
    exit(1);
  }
  printf("Output in %s\n", ofile_name);

  
  // Sort the rows by decreasing frequency.   We won't know what the letters were
  qsort(histoFirstLetterV, 26, sizeof(double), dblCmp);
  qsort(histoFirstLetterC, 26, sizeof(double), dblCmp);
  qsort(histoLastLetterV, 26, sizeof(double), dblCmp);
  qsort(histoLastLetterC, 26, sizeof(double), dblCmp);
  qsort(histoLetterV, 26, sizeof(double), dblCmp);
  qsort(histoLetterC, 26, sizeof(double), dblCmp);
  
  
  fprintf(out, "\n#Ranked statistics from %s.  The i-th row means the i-th most frequent letter\n",
	  ofile_name);

  fprintf(out, "#Rank     FirstVocab FirstCorp LastVocab  LastCorp  AnyVocab   AnyCorp\n");
  for (i = 0; i < 26; i++) {
    fprintf(out, "%5d      %9.7f %9.7f %9.7f %9.7f %9.7f %9.7f\n",
	    i + 1, histoFirstLetterV[i], histoFirstLetterC[i], 
	    histoLastLetterV[i], histoLastLetterC[i],
	    histoLetterV[i], histoLetterC[i]);
  }

  fclose(out);

  ofile_name[pathRootLen] = 0;
  
  generate_plots(ofile_name);
  
}
