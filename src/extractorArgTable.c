// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license.

// Table of command line argument definitions for the corpus
// property extractor.  The functions in argParser.c operate
// on this array.

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "utils/dynamicArrays.h"
#include "corpusPropertyExtractor.h"
#include "utils/argParser.h"

arg_t args[] = {
  { "inputFileName", ASTRING, (void *)&(params.inputFileName), "This is the file of text containing the corpus whose contents are to be extracted.  Supported formats: TSV (.tsv), simplified TREC (.trec) or simple text archive (.starc)."},
  { "outputStem", ASTRING, (void *)&(params.outputStem), "The names of all the files containing extracted properties will share this prefix."},
  { "headTerms", AINT, (void *)&(params.headTerms), "The number of terms which are explicitly modelled in the term frequency distribution (TFD) model."},
  { "piecewiseSegments", AINT, (void *)&(params.piecewiseSegments), "The number of linear segments in the middle section of the TFD model."},
  { "minNgramWords", AINT, (void *)&(params.minNgramWords), "Only record Ngrams with at least this many words."},
  { "maxNgramWords", AINT, (void *)&(params.maxNgramWords), "Only record Ngrams with at most this many words."},
  { "zScoreCriterion", AFLOAT, (void *)&(params.zScoreCriterion), "If greater than 0.0, only 'significant' Ngrams - those whose Zscore exceeds the criterion - will be written."},
  { "ignoreDependencies", ABOOL, (void *)&(params.ignoreDependencies), "If TRUE time-consuming extraction of word compounds and repetitions will be skipped and those files not written."},  
  { "separatelyReportBigrams", ABOOL, (void *)&(params.separatelyReportBigrams), "If TRUE the _bigrams.* files will be written (provided that minNgramWords is 2)."},
  { "ngramObsThresh", AINT, (void *)&(params.ngramObsThresh), "The minimum number of occurrences required to bother recording an ngram."},
  { "ngramOnTheFlyFiltering", ABOOL, (void *)&(params.ngramOnTheFlyFiltering), "If TRUE low frequency ngrams will be dropped from the hash table during scanning."},
  { "ngramPostFilterFraction", AFLOAT, (void *)&(params.ngramPostFilterFraction), "Used to control on-the-fly low frequency filtering of the ngrams hash table."},
  
  { "ngramHashBits", AINT, (void *)&(params.ngramHashBits), "The initial size of the ngrams hash table is 2 to the power of this."},
  { "ngramStringBytes", AINT, (void *)&(params.ngramStringBytes), "Max string length for storing ngrams. Longer ngrams are ignored. With ngramHashBits, determines mem size of ngrams hashtable."},
  { "hashCollisionHandling", AINT, (void *)&(params.hashCollisionHandling), "0 - relatively prime rehash, 1 - linear probing."},
  { "maxPostings", AINTLL, (void *)&(params.maxPostings), "Stop extracting after this number of word instances have been extracted."},
  { "", AEOL, NULL, "" }
  };


void initialiseParams(params_t *params) {
  params->inputFileName = NULL;
  params->outputStem = NULL;
  params->headTerms = 10;
  params->piecewiseSegments = 10;
  params->ignoreDependencies = FALSE;
  params->minNgramWords = 2;
  params->maxNgramWords = 4;
  params->ngramOnTheFlyFiltering = TRUE;
  params->ngramPostFilterFraction = 0.5;
  params->zScoreCriterion = 1.6449;  
  params->ngramObsThresh = 10;
  params->ngramHashBits = 27;   // Comfortable on a machine with 16GB, may work on 8GB.
  params->ngramStringBytes = 39;
  params->hashCollisionHandling = 0;
  params->maxPostings = 0X7FFFFFFFFFFFFFFF; // about 8 quadrillion, i.e. no limit
}


void sanitiseParams(params_t *params) {
  // Make sure the Ngram length parameters make some kind of sense
  if (params->minNgramWords < 1) params->minNgramWords = 1;
  if (params->maxNgramWords < 1) params->maxNgramWords = 1;
  if (params->maxNgramWords > MAX_NGRAM_WORDS) params->maxNgramWords = MAX_NGRAM_WORDS;
  if (params->minNgramWords > params->maxNgramWords) params->minNgramWords = params->maxNgramWords;
  if (params->ngramHashBits < 16) params->ngramHashBits = 16;
  else if (params->ngramHashBits > 40)  params->ngramHashBits = 40;  // Even this is ridiculous - a trillion entries???
  if (params->hashCollisionHandling < 0) params->hashCollisionHandling = 0;  // Relatively prime rehash
  else if (params->hashCollisionHandling > 1) params->hashCollisionHandling = 1;  // Linear probing.
  dahash_set_probing_method(params->hashCollisionHandling);
  if (params->maxPostings < 0) params->maxPostings = 1;  
}




