// Copyright (c) David Hawking, 2019. All rights reserved.
// Licensed under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "utils/dynamicArrays.h"
#include "imported/GammaFitting/tminka.h"

void fitGammaFromHisto(dyna_t *histo, int N, double *gshape, double *gscale) {
  // Take a histogram and calculate shape and scale parameters of a
  // gamma function of best fit.
  // The domain of the function represented by the histogram is 0 .. N-1.
  // The ith element of histo contains the number of occurrences of i
  // in the data.
  // Fitting is done using Tom Minka's method:
  //   "Estimating a Gamma distribution" by T. Minka, 2002.

  double sum = 0.0, sumLogs = 0.0, count = 0, dh, di, logi,
    meanLog, logMean;
  int i, rslt;
  long long *lHistoEntry;

  for (i = 0; i < N; i++) {
    lHistoEntry = (long long *)dyna_get(histo, (long long)i, DYNA_DOUBLE);
    if (*lHistoEntry > 0) {
      di = (double)i;
      dh = (double)*lHistoEntry;
      logi = log(di);
      sum += di * dh;
      sumLogs += logi * dh;
      count += dh;
    }
  }

  meanLog = sumLogs / count;
  logMean = log(sum / count);

  rslt = fromLogMeanAndMeanLog(logMean, meanLog, gshape, gscale);
  if (rslt < 0) {
    printf("Gamma fitting failed.  Taking exit.\n");
    exit(1);
  }

}
