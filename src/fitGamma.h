// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

void fitGammaFromHisto(dyna_t *histo, int N, double *gshape, double *gscale);
