This directory contains code to fit a gamma distribution to a set of x,y data by estimating shape and scale.

The method used is that of T. Minka 2002 "Estimating a Gamma distribution".  I have translated his
MIT-licensed C# code into C in tminka.c.   His code relied on digamma() and trigamma() functions in
the .NET math library.  These are not available in the C math library, so I found open source alternatives
on the web:

digamma.c	Radford M. Neal	https://github.com/wbuntine/libstb/blob/master/lib/digamma.c     Very permissive licence.

trigamma.c	John Burkardt           http://people.math.sc.edu/Burkardt/c_src/asa121/asa121.[ch]          GNU LGPL licence

The fromLogMeanAndMeanLog() function in tminka.c is called by own function fitGammaFromHisto() in ../../fitGamma.c

David Hawking
david.hawking@acm.org
22 Dec 2019
