// The function below is translated from Tom Minka's Infer.NET code at
// https://github.com/dotnet/infer/blob/master/src/Runtime/Distributions/Gamma.cs
//  (function FromLogMeanAndMeanLog(double logMean, double meanLog) at around line 290)
// It is used under the version of the MIT license at https://github.com/dotnet/infer/blob/master/LICENSE.txt

/// Estimates shape and scale of a Gamma distribution with the given mean and mean logarithm.
/// This function is equivalent to maximum-likelihood estimation of a Gamma distribution
/// from data given by sufficient statistics.
/// This function is significantly slower than the other constructors since it
/// involves nonlinear optimization. The algorithm is a generalized Newton iteration, 
/// described in "Estimating a Gamma distribution" by T. Minka, 2002.

// digamma() and trigamma() functions are in the .NET library but not in math.h
// (https://fresh2refresh.com/c-programming/c-function/c-math-h-library-functions/)
// Accordingly open source implementations have been located


#include <stdio.h>
#include <math.h>
#include "digamma.h"
#include "trigamma_burkardt.h"

int fromLogMeanAndMeanLog(double logMean, double meanLog, double *gshape, double *gscale) {
  // logMean = log(shape)-log(rate)
  // meanLog = Psi(shape)-log(rate)
  // delta = log(shape)-Psi(shape)
  double delta = logMean - meanLog;
  int iFault;
  
  if (delta <= 2e-16) {
    // return Gamma.PointMass(Math.Exp(logMean));
  }
  double shape = 0.5 / delta;
  int iter;
  
  for (iter = 0; iter < 100; iter++)
    {
      double oldShape = shape;
      double g = log(shape) - delta - digamma(shape);
      shape /= 1 + g / (1 - shape * trigamma(shape, &iFault));
      if (iFault != 0) {
	printf("Error in estimateGammaShapeAndScale().  trigamma fault\n");
	return -1;
      }
	
      if (fabs(shape - oldShape) < 1e-8) break;
    }
  if (isnan(shape)) {
    printf("Error in estimateGammaShapeAndScale().  Shape is not a number\n");
    return -1;
  }

  if (shape < 2e-16) {
    printf("Error in estimateGammaShapeAndScale().  Shape %.8g is too close to zero\n", shape);
    return -1;

  }
  *gshape = shape;
  *gscale = exp(logMean) / shape;   // Tom computed rate (see below) -- scale is 1 / rate
  // Gamma result = Gamma.FromShapeAndRate(shape, shape / Math.Exp(logMean));
  return 0;  // Success
}
