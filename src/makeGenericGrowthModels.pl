#! /usr/bin/perl -w
#
# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Corpus-specific growth models are found in files such as $expRoot/Sampling/Sample/scaling_model_for_ap_1.txt
# where ap is the name of the corpus and 1 is the percentage of the corpus from which the scaling model was
# derived.  The purpose of this script is to average the coefficients across multiple corpora into a single
# generic model for the given percentage.  I'll put the generic models in $expRoot/Sampling/GenericGrowth.
#
# I don't think we have to worry about temporal subsets since we only have one time-ordered corpus.


die "Warning:  This script and the scaling up mechanism needs considerable work.

   The current growth models include constants which, to permit generic scaling, should instead 
   be read from the corpus being scaled up.  Sorry.\n";


die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
die "Experiment root $expRoot doesn't exist\n" unless -d $expRoot;
$genericDir = "$expRoot/GenericGrowth";
mkdir $genericDir unless -d $genericDir;
$sampleDir = "$expRoot/Sampling/Sample";
die "Sample directory $sampleDir doesn't exist\n" unless -d $sampleDir;

@percentages = ("1", "10", "50");   # These are the percentages for which samplingExperiments.pl created growth models.

foreach $perc (@percentages) {
  undef %attrs;
  undef $corpora;
  $corpusCount = 0;
  foreach $growthModel (glob "$sampleDir/scaling_model_for_*_$perc.txt") {
    print "Growth model: $growthModel\n";
    ($corpus) = $growthModel =~ /for_([^_]+)_/;

    die "Can't open growth model file $growthModel\n" unless open SMF, $growthModel;   
    $corpora .= "$corpus ";
    $corpusCount++;
    while (<SMF>) {
    next if (/^\s*#/);   # Skip comment lines
    next if (/^\s*$/);   # Skip blank lines
    chomp;
    s/#.*//;  # Strip trailing comments
    #print "+ $_\n";
    if (m@(.*?)=(.*)@) {
      $attr = $1; 
      $rawval = $2;
      # There are three types of model: power law, linear and constant
      # Pattern match to distinguish them and set $val accordingly.
      if ($rawval =~ m@([\-0-9.]+)\s*\*\s*SF\s*\*\*([\-0-9.]+)@) {
	# Power law
	$const = $1;
	$exponent = $2;
	$attrs{"$attr/C"} += $const;
	$attrs{"$attr/E"} += $exponent;
	$c = $attrs{"$attr/C"};
	$e = $attrs{"$attr/E"};
	#print "  Power law: $c  $e\n";
      } elsif ($rawval =~ m@([\-0-9.]+)\s*\*\s*SF@) {
	# Simple linear.  E.g. -synth_postings= 1132642.1429*SF
	$attrs{"$attr/L"} += $1;
	$l = $attrs{"$attr/L"};
	#print "  Linear: $l\n";
      } elsif ($rawval =~ m@,@) {
	# A comma separated list. E.g.
	# -head_term_percentages=0.8820;0.82857726,0.3836;0.360365348,0.3597;0.337912971, .... or
	# -head_term_percentages=0.8820,0.3836,0.3597, ....
	$attrs{$attr} = accumulateList($rawval, $attrs{$attr});
	#print "  Comma-separated list: $attrs{$attr}\n";
      } elsif ($rawval =~ /([\-0-9.]+);([\-0-9.]+)/) {
        # Linear with y intercept.  E.g. -zipf_alpha=-0.1554;-0.16442874
	$attrs{"$attr/Y"} += $1;
	$attrs{"$attr/I"} += $2;
	#print "  Linear with intercept $1  $2\n";
      } elsif ($rawval =~ m@^\s*([\-0-9.]+)\s*$@) {
	# Constant. E.g. -zipf_tail_perc=36.9199
	$val = $1;
	$attrs{$attr} += $val;
	#print "  Constant: $attrs{$attr}\n";
      } else {
	print "Unrecognized value expression: $rawval\n";
	exit(1);
      }
    } else {
      print "Erroneous line in $growthModel:

$_
";
      exit(1);
    }
   
  }
    close(SMF);

  }  # End of loop over corpus-specific growth models of this percentage


  # Time to write the generic model
  $genericModel = "$genericDir/generic_model_${perc}.txt";
  die "Can't write to $genericModel\n" unless open GM, ">$genericModel";
  print GM "# Generic scaling model derived from $perc% samples of $corpora
#  Replace SF wherever it occurs with the actual scale-up factor.
#  The argument names are those for corpusGenerator.exe

";
  foreach $k (keys %attrs) {
    next if $k =~ m@/E@;  # We'll deal with this via the /C entry
    next if $k =~ m@/I@;  # We'll deal with this via the /Y entry
    #print "KEY is: '$k'\n";
    if ($k =~ m@(.*)/C@) {
      $attr = $1;
      $const = $attrs{$k} / $corpusCount;
      $k2 = $k;
      $k2 =~ s@/C@/E@;
      $exponent = $attrs{$k2} / $corpusCount;
      print GM "$attr=$const*SF**$exponent   # power\n";
    } elsif ($k =~ m@(.*)/Y@) {
      $attr = $1;
      $y0 = $attrs{$k} / $corpusCount;
      $k2 = $k;
      $k2 =~ s@/Y@/I@;
      $yint = $attrs{$k2} / $corpusCount;
      print GM "$attr=$y0;$yint   # linear with intercept\n";
    } elsif ($k =~ m@(.*)/L@) {
      $attr = $1;
      $const = $attrs{$k} / $corpusCount;
      print GM "$attr=$const*SF   # simple linear\n";
    } elsif ($attrs{$k} =~ /,/) {
      $attr = $k;
      # A comma-separated list.
      $val = $attrs{$k};
      $liszt = makeMeansList($val, $corpusCount);
      print GM "$attr=$liszt\n";
    } else {
      $attr = $k;
      $val = $attrs{$k};
      $const = $val / $corpusCount;
      print GM "$attr=$const   # constant\n";
    }
  }
      
  close(GM);
  print "Wrote: $genericModel\n\n";
}



exit(0);

# -----------------------------------------------------------------------------------------------

sub accumulateList {
  my $list1 = shift;
  my $list2 = shift;
  my $rezo = "";

  return $list1 unless defined($list2);

  my @f1 = split /,/, $list1;
  my @f2 = split /,/, $list2;
  die "accumulateList() - lists are of different length\n"
    unless $#f1 == $#f2;

  # There are two cases: One where each list item is a single number, and the other
  # where each item is a semicolon-separated pair of numbers.

  if ($list1 =~ /;/) {   # The semicolon case
    for (my $i = 0; $i <= $#f1; $i++) {
      if ($f1[$i] =~ /([\-0-9.]+);([\-0-9.]+)/) {
	$v11 = $1;
	$v12 = $2;
      } else {
	die "Expected semicolons in list1 $list1\n";
      }
      if ($f2[$i] =~ /([\-0-9.]+);([\-0-9.]+)/) {
	$v21 = $1;
	$v22 = $2;
      } else {
	die "Expected semicolons in list2 $list2\n";
      }
      my $val = sprintf("%.5f;%.5f", $v11 + $v21, $v12 + $v22);
      $rezo .= ',' unless $i == 0;
      $rezo .= $val;
    }
  } else {  # single numbers case
    for (my $i = 0; $i <= $#f1; $i++) {
      my $val = sprintf("%.5f", $f1[$i] + $f2[$i]);
      $rezo .= ',' unless $i == 0;
      $rezo .= $val;
    }
  }
  #print "   $list1 + \n$list2 = \n$rezo\n\n";
  return $rezo;
}
  
sub makeMeansList {
  my $list = shift;
  my $nobs = shift;
  my $rezo = "";
  die "Nobs $nobs must be positive.\n" unless $nobs > 0;
  my @f = split /,/, $list;

  # Two cases:

  if ($list =~ /;/) {  # List items are semicolon separated pairs
    for (my $i = 0; $i <= $#f; $i++) {
      if ($f[$i] =~ /([\-0-9.]+);([\-0-9.]+)/) {
	$v1 = $1;
	$v2 = $2;
      } else {
	die "Means: Expected semicolons in list1 $list1\n";
      }
      my $val = sprintf("%.5f;%.5f", $v1 / $nobs, $v2 / $nobs);
      $rezo .= ',' unless $i == 0;
      $rezo .= $val;
    }
  } else {  # List items are single numbers;
    for (my $i = 0; $i <= $#f; $i++) {
      my $val = sprintf("%.5f", $f[$i] / $nobs);
      $rezo .= ',' unless $i == 0;
      $rezo .= $val;
    }
  }
 
  #print "   $list / $nobs = \n$rezo\n\n";
  return $rezo;
}
