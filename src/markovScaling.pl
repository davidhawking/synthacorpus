#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Take a sample of a corpus, train a Markov model on the sample, then run MarkovGenerator
# to produce as many postings as were in the original corpus.   Compare the scaled-up version
# with the real thing.  Do all this for 1%, 10% and 50% samples.

$|++;

die "Many SynthaCorpus scripts require that the environment variable SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct. 
If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

die "Experiment root $expRoot doesn't exist\n" unless -d $expRoot;
$baseDir = "$expRoot/Base";
$scalingRoot = "$expRoot/Scalingup/";
$emuDir0 = "$scalingRoot/MarkovW0";
mkdir $emuDir0 unless -d $emuDir0;
$emuDir1 = "$scalingRoot/MarkovW1";
mkdir $emuDir1 unless -d $emuDir1;

@corpora = ("ap");

@samples = (0.01, 0.1, 0.5);

foreach $corpus (@corpora) {
  foreach $sam (@samples) {
    $perc = 100 * $sam;
    $sup = 1 / $sam;
    $baseCorpusFile = "$baseDir/${corpus}.trec";
    $sampleFile = "${baseDir}/${corpus}_${perc}.trec";
    $cmd = "./selectRecordsFromFile.exe $baseCorpusFile $sampleFile random $sam";
    print $cmd, "\n";
    system($cmd);
     die "Cmd $cmd failed \n" if $?;

    print " ------------- Markov order 0 ----------------------- \n";
    $dir = "$emuDir0/M-$perc-$sup";
    mkdir $dir unless -d $dir;
    $cmd = "./MarkovGenerator.exe inFile=$sampleFile outFile=$dir/$corpus.trec -postingsScaleFactor=$sup -useOrderZero";
    print $cmd, "\n";
    system($cmd);
    die "Cmd $cmd failed \n" if $?;

    # Now compare the order-0 scaleup with the base file
    $cmd = "perl compareBaseWithMimic.pl $corpus $dir 2 2";
    print $cmd, "\n";
    system($cmd);
    die "Cmd $cmd failed \n" if $?;
    
    print " ------------- Markov order 1 ----------------------- \n";
    $dir = "$emuDir1/M-$perc-$sup";
    mkdir $dir unless -d $dir;
    $cmd = "./MarkovGenerator.exe inFile=$sampleFile outFile=$dir/$corpus.trec -postingsScaleFactor=$sup";
    print $cmd, "\n";
    system($cmd);
    die "Cmd $cmd failed \n" if $?;

    # Now compare the order-0 scaleup with the base file
    $cmd = "perl compareBaseWithMimic.pl  $corpus $dir 2 2";
    print $cmd, "\n";
    system($cmd);
    die "Cmd $cmd failed \n" if $?;
  }
}

print "

. The samples used in training the Markov models should be in  ${baseCorpusFile}_perc where perc is the percentage.

. The emulated corpus for order zero Markov should be in $emuDir0/M-perc-sup where perc is the percentage sample and sup is 
  the scale-up factor.  All the base_v_mimic etc. comparison files are in the same directory.

. The order 1 Markov stuff is under $emuDir1/M-perc-sup

";
    
exit(0);


