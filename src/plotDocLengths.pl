#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.

# Run in Experiments/Emulation directory
# Plot the doclenhist files for real and mimic versions of the same index

use File::Copy "cp";
use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
    $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
  }

die "Usage: $0 <corpusName> <emulationDir> [<alt. baseDir>]
    - emulation_dir can be an absolute path or a path relative to the Experiments/Emulation directory
    - alternate baseDir can be an absolute path or a path relative to the Experiments/ directory
"
  unless $#ARGV >= 1;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$baseDir = "$expRoot/Base";

$corpusName = $ARGV[0];
if ($ARGV[1] =~ /^([a-zA-Z]:)?\//) {
  # Path is absolute
  $emuDir = $ARGV[1];
} else {
  # Path is relative
  $emuDir = "$expRoot/Emulation/$ARGV[1]";
}

if ($#ARGV >= 2) {
  # Alternate base directory has been specified
  if ($ARGV[2] =~ /^([a-zA-Z]:)?\//) {
    # Path is absolute
    $baseDir = $ARGV[2];
  } else {
    # Path is relative
    $baseDir = "$expRoot/$ARGV[2]";
  }
}

die "Directory $baseDir doesn't exist\n" unless -d $baseDir;
die "Directory $emuDir doesn't exist\n" unless -d $emuDir;

$xaxisMax = 3000;

$plotter = `which gnuplot 2>&1`;
chomp($plotter);
if ($plotter =~/^which: no/) {
  undef $plotter;
} else {
  $plotter = "gnuplot";   # Rely on it being in our path
}
  
$dlh[0] = "$baseDir/${corpusName}_docLenHist.tsv";
$dlh[1] = "$emuDir/${corpusName}_docLenHist.tsv";

print "$0: Doclenhists: $dlh[0], $dlh[1]\n$0:Corpus Name = $corpusName\n";

$T = "$baseDir/$corpusName.tmp";
$U = "$emuDir/$corpusName.tmp";

avoid_zero_data_in_file($dlh[0], $T);
avoid_zero_data_in_file($dlh[1], $U);


$pcfile = "$emuDir/${corpusName}_base_v_mimic_doclens_plot.cmds";
die "Can't open $pcfile for writing\n" unless open P, ">$pcfile";

print P "
set terminal pdf
set size ratio 1
set xlabel \"Doc Length\"
set ylabel \"Frequency\"
set style line 1 linewidth 4
set style line 2 linewidth 4
set style line 3 linewidth 4
set pointsize 3
";
    print P "
set output \"$emuDir/${corpusName}_base_v_mimic_doclens.pdf\"
plot [0:$xaxisMax] \"$T\" title \"Base\" pt 7 ps 0.4, \"$U\" title \"Mimic\" pt 7 ps 0.17
";

close P;


if (defined($plotter)) {
    $cmd = "$plotter '$pcfile'\n";
    $code = system($cmd);
    die "$cmd failed" if $code;


    print "Plot commands in $pcfile (but tmp data deleted). To view the PDF use:

    acroread $emuDir/${corpusName}_base_v_mimic_doclens.pdf
\n";
} else {
    warn "\n\n$0: Warning: gnuplot not found.  PDFs of graphs will not be generated.\n\n";
}


# Remove temporary files
unlink $T;
unlink $U;

exit(0);


# -----------------------------------------------------------------------------

sub avoid_zero_data_in_file {
    my $inn = shift;
    my $outt = shift;

    print "\nOpening $inn and >$outt\n\n";
    die "Can't open $inn" unless open I, $inn;
    die "Can't open $outt" unless open O, ">$outt";
    while (<I>) {
	next unless /^\s*([0-9]+)\t\s*([0-9]+)/;
	next if $1 == 0;  # Zero length
	next if $2 == 0;  # Zero freq
	print O $_;
    }
    close I;
    close O;
}
