#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.

# Run in GIT/SynthaCorpus/src

# Plot the vocab.wdlens and vocab.wdfreqs files for base and mimic corpora

use File::Copy "cp";
use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
    $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

die "Usage: $0 <corpusName> <emulationDir> [<alt. baseDir>]
    - emulation_dir can be an absolute path or a path relative to the Experiments/Emulation directory
    - alternate baseDir can be an absolute path or a path relative to the Experiments/ directory
"
  unless $#ARGV >= 1;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});


$expRoot = $ENV{SC_EXPERIMENTS_ROOT};
$baseDir = "$expRoot/Base";

$corpusName = $ARGV[0];
if ($ARGV[1] =~ /^([a-zA-Z]:)?\//) {
  # Path is absolute
  $emuDir = $ARGV[1];
} else {
  # Path is relative
  $emuDir = "$expRoot/Emulation/$ARGV[1]";
}

if ($#ARGV >= 2) {
  # Alternate base directory has been specified
  if ($ARGV[2] =~ /^([a-zA-Z]:)?\//) {
    # Path is absolute
    $baseDir = $ARGV[2];
  } else {
    # Path is relative
    $baseDir = "$expRoot/$ARGV[2]";
  }
}


$termRepMeth = "";
if ($emuDir =~ /(Markov1|Caesar1|Nomenclator|Markov-[0-9]e?|base26|bubble_babble|tnum|simpleWords|from_tsv)/) {
  $termRepMeth = $1;
}

die "Directory $baseDir doesn't exist\n" unless -d $baseDir;
die "Directory $emuDir doesn't exist\n" unless -d $emuDir;



$plotter = `which gnuplot 2>&1`;
chomp($plotter);
if ($plotter =~/^which: no/) {
  undef $plotter;
} else {
  $plotter = "gnuplot";   # Rely on it being in our path
}



print "

baseDir: $baseDir
emuDir: $emuDir

Note: If the term representation method is a Markov variant you'll have to look in the 
logfiles to find out what Markov parameters were used in generating .wdfreqs -- 
unless you remember of course

";

  
$wdl[0] = "$baseDir/${corpusName}_vocab.wdlens";
$wdl[1] = "$emuDir/${corpusName}_vocab.wdlens";
$wdf[0] = "$baseDir/${corpusName}_vocab.wdfreqs";
$wdf[1] = "$emuDir/${corpusName}_vocab.wdfreqs";


getNormalDistParamsFromBaseWdlens($wdl[0]);   # Sets $mean, $stdev and $scale


print 
"$0: Wdlens: $wdl[0], $wdl[1]
$0: Wdfreqs: $wdf[0], $wdf[1]
$0: Corpus name=$corpusName\n";



$pcfile = "${emuDir}/${corpusName}_base_v_mimic_wdlens_plot.cmds";
die "Can't open $pcfile for writing\n" unless open P, ">$pcfile";

print P "
set terminal pdf
set size ratio 1
set style line 1 linewidth 4
set style line 2 linewidth 4
set style line 3 linewidth 4
set pointsize 3

f(x) = (1 / $stdev * sqrt(2 * pi)) * exp(- ((x - $mean) / $stdev)**2 / 2)

";
    print P "
set xlabel \"Word length (Unicode characters)\"
set ylabel \"Probability (for distinct words)\"
set output \"${baseDir}/${corpusName}_base_normal_wdlens.pdf\"
plot \"$wdl[0]\" title \"Base\" pt 7 ps 0.5 with linespoints, $scale*f(x) title \"Normal\"

set xlabel \"Word length (Unicode characters)\"
set ylabel \"Probability (for distinct words)\"
set output \"${emuDir}/${corpusName}_base_v_mimic_wdlens.pdf\"
plot \"$wdl[0]\" title \"Base\" pt 7 ps 0.5 with linespoints, \"$wdl[1]\" title \"Mimic $TermRepMeth\" pt 7 ps 0.17 with linespoints

set logscale y 

set xlabel \"Word length (Unicode characters)\"
set ylabel \"Mean freq. for words of given length\"
set output \"${baseDir}/${corpusName}_base_wdfreqs.pdf\"
plot \"$wdf[0]\" title \"Base\" pt 7 ps 0.5 with errorbars

set xlabel \"Word length (Unicode characters)\"
set ylabel \"Mean freq. for words of given length\"
set output \"${emuDir}/${corpusName}_base_v_mimic_wdfreqs.pdf\"
plot \"$wdf[0]\" title \"Base\" pt 7 ps 0.5 with linespoints, \"$wdf[1]\" title \"Mimic $TermRepMeth\" pt 7 ps 0.17 with linespoints
";

close P;

if (defined($plotter)) {
    $cmd = "$plotter '$pcfile'\n";
    $code = system($cmd);
    die "$cmd failed" if $code;
} else {
    warn "\n\n$0: Warning: gnuplot not found.  PDFs of graphs will not be generated.\n\n";
}



print "Plot commands in $pcfile. To view the PDFs use:

    preview ${baseDir}/${corpusName}_base_normal_wdlens.pdf
    preview ${baseDir}/${corpusName}_base_wdfreqs.pdf
    preview ${emuDir}/${corpusName}_base_v_mimic_wdlens.pdf
    preview ${emuDir}/${corpusName}_base_v_mimic_wdfreqs.pdf


";
exit(0);



#--------------------------------------------------------------------------------------------------

sub getNormalDistParamsFromBaseWdlens {
  my $file = shift;
  die "Can't read from $file\n" unless open WL, $file;
  while (<WL>) {
    if (/Length of distinct words.  Mean: ([0-9.]+), StDev: ([0-9.]+), Peak proportion: ([0-9.]+)/) {
      $mean = $1;
      $stdev = $2;
      $scale = $3;
      last;
    }
  }

  close(WL);
}
