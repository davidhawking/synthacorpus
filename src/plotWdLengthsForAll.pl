#! /usr/bin/perl -w

# Copyright (c) David Hawking. All rights reserved.
# Licensed under the MIT license.

# Run the plot_wdlengths.pl script and extractLetterFrequenciesFromVocab.exe on every emulated corpus
# Also extract letter frequencies for the base corpora

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.  

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});


$expRoot = $ENV{SC_EXPERIMENTS_ROOT};

$emuRoot = "$expRoot/Emulation";
$baseDir = "$expRoot/Base";


foreach $file ((glob "$emuRoot/*/*_vocab.tsv"), (glob "$emuRoot/*/*/*_vocab.tsv")) {
  ($emuDir, $corpus) = $file =~ m@(.*)/([^/]+)_vocab.tsv@;
  $cmd = "perl plot_wdlengths.pl $corpus $emuDir\n";
  print $cmd;
  system($cmd) == 0
    or warn "Command failed\n";
  $cmd = "./extractLetterFrequenciesFromVocab.exe $corpus $emuDir\n";
  print $cmd;
  system($cmd) == 0
    or warn "Command failed\n";
  $cmd = "./extractLetterPairFrequenciesFromVocab.exe $corpus $emuDir\n";
  print $cmd;
  system($cmd) == 0
    or warn "Command failed\n";
}


foreach $file ((glob "$baseDir/*_vocab.tsv")) {
  ($dir, $corpus) = $file =~ m@(.*)/([^/]+)_vocab.tsv@;
  $cmd = "./extractLetterFrequenciesFromVocab.exe $corpus $dir\n";
  print $cmd;
  system($cmd) == 0
    or warn "Command failed\n";
  $cmd = "./extractLetterPairFrequenciesFromVocab.exe $corpus $dir\n";
  print $cmd;
  system($cmd) == 0
    or warn "Command failed\n";
}


exit(0);
