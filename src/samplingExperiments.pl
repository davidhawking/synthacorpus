#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.


# Given the name of a corpus, this script creates random samples of different 
# sizes and extracts properties for each sample size:
#
#   1. Vocabulary size
#   2. Tail percentage (Singletons as %age of total vocab size)
#   3. Frequency %age (of total postings) of ten most common words
#   4. Closest approximation to Zipf Alpha
#   5. Mean and St.dev of document lengths
#   6. Doc length gamma parameters
#
# This version is capable of running the temporal growth scenarios.
#
# The experiment is controlled by the @samples[] array, whose entries must be
# stored in order of increasing sample fraction.
#
# Because observed characteristics will vary from sample to sample even 
# when the samples are the same size, and because the variability will 
# increase as the sample size gets smaller, we do repeated runs and 
# average the characteristics, but we do more runs for the smaller 
# sample sizes.   This approach should give reliable averages while
# reducing the potentially crippling time cost of doing many runs over large 
# sample sizes.


# Three types of data files are written:
#   .raw - the raw values for the sample
#   .dat - in which the sample values are fractions of the value for the biggest sample
#   .tad - in which the sample values are multiples of the value for the smallest sample


# Scripts are now called via $^X in case the perl we want is not in /usr/bin/

$perl = $^X;
$perl =~ s@\\@/@g;

use File::Copy "cp";
use Cwd;
$cwd = getcwd();
if ($cwd =~ m@/cygdrive@) {
    $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

$|++;

use Time::HiRes;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$experimentRoot = $ENV{SC_EXPERIMENTS_ROOT};


die "Usage: $0 <corpusName> [-temporalGrowth]

    Root directory is $experimentRoot/Sampling

    - if -temporal_growth is given, temporal subsets are used instead of
      samples, only one iteration is done for each subset, and different
      directories are used: TemporalSubset instead of Sample.

    Other options are permitted but ignored.\n"    
    unless $#ARGV >= 0;

print "\n*** $0 @ARGV\n\n";


$corpusName = $ARGV[0];

$plotter = `which gnuplot 2>&1`;
chomp($plotter);
if ($plotter =~/^which: no/) {
  undef $plotter;
} else {
  $plotter = "gnuplot";   # Rely on it being in our path
}

$samplingRoot = "$experimentRoot/Sampling";
mkdir $samplingRoot unless -d $samplingRoot;
$baseDir = "$experimentRoot/Base";

$usualWorkingDir = "$samplingRoot/Sample";   # Partially overridden for 100% sample
mkdir $usualWorkingDir
    unless -d $usualWorkingDir;  # A directory in which to do all our work
$temporalWorkingDir = "$samplingRoot/TemporalSubset";

die "$experimentRoot must exist\n"
  unless -d $experimentRoot;
die "$baseDir must exist and must contain the corpus file to be worked on\n"
  unless -d $baseDir;

# The executables and scripts we need, using paths relative to $samplingRoot  
$propertyExtractor = check_exe("corpusPropertyExtractor.exe");
$docSelector = check_exe("selectRecordsFromFile.exe");
$lsqcmd = check_exe("lsqfitGeneral.pl");


foreach $suff (".trec", ".TREC", ".tsv", ".starc", ".TSV", ".STARC", ) {
    $cf = "$baseDir/$corpusName$suff";
    if (-r $cf) {
	$corpusFile = $cf;
	$fileType = $suff;
	last;
    }  
}
    
die "Corpus file not found in $baseDir -- none of .tsv, .starc, or .trec\n"
	unless defined($corpusFile);


$temporalGrowth = 0;

for ($a = 1; $a <=$#ARGV; $a++) {
    if ($ARGV[$a] eq "-temporalGrowth") {
	$temporalGrowth = 1;
	print "  -- Working with temporal subsets --\n";
    }
}

if ($temporalGrowth) {
    mkdir $temporalWorkingDir 
	unless -d $temporalWorkingDir;  

    $workingDir = $temporalWorkingDir;
    $plotdir = $workingDir;
    $prefix1 = "subset_";
    $prefix2 = "growth_";

    # In temporal subsetting we need to know the total number of documents.
    $totDox = 0;
    $docCounter = check_exe("countDocsInCorpus.exe");
    $cmd = "$docCounter '$corpusFile'";
    $out = `$cmd`;
    die "Command $cmd failed with code $?\n"
	if $?;
    die "Output from $cmd unparseable: $out\n"
	unless ($out =~ /Documents:\s+([0-9]+)/s);
    $totDox = $1;
  } else {
    $plotdir = $usualWorkingDir;
    $prefix1 = "sample_";
    $prefix2 = "growth_";

  }



# samples lines comprise white space separated items, with the following fields:

$field_explanations = 
"# 0: percentage_sample 
# 1: iterations 
# 2: num_docs 
# 3: vocab_size
# 4: total_postings
# 5: perc_singleton_words 
# 6: highest_word_freq
# 7: alpha 
# 8: bigram_alpha
# 9: no distinct significant bigrams
# 10: highest_bigram_freq  ## Can we calculate this one
# 11: mean doc length
# 12: doc length st dev
# 13: tail percentage - percentage of all term instances due to singletons
# 14-23: head term percentages
# 24: doclen_gamma_shape
# 25: doclen_gamma_scale
# each of the 24 columns starting at 2 (num_docs) is a cumulative total 
# over all the samples run so far. 
";

$columns = 26;

@samples = (
    "Ignore element zero",
    "100 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    "50 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    "20 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    "10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    "5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    "2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    "1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
    );


$num_iters = $#samples;  # Cos we ignore element zero.
 
for ($i = 1; $i <= $num_iters; $i++) { # This loop enumerates iterations, but each
                                       # iteration, one less sample takes part
    # We take an early exit from this outer loop if we're doing Temporal
    $iter = $num_iters - $i + 1;
    for ($j = $num_iters; $j >= $i; $j--) {
	@fields = split /\s+/, $samples[$j];
	die "Wrong number of columns in samples[$j]\n"
	    unless ($#fields == ($columns - 1));
	print "$iter: Iteration $iter: $fields[0]% sample.\n\n";
	
	# ***** No longer take this shortcut, because Base property extraction may have used different params ******
	# if ($fields[0] == 100 && -r "$baseDir/${corpusName}_summary.txt") {
	#     # Save a lot of time and effort if the properties of the base have
	#     # already been extracted.  Both for samples and temporal subsets
	#     $workingDir = $baseDir;
	#     print "Avoiding the need to generate a 100% sample and extracting its properties\n";
	# } else {
	# -----------------  1. Generate a temporal subset or sample of this size  ----------
	if ($temporalGrowth) {
	  $workingDir = "$temporalWorkingDir/$fields[0]%";
	  mkdir $workingDir unless -e $workingDir;
	  $dox = int(($totDox * $fields[0]) / 100.0);
	  print "    Generating the temporal subset for $corpusName...   $dox out of $totDox.\n\n";
	  $cmd = "$docSelector '$corpusFile' '$workingDir'/$corpusName$fileType head $dox";
	  $rslts = `$cmd`;
	  die "Command $cmd failed with code $?\n$rslts\n"
	    if $?;
	  die "Can't match doc selector output\n$rslts"
	    unless $rslts =~ m@SelectHeadRecords: ([0-9]+) documents output.@s;
	  die "Wrong number $1 of doc.s extracted by $docSelector.\n  Should have been $dox\n"
	    unless $1 == $dox;
	  
	  # $fields[2] = $1; # Don't do this cos it should be added and it's done later anyway
	} else {
	  $workingDir = "$usualWorkingDir/$fields[0]%";
	  mkdir $workingDir unless -e $workingDir;
	  print "    Generating the sample for $corpusName...\n\n";
	  $proportion = $fields[0] / 100;
	  $cmd = "$docSelector '$corpusFile' '$workingDir'/$corpusName$fileType random $proportion";
	  $rslts = `$cmd`;
	  die "Command $cmd failed with code $?\n$rslts\n"
	    if $?;
	  die "Can't match doc selector output\n$rslts"
	    unless $rslts =~ m@SelectRandomRecords: ([0-9]+) /@s;
	  # $fields[2] = $1; # Don't do this cos it should be added and it's done later anyway
	}
	
	# ----------------- 2. Extract the properties of the sample ------------------------------------------------
	print "    Extracting properties from the $corpusName sample ...\n\n";
	
	# ---------------- 2A. Get stuff from ${workingDir}/${corpusName}_summary.txt  -----------------------------
	$cmd = "$propertyExtractor inputFileName='$workingDir'/$corpusName$fileType outputStem='$workingDir'/$corpusName -ngramHashBits=25\n";
	$rout = `$cmd`;
	die "Command $cmd failed\n$rout\n" if $?;
	undef %sample;

#	}
	
	print "    Extracting info from the ${corpusName}_summary.txt file\n\n";
	# Extract some stuff from the the summary file  - Format: '<attribute>=<value>'
	$sumryfile = "$workingDir/${corpusName}_summary.txt";
	die "Can't open $sumryfile\n"
	    unless open S, $sumryfile;
	while (<S>) {
	    next if /^\s*#/;  # skip comments
	    if (/(\S+)=(\S+)/) {
	      $sample{$1} = $2;
	    }
	}
	close(S);

	$fields[2] += $sample{docs};
	$fields[3] += $sample{vocab_size};
	$vocabSize = $sample{vocab_size};
	$fields[4] += $sample{total_postings};
	$totalPostings = $sample{total_postings};
	$fields[6] += $sample{longest_list};
	$fields[11] += $sample{doclen_mean};
	$fields[12] += $sample{doclen_stdev};
	$fields[24] += $sample{doclen_gamma_shape};
	$fields[25] += $sample{doclen_gamma_scale};
	

	# ---------------- 2B. Get stuff from ${workingDir}/${corpusName}_vocab.tfd  -----------------------------
	print "    Extracting info from the ${corpusName}_vocab.tfd file\n\n";
	die "Can't read ${workingDir}/${corpusName}_vocab.tfd"
	    unless open V, "${workingDir}/${corpusName}_vocab.tfd";

	while (<V>) {
	    next if /^\s*#/;  # skip comments
	    if (/-zipf_tail_perc=([0-9.]+)/) {   #  percentage of distinct words
		$fields[5]  += $1;
		$fields[13] += 100.0 * (($1 / 100.0 * $vocabSize) / $totalPostings);
	    } elsif (/-head_term_percentages=([0-9.,]+)/) {
		my $list = $1;
		my @hpercs = split /,/,$list;
		for (my $r = 0; $r < 10; $r++) {
		    $fields[14 + $r] += $hpercs[$r];  # 14 - 23 are the frequencies of the top-10 words
		}
	    } elsif (/-zipf_alpha=([-0-9.]+)/) {
		$fields[7] += $1;
	    }
	}
	close(V);

	# ---------------- 2C. Get no. distinct bigrams and highest bigram freq. from ${workingDir}/${corpusName}_bigrams.tfd  ----------
	print "    Extracting info from the ${corpusName}_bigrams.tfd file\n\n";
	die "Can't read ${workingDir}/${corpusName}_bigrams.tfd"
	    unless open B, "${workingDir}/${corpusName}_bigrams.tfd";

	while (<B>) {
	    next if /^\s*#/;  # skip comments
	    if (/-synth_postings=([0-9]+)/) {  # This line comes [must come] ahead of head_term_percentages
		$totBigramPostings = $1;   
	    } elsif (/-zipf_alpha=([\-0-9.]+)/) {
		$fields[8] += $1;
	    }  elsif (/-head_term_percentages=([0-9.]+)/) {
		$highestBigramFreq = $1 * $totBigramPostings / 100.0;
		$fields[10] += $highestBigramFreq;
	    } 	
	}
	close(B);

	# ---------------- 2D. Get the number of distinct bigrams from ${workingDir}/${corpusName}_bigrams.tsv  ------------------------
	$cmd = "wc -l '${workingDir}'/${corpusName}_bigrams.tsv";
	$rout = `$cmd`;
	die "Command $cmd failed with code $?\n"
	    if $?;
	$rslts =~ m@([0-9]+)@;
	$fields[9] += $1;

        # 6. Update fields and write back into samples[$i]
	$fields[1]++;  # How many observations for this sample size

	# 7. Write the incremented values.
	$samples[$j] = "@fields";
    }
    # 8. Calculate and write averages.
    show_data();
    last if $temporalGrowth;  # No need for averaging in this case  # ---------------------->
}  # End of outer loop

print $field_explanations;

# At this point all the data we need is in the samples array.  The i-th element of the array is
# a string of values for the i-th sample size, and the values are the sums of all the observations
# for this sample size.  We rely on generate_plots() to convert the sums into averages.

generate_plots();  # Generate plot.cmds files even if there's gno gnuplot

exit(0);

# ---------------------------------------------------------------------------------------

sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}


sub show_data {
  # Print all the averages in TSV format, up to this point in the processing
  # No data is changed, this is just for progress information
    print "\n";
    for (my $i = $num_iters; $i > 0; $i--) {  # Counting down.  I.e. increasing the size of the sample
	my @r = split /\s+/, $samples[$i];
	my @f = @r;  # Can't muck with the real array.
	$nobs = $f[1];
	$nobs = 1 if $nobs < 1;
	for (my $j = 2; $j < $columns; $j++) { $f[$j] = sprintf("%.2f", $f[$j]/$nobs);}
	print "S:\t$f[0]\t$nobs";
	for (my $k = 2; $k < $columns; $k++) {  print "\t$f[$k]"; }
	print "\n";
    }
    print "\n";
}


sub generate_plots {
  my $k;
    # We're going to write the unnormalised data to the .raw file
    $pr = "${plotdir}/${prefix2}" . $corpusName . ".raw";   # E.g. /opt/local/BigData/Experiments/Sampling/Sample/growth_ap.raw
    die "Can't write to $pr\n" unless open PR, ">$pr";
    print PR "# This file shows the raw values for each variable for each sample or subset size.
    #  Where appropriate the values are averaged across multiple samples of the same size.\n";


    for (my $i = $num_iters; $i > 0; $i--)  {# Counting down.  I.e. increasing the size of the sample
	my @r = split /\s+/, $samples[$i];
	my @f = @r;  # Just in case: Don't muck with the real array.


        # Average across the multiple observations for this sample
	$nobs = $f[1];
	$nobs = 1 if $nobs < 1;  # Hopefully this isn't ever needed!
	for (my $j = 2; $j < $columns; $j++) {  
	    print "  Iter $i: Col $j:  $f[$j] / $nobs\n";   # Odd that it's called 'Iter' since it's really a sample size.
	    $f[$j] = sprintf("%.4f", $f[$j]/$nobs);
	}

	@sample1 = @f if $f[0] == 1 ; # Save the averaged raw values for the scaling model.
	@sample10 = @f if $f[0] == 10 ; # Save the averaged raw values for the scaling model.
	@sample50 = @f if $f[0] == 50 ; # Save the averaged raw values for the scaling model.
	@sample100 = @f if $f[0] == 100 ; # Save the averaged raw values for the scaling model.
	
	# Write a row to the .raw file in TSV format
	print PR "$f[0]";
	for (my $j = 1; $j < $columns; $j++) {
	    print PR "\t$f[$j]";
	}
	print PR "\n";

	# Make an array of the observation-averaged data for each column
	$k = $num_iters - $i;    # $k ranges from 0 to 6

	# @f relates to the i-th sample, so we're setting the k-th element of
	# e.g sample_size to the i-th sample.  Sample[6] relates to 1% but
	# sample_size[6] relates to the 100% sample.  Odd.
	$sample_size[$k] = $f[0];
	$num_docs[$k] = $f[2];
	$vocab_size[$k] = $f[3];
	$total_postings[$k] = $f[4];
	$perc_singletons[$k] = $f[5];
	$highest_word_freq[$k] = $f[6];
	$alpha[$k] = $f[7];
	$bigram_alpha[$k] = $f[8];
	$no_distinct_significant_bigrams[$k] = $f[9];
	$highest_bigram_freq[$k] = $f[10];
	$doclen_mean[$k] = $f[11];
	$doclen_stdev[$k] = $f[12];
	$tail_perc[$k] = $f[13];
	$h1_perc[$k] = $f[14];
	$h2_perc[$k] = $f[15];
	$h3_perc[$k] = $f[16];
	$h4_perc[$k] = $f[17];
	$h5_perc[$k] = $f[18];
	$h6_perc[$k] = $f[19];
	$h7_perc[$k] = $f[20];
	$h8_perc[$k] = $f[21];
	$h9_perc[$k] = $f[22];
	$h10_perc[$k] = $f[23];
	$doclen_gamma_shape[$k] = $f[24];
	$doclen_gamma_scale[$k] = $f[25];
     }

    close(PR);


    # Now normalise the relevant columns relative to the value for the full collection
    # for writing in the .dat file
    # That value is in $array[$num_iters - 1];
    # Also normalise relative to the smallest sample and write into .tad -- variables
    # prefixed with "g_" for growth

    my $f = $num_iters - 1;   # $f selects the value for the 100% sample from the arrays.


    for ($k = 0; $k < $num_iters; $k++) {   # ranging from 0 to 6


	# ------------ .dat ----------------------------
	$num_docs[$k] = sprintf("%.4f", $num_docs[$k] / $num_docs[$f] * 100);
	$vocab_size[$k] = sprintf("%.4f", $vocab_size[$k] / $vocab_size[$f] * 100);
	$total_postings[$k] = sprintf("%.4f", $total_postings[$k] / $total_postings[$f] * 100);
	$perc_singletons[$k] = sprintf("%.4f", $perc_singletons[$k] / $perc_singletons[$f] * 100);
	$highest_word_freq[$k] =  sprintf("%.4f", $highest_word_freq[$k] / $highest_word_freq[$f] * 100);
	$alpha[$k] = sprintf("%.4f", $alpha[$k] / $alpha[$f] * 100);

	$no_distinct_significant_bigrams[$k] = sprintf("%.4f", $no_distinct_significant_bigrams[$k] 
						       / $no_distinct_significant_bigrams[$f] * 100);
	$highest_bigram_freq[$k] = sprintf("%.4f", $highest_bigram_freq[$k] / $highest_bigram_freq[$f] * 100);
	$bigram_alpha[$k] = sprintf("%.4f", $bigram_alpha[$k] / $bigram_alpha[$f] * 100);

 	$doclen_mean[$k] = sprintf("%.4f", $doclen_mean[$k] * 100.0 / $doclen_mean[$f]);
	$doclen_stdev[$k] = sprintf("%.4f", $doclen_stdev[$k] * 100.0 / $doclen_stdev[$f]);
	$tail_perc[$k] = sprintf("%.4f", $tail_perc[$k] * 100.0 / $tail_perc[$f]);
	$h1_perc[$k] = sprintf("%.4f", $h1_perc[$k] * 100.0 / $h1_perc[$f]);
	$h2_perc[$k] = sprintf("%.4f", $h2_perc[$k] * 100.0 / $h2_perc[$f]);
	$h3_perc[$k] = sprintf("%.4f", $h3_perc[$k] * 100.0 / $h3_perc[$f]);
	$h4_perc[$k] = sprintf("%.4f", $h4_perc[$k] * 100.0 / $h4_perc[$f]);
	$h5_perc[$k] = sprintf("%.4f", $h5_perc[$k] * 100.0 / $h5_perc[$f]);
	$h6_perc[$k] = sprintf("%.4f", $h6_perc[$k] * 100.0 / $h6_perc[$f]);
	$h7_perc[$k] = sprintf("%.4f", $h7_perc[$k] * 100.0 / $h7_perc[$f]);
	$h8_perc[$k] = sprintf("%.4f", $h8_perc[$k] * 100.0 / $h8_perc[$f]);
	$h9_perc[$k] = sprintf("%.4f", $h9_perc[$k] * 100.0 / $h9_perc[$f]);
	$h10_perc[$k] = sprintf("%.4f", $h10_perc[$k] * 100.0 / $h10_perc[$f]);
 	$doclen_gamma_shape[$k] = sprintf("%.4f", $doclen_gamma_shape[$k] * 100.0 / $doclen_gamma_shape[$f]);
	$doclen_gamma_scale[$k] = sprintf("%.4f", $doclen_gamma_scale[$k] * 100.0 / $doclen_gamma_scale[$f]);

	# -------------  .tad ----------------------------
	$g_sample_size[$k] = sprintf("%.4f", $sample_size[$k] / $sample_size[0]);
	$g_num_docs[$k] = sprintf("%.4f", $num_docs[$k] / $num_docs[0]);
	$g_vocab_size[$k] = sprintf("%.4f", $vocab_size[$k] / $vocab_size[0]);
	$g_total_postings[$k] = sprintf("%.4f", $total_postings[$k] / $total_postings[0]);
	$g_perc_singletons[$k] = sprintf("%.4f", $perc_singletons[$k] / $perc_singletons[0]);
	$g_highest_word_freq[$k] =  sprintf("%.4f", $highest_word_freq[$k] / $highest_word_freq[0]);
	$g_alpha[$k] = sprintf("%.4f", $alpha[$k] / $alpha[0]);

	$g_no_distinct_significant_bigrams[$k] = sprintf("%.4f", $no_distinct_significant_bigrams[$k] / $no_distinct_significant_bigrams[0])
	    unless $no_distinct_significant_bigrams[0] == 0;
	$g_highest_bigram_freq[$k] = sprintf("%.4f", $highest_bigram_freq[$k] / $highest_bigram_freq[0])
	    unless $highest_bigram_freq[0] == 0;
	$g_bigram_alpha[$k] = sprintf("%.4f", $bigram_alpha[$k] / $bigram_alpha[0])
	    unless $bigram_alpha[0] == 0;

 	$g_doclen_mean[$k] = sprintf("%.4f", $doclen_mean[$k] / $doclen_mean[0]);
	$g_doclen_stdev[$k] = sprintf("%.4f", $doclen_stdev[$k] / $doclen_stdev[0]);
	$g_tail_perc[$k] = sprintf("%.4f", $tail_perc[$k] / $tail_perc[0]);
	$g_h1_perc[$k] = sprintf("%.4f", $h1_perc[$k] / $h1_perc[0]);
	$g_h2_perc[$k] = sprintf("%.4f", $h2_perc[$k] / $h2_perc[0]);
	$g_h3_perc[$k] = sprintf("%.4f", $h3_perc[$k] / $h3_perc[0]);
	$g_h4_perc[$k] = sprintf("%.4f", $h4_perc[$k] / $h4_perc[0]);
	$g_h5_perc[$k] = sprintf("%.4f", $h5_perc[$k] / $h5_perc[0]);
	$g_h6_perc[$k] = sprintf("%.4f", $h6_perc[$k] / $h6_perc[0]);
	$g_h7_perc[$k] = sprintf("%.4f", $h7_perc[$k] / $h7_perc[0]);
	$g_h8_perc[$k] = sprintf("%.4f", $h8_perc[$k] / $h8_perc[0]);
	$g_h9_perc[$k] = sprintf("%.4f", $h9_perc[$k] / $h9_perc[0]);
	$g_h10_perc[$k] = sprintf("%.4f", $h10_perc[$k] / $h10_perc[0]);
 	$g_doclen_gamma_shape[$k] = sprintf("%.4f", $doclen_gamma_shape[$k] / $doclen_gamma_shape[0]);
	$g_doclen_gamma_scale[$k] = sprintf("%.4f", $doclen_gamma_scale[$k] / $doclen_gamma_scale[0]);
   }

    $pc = "${plotdir}/${prefix2}" . $corpusName . "_plot.cmds";  # E.g. /opt/local/BigData/Experiments/Sampling/Sample/growth_ap_plot.cmds
    $pd = "${plotdir}/${prefix2}" . $corpusName . ".dat";
    $pt = "${plotdir}/${prefix2}" . $corpusName . ".tad";

    die "Can't write to $pd\n" unless open PD, ">$pd";
    die "Can't write to $pt\n" unless open PT, ">$pt";
    die "Can't write to $pc\n" unless open PC, ">$pc";

    print PD "#The values in each row are expressed as percentages of those for the full collection
";
    print PT "#The values in each row are expressed as multiples of those for the smallest (1%) sample
";
    # Now write the data file for plotting
    for ($k = 0; $k <= $num_iters - 1; $k++) {
	# Include the sample_size and zero (for the number of observations) so that columns in .tad and .dat line up 
      # with the field explanations.   However, gnuplot numbers data columns from 1, so will need to add one to
      # every field number referenced.
	print PD "$sample_size[$k] 0 $num_docs[$k] $vocab_size[$k] $total_postings[$k] $perc_singletons[$k] $highest_word_freq[$k] $alpha[$k] $bigram_alpha[$k] $no_distinct_significant_bigrams[$k] $highest_bigram_freq[$k] $doclen_mean[$k] $doclen_stdev[$k] $tail_perc[$k] $h1_perc[$k] $h2_perc[$k] $h3_perc[$k] $h4_perc[$k] $h5_perc[$k]  $h6_perc[$k] $h7_perc[$k] $h8_perc[$k] $h9_perc[$k] $h10_perc[$k] $doclen_gamma_shape[$k] $doclen_gamma_scale[$k]\n";
 	print PT "$sample_size[$k] 0 $g_num_docs[$k] $g_vocab_size[$k] $g_total_postings[$k] $g_perc_singletons[$k] $g_highest_word_freq[$k] $g_alpha[$k] $g_bigram_alpha[$k] $g_no_distinct_significant_bigrams[$k] $g_highest_bigram_freq[$k] $g_doclen_mean[$k] $g_doclen_stdev[$k] $g_tail_perc[$k] $g_h1_perc[$k] $g_h2_perc[$k] $g_h3_perc[$k] $g_h4_perc[$k] $g_h5_perc[$k]  $g_h6_perc[$k] $g_h7_perc[$k] $g_h8_perc[$k] $g_h9_perc[$k] $g_h10_perc[$k] $g_doclen_gamma_shape[$k] $g_doclen_gamma_scale[$k]\n";
   }
    close(PD);
    close(PT);

    # Now write the plot commands
    print PC "
set terminal pdf
set size ratio 1
set xlabel \"Percentage Sample\"
set ylabel \"Percentage of value for full collection\"
set style line 1 linewidth 2
set style line 2 linewidth 2
set style line 3 linewidth 2
set style line 4 linewidth 2
set style line 5 linewidth 2
set style line 6 linewidth 2
set style line 7 linewidth 2
set style line 8 linewidth 2
set style line 9 linewidth 2
set style line 10 linewidth 2
set style line 11 linewidth 2
set pointsize 0.5

";
    print PC "
set output \"${plotdir}/${prefix1}${corpusName}_vocab.pdf\"
plot [0:100][0:120] \"$pd\" using 3:4 title \"vocab size\" w lp ls 2, \"$pd\" using 3:10 title \"distinct significant bigrams\" w lp ls 3, \"$pd\" using 3:5 title \"total postings\" w lp ls 7

";

    print PC "
set output \"${plotdir}/${prefix1}${corpusName}_hifreq.pdf\"
plot [0:100][0:120] \"$pd\" using 3:7 title \"highest word freq\" w lp ls 2, \"$pd\" using 3:11 title \"highest bigram freq\" w lp ls 3

";



    print PC "
set output \"${plotdir}/${prefix1}${corpusName}_zipf.pdf\"
plot [0:100][0:120] \"$pd\" using 3:8 title \"Zipf alpha\" w lp ls 2

";

    print PC "
set output \"${plotdir}/${prefix1}${corpusName}_singletons.pdf\"
plot [0:100][0:120] \"$pd\" using 3:6 title \"Singletons\" w lp ls 2

";

    generate_tad_plots();  ### ------------------------------------>

    close PC;

    if (defined($plotter)) {
	
	$cmd = "$plotter '$pc'\n";
	$plout = `$cmd`;
	die "$plotter failed for $pc with code $?!\nOutput:\n$plout" if $?;

	print "
Dat plots (generally linear scales, percentages relative to full collection):
  acroread '${plotdir}'/${prefix1}${corpusName}_vocab.pdf
  acroread '${plotdir}'/${prefix1}${corpusName}_hifreq.pdf
  acroread '${plotdir}'/${prefix1}${corpusName}_zipf.pdf
  acroread '${plotdir}'/${prefix1}${corpusName}_singletons.pdf

";

	print "Tad plots (often log log, ratios relative to smallest sample):
    acroread '${plotdir}'/scaling_${corpusName}_total_postings.pdf
    acroread '${plotdir}'/scaling_${corpusName}_vocab_size.pdf
    acroread '${plotdir}'/scaling_${corpusName}_alphalog.pdf
    acroread '${plotdir}'/scaling_${corpusName}_alphalinear.pdf
    acroread '${plotdir}'/scaling_${corpusName}_doc_length.pdf
    acroread '${plotdir}'/scaling_${corpusName}_tail_perc.pdf  
    acroread '${plotdir}'/scaling_${corpusName}_head_terms.pdf
    acroread '${plotdir}'/scaling_${corpusName}_bigram_alpha.pdf
    acroread '${plotdir}'/scaling_${corpusName}_significant_bigrams.pdf
    acroread '${plotdir}'/scaling_${corpusName}_highest_bigram_freq.pdf

";

    }  else {
	warn "\n\n$0: Warning: gnuplot not found.  PDFs of graphs will not be generated.\n\n";
    }


}


sub generate_tad_plots {
  # TAD measurements are relative to the smallest (1%) sample
  # Output plot commands to show how key property values change
  # as scaling factor increases from 1 to 100

  # Note that because gnuplot numbers data columns from one we have to
  # add one to the field definition numbers.

  $maxind = $num_iters - 1;

  # Note that as we worked through the sampling iterations $workingDir was set
  # to the relevant sub-directory (e.g. 1%, 10% etc.) of either $usualWorkingDir or
  # $temporalWorkingDir.  $TFix determines where various plotting files are stored.
  # It should be based on the plotting directory not the sub-directories.
  $Tfix = "$plotdir/${corpusName}_";  #Naming prefix for tmp files.
    
print PC "set xlabel \"Multiple of smallest sample\"
set ylabel \"Multiple of value for smallest sample\"
";


# no. postings
    print PC "
set output \"${plotdir}/scaling_${corpusName}_total_postings.pdf\"
plot [0:120][0:120]\"$pt\" using 3:5 title \"Averaged ob.s\" w lp ls 2, x title \"y=x\" ls 3";


# vocab size - model as y = x^a, plot in log log space.
    die "Can't write to ${Tfix}tad.vsize.plot\n"
	unless open TT, ">${Tfix}tad.vsize.plot";

    for (my $k = 0; $k <= $#g_vocab_size; $k++) {
	my $x = log($g_total_postings[$k]);
	my $y = log($g_vocab_size[$k]);
	print TT "$x\t$y\n";
    }
    close(TT);
    my $cmd = "$lsqcmd '${Tfix}'tad.vsize -plotLinearFit -xlabel=log\\(scale-up\\) -ylabel=log\\(vocab_size_ratio\\)\n";
    my $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

    if ($lsqout =~ /,\s*[\-0-9.]+\s*\+\s*([\-0-9.]+)\s*\*x\s*title "Linear fit"/s) {
	$slope_vs = $1;
    } else {
	die "Error: Unable to extract stuff from $cmd output, which was:\n$lsqout\n";
    }

    print PC "
set logscale xy
set output \"${plotdir}/scaling_${corpusName}_vocab_size.pdf\"
plot [1:120][1:100] \"$pt\" using 3:4 title \"Averaged ob.s\" w lp ls 2, x**$slope_vs ls 3
unset logscale xy
";

    
# Zipf alpha - model as y = x^a, plot in log log space, also in linear
    die "Can't write to ${Tfix}tad.alphalog.plot\n"
	unless open TT, ">${Tfix}tad.alphalog.plot";
    die "Can't write to ${Tfix}tad.alphalinear.plot\n"
	unless open LIN, ">${Tfix}tad.alphalinear.plot";

    for (my $k = 0; $k <= $#g_alpha; $k++) {
	my $x = log($g_total_postings[$k]);
	my $y = log($g_alpha[$k]);
	print TT "$x\t$y\n";
	print LIN "$g_total_postings[$k]\t$g_alpha[$k]\n";
    }
  close(TT);
  close(LIN);
  
    $cmd = "$lsqcmd '${Tfix}'tad.alphalog -plotLinearFit -plotQuadraticFit -xlabel=log\\(scale-up\\) -ylabel=log\\(zipf_alpha_ratio\\)\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

    if ($lsqout =~ /,\s*[\-0-9.]+\s*\+\s*([\-0-9.]+)\s*\*x\s*title "Linear fit"/s) {
	$slope_za = $1;
    } else {
	die "Error: Unable to extract stuff from $cmd output, which was:\n$lsqout\n";
    }
  
    print PC "
set logscale xy
set output \"${plotdir}/scaling_${corpusName}_alphalog.pdf\"
plot [1:120][1:2] \"$pt\" using 3:8 title \"Averaged ob.s\" w lp ls 2, x**$slope_za ls 3
unset logscale xy
";

  # Linear version of Zipf alpha
  
    $cmd = "$lsqcmd '${Tfix}'tad.alphalinear -plotLinearFit -xlabel=scale-up -ylabel=zipf_alpha_ratio\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

  if ($lsqout =~ /,\s*([\-0-9.]+)\s*\+\s*([\-0-9.]+)\s*\*x\s*title "Linear fit"/s) {
    $const_za2 = $1;
    $slope_za2 = $2;
    } else {
	die "Error: Unable to extract stuff from $cmd output, which was:\n$lsqout\n";
    }
  
    print PC "
set output \"${plotdir}/scaling_${corpusName}_alphalinear.pdf\"
plot [0:120][0:2] \"$pt\" using 3:8 title \"Averaged ob.s\" w lp ls 2, $const_za2 + x*$slope_za2 ls 3
";

  

    
# doc. length
    print PC "
set output \"${plotdir}/scaling_${corpusName}_doc_length.pdf\"
plot \"$pt\" using 3:12 title \"Mean\" w lp ls 2, \"$pt\" using 3:13 title \"St. dev\" w lp ls 3
";

# singleton_terms  - model as y = x^a, plot in log log space.
    die "Can't write to ${Tfix}tad.singleslog.plot\n"
	unless open TT, ">${Tfix}tad.singleslog.plot";
    die "Can't write to ${Tfix}tad.singleslinear.plot\n"
	unless open LIN, ">${Tfix}tad.singleslinear.plot";

    for (my $k = 0; $k <= $#g_perc_singletons; $k++) {
	my $x = log($g_total_postings[$k]);
	my $y = log($g_perc_singletons[$k]);
	print TT "$x\t$y\n";
	print LIN "$g_total_postings[$k]\t$g_perc_singletons[$k]\n";
    }
  close(TT);
  close(LIN);
  
    $cmd = "$lsqcmd '${Tfix}'tad.singleslog -plotLinearFit -xlabel=log\\(scale-up\\) -ylabel=log\\(perc_singletons_ratio\\)\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

    if ($lsqout =~ /,\s*[\-0-9.]+\s*\+\s*([\-0-9.]+)\s*\*x\s*title "Linear fit"/s) {
	$slope_tp = $1;
    } else {
	die "Error: Unable to extract stuff from $cmd output, which was:\n$lsqout\n";
    }

    print PC "
set logscale xy
set output \"${plotdir}/scaling_${corpusName}_tail_perc_log.pdf\"
plot [1:120] \"$pt\" using 3:6 title \"Averaged ob.s\" w lp ls 2, x**$slope_tp ls 3
unset logscale xy
";

  $cmd = "$lsqcmd '${Tfix}'tad.singleslinear -plotLinearFit -xlabel=scale-up -ylabel=perc_singletons_ratio\n";
    $lsqout = `$cmd`;
    die "$lsqcmd failed with code $?.\nCommand was $cmd.\n" if $?;

  if ($lsqout =~ /,\s*([\-0-9.]+)\s*\+\s*([\-0-9.]+)\s*\*x\s*title "Linear fit"/s) {
    $const_tp = $1;
    $slope_tp = $2;
  } else {
    die "Error: Unable to extract stuff from $cmd output, which was:\n$lsqout\n";
  }

    print PC "
set output \"${plotdir}/scaling_${corpusName}_tail_perc_linear.pdf\"
plot [0:120][0:1.6]\"$pt\" using 3:6 title \"Averaged ob.s\" w lp ls 2, $const_tp + x*$slope_tp ls 3
";



  # head terms -- just use h1.
  $h1_slope = sprintf("%.5f", ($g_h1_perc[$maxind] - $g_h1_perc[0]) / ($g_total_postings[$maxind] - $g_total_postings[0]));
  $h1_const = sprintf("%.5f", 1 - $h1_slope);
  
    print PC "
set output \"${plotdir}/scaling_${corpusName}_head_terms.pdf\"
plot [0:120][0:3.2] \"$pt\" using 3:15 title \"Word 1\" w lp ls 1, \"$pt\" using 3:16 title \"Word 2\" w lp ls 2, \"$pt\" using 3:17 title \"Word 3\" w lp ls 3, \"$pt\" using 3:18 title \"Word 4\" w lp ls 4, \"$pt\" using 3:19 title \"Word 5\" w lp ls 5, \"$pt\" using 3:20 title \"Word 6\" w lp ls 6, \"$pt\" using 3:21 title \"Word 7\" w lp ls 7, \"$pt\" using 3:22 title \"Word 8\" w lp ls 8, \"$pt\" using 3:23 title \"Word 9\" w lp ls 9, \"$pt\" using 3:24 title \"Word 10\" w lp ls 10, $h1_const + $h1_slope * x  ls 11
";

    if ($g_bigram_alpha[$maxind] > 0.0) {
	# bigram alpha
	# Try a cheating way of estimating alpha.  Slope = log(value of biggest sample) / log(size of biggest sample)
	##$maxind = $#samples - 1;   ### This seems bizarre
	$alf_bigal = sprintf("%.4f", log($g_bigram_alpha[$maxind]) / log($g_sample_size[$maxind]));
	
	print PC "
set logscale xy
set output \"${plotdir}/scaling_${corpusName}_bigram_alpha.pdf\"
plot \"$pt\" using 3:9 title \"Averaged ob.s\" w lp ls 1, x**$alf_bigal title \"y=x**$alf_bigal\" ls 2
unset logscale xy
";
    } else {
	print "Disaster averted: g_bigram_alpha[$maxind] = $g_bigram_alpha[$maxind]\n";
    }
	
    if ($g_no_distinct_significant_bigrams[$maxind] > 1.0) {
	# number of significant bigrams
	# Try a cheating way of estimating alpha.  Slope = log(value of biggest sample) / log(size of biggest sample)
	$alf_sigbigs = sprintf("%.4f", log($g_no_distinct_significant_bigrams[$maxind]) / log($g_sample_size[$maxind]));

	print PC "
set logscale xy
set output \"${plotdir}/scaling_${corpusName}_significant_bigrams.pdf\"
plot [1:120][1:1000]\"$pt\" using 3:10 title \"Averaged ob.s\" w lp ls 1, x**$alf_sigbigs title \"y=x**$alf_sigbigs\" ls 2
unset logscale xy
";
      } else {
	print "Disaster averted: g_no_distinct_significant_bigrams[$maxind] = $g_no_distinct_significant_bigrams[$maxind]\n";
    }
 
    # highest bigram frequency
     print PC "
set output \"${plotdir}/scaling_${corpusName}_highest_bigram_freq.pdf\"
plot [0:120][0:120] \"$pt\" using 3:11 title \"Averaged ob.s\" w lp ls 1, x title \"y=x\" ls 2
";

    #  --------- Now write the scaling model, based on the 1%  sample  -----------
    $smf = "$plotdir/scaling_model_for_${corpusName}_1.txt";
    die "Can't write to $smf\n"
	unless open SM, ">$smf";
    print SM "#scaling model for $corpusName collection based on a 1% sample
#  Replace SF wherever it occurs with the actual scale-up factor.
#  The argument names are those for generate_a_corpus_plus.exe

";


  # Let's recalculate slope and y intercept of the alpha plot by drawing a line between the
  # 1% and 100% values .  Slope is based on current x value being 1
  $slope_za2 = ($sample100[7] - $sample1[7]) / 99.0;
  $y_intercept = $sample1[7] - $slope_za2;  # Y axis is 1 unit away.
  print "\n\nSlope_za2 = $slope_za2. Sample1[7] =  $sample1[7] ===> y_intercept = $y_intercept\n\n";

     
    
    print SM "-synth_postings= $sample1[4]*SF   # simple linear, zero intercept, unit slope
-synth_vocab_size=$sample1[3]*SF**$slope_vs     # power law
-synth_doc_length=$sample1[11]                  # constant
-synth_doc_length_stdev=$sample1[12]            # constant
-synth_dl_gamma_shape=$sample1[24]              # Assumed constant
-synth_dl_gamma_scale=$sample1[25]              # Assumed constant
-zipf_alpha=$sample1[7];$slope_za2              # linear  -- current-y-value semicolon slope
-zipf_tail_perc=$sample1[5]                     # constant.
-head_term_percentages=";                       # list of constants
  for ($v = 14; $v <= 23; $v++) {
    print SM "," unless $v == 14;
    print SM $sample1[$v];
  }
print SM "

#bigram_alpha=$sample1[8]*SF**$alf_bigal             # power law
#bigrams_highest_freq=$sample1[10]*SF                # simple linear, zero intercept, unit slope
#bigrams_tot_signif=$sample1[9]*SF**$alf_sigbigs     # power law
";
    close(SM);

#  --------- Now write the scaling model, based on the 10%  sample  -----------

    $smf = "$plotdir/scaling_model_for_${corpusName}_10.txt";
    die "Can't write to $smf\n"
	unless open SM, ">$smf";
    print SM "#scaling model for $corpusName collection based on a 10% sample
#  Replace SF wherever it occurs with the actual scale-up factor.
#  The argument names are those for generate_a_corpus_plus.exe

";

  # Let's recalculate slope and y intercept of the alpha plot by drawing a line between the
  # 10% and 100% values   Slope is based on current x value being 1.
  $slope_za2 = ($sample100[7] - $sample10[7]) / 9.0;
  $y_intercept = $sample10[7] - $slope_za2;  # Y axis is considered one unit away.
  print "\n\nSlope_za2 = $slope_za2. Sample1[7] =  $sample1[7] ===> y_intercept = $y_intercept\n\n";


    print SM "-synth_postings= $sample10[4]*SF   # simple linear, zero intercept
-synth_vocab_size=$sample10[3]*SF**$slope_vs     # power law
-synth_doc_length=$sample10[11]                  # constant
-synth_doc_length_stdev=$sample10[12]            # constant
-synth_dl_gamma_shape=$sample10[24]              # Assumed constant
-synth_dl_gamma_scale=$sample10[25]              # Assumed constant
-zipf_alpha=$sample10[7];$slope_za2              # linear  -- need current-y-value semicolon slope
-zipf_tail_perc=$sample10[5]                     # constant.
-head_term_percentages=";                        # list of constants
  for ($v = 14; $v <= 23; $v++) {
    print SM "," unless $v == 14;
    print SM "$sample10[$v]";
  }
print SM "

#bigram_alpha=$sample10[8]*SF**$alf_bigal             # power law
#bigrams_highest_freq=$sample10[10]*SF                # simple linear, zero intercept
#bigrams_tot_signif=$sample10[9]*SF**$alf_sigbigs     # power law
";

  
    close(SM);

#  --------- Now write the scaling model, based on the 50%  sample  -----------
# Note that all the scaling constants and exponents are derived from the
# one percent model. That's probably not right.
    $smf = "$plotdir/scaling_model_for_${corpusName}_50.txt";
    die "Can't write to $smf\n"
	unless open SM, ">$smf";
    print SM "#scaling model for $corpusName collection based on a 50% sample
#  Replace SF wherever it occurs with the actual scale-up factor.
#  The argument names are those for generate_a_corpus_plus.exe

";

  # Let's recalculate slope and y intercept of the alpha plot by drawing a line between the
  # 50% and 100% values   Slope is based on current x value being 1.
  $slope_za2 = ($sample100[7] - $sample50[7]);
  $y_intercept = $sample50[7] - $slope_za2 ;  # Y axis is assumed 1 unit away.
  print "\n\nSlope_za2 = $slope_za2. Sample1[7] =  $sample1[7] ===> y_intercept = $y_intercept\n\n";
  

      print SM "-synth_postings= $sample50[4]*SF   # simple linear, zero intercept, unit slope
-synth_vocab_size=$sample50[3]*SF**$slope_vs     # power law
-synth_doc_length=$sample50[11]                  # constant
-synth_doc_length_stdev=$sample50[12]            # constant
-synth_dl_gamma_shape=$sample50[24]              # Assumed constant
-synth_dl_gamma_scale=$sample50[25]              # Assumed constant
-zipf_alpha=$sample50[7];$slope_za2              # linear  -- current-y-value semicolon slope
-zipf_tail_perc=$sample50[5]                     # constant.
-head_term_percentages=";                        # list of constants
  for ($v = 14; $v <= 23; $v++) {
    print SM "," unless $v == 14;
    print SM "$sample50[$v]";
  }
print SM "

#bigram_alpha=$sample50[8]*SF**$alf_bigal             # power law
#bigrams_highest_freq=$sample50[10]*SF                # simple linear, zero intercept, unit slope
#bigrams_tot_signif=$sample50[9]*SF**$alf_sigbigs     # power law
";
    
    close(SM);

print "\nScaling models derived from 1%, 10% and 50% samples or subsets:

    cat $plotdir/scaling_model_for_${corpusName}_1.txt;
    cat $plotdir/scaling_model_for_${corpusName}_10.txt;
    cat $plotdir/scaling_model_for_${corpusName}_50.txt;

";


}
