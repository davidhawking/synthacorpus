#! /usr/bin/perl - w

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license.


# Try to synthesize a text corpus with properties as close as possible
# to a real one, based on a sample of the latter.   We assume that 
# samplingExperiments.pl from this directory has been run for 
# the corpus whose name C is given to this script and it has
# produced datafiles SamplePlots/scaling_model_for_C_P.txt, recording 
# parameter values extracted from P% samples and 
# models of how those parameters grow when the collection is scaled
# up by a scaling factor SF.

# Iff the -compareWithBase option is given, we try to compare the scaled-up sample
# with the original base corpus.

# Scripts are now called via $^X in case the perl we want is not in /usr/bin/
 
$perl = $^X;
$perl =~ s@\\@/@g;

use Cwd;
$cwd = getcwd;
if ($cwd =~ m@/cygdrive@) {
    $cwd =~ s@/cygdrive/([a-zA-Z])/@$1:/@;
}

$|++;

die "Many SynthaCorpus scripts require that the environment variable $SC_EXPERIMENTS_ROOT be set
to the name of a directory under which all experimental data will be stored.  It will
need to have enough free space to accommodate the experiments you wish to conduct.

If you use Bash you may need something like:   

     export SC_EXPERIMENTS_ROOT=/opt/local/BigData/Experiments

"
  unless defined($ENV{SC_EXPERIMENTS_ROOT});

$experimentRoot = $ENV{SC_EXPERIMENTS_ROOT};


die "Usage: $0 <corpusName> <scalingModel> <scalingFactor> <subDir> <sampleCorpusDir> <tf_model> <doc_length_model> <term_repn_model> <dependence_model> [-compareWithBase]
    <corpusName> is a name (e.g. ap) not a path.  When comparing we expect to find a single file called
          <corpusName>.trec or <corpus_name>.starc under the directory ../Experiments/Emulation
    <scalingModel> is the name of a scalingModelfile produced by samplingExperiments.pl
    <scalingFactor> is a number representing how many times larger than the original is the 
          corpus to be generated.
    <subDir> is the name of the subdirectory of Scaling into which the generated corpus will be written.
    <sampleCorpusDir> is the name of the directory, e.g. $experimentRoot/Sampling/Sample/1%
    <tf_model> ::= Piecewise|Linear.   *** Unfortunately Piecewise is not implemented yet ***
          If Piecewise we'll use a 3-segment term-frequency model with 10 headpoints, 10 linear
          segments in the middle and an explicit count of singletons.  If Linear we'll approximate
          the whole thing as pure Zipf.
    <doc_length_model> ::= dlnormal|dlgamma 
    <term_repn_model> ::= tnum|base26|base26s|bubble_babble|simpleWords|markov-9e?
          The Markov order is specified by the single digit, represented by '9'.
          If present, the 'e' specifies use of the end-of-word symbol.  Otherwise
          a random length will be generated for each word and it will be cut off there.
          The Markov model will be trained on the base corpus.
          Recommended: markov-5e
    <dependence_model> ::= ind|ngrams[2-5]|bursts|coocs|fulldep
          Currently, only ind(ependent) is implemented.  Ind means that words are 
          generated completely independently of each other.  Fulldep means ngrams + bursts 
          + coocs.  Dependence models are only applied if the relevant files, i.e ngrams.termids,
          bursts.termids, coocs.termids, are available for the base corpus. 
\n"

    unless $#ARGV >= 7 ;

print "\n*** $0 @ARGV\n\n";

$plotter = `which gnuplot 2>&1`;
chomp($plotter);
if ($plotter =~/^which: no/) {
  undef $plotter;
} else {
  $plotter = "gnuplot";   # Rely on it being in our path
}

$markov_defaults = "-markov_lambda=0 -markov_model_word_lens=TRUE -markov_use_vocab_probs=FALSE -markov_full_backoff=FALSE -markov_assign_reps_by_rank=TRUE -markov_favour_pronouncable=TRUE";   # 11 Feb 2020:  Confirmed these are the same as in emulateARealCorpus.pl

$corpusName = $ARGV[0];
$scalingModelFile = $ARGV[1];   # E.g. ../Experiments/Sampling/Sample/scaling_model_for_ap_1.txt 
$sf = $ARGV[2];                 # Scale-up factor
$subDir = $ARGV[3];             # Subdirectory (e.g. GS-50-2) of ../Experiments/Scalingup/
$sampleCorpusDir = $ARGV[4];    # Directory containing a sampled corpus, e.g. ../Experiments/Sampling/Sample/1%.  It's needed for the markov-? training vocab.
$compareWithBase = 0;

for ($a = 5; $a <=$#ARGV; $a++) {
    if ($ARGV[$a] eq "dlnormal") { $dlModel = $ARGV[$a];}
    elsif ($ARGV[$a] eq "dlgamma") { $dlModel = $ARGV[$a];}
    
    elsif ($ARGV[$a] eq "tnum") { $trModel = $ARGV[$a];}
    elsif ($ARGV[$a] eq "base26") { $trModel = $ARGV[$a];}
    elsif ($ARGV[$a] eq "base26s") { $trModel = $ARGV[$a];}
    elsif ($ARGV[$a] eq "bubble_babble") { $trModel = $ARGV[$a];}
    elsif ($ARGV[$a] eq "simpleWords") { $trModel = $ARGV[$a];}
    #elsif ($ARGV[$a] eq "from_tsv") { $trModel = $ARGV[$a];}   # Doesn't make sense
    elsif ($ARGV[$a] =~ /^markov-[0-9]e?/) { $trModel = $ARGV[$a];}

    elsif ($ARGV[$a] eq "Piecewise") {
	print "\nUnfortunately Piecewise scale-up is not yet implemented.  Falling back to Linear\n\n";
	$syntyp = "Linear";
    } 
    elsif ($ARGV[$a] eq "Linear") {$syntyp = $ARGV[a];}
    
    elsif ($ARGV[$a] eq "ind") {$depModel = "ind";}
    elsif ($ARGV[$a] =~ /^ngrams([2-5])$/) {
      print "\n Unfortunately ngrams are not yet implemented in $0.  Falling back to ind\n\n";
      $depModel = "ind";
      #$ngram_degree = $1; $depModel = "ngrams$ngram_degree";
    }
    elsif ($ARGV[$a] eq "bursts") {
	print "\nUnfortunately \"bursts\" is not yet implemented.  Falling back to ind\n\n";
	$depModel = "ind";
    }
    elsif ($ARGV[$a] eq "coocs") {
	print "\nUnfortunately \"coocs\" is not yet implemented.  Falling back to ind\n\n";
	$depModel = "ind";
    }
    elsif ($ARGV[$a] eq "fulldep") {
      print "\nUnfortunately \"fulldep\" is not yet implemented.  Falling back to ind\n\n";
      $depModel = "ind";
    }
    elsif ($ARGV[$a] =~ /compareWithBase/i) {
      $compareWithBase = 1;
    }
    else {die "Unrecognized argument $ARGV[$a]\n";}
}

die "No document length model specified\n"
    unless defined($dlModel);

die "No term representation model specified\n"
    unless defined($trModel);

die "No term frequency distribution model specified\n"
    unless defined($syntyp);

die "No term dependence model specified\n"
    unless defined($depModel);

$emuMethod = "${trModel}_${dlModel}_${depModel}";

$top_terms = 25;

die "Experiment root $experimentRoot must exist\n"
  unless -d $experimentRoot;

$baseDir = "$experimentRoot/Base";
die "Base directory $baseDir must exist\n"
  unless -d $baseDir;

die "Can't read ScalingModelFile $scalingModelFile\n" 
  unless open SMF, $scalingModelFile;

# The executables and scripts we need
$generator = check_exe("corpusGenerator.exe");
$comparator = check_exe("compareBaseWithMimic.pl");


$experimentDir = "$experimentRoot/Scalingup/$subDir";
mkdir $experimentDir unless -d $experimentDir;
# Let's use the same directory for everything, as we do in Emulations
$workDir = $experimentDir;
$plotDir = $experimentDir;

print "
------------------------- Settings Summary ----------------------------
Output directories: $workDir
Term frequency model: $syntyp
Document length model: $dlModel
Term representation model: $trModel
Term dependence model: $depModel
Corpus name: $corpusName
-----------------------------------------------------------------------

";


$genOpts = "-file_synth_docs='$workDir'/${corpusName}.trec -synth_term_repn_method=$trModel";
if ($trModel =~ m@markov@i) {
  # We have to train the Markov vocabulary from a vocab.tsv file.   Is it OK to use the one from the Base?  No!  We should use
  # the vocab from the sample.
  # .. but scaling up by a large factor, say 100, causes the word generation to bog down unless we introduce some smoothing.
  $genOpts .= " -synth_input_vocab='$sampleCorpusDir'/${corpusName}_vocab.tsv -markov_lambda=0.02";
}
$gencmd = "$generator $genOpts";

while (<SMF>) {
    next if (/^\s*#/);   # Skip comment lines
    next if (/^\s*$/);   # Skip blank lines
    chomp;
    s/#.*//;  # Strip trailing comments
    if (m@(.*?=)(.*)@) {
	$attr = $1; 
	$rawval = $2;

	# Only generate arguments for the doc length model we are using.
	next if (($dlModel eq "dlgamma") && ($attr =~ /doc_length/));    # ----------------->
	next if (($dlModel eq "dlnormal") && ($attr =~ /dl_gamma_/));	  # ----------------->
	
	# There are six types of model: power law, simple linear (zero intercept), linear, list of linear, constant, and list of constants
	# Pattern match to distinguish them and set $val accordingly.
	# Note that when the model is linear with non-zero intercept, i.e. y = mx + c, we can calculate
	# the value of ys for s*x as ys = s * y - (s -1) * c
	if ($rawval =~ m@([\-0-9.]+)\s*\*\s*SF\s*\*\*([\-0-9.]+)@) {
	    # Power law
	    $const = $1;
	    $val = sprintf("%.4f", $1 * ($sf**$2));
	} elsif ($rawval =~ m@([\-0-9.]+)\s*\*\s*SF@) {
	  # Simple linear  -- constant * SF
	  $val = sprintf("%.4f", $1 * $sf);
	} elsif ($rawval =~ /,/) {
	  if ($rawval =~/;/) {
	    # Comma separated list of linear elements
	    $val = scaleUpCommaSeparatedList($rawval, $sf);
	  } else {
	    # Comma separated list of constants.
	    $val = $rawval;
	  }
	} elsif ($rawval =~ /([\-0-9.]+);([\-0-9.]+)/) {
	  # Linear  current-y-value semicolon slope based on the assumption that the current x value is 1.
	  $val = linearScale($1, $2, $sf);
	  print "$attr:  $1, $2, $sf ---> $val\n";
	} elsif ($rawval =~ m@([\-0-9.]+)@) { 
	  # Constant
	  $val = $1;
 	} else {
	    print "Unrecognized value expression: $rawval\n";
	    exit(1);
	}
	$gencmd .=" $attr$val";
    } else {
	print "Erroneous line in $scalingModelFile:

$_
";
	exit(1);
    }
   
}
close(SMF);

print "\nGenerator command is: $gencmd\n";
sleep(3);  # Give people an instant to read the settings.


####################################################################################################
#                                   Now generate the scaled-up corpus                              #
####################################################################################################


$cmd = "$gencmd > '$workDir'/${corpusName}_generator.log\n";
print $cmd, "\n";
$code = system($cmd);
die "$generator failed in directory $workDir with code $code.\n" 
    if $code;

print "\n\n  ----- Corpus generated -----\n\n";

if (! $compareWithBase) {
    printf "Haven't asked for a comparison with Base corpus.   Properties will not be extracted.

                        Farewell cruel world!\n\n";
    exit(0);
}

####################################################################################################
#                              Preparing to compare Base v. scaled-up Mimic                        #
####################################################################################################

print "We're going to try to do a comparison.  ";

# Assume that the properties of the base index have been extracted into
# $baseDir/${corpusName}_* 

# Extract some stuff from the Base _summary.txt  (for comparison with the generated one)

$sumry = "$baseDir/${corpusName}_summary.txt";
die "Can't open $sumry\n" unless open S, $sumry;

while (<S>) {
    next if /^\s*#/;  # skip comments
    if (/(\S+)=(\S+)/) {
	$base{$1} = $2;
    }
}
close(S);
    


####################################################################################################
#           Now extract the properties of the Scaled_Up index and compare with Base                #
####################################################################################################

$cmd = "$comparator $corpusName '$experimentDir' 2 2";
print $cmd, "\n";
system($cmd);
die "Command $cmd failed.\n" if $?;

exit 0;

#----------------------------------------------------------------------

sub check_exe {
  # The argument is expected to be the name of either a
  # perl script or an executable.  For a perl script we check that
  # a script of that name exists in the current directory.  If it
  # does we convert its name into an absolute path and return the
  # command to run it using the perl interpreter which invoked us.
  #
  # In the case of an EXE, we look in the current directory.
  #
  # Error exit if we don't find what we want.
  
  my $exe = shift;
  if ($exe =~ /\.pl$/) {
    die "$exe doesn't exist.\n"
      unless -r $exe;
    return "$perl '$exe'" if ($exe =~ m@/@);  # It was a path, not a name.
    return "$perl './$exe'";
  } else {
    if (-x $exe) {
      print "Is executable: $exe\n";
      return $exe if ($exe =~ m@/@);  # It was a path, not a name.
      return "./$exe";
    } else {
      die "$exe doesn't exist or isn't executable\n";
    }
  }
}


sub scaleUpCommaSeparatedList {
  my $list = shift;
  my $sf = shift;
  my @l = split /,/, $list;
  #print "scaleUpCommaSeparatedList() called with $list\n";
  my $rlist = "";
  foreach my $le (@l) {
    ($y1, $c) = $le =~ m@([\-0-9.]+);([\-0-9.]+)@;  #Each value in the list should be current-y-value semicolon slope based on current_x_value = 1
    #print " ??? $y1  --- $c\n";
    my $y2 = linearScale($y1, $c, $sf);
    $rlist .= "," unless $rlist eq "";
    $rlist .= sprintf("%.4f", $y2);
  }
  return $rlist;
}

sub linearScale {
  # y1 is the y value of a point on a line y = mx + c.  It is assumed that x1, corresponding to y1 is 1.
  my $y1 = shift;
  my $m = shift;
  my $s = shift;   # scale factor
  my $y2 = $y1 + $m * ($s - 1);

  print "  -- linearScale($y1, $c, $s) -- $y2\n";
  return $y2;
}
  
