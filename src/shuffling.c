// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>  // For memcpy

#include "definitions.h"
#include "utils/general.h"
#include "utils/randomNumbers.h"
#include "corpusGenerator.h"
#include "shuffling.h"

long long random_long_long(long long min, long long max) {
  long long range = max - min, rezo;
  double r, drange = (double) (range + 1);
  if (max < min || range < 1) {
    printf("Error: invalid range %lld to %lld in random_long_long()\n",
	   min, max);
    exit(1);
  }

  if (0) printf("RLL(%lld, %lld)\n", min, max);
  r = rand_val(0);  //  0 <= r < 1, rand_val is a front end to TinyMT64
  rezo = min + floor(r * drange);
  if (0) printf("R_L_L: r = %.6f, drange = %.3f, min = %lld, max = %lld ---> %lld\n",
		r, drange, min, max, rezo);
  if (rezo < min || rezo > max) {
    printf("Error: result %lld out of range %lld to %lld in random_long_long()\n",
	   rezo, min, max);
  }
    return rezo;
}


void knuth_shuffle(void *array, size_t elt_size, long long num_elts) {
  // Shuffle an array of elements, where an element can be any 
  // size between 1 and 8 bytes.
  // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
  if (elt_size > 8 || elt_size < 1) {
    printf("Error: Elt size must be between 1 and 8 in knuth_shuffle(); it was %zd\n",
	   elt_size);
    exit(1);
  }
  byte *bi, *bj, t;
  long long b, i, j, l, m;

  if (0) printf("Knuth_shuffle(elt_size = %zd, num_elts = %lld)\n", elt_size, num_elts);
  
  if (array == NULL || num_elts < 2) {
    printf("knuth_shuffle(): nothing to be done\n");
    return;
  }
  
  l = num_elts - 2;
  m = l + 1;
  for (i = 0;  i < l; i++) {
    // We're going to swap the i-th element with a random element whose index j > i
    j = random_long_long(i + 1, m);
    // Need to turn them into byte addresses because the entries may be of any length
    bi = (byte *)array + (i * elt_size);
    bj = (byte *)array + (j * elt_size);
    // swap byte at a time -  inefficient, particularly for elt_sizes 8 and 4
    if (0) printf("Swapping %lld with %lld. \n", i, j);
    for (b = 1; b <= elt_size; b++) {
      t = *bi;
      *bi = *bj;
      *bj = t;
      bi++;
      bj++;
    }
  }
}


void knuth_shuffle_uint(u_int *array, long long num_elts) {
  // Shuffle an array of elements, where an element is assumed to be a u_int
  // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
  // For num_elts = 30 million on a high-end laptop, using this function
  // rather than the byte by byte one, reduced runtime of the shuffle from
  // around 4.7 sec to around 3.6 sec.
  long long i, j, l, m;
  u_int t;
  if (0) printf("Knuth_shuffle_uint(num_elts = %lld)\n",num_elts);
  
  if (array == NULL || num_elts < 2) {
    printf("knuth_shuffle(): nothing to be done\n");
    return;
  }
  
  l = num_elts - 2;
  m = l + 1;
  for (i = 0;  i < l; i++) {
    // We're going to swap the i-th element with a random element whose index j > i
    j = random_long_long(i + 1, m);
    t = array[i];
    array[i] = array[j];
    array[j] = t;
  }
}

#if 0
void knuth_shuffle_uint_respect_phrases(u_int *array, long long num_elts) {
  // Shuffle an array of elements, where an element is assumed to be a u_int
  // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
  //
  // In this version, we recognize that array to be shuffled may contain
  // phrases.  A phrase is a sequence of u_uints in which the first has
  // the SON_FLAG set and the rest have the CON_FLAG set. (SON - Start of Ngram,
  // CON - continuation of Ngram.)
  //
  // Unfortunately, the current implementation doesn't reflect the needed complexity.
  // It handles the case where the methodically selected element is a phrase, but the
  // randomly picked one is not.  The code needs to be able to handle, swapping:
  // a. word with word
  // b. n-gram with n words
  // c. n-gram with n-gram
  // d. n-gram with m-gram
  // e. all sorts of horrible complications.
  
  long long i, j, k, l, m, limit;
  int gramlen, sonFlagsSeen = 0;
  u_int t;
  
  if (array == NULL || num_elts < 2) {
    printf("knuth_shuffle(): nothing to be done\n");
    return;
  }
  
  l = num_elts - 2;
  m = l + 1;
  // Loop over all the items up to the second last.
  for (i = 0;  i < l; i++) {
    // We're going to swap the i-th element with a random element whose index j > i
    // unless the i-th element has the CON_FLAG set.   If it has the SON_flag set,
    // we have to swap a group of n u_ints after first checking whether the n bytes
    // from the j-th either constitute a phrase OR include no part of a phrase.
    if (array[i] & CON_FLAG) continue;  // ----------------->
    if (array[i] & SON_FLAG) {
      sonFlagsSeen++;
      gramlen = 1;
      k = i + 1;
      while (array[k] & CON_FLAG) {k++; gramlen++;}
      // We have to swap the current gramlen u_ints with gramlen u_ints somehwere beyond the
      // end of this ngram (the swapper), but that may not be possible
      limit = m - gramlen + 1;  // limit is now the last point at which a swappee could start
      if (i + gramlen > limit) break;      // ==================>  // Can't swap with part of itself or beyond limit
      if (i + gramlen == limit) {
	j = limit;
	for (k = 0; k < gramlen; k++) {
	  if (array[j + k] & NGRAM_FLAGS) {
	    // Not sure whether to retry or to just give up
	    // For the moment, let's give up.
	    gramlen = 0;  // Signal the give-up
	    printf("Giving up 1..........................\n");
	    break;
	  }
	}
      } else {
	int inner_retry_limit = 100, flag_found = 0;
	while (inner_retry_limit-- > 0) {
	  j = random_long_long(i + gramlen, limit);
	  // Now check that none of the words in swappee has a flag set.
	  flag_found = 0;
	  for (k = 0; k < gramlen; k++) {
	    if (array[j + k] & NGRAM_FLAGS) {
	      flag_found = 1;
	      break;
	    }
	  }
	  if (!flag_found) break;  // ---------> out of inner retry loop
	  if (0) printf(" ^^^^ failed for j = %lld\n", j);
	}
	if (flag_found) {
	  gramlen = 0;  // Signal the give-up
	  printf("Giving up 2 after looping ..........................\n");
	}	      
      }

      if (gramlen) {
	for (k = 0; k < gramlen; k++) {   
	  t = array[i + k];
	  array[i + k] = array[j];   
	  array[j++] = t;
	}
	i += (gramlen -1);  // Loop will increment it once more.
      }
    } else {
      // This is the normal case, but we must check that array[j] is not
      // part of an n-gram.  If it is, we just skip
      if (i + 1 == m) j = m;
      else j = random_long_long(i + 1, m);
      if (array[j] & NGRAM_FLAGS) continue;  // -------------->
      t = array[i];
      array[i] = array[j];
      array[j] = t;
    }
  }    // End of for i loop

  if (0) printf("Knuth_shuffle_uint_respect_phrases(num_elts = %lld), %d phrases encountered.\n",
		num_elts, sonFlagsSeen);
}

#endif

void knuth_shuffle_uint_respect_phrases(u_int *array, long long num_elts) {
  // Shuffle an array of elements, where an element is assumed to be a u_int
  // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
  //
  // In this version, we recognize that array to be shuffled may contain
  // phrases.  A phrase is a sequence of u_uints in which the first has
  // the SON_FLAG set and the rest have the CON_FLAG set. (SON - Start of Ngram,
  // CON - continuation of Ngram.)
  // This is a much slower and space-consumptive version of the incorrect one above.
  // It:
  //     1. counts how many terms (words or compounds) there are in array.  num_terms
  //     2. creates a permutation array permute[num_terms] containing the start index of each term in array.
  //     3. shuffles the permutation array using knuth_shuffle_uint()
  //     4. creates a tmpCopy array tmpCopy[num_elts]
  //     5. transfers the terms from array into the next free spot in tmpCopy in permuted order.
  //     6. copies tmpCopy back into array
  //     7. frees the space for tmpCopy and permute

  u_int *permute = NULL, *tmpCopy = NULL;
  int i, j, p, t, num_terms = 0;

  // Step 1
  for (i = 0; i < num_elts; i++) {
    if (!(array[i] & CON_FLAG)) num_terms++;
  }
  if (0) printf("Num_terms = %d, num_elts = %lld\n", num_terms, num_elts);
  
  // Step 2  -- Note: cmalloc zeroes memory.
  permute = (int *)cmalloc(num_terms * sizeof(u_int), "k-s-u-r-p permute", 0);
  num_terms = 0;
  for (i = 0; i < num_elts; i++) {
    if (!(array[i] & CON_FLAG)) {
      permute[num_terms++] = i;
    }
  }
  if (0) printf("  Num_terms+ = %d\n", num_terms);

  // Step 3
  knuth_shuffle_uint(permute, num_terms);


  // Step 4
  tmpCopy = (u_int *) cmalloc(num_elts * sizeof(u_int), "k-s-u-r-p tmpCopy", 0);

  // Step 5
  i = 0;  // Index in array
  j = 0;  // Index in tmpCopy
  t = 0;  // Index in permute
  for (t = 0; t < num_terms; t++) {
    p = permute[t];
    if (array[p] & SON_FLAG) {
      tmpCopy[j++] = array[p++];
      while (array[p] & CON_FLAG) tmpCopy[j++] = array[p++];
    } else if (array[p] & CON_FLAG) {
      printf("Error in knuth_shuffle_uint_respect_phrases():  Aaargh!  CON_FLAG encountered.\n");
      exit(1);
    } else {
      tmpCopy[j++] = array[p];
    }
  }
  free(permute);
  if (0) printf("  Num_elts = %lld\n", num_elts);

  // Step 6
  for (i = 0; i < num_elts; i++) array[i] = tmpCopy[i];
  free(tmpCopy);
}



void test_knuth_shuffle_uint_respect_phrases() {
  u_int ta[15] = {1|SON_FLAG, 2|CON_FLAG, 3|SON_FLAG, 4|CON_FLAG, 5|CON_FLAG, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
  int i, j, k, gramlen = 0, ngrams_found = 0;

  if (0) {
    printf("Test_ksurp_initial: ");
    for (j = 0; j < 15; j++) printf("%x, ", ta[j]);
    printf("\n");
  }
  // Shuffle many times and keep checking that the phrases are preserved
  for (i = 0; i < 500; i++) {
    knuth_shuffle_uint_respect_phrases(ta, 15);
    if (0) {
      printf("Test_ks_%d: ", i);
      for (j = 0; j < 15; j++) printf("%x, ", ta[j]);
      printf("\n");
    }
    ngrams_found  = 0;
    for (j = 0; j < 15; j++) {
      if (ta[j] & SON_FLAG) {
	ngrams_found++;
	k = j + 1;
	gramlen = 1;
	while (k < 15 && (ta[k] & CON_FLAG)) { k++; gramlen++;}
	if (((ta[j] & TERM_RANK_MASK) == 1) && gramlen != 2) {
	  // Term rank 1 is the start of a bigram
	  printf("Error: test_knuth_shuffle_uint_respect_phrases(): 2-gram mucked up. i = %d, k = %d, gramlen = %d\n  --- ", i, k, gramlen);
	  for (k = 0; k < 15; k++) printf("%d, ",ta[k] & TERM_RANK_MASK);
	  printf("\n\n");
	  exit(1);
	} else if (((ta[j] & TERM_RANK_MASK) == 3) && gramlen != 3) {
	  // Term rank 3 is the start of a trigram.
	  printf("Error: test_knuth_shuffle_uint_respect_phrases(): 3-gram mucked up. i = %d, k = %d, gramlen = %d  --- \n", i, k, gramlen);
	  for (k = 0; k < 15; k++) printf("%d, ",ta[k] & TERM_RANK_MASK);
	  printf("\n\n");
	  exit(1);
	}
      }
    }
    if (ngrams_found != 2) {
      printf("Error: test_knuth_shuffle_uint_respect_phrases(): SON_FLAG evaporated (or multiplied).\n  --- ");
      for (k = 0; k < 15; k++) printf("%d, ",ta[k]);
      printf("\n\n");
      exit(1);
    }
  }
  printf("Internal test of test_knuth_shuffle_uint_respect_phrases() passed.\n");
}






void test_knuth_shuffle() {
  int ta[15] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, i;
  byte tab[75], *bp, b;
  long long ll, ll2;
  knuth_shuffle(ta, 4, 15);
  printf("Test_ks_1: ");
  for (i = 0; i < 15; i++) printf("%d, ", ta[i]);
  printf("\n");

  bp = tab;
  for (i = 0; i < 15; i++) {
    // Represent fifteen integers in an array of 5-byte quantities
    ll = 100000000 + (long long)i;
    for (b = 0; b < 5; b++) {
      *bp++ = (byte)(ll & 0xFF);
      ll >>= 8;
    }
  }
  
  knuth_shuffle(tab, 5, 15);
  printf("Test_ks_2: ");
  
  bp = tab + 4;  // Need to reverse the order of the bytes
  for (i = 0; i < 15; i++) {
    ll = 0;
    for (b = 0; b < 5; b++) {
      ll <<= 8;
      ll2 = *bp--;
      ll |= ll2;
    } 
    bp += 10;
    printf("%lld, ", ll);
  }
  printf("\n");   
}


void light_shuffle(void *array, size_t elt_size, long long num_elts, int max_step) {
  // Shuffle an array of elements, where an element can be any 
  // size up to 1000 bytes.  In this funny version only a random fraction
  // of items are shuffled


  byte *bi, *bj, t;
  long long b, i, j, l, m;
  double start;
  start = what_time_is_it();
  
  if (array == NULL || num_elts < 2) {
    printf("funny_shuffle(): nothing to be done\n");
    return;
  }

  
  l = num_elts - 2;
  m = l + 1;
  if (max_step < 2) i = 0;
  else i = random_long_long(0, max_step - 1);
  while (i < l) {
    //for (i = s;  i < l; i += random_long_long(1, max_step)) {
    // We're going to swap the i-th element with a random element whose index j > i
    j = random_long_long(i + 1, m);
    // Need to turn them into byte addresses because the entries may be of any length
    bi = (byte *)array + (i * elt_size);
    bj = (byte *)array + (j * elt_size);
    // swap byte at a time -  inefficient, particularly for elt_sizes 8 and 4
    if (0) printf("Swapping %lld with %lld. \n", i, j);
    //memcpy(t, bi, elt_size);
    //memcpy(bi, bj, elt_size);
    //memcpy(bj, t, elt_size);
    // swap byte at a time -  inefficient, particularly for elt_sizes 8 and 4
    for (b = 1; b <= elt_size; b++) {
      t = *bi;
      *bi = *bj;
      *bj = t;
      bi++;
      bj++;
    }
    if (max_step < 2) i++;
    else i += random_long_long(1, max_step);
  }
  if (1) printf("Time taken for light shuffle: %.3f sec\n", what_time_is_it() - start);
}


