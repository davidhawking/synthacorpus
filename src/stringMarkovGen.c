// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                             stringMarkovGenerator.c                              //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Takes a training corpus C of documents in STARC, TSV or TREC format and builds an
// order k Markov model, based on letter sequences, then generates a random output corpus
// using the transition probabilities from the model.
//
// There are two methods for defining the alphabet of symbols used.  The default method is
// to do a preliminary scan of the text and find all the characters used.   Currently bytes
// with most significant bit set (UTF-8 or ISO-8859-x) are ignored, however it is planned
// to switch to assuming UTF-8 and counting non-ASCII UTF-8 letters as valid symbols. In the
// current implementation there is a limit of 79 on the number of distinct symbols, since
// internally they are mapped to ASCII characters '0' (decimal 48) to '~' (decimal 126).
// Increasing the size of the alphabet beyond 79 would result in a very large transition
// matrix.  In this method, ASCII punctuation, spaces and newlines in the input are
// accepted as symbols.
//
// The second method is activated when -predefinedAlphabet=TRUE is given.  In this case,
// the set of symbols comprises the case-folded ASCII letters plus three symbols representing
// enod of word, end of sentence and end of document.  Alphabet size is thus 29.
//
// The transition matrix is represented using a hash table to access the context string.
// Each entry in the hash table includes a total occurrence count plus an array of one
// single-byte counter for every possible following symbol.  There is one element in the
// array for each symbol in the alphabet, allowing random access during input scanning.
// As each acceptable byte is read from the training data, it is mapped to the corresponding
// internal symbol and that is trivially converted to an index in the counter array.
// Once a counter in the array reaches 255, an external dynamic array of int counters is created,
// a flag is set in the total occurrence count, and the byte array is partly over-written
// with an offset into the external counter array.
//
// During scanning of the input, the entries in the counter array, either internal or
// external, are in normal ASCII order.  I.e. the zeroth element of the counter array for
// the 3-symbol context 'fox' is a count of how many times the character which maps to the
// internal symbol '0' follows 'fox'.
// 
// After scanning is complete, counts are converted into an array of conditional
// probabilities.  The array of conditional probabilities consists of 4-byte entries,
// in which the most significant byte represents the internal symbol, and the remaining
// three bytes represent the cumulative probability in a 24-bit fixed-point representation.
// Internal symbols with zero probabilities are moved to the high end of the array.
//
// If the counts are still maintained as single bytes within the hashtable entry, and
// there are sufficiently few of them, the array of conditional probabilities can be
// written into the hashtable entry, over the top of the counters.  If there are too
// many, an external probability array must be created, a flag must be set and a pointer
// to the array must be written over the counters.
//
// The design of the data structures was motivated by the observation that a very small
// percentage of contexts have large numbers of possible successors and that most
// successor counts are very small.  In the initial simple implementation
// all the counts were ints internal to the hash table.  They were converted to floats to
// represent cumulative probabilities, and the array of floats was binary searched.
// In a test using WT10g, the pre-defined alphabet, lambda=0, wildcards=0 and k=10,
// the new, more complex arrangement proved to reduce overall runtime by a factor of 3.7
// and memory requirements by a factor of 2.6.  Of the 537 million entries (29 bits) in the
// hashtable, 318 million were used, and 5.15 million (less than 1% of available table
// entries) were promoted to use external blocks.  Two thirds of the promotions were
// due to the context having more than 8 successors.
//
// When generating from the finished model, a random number in the range 0 - 2^24 -1
// is generated and a linear scan used to find the corresponding internal symbol, which
// is converted to output using a reverse mapping array.   Linear scan, rather than
// binary search is used because the vast majority of contexts have very few successors.
// There is no need to explicitly store a count of the number of non-zero elements in
// the cumulative probability array since scanning stops once the cumulative probability
// reaches 1. (represented as 2^24 -1). A potential future speed up would be to sort the
// cumulative probability array by descending probability but gains would likely be small.
//
// WILDCARDS: wildcard versions of contexts, replace the leading characters in the context
// with asterisks. For example if we encountered a context of wildcard 'wildcard', followed
// by an 's' we might also add entries for '****card' followed by 's', '*****ard' followed
// by 's', '******rd' followed by 's', and '*******d' followed by 's'.  If during generation
// we encountered a context ' bollard' which we had never seen in training, we could fall
// back to the effectively shorter context '*****ard' and realise that it could be followed
// by an 's'.  The command line option -wildcards= specifies the minimum number of
// asterisks.   If k=8 and wildcards=4 as in the above example, wildcarded versions
// from the minimum number of asterisks up to k-1 asterisks will be stored.  If no
// wildcards option is given, it will default to k-2.  -wildcards=0 switches off
// wildcarding.  Obviously the smaller than value of the wildcards parameter, the
// more additional entries inserted in the hashtable, and the slower the process
// due to a multiplication in the number of insertions.
//
// There are three reasons for using wildcarding:
//   1. To deal with contexts not seen during training.
//   2. To reduce the effect of overfitting by introducing a small amount of noise,
//      lambda > 0
//   3. To overcome the inevitable undergeneration of vocabulary when emulating
//      a corpus.  lambda > 0.
//
// TRAINING ON PARTIAL DATA: 
// -trainingFraction=<proper-fraction>
// For each document in the training corpus, a probabilistic die is cast to decide
// whether this document is to be used in building the model.  This allows
// us to train from a sample of a corpus whose full model would be too large.
// Note that the entire corpus is used for learning the alphabet, so there is
// no chance of finding a valid symbol in training that wasn't encountered
// when learning the alphabet.
//
// SAVING/RESTORING A TRAINED MODEL:  
// -saveModelTo=<name-of-saved-model-file>
// -restoreModelFrom=<name-of-saved-model-file>
// Have changed the implementation of external arrays to use a dynamic
// array (ebMalloc) rather than cmalloc.  (Change pointers to offsets or indexes?)
// Save:
//   1. k
//   2. globals.NUM_SYMBOLS
//   3. the number of external blocks
//   4. globals.charMap and globals.rCharMap
//   5. the dahash_table_t structure 
//   6. the actual hash table
//   7. the dynamic array.


#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <sys/uio.h>
#include <unistd.h>

#include "../src/definitions.h"
#include "../src/utils/dahash.h"
#include "../src/utils/dynamicArrays.h"
#include "../src/characterSetHandling/unicode.h"
#include "../src/utils/general.h"
#include "../src/utils/argParser.h"
#include "stringMarkovGenArgTable.h"
#include "stringMarkovGen.h"
#include "../src/imported/TinyMT_cutdown/tinymt64.h"
#include "../src/utils/randomNumbers.h"
#include "../src/shuffling.h"


static u_char docCopy[MAX_DOC_LEN + 129], *docWords[MAX_DOC_WORDS];
static int externalBlockPromotions1 = 0;
static tinymt64_t tinymt;

globals_t globals;

params_t params;

static void initialiseGlobals(globals_t *globals) {
  int i;
  globals->startTime = what_time_is_it();
  globals->numDocs = 0;
  globals->numEmptyDocs = 0;
  globals->docsUsedForTraining = 0;
  globals->totalPostingsIn = 0;
  globals->wildcardStringsAdded = 0;
  globals->sentenceCount = 0;
  globals->gMarkovHash = NULL;
  globals->externalBlocks = NULL;
  globals->externalBlocksUsed = 0;
  globals->vocabOut = 0;
  globals->debug = 0;
  for (i = 0; i < 128; i++) {
    globals->charMap[i] = 0;
    globals->rCharMap[i] = 0;
    globals->EOSMap[i] = 0;
    globals->EOWMap[i] = 0;
  }
  
  // Flag all the characters which imply end of sentence.
  globals->EOSMap['.'] = 1;   // This will be problematic with acronyms.
  globals->EOSMap['!'] = 1;
  globals->EOSMap['?'] = 1;

  // Flag all the characters which imply end of word.
  globals->EOWMap[' '] = 1;
  globals->EOWMap['\n'] = 1;
  globals->EOWMap['\r'] = 1;
  
  
  globals->EOD = 0;  // It's set dynamically as the last internal symbol
  globals->NUM_SYMBOLS = 0;
  globals->NUM_SYMBOLS_RU = 0; // Smallest multiple of 4 at least as large as NUM_SYMBOLS  (RU - Rounded Up)
  globals->MAX_INPLACE_COUNTS = 0;
}

  int k, wildcards;  // Will be copied from params
  double startTime;
  long long numDocs, numEmptyDocs, totalPostingsIn, desiredTotalPostings,
    postingsOut, docsOut, vocabOut, wildcardStringsAdded, sentenceCount,
    docsUsedForTraining;
  dahash_table_t *gMarkovHash;
  dyna_t externalBlocks;
  int externalBlocksUsed;
  u_char *inputInMemory;
  byte *corpusInMemory;
  size_t inputSize;
  CROSS_PLATFORM_FILE_HANDLE inputFH;
  HANDLE inputMH;
  FILE *corpusOut, *simplifiedText;
  int debug;
  double veryStart, LATime, scanTime, finalisationTime, generationTime, generationRate;
  u_char charMap[128], rCharMap[128], EOSMap[128], EOWMap[128], EOD; 
  int NUM_SYMBOLS, // Size of the Markov alphabet
    NUM_SYMBOLS_RU, // Smallest multiple of 4 at least as large as NUM_SYMBOLS  (RU - Rounded Up)
    MAX_INPLACE_COUNTS // = NUM_SYMBOLS_RU / 4
;


static int allASCIILetters(u_char *str) {
  // Check that all the characters in the string are ASCII letters (return zero otherwise) and
  // convert upper to lower case.
  if (str == NULL || *str == 0 || *str == '\n') return 0;  // ------------->
  while (*str && *str != '\n') {
    if (*str > 'z') return 0;  // Eliminates any char with leading bit set --------------->  
    if (*str >= 'A' && *str <= 'Z') *str += ('a' - 'A');  // convert to lower
    else if (*str < 'a') return 0;   // Already tested > 'z'   -------------------------->
    str++;
  }
  return 1;    // -------------------------->
}


static void alphabetScan(globals_t *globals, u_char *docText, size_t docLen) {
  int i;
  u_char c;
  // Set a flag for each distinct ASCII character present.
  for (i = 0; i < docLen; i++) {
    c = docText[i];
    if ((! (c & 0x80)) && (c >= ' ' || c == '\n'))  // printable ASCII and newline only
      globals->charMap[c] = 1;
  }
}


static u_int *ebMalloc(size_t ebSize, u_char *msg, int verbose) {
  // Return a pointer to the next block of size ebSize in the dynamic array
  // globals.externalBlocks.
  u_int *rslt;
  rslt = (u_int *)dyna_get(&(globals.externalBlocks), globals.externalBlocksUsed++, DYNA_DOUBLE);
  if (rslt == NULL) {
    printf("Error: ebMalloc(%s): dyna_get failed\n", msg);
    exit(1);
  }
  return rslt;
}

long long totalOverflows = 0;   

static void updateCount(u_int *hashValueP, int successorIndex) {
  // Update overall count for this context and individual successor count
  // using the new reduced-memory method.
  if ((hashValueP[0] & COUNT_MASK) == COUNT_MASK && totalOverflows == 0) {
    totalOverflows++;
    printf("Warning:  Overflow in total count for %s.  Only possible to get to 2^31 -1.\n"
	   "  -- Subsequent overflows for any context will be ignored and the count will not be updated.\n",
	   (byte *)hashValueP - globals.gMarkovHash->key_size);
  }
  if (hashValueP[0] == COUNT_MASK) return;  // Reached 2^31.  Ignore this occurrence to avoid changing HIFREQFLAG
  hashValueP[0]++;    // Total occurrence count for this context string
  if (hashValueP[0] & HIFREQFLAG) {   // We're using a high frequency block
    u_int *HFBPtr = (u_int *)dyna_get(&(globals.externalBlocks), hashValueP[1], DYNA_DOUBLE);
    HFBPtr[successorIndex]++;
  } else {
    byte *counts = (byte *)(hashValueP + 1);
    if (counts[successorIndex] == 255) {  // Max poss. value in one byte
      // Have to switch over to using a high frequency block
      int i;
      u_int *tmp = ebMalloc(globals.NUM_SYMBOLS * sizeof(u_int), "High Frequency Block", 0);
      // Copy old counts into new block
      for (i = 0; i < globals.NUM_SYMBOLS; i++) {
	tmp[i] = (int)counts[i];  // Upgrade from byte to int
      }
      if (tmp[successorIndex] == CUMPROB_MASK) {
	printf("Error: Overflow in 24 bit count.\n");
	exit(1);
      }
      tmp[successorIndex]++;  // Update the relevant count
      // now overwrite the first 4 bytes with an entry number in the external blocks array
      hashValueP[1] = globals.externalBlocksUsed - 1;
      hashValueP[0] |= HIFREQFLAG;   // Set the flag so we know what's going on.
      externalBlockPromotions1++;
     } else {
      // Just increment the byte sized counter.
      counts[successorIndex]++;
    }
  }
}

static u_char simplifiedString[MAX_DOC_LEN + MAX_K + 2], *sSP = NULL;
static long long simplifiedDocNo = 0;


static int buildMarkovModelLA(globals_t *globals, u_char *docText, size_t docLen, int docNum) {
  // This is the Learned Alphabet version.  It takes no notice of words (doesn't use
  // utf8__split_line_into_null_terminated_words().  Instead it works with subsequences of
  // a version of the text from which all non-ASCII bytes have been stripped and in which
  // all the ASCII characters have been stripped.    This version currently doesn't write
  // a simplified file.
  int i, j, successorIndex;
  u_char *w, *context, successor, c;
  u_int *hashValueP;

  // MapCopy docText into docCopy after inserting k EOD symbols.
  for (i = 0; i < params.k; i++) docCopy[i] = globals->EOD;
  w = docCopy + params.k;
  for (i = 0; i < docLen; i++) {
    if (!(docText[i] & 0x80)) {
      // We're going to ignore all non-ASCII bytes
      if (globals->EOWMap[docText[i]]) globals->totalPostingsIn++;
      if (globals->EOSMap[docText[i]]) globals->sentenceCount++;
      c = globals->charMap[docText[i]];
      if (c > 0) *w++ = c;
    }
  }
  *w++ = globals->EOD;   // This is correct, the mapped version of doc end.
  *w = 0;
  docLen = w - (docCopy + params.k);

  if (0) printf("LA docCopy is %s\n", docCopy);
  
  // Now insert all the strings into the hashtable with their successors.
  for (i = 0; i < docLen; i++) {
    context = docCopy + i;
    successor = context[params.k];
    context[params.k] = 0; 
      
    // dahash_lookup() returns a pointer to the first byte of the value part of the entry.
    hashValueP = (u_int *)dahash_lookup(globals->gMarkovHash, context, 1);
    if (hashValueP == NULL) {
      printf("Error: buildMarkovModelLA(): lookup of %s in gMarkovHash failed\n", context);
      exit(1);
    }
    successorIndex = successor - BASE_SYMBOL;  // convert for use as an index into counts array
      
    updateCount(hashValueP, successorIndex);    
    if (globals->debug)  {
      printf("Appending %c to entry for %s\n",
	     successor, (byte *)hashValueP - globals->gMarkovHash->key_size);
      if (strlen(context) != params.k) {
	printf("Error: buildMarkovModelLA(): Wrong context length %zu\n", strlen(context));
	exit(1);
      }
    }
    
    // ----- If applicable, add wildcard versions of context -----
    if (params.wildcards > 0) {
      // Wildcarding works by replacing the leading part of the string with wildcards
      u_char context2[MAX_K + 1];
      strcpy(context2, context);
      for (j = 0; j < params.wildcards - 1; j++) context2[j] = WILDCARD;  // Fill in one less than the minimum number of leading wildcards
      for (j = params.wildcards - 1; j < params.k; j++) {  // Now fill in the rest, including all-wildcards
	context2[j] = WILDCARD;
	if (0) printf("Subsidiary context: '%s'\n", context2);
	hashValueP = (u_int *)dahash_lookup(globals->gMarkovHash, context2, 1);
	if (hashValueP == NULL) {
	  printf("Error: buildMarkovModelLA(): lookup of %s in gMarkovHash failed\n", context2);
	  exit(1);
	}
	if (hashValueP[0] == 0) {
	  if (0) printf("New wildcard entry %s added\n", context2);
	  globals->wildcardStringsAdded++;   // Count the distinct wildcardStrings
	}
	
	updateCount(hashValueP, successorIndex);    
	if (globals->debug)  
	  printf("Appending %c to [subsidiary] entry for %s\n",
		 successor, (byte *)hashValueP - globals->gMarkovHash->key_size);	
      }      
    }
      // --------------------------------------------------------------
      
    context[params.k] = successor;  // Putting back what we zeroed
  }  // End of loop over contexts
  return 0; // ------------------------------------->
}


static int buildMarkovModel(globals_t *globals, u_char *docText, size_t docLen, int docNum) {
  // This function adds all the information from a single document represented by docText
  // to the Markov transition matrix (at this stage containing frequencies rather than
  // transition probabilities.)
  // If we are using a learned rather than a pre-defined alphabet, we call a simpler variant
  // of this function buildMarkovModelLA();
  int numWords, w, i, successorIndex;
  u_int *hashValueP;
  u_char *context, successor, *wP;
  size_t filteredTextLen;


  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.

  if (params.trainingFraction < 1.0) {
    // Throw a die to see if we're going to process this document.
    if (tinymt64_generate_double(&tinymt) > params.trainingFraction) return 0;  // ----------------->
  }
  globals->docsUsedForTraining++;
  if (docLen > MAX_DOC_LEN) docLen = MAX_DOC_LEN;
  if ((docText[docLen] & 0xC0) == 0x80) {
    do {
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80 && docLen > 0);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }

  if (params.predefinedAlphabet) {
    // ----- Using the predefined lower-case ASCII alphabet. -----
    // Copy document text so we can write on it
    memcpy(docCopy, docText, docLen);
    docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
    if (globals->debug) {
      printf("stringMarkovModel: len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n",
	     docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	     globals->inputSize - 1);
      //put_n_chars(docCopy, 100);
      printf("%s", docCopy);
      printf("\n\n\n");
    }
    
    numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							  MAX_DOC_WORDS, MAX_WORD_LEN,
							  TRUE,  // case-fold line before splitting
							  FALSE, // remove accents before splitting
							  FALSE,  // Perform some heuristic substitutions 
							  FALSE   // True if words must contain an ASCII alnum
							  );
    
    if (numWords <= 0) {
      globals->numEmptyDocs++;
      return 0;  // ----------------------------->
    }
    
    // For ease of processing, copy the words, plus EOW, EOS and EOD symbols into a character array.  But first start with k EODs
    sSP = simplifiedString;
    for (i = 0; i < params.k; i++) *sSP++ = EOD_IN;
    
    for (w = 0; w < numWords; w++) {
      if (allASCIILetters(docWords[w])) {   // Side Effect: allASCIILetters converts upper to lower case 
	wP = docWords[w];
	while (*wP  && *wP != '\n') {
	  *sSP++ = *wP++;
	}
	if (*wP == '\n') {
	  *sSP++ = '\n';
	  globals->sentenceCount++;
	} else {
	  *sSP++ = ' ';
	}
	globals->totalPostingsIn++;
      }
    }
    *sSP++ = EOD_IN;   // Must use the character which will be mapped to globals->EOD
    *sSP = 0;
    filteredTextLen = sSP - simplifiedString;
    if (0) printf("filteredTextLen = %zu, String is:\n%s\n", filteredTextLen, simplifiedString);

    // If requested write the simplified version of the input text  -- used to check sentence overlap
    if (globals->simplifiedText != NULL) {
      fprintf(globals->simplifiedText, "<DOC>\n<DOCNO>DOC%lld</DOCNO>\n<TEXT>\n", ++simplifiedDocNo);
      sSP = simplifiedString + params.k;
      while (*sSP && *sSP != EOD_IN) {
	sSP++;
      }
      *sSP = 0;
      fprintf(globals->simplifiedText, "%s\n</TEXT>\n</DOC>\n", simplifiedString + params.k);
      *sSP = EOD_IN;
    }

    // Map all the characters in the simplified string.
    for (i = 0; i < filteredTextLen; i++) {
      if (globals->charMap[simplifiedString[i]] < BASE_SYMBOL || globals->charMap[simplifiedString[i]] >= BASE_SYMBOL + globals->NUM_SYMBOLS) {
	printf("Error: No mapping for '%c' (%d) in simplifiedString[%d]\n", simplifiedString[i], simplifiedString[i], i);
	exit(1);
      }
      simplifiedString[i] = globals->charMap[simplifiedString[i]];
    }
    
    // Now rip through the mapped string and add to the transition matrix
    for (i = 0; i < filteredTextLen - params.k; i++) {
      context = simplifiedString + i;
      successor = context[params.k];
      context[params.k] = 0;
      if (successor < BASE_SYMBOL || successor >= BASE_SYMBOL + globals->NUM_SYMBOLS) {
	printf("Error: buildMarkovModel(%d / %zu): Invalid successor '%c' context %s\n", i, filteredTextLen, successor, context);
	exit(1);
      }
      
      // dahash_lookup() returns a pointer to the first byte of the value part of the entry.
      hashValueP = (u_int *)dahash_lookup(globals->gMarkovHash, context, 1);
      if (hashValueP == NULL) {
	printf("Error: buildMarkovModel(): lookup of %s in gMarkovHash failed\n", context);
	exit(1);
      }
      successorIndex = successor - BASE_SYMBOL;  // convert into index into counts array
      
      updateCount(hashValueP, successorIndex);    
      if (globals->debug)  
	printf("Appending %c to entry for %s\n",
	       successor, (byte *)hashValueP - globals->gMarkovHash->key_size);
      
      // ----- If applicable, add wildcard versions of context -----
      if (params.wildcards > 0) {
	// Wildcarding works by replacing the leading part of the string with wildcards
	int w;
	u_char context2[MAX_K + 1];
	strcpy(context2, context);
	for (w = 0; w < params.wildcards - 1; w++) context2[w] = WILDCARD;  // Fill in one less than the minimum number of leading wildcards
	for (w = params.wildcards - 1; w < params.k; w++) {  // Now fill in the rest, including all wildcards
	  context2[w] = WILDCARD;
	  if (0) printf("Subsidiary context: '%s'\n", context2);
	  hashValueP = (u_int *)dahash_lookup(globals->gMarkovHash, context2, 1);
	  if (hashValueP == NULL) {
	    printf("Error: buildMarkovModel(): lookup of %s in gMarkovHash failed\n", context2);
	    exit(1);
	  }
	  if (hashValueP[0] == 0) {
	    if (0) printf("New wildcard entry %s added\n", context2);
	    globals->wildcardStringsAdded++;   // Count the distinct wildcardStrings
	  }

	  updateCount(hashValueP, successorIndex);    
	  if (globals->debug)  
	    printf("Appending %c to [subsidiary] entry for %s\n",
		   successor, (byte *)hashValueP - globals->gMarkovHash->key_size);
	  
	}      
      }
      // --------------------------------------------------------------
      
      context[params.k] = successor;
    }
    
    return 0; // ------------------------------------------>
  } else {
    // We've learned an alphabet from the text, use the simplified Learned Alphabet version.
    buildMarkovModelLA(globals, docText, docLen, docNum);
    return 0; // ------------------------------------------>
  }
}



static int externalBlockPromotions2 = 0;
static u_int *tmpCumProbs = NULL;

static void convertToProbs() {
  off_t htOff;
  u_char *htEntry;
  int i, e, nonZeroElts;
  u_int *htVal, iCumProb, symbol;
  double totalCount, dProb, cumProb;
  
  htOff = 0;
  for (e = 0; e < globals.gMarkovHash->capacity; e++) {
    htEntry = ((u_char *)(globals.gMarkovHash->table)) + htOff;
    if (htEntry[0]) {    // Entry is used if first byte of key is non-zero
      cumProb = 0;
      htVal = (u_int *)(htEntry + globals.gMarkovHash->key_size);
      totalCount = (double)(*htVal & COUNT_MASK);
 
      if (*htVal & HIFREQFLAG) {
	// The counts are ints in an external high frequency block (HFB) whose index is in htVal
	// We'll continue to use the HFB to store the cumulative probabilities
	int *rawCounts, w = 0;  // w is the location in which a
	                        // cumprob corresponding to a non-zero
	                        // prob will be written.
	u_int *cumProbs;
	rawCounts = (int *)dyna_get(&(globals.externalBlocks), htVal[1], DYNA_DOUBLE);
	cumProbs = (u_int *)rawCounts;
	for (i = 0; i < globals.NUM_SYMBOLS; i++) {
	  if (rawCounts[i] > 0) {
	    symbol = BASE_SYMBOL + i;
	    dProb = (double)rawCounts[i] / totalCount;
	    cumProb += dProb;
	    iCumProb = (u_int)round((cumProb * DCUMPROB1));
	    if (0) printf("     %d  %u  %.6f  %u\n", i, rawCounts[i], cumProb, iCumProb & CUMPROB_MASK);
	    cumProbs[w++] = (symbol << 24) | iCumProb;
	  }
	}
	if ((cumProbs[w - 1] & CUMPROB_MASK) != CUMPROB_MASK) {
	  printf("Error: convertToProbs(A): Cumulative probabilities %u don't reach %u for '%s'\n",
		 cumProbs[w - 1] & CUMPROB_MASK, CUMPROB_MASK, htEntry);
	  exit(1);
	}	
      } else {
	// The counts are single bytes in the actual hash table entry.  If there are less than MAX_INPLACE_COUNTS of them
	// we can store the cumulative probabilities over the top of them, otherwise we have to create an external block
	byte *rawCounts = (byte *)(htVal + 1), w = 0;
	if (tmpCumProbs == NULL)  // tmpCumProbs is static.  It's malloced only once
	  tmpCumProbs = cmalloc(globals.NUM_SYMBOLS * sizeof(u_int), "cumProbs", 0);  
	nonZeroElts = 0;
	for (i = 0; i < globals.NUM_SYMBOLS; i++) {
	  if (rawCounts[i] > 0) {
	    symbol = BASE_SYMBOL + i;
	    nonZeroElts++;
	    dProb = (double)rawCounts[i] / totalCount;
	    cumProb += dProb;
	    iCumProb = (u_int)round((cumProb * DCUMPROB1));
	    tmpCumProbs[w++] = (symbol << 24) | iCumProb;
	  }
	}
	if ((tmpCumProbs[w - 1] & CUMPROB_MASK) != CUMPROB_MASK) {
	  printf("Error: convertToProbs(B): Cumulative probabilities %u don't reach %u for '%s'\n",
		 tmpCumProbs[w - 1] & CUMPROB_MASK, CUMPROB_MASK, htEntry);
	  exit(1);
	}
	
	// Now decide whether to overwrite the hashtable entry or to switch to an external block
	if (nonZeroElts > globals.MAX_INPLACE_COUNTS) {
	  u_int *eb;
	  // Allocate space for the new external block
	  eb = ebMalloc(globals.NUM_SYMBOLS * sizeof(u_int), "cumProbs", 0);
	  // Link it up
	  htVal[1] = globals.externalBlocksUsed - 1;
	  // Copy in the contents of the tmpCumProbs array
	  for (i = 0; i < w; i++) eb[i] = tmpCumProbs[i]; // Don't worry about the wasted space caused by empty entries.
	  *htVal |= HIFREQFLAG;  // Set the flag
	  externalBlockPromotions2++;
	} else {
	  for (i = 0; i < w; i++) {   // Copy into the hashtable entry
	    htVal[i + 1] = tmpCumProbs[i];
	  }
	}
      }
    }
    htOff += globals.gMarkovHash->entry_size;
  }
}



// The following three processXXXFormat functions are indirectly copied from Nomenclator.c
// ... which may have copied them from other modules in this directory.
// Note that they each version calls different functions

static void processTSVFormat(globals_t *globals, BOOL scanForAlphabet) {
  // Input is assumed to be in TSV format with an arbitrary (positive) number of
  // columns (including one column), in which the document text is in
  // column one and the other columns are ignored.   (All LFs and TABs are assumed
  // to have been removed from the document.)
  char *lineStart, *p, *inputEnd;
  long long printerval = 1000;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  lineStart = globals->inputInMemory;
  globals->numDocs = 0; 
  p = lineStart;
  while (p <= inputEnd) {
    // Find the length of column 1, then process the doc.
    while (p <= inputEnd  && *p >= ' ') p++;  // Terminate with any ASCII control char
    docLen = p - lineStart;
    if (scanForAlphabet) alphabetScan(globals, lineStart, docLen);
    else buildMarkovModel(globals, lineStart, docLen, globals->numDocs);
    globals->numDocs++;
    if (scanForAlphabet) printf("   alphabetScan");
    else printf("   buildMarkovModel");
    if (globals->numDocs % printerval == 0) {
      printf("   --- %lld docs scanned @ %.3f msec per record\n",
             globals->numDocs,
             (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs);
      if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
    }

    // Now skip to end of line (LF) or end of input.
    while (p <= inputEnd  && *p != '\n') p++;  // Terminate with LineFeed only
    p++;
    lineStart = p;
  }
}


static void processTRECFormat(globals_t *globals, BOOL scanForAlphabet) {
  // Input is assumed to be in highly simplified TREC format, such as that produced by
  // the detrec program.  Documents are assumed to be DOC elements, each starting with
  // a DOCNO element.  It is also assumed that the content contains no other sub-elements
  // and no stray angle brackets.  Ideally, the text is just words separated by spaces.
  char *docStart, *p, *q, *inputEnd;
  long long printerval = 1;
  size_t docLen;
  double startTime = what_time_is_it();

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  globals->numDocs = 0;
  while (p <= inputEnd) {
    if (0) printf("Looking for <TEXT>\n");
    q = mlstrstr(p, "<TEXT>", inputEnd - p - 6, 0, 0);
    if (q == NULL) break;   // Presumably the end of the file. ... or wrong format file.
    docStart = q + 6;
    if (0) printf("Looking for </TEXT>\n");
    q = mlstrstr(docStart, "</TEXT>", inputEnd - docStart - 7, 0, 0);
    if (q == NULL) break;   // Presumably file is incorrectly formatted.    
    docLen = q - docStart;
    if (scanForAlphabet) alphabetScan(globals, docStart, docLen);
    else buildMarkovModel(globals, docStart, docLen, globals->numDocs);
    globals->numDocs++;
    if (globals->numDocs % printerval == 0) {
      if (scanForAlphabet) printf("   alphabetScan");
      else printf("   buildMarkovModel");
      printf(" --- %lld docs scanned @ %.3f msec per record\n",
      globals->numDocs,
      (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs);
      if (printerval < 10000) printerval *= 10;
    }
    p = q + 7;  // Move to char after </TEXT>
  }
  if (scanForAlphabet) printf("   Alphabet scanned --- %lld docs scanned @ %.3f msec per record\n",
			      globals->numDocs,
			      (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs);
  else printf("   MarkovModel built --- %lld docs scanned @ %.3f msec per record --- total postings: %lld\n\n",
	 globals->numDocs,
	 (1000.0 * (what_time_is_it() - startTime)) / (double)globals->numDocs,
	 globals->totalPostingsIn);

}




static void processSTARCFormat(globals_t *globals, BOOL scanForAlphabet) {
  // Input is assumed to be docs in in a very simple <STARC
  // header><content> format. The STARC header begins and ends with a
  // single ASCII space.  Following the leading space is the content
  // length in decimal bytes, represented as an ASCII string,
  // immediately followed by a letter indicating the type of record.
  // Record types are H - header, D - document, or T - Trailer.  The
  // decimal length is expressed in bytes and is represented in
  // ASCII. If the length is L, then there are L bytes of document
  // content following the whitespace character which terminates the
  // length representation.  For example: " 13D ABCDEFGHIJKLM 4D ABCD"
  // contains two documents, the first of 13 bytes and the second of 4
  // bytes.
  //
  // Although this representation is hard to view with editors and
  // simple text display tools, it completely avoids the problems with
  // TSV and other formats which rely on delimiters, that it's very
  // complicated to deal with documents which contain the delimiters.
  //
  // This function skips H and T docs.
  
  size_t docLen;
  u_char *docStart, *p, *q, *inputEnd;
  long long printerval = 10;
  byte recordType;

  globals->numDocs = 0; 
  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  while (p <= inputEnd) {
    // Decimal length should be encoded in ASCII at the start of this doc.
    if (*p != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     p - globals->inputInMemory);
      exit(1);

    }
    errno = 0;
    docLen = strtol(p, (char **)&q, 10);  // Making an assumption here that the number isn't terminated by EOF
    if (errno) {
      fprintf(stderr, "processSTARCFile: Error %d in strtol() at offset %zd\n",
	     errno, p - globals->inputInMemory);
      exit(1);
    }
    if (docLen <= 0) {
      printf("processSTARCFile: Zero or negative docLen %zd at offset %zd\n",
	     docLen, p - globals->inputInMemory);
      exit(1);
    }

    recordType = *q;    
    if (recordType != 'H' && recordType != 'D' && recordType != 'T') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);
    }
    q++;
    if (*q != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't end with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);

    }
    
    docStart = q + 1;  // Skip the trailing space.
    if (0) printf(" ---- Encountered %c record ---- \n", recordType);
    if (recordType == 'D') {
      if (scanForAlphabet) alphabetScan(globals, docStart, docLen);
      else buildMarkovModel(globals, docStart, docLen, globals->numDocs);
      globals->numDocs++;
      if (globals->numDocs % printerval == 0) {
	if (scanForAlphabet) printf("   alphabetScan");
	else printf("   buildMarkovModel");
        printf("   --- %lld docs scanned @ %.3f msec per record\n",
          globals->numDocs,
          (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs);
        if (globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    }
    p = docStart + docLen;  // Should point to the first digit of the next length, or EOF
  }
}

 
static u_char rand_cumdist_lsearch(u_int *cumProbs, int numProbs) {
  // CumProbs[] is an array 0 .. numProbs - 1 representing a cumulative
  // probability distribution, of which entries at the tail may be empty.
  // Each entry comprises a symbol in the most significant byte and
  // a 24 bit integer representing the cumulative probability.  Generate
  // a random integer between 
  // Returns an integer in 0 .. numProbs - 1
  u_int uniRand = (u_int)(tinymt64_generate_uint64(&tinymt) & CUMPROB_MASK);  // Uniform in range 0 - 2^24 - 1
  u_int cumProb;
  int i;
  u_char symbol = 0;

  for (i = 0; numProbs; i++) {
    cumProb = cumProbs[i] & CUMPROB_MASK;
    symbol = (cumProbs[i] & SYMBOL_MASK) >> 24;
    if (uniRand <= cumProb) {
      if (0) printf("rand_cumdist_lsearch(): returning internal symbol %c\n", symbol);
      return symbol;   /// ------------->
    }
  }
  printf("Error: rand_cumdist_lsearch(): Value not found.  uniRand = %u\n", uniRand);
  exit(1);
}

static long long failed_lookups = 0;

static int chooseSuccessor(u_char *context) {
  // Look up context in the hash table to find the array of cumulative probabilities
  // for all possible successors, then throw a random probability to choose which one.
  // If lookup fails or smoothing/shrinkage is in effect, choose the successor from a shortened
  // context, achieved by wild-carding
  u_int *htVal = NULL, *htVal2 = NULL, *cumProbs = NULL;
  int w = 0;
  u_char *modcon = NULL, successor;
  double uniRand;

  if (params.wildcards > 0) {
    // Make a minimally wild-carded version of the context and look it up
    modcon = make_a_copy_of(context);
    while (w < params.wildcards) modcon[w++] = WILDCARD;
    htVal2 = (u_int *)dahash_lookup(globals.gMarkovHash, modcon, 0);
    while (htVal2 == NULL && w < params.k) {   // Including all wildcards
      // Add more wildcards until it works.
      modcon[w++] = WILDCARD;
      if (0) printf("Looking up modcon '%s'\n", modcon);
      htVal2 = (u_int *)dahash_lookup(globals.gMarkovHash, modcon, 0);
    }
    
    if (htVal2 == NULL) {
      printf("Error: Wildcard lookup of '%s' failed - last try was %s\n", context, modcon);
      exit(1);
    }
  }

  // If lambda > 0 roll a die and see if we should use modcon
  uniRand = tinymt64_generate_double(&tinymt);
  if (htVal2 != NULL && params.lambda > 0.0 && (uniRand < params.lambda)) {
    // Yes we're using a diminished context
    // Are the cumulative probabilities in the hashtable value or in an external block?
    if (*htVal2 & HIFREQFLAG) {
      // Using an external block
      cumProbs = (u_int *)dyna_get(&(globals.externalBlocks), htVal2[1], DYNA_DOUBLE);
      successor = rand_cumdist_lsearch(cumProbs, globals.NUM_SYMBOLS);
      free(modcon);
      return successor;   //  ----------------------------------------------->
    } else {
      // Not using an external block;
      cumProbs = htVal2 + 1;
      successor = rand_cumdist_lsearch(cumProbs, globals.MAX_INPLACE_COUNTS);
      free(modcon);
      return successor;   //  ----------------------------------------------->
    }      
  } else {
    // Look up original context
    htVal = (u_int *)dahash_lookup(globals.gMarkovHash, context, 0);
    if (htVal == NULL) {  // Failed.  If possible, fall back to modcon.
    if (params.verbose) printf("\nLookup failed for '%s' (using %s)\n", context, modcon);
      failed_lookups++;
      if (htVal2 == NULL) {
	printf("Error: chooseSuccessor(): Fallback Lookup of '%s' failed.\n", context);
	if (modcon != NULL) printf("       modcon was '%s'\n", modcon);
	exit(1);
      } else {
	// Using restricted context because of lookup failure.  External block?
	if (*htVal2 & HIFREQFLAG) {
	  // Using an external block
	  cumProbs = (u_int *)dyna_get(&(globals.externalBlocks), htVal2[1], DYNA_DOUBLE);
	  successor = rand_cumdist_lsearch(cumProbs, globals.NUM_SYMBOLS);
	  free(modcon);
	  return successor;   //  ----------------------------------------------->
	} else {
	  // Not using an external block;
	  cumProbs = htVal2 + 1;
	  successor = rand_cumdist_lsearch(cumProbs, globals.MAX_INPLACE_COUNTS);
	  free(modcon);
	  return successor;   //  ----------------------------------------------->
	}
      }
    } else {   // Normal lookup succeeded.
      if (*htVal & HIFREQFLAG) {
	// Using an external block
	cumProbs = (u_int *)dyna_get(&(globals.externalBlocks), htVal[1], DYNA_DOUBLE);
	successor = rand_cumdist_lsearch(cumProbs, globals.NUM_SYMBOLS);
	free(modcon);
	return successor;   //  ----------------------------------------------->
      } else {
	// Not using an external block;
	cumProbs = htVal + 1;
	successor = rand_cumdist_lsearch(cumProbs, globals.MAX_INPLACE_COUNTS);
	free(modcon);
	return successor;   //  ----------------------------------------------->
      }
    }
  }
}


static void setContextForDocStart(u_char *context) {
  // If the number of documents used for training is greater than 1000 we set up
  // a context comprising all EOD symbols.  Otherwise we set up a context starting
  // with wildcards and ending with EOS (or ". ")
  int i;

  if (globals.numDocs > 1000 || params.wildcards < 1) {
    for (i = 0; i < params.k; i++) context[i] = globals.EOD;
  } else if (params.predefinedAlphabet) {
    for (i = 0; i < params.k - 1; i++) context[i] = WILDCARD;
    context[params.k - 1] = globals.charMap['\n'];  
  } else {
    for (i = 0; i < params.k - 2; i++) context[i] = WILDCARD;
    context[params.k - 2] = globals.charMap['.'];
    context[params.k - 1] = globals.charMap[' '];
  }
  
  context[params.k] = 0;
}
  

static void generateCorpusFromModel(u_char *suppliedContext) {
  long long postingsOut = 0, numDocs = 0;
  int i;
  double startTime = what_time_is_it();
  int printerval = 1;
  u_char context[params.k + 1], successor, outSymbol;

  if (!params.interactive) {
    if (params.verbose) setvbuf(globals.corpusOut, NULL, _IONBF, 0);
    fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);
  }
  
  if (suppliedContext != NULL) {
    size_t len = strlen(suppliedContext);
    int r = 0, w = 0;
    if (len >= params.k) {
      for (r = len - params.k; r < len; r++) context[w++] = globals.charMap[suppliedContext[r]];
      context[w] = 0;
    } else {
      for (w = 0; w < params.k - len; w++) context[w] = WILDCARD;
      w = params.k - len;
      for (r = 0; r < len; r++) context[w++] = globals.charMap[suppliedContext[r]];
      context[w] = 0;
    }
    if (0) printf("Supplied context.  Using '%s'\n", context);
  } else {
    setContextForDocStart(context);
  }

  if (params.verbose) printf("\nContext set to '%s'\n", context);
  
  while (postingsOut < globals.desiredTotalPostings) {
    successor = chooseSuccessor(context);
    outSymbol = globals.rCharMap[successor];
    if (globals.EOWMap[outSymbol]) postingsOut++;
    if (0) printf("Successor(%s) --> %c\n", context, successor);
    if (successor == globals.EOD) {
      if (!params.interactive) fprintf(globals.corpusOut, "\n</TEXT>\n</DOC>\n");
      postingsOut++;   // EOD must be EOW as well
      if (postingsOut >= globals.desiredTotalPostings) break;   // ----------------------->
      if (!params.interactive) fprintf(globals.corpusOut, "<DOC>\n<DOCNO>Doc%06lld</DOCNO>\n<TEXT>\n", numDocs++);
      if (numDocs % printerval == 0 && !params.interactive) {
	printf("   stringMarkov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	       numDocs, postingsOut, globals.desiredTotalPostings, (double)postingsOut
	       / 1000000.0 / (what_time_is_it() - startTime));
	if (printerval < 10000) printerval *= 10;
      }

      // We don't want to carry context across document boundaries, so completely reset it
        setContextForDocStart(context);
    } else {
      if (params.interactive) fputc(outSymbol, stdout);
      else fputc(outSymbol, globals.corpusOut);
      for (i = 0; i < params.k - 1; i++) context[i] = context[i + 1];
      context[params.k - 1] = successor;
    }
  }
     
  if (!params.interactive && successor != globals.EOD) {   // Don't output end of document if we've done so immediately before
    fprintf(globals.corpusOut, "\n</TEXT>\n</DOC>\n");
  }
  if (!params.interactive) printf("   stringMarkov generator:  %lld docs, %lld / %lld postings. Average rate: %.3f Mpostings/sec.\n",
	 numDocs, postingsOut, globals.desiredTotalPostings, (double)postingsOut
	 / 1000000.0 / (what_time_is_it() - startTime));

  globals.postingsOut = postingsOut;
  globals.docsOut = numDocs;
}


static void printSummary(globals_t *globals) {  
  printf("docs=%lld # Including %lld zero-length\n", globals->numDocs, globals->numEmptyDocs);
  printf("no. context strings=%zd\n", globals->gMarkovHash->entries_used);
}

static void printUsage(char *progName, arg_t *args) {
  printf("Usage: %s inFile=<blah> outFile=<blah> [ <arg=val> ...]\n\n", progName);
  print_args(TEXT, args, "Default");
  exit(1);
}

static void setupCharMapsFromText() {
  // A scan of the text has set a flag in globals.charMap for each non-control ASCII character encountered
  // Pass through that array and set each flagged entry to the next in a sequence of symbols,
  // starting with BASE_SYMBOL.
  int i;
  u_char symbol = BASE_SYMBOL;
  for (i = 0; i < 128; i++) {
    if (globals.charMap[i]) {
      if (symbol >= '~') {
	printf("Error: More distinct symbols encountered than can fit in the ASCII table from '%c' on.\n",
	       BASE_SYMBOL);
	exit(1);
      }
      globals.charMap[i] = symbol;
      globals.rCharMap[symbol] = (u_char)i;
      symbol++;
    }
  }
  globals.EOD = symbol++;
  globals.NUM_SYMBOLS = symbol - BASE_SYMBOL;
  globals.NUM_SYMBOLS_RU =  4 * ((globals.NUM_SYMBOLS + 3) / 4); //(RU - Rounded Up)
  globals.MAX_INPLACE_COUNTS = globals.NUM_SYMBOLS_RU / 4;

  printf("setupCharMapsFromText(): %d distinct symbols found.\n", globals.NUM_SYMBOLS);
}
  

static void loadPredefinedAlphabet() {
  int i, diff = 'a' - 'A';
  u_char symbol = BASE_SYMBOL;
  for (i = 'A'; i <= 'Z'; i++) {
    // Uppers and lowers map to the same symbol
    globals.charMap[i] = symbol;
    globals.charMap[i + diff] = symbol;
    globals.rCharMap[symbol] = (u_char)(i + diff);
    symbol++;
  }
  // Now the specials:
  // -- end of word
  globals.charMap[' '] = symbol;
  globals.rCharMap[symbol] = ' ';
  symbol++;

  // -- end of sentence
  globals.charMap['\n'] = symbol;
  globals.rCharMap[symbol] = '\n';
  globals.charMap['.'] = symbol;
  globals.rCharMap[symbol] = '\n';
  globals.charMap['!'] = symbol;
  globals.rCharMap[symbol] = '\n';
  globals.charMap['?'] = symbol;
  globals.rCharMap[symbol] = '\n';
  symbol++;

  // -- end of document
  globals.EOD = symbol++;
  globals.charMap[EOD_IN] = globals.EOD;
  globals.NUM_SYMBOLS = symbol - BASE_SYMBOL;
  globals.NUM_SYMBOLS_RU = (4 * globals.NUM_SYMBOLS + 3) / 4; //(RU - Rounded Up)
  globals.MAX_INPLACE_COUNTS = globals.NUM_SYMBOLS_RU / 4;
  printf("Symbol representing end-of-document: '%c'\n", globals.EOD);
}


static void showAlphabet() {
  int i;
  printf("\nMarkov mapping.  (ASCII char in input --> ASCII char in transition matrix)\n");
  for (i = 0; i < ' '; i++) {
    if (globals.charMap[i]) printf("    '%c' --> '%c'\n", (u_char)i, globals.charMap[i]);
  }
  for (i = ' '; i < 128; i++) {
    printf("%3d: '%c' --> ", i, (u_char)i);
    if (globals.charMap[i]) putchar(globals.charMap[i]);
    putchar('\n');
  }
  putchar('\n');

  printf("\nMarkov reverse mapping.  (ASCII char in transition matrix --> ASCII char to be emitted)\n");
  for (i = ' '; i < 128; i++) {
    if (globals.rCharMap[i]) printf("    '%c' --> '%c'\n", (u_char)i, globals.rCharMap[i]);
  }
  putchar('\n');
}


static void showGlobals() {
  printf("\nCurrent state of globals:\n");
  printf("   k: %d\n", globals.k);
  printf("   wildcards: %d\n", globals.wildcards);
  printf("   numDocs: %lld\n", globals.numDocs);
  printf("   numEmptyDocs: %lld\n", globals.numEmptyDocs);
  printf("   totalPostingsIn: %lld\n", globals.totalPostingsIn);
  printf("   desiredTotalPostings: %lld\n", globals.desiredTotalPostings);
  printf("   postingsOut: %lld\n", globals.postingsOut);
  printf("   docsOut: %lld\n", globals.docsOut);
  printf("   vocabOut: %lld\n", globals.vocabOut);
  printf("   wildcardStringsAdded: %lld\n", globals.wildcardStringsAdded);
  printf("   sentenceCount: %lld\n", globals.sentenceCount);
  printf("   docsUsedForTraining: %lld\n", globals.docsUsedForTraining);
  printf("   externalBlocksUsed: %d\n", globals.externalBlocksUsed);
  printf("   inputSize: %zd\n", globals.inputSize);
  printf("   NUM_SYMBOLS: %d\n", globals.NUM_SYMBOLS);
  printf("   NUM_SYMBOLS_RU: %d\n", globals.NUM_SYMBOLS_RU);
  printf("   MAX_INPLACE_COUNTS: %d\n", globals.MAX_INPLACE_COUNTS);
  showAlphabet();
  printf("\n");
}


static void openOutAndSimple() {
  // Make sure we can write to the output file
  if (params.outFileName != NULL) {
    globals.corpusOut = fopen(params.outFileName, "wb");
    if (globals.corpusOut == NULL) {
      printf("Error: Can't open %s for writing.\n", params.outFileName);
      exit(1);
    }
    printf("Output file %s opened for writing.\n", params.outFileName);
  }
  
  // If applicable, make sure we can write to the simplified text file
  if (params.simplifiedFileName != NULL) {
    globals.simplifiedText = fopen(params.simplifiedFileName, "wb");
    if (globals.simplifiedText == NULL) {
      printf("Error: Can't open %s for writing.\n", params.simplifiedFileName);
    }
    printf("Output file %s opened for writing.\n", params.simplifiedFileName);
  }  
}


static void mmapInput() {
  int error_code;
  // Memory map the whole input file
  if (! exists(params.inFileName, "")) {
    printf("Error: Input file %s doesn't exist.\n", params.inFileName);
    exit(1);
  }

  globals.inputInMemory = (char *)mmap_all_of(params.inFileName, &(globals.inputSize),
					      FALSE, &(globals.inputFH), &(globals.inputMH),
					       &error_code);
  if (globals.inputInMemory == NULL ) {
    printf("Error: mmap_all_of(%s) failed with code %d\n", params.inFileName, error_code);
    exit(1);
  }

  madvise(globals.inputInMemory, globals.inputSize, MADV_SEQUENTIAL);

  printf("Input mapped.\n");
}


static void loadOrLearnAlphabet() {
  double startTime = what_time_is_it();
  if (params.predefinedAlphabet) {
    loadPredefinedAlphabet();
    printf("Using pre-defined alphabet comprising case-folded ASCII letters only.\n");
  } else {
    // learn the alphabet by scanning the text
    if (tailstr(params.inFileName, ".tsv") != NULL || tailstr(params.inFileName, ".TSV") != NULL) {
      processTSVFormat(&globals, TRUE);
    } else if (tailstr(params.inFileName, ".trec") != NULL || tailstr(params.inFileName, ".TREC") != NULL) {
      processTRECFormat(&globals, TRUE);
    } else {
      processSTARCFormat(&globals, TRUE);
    }
    setupCharMapsFromText();  
  }

  showAlphabet();  
  printf("Alphabet: %d symbols\n", globals.NUM_SYMBOLS);
  globals.LATime = what_time_is_it() - startTime;
  printf("Time to load alphabet %.3f seconds. \n",
	  globals.LATime);
}


static void scanInput() {
  int keyLen;
  double startTime;
  
    // Set up global hash with room for overall occurrence frequency, and an array of symbol frequencies
  keyLen = 3; // keyLen should be a multiple of four, minus one.  The minus one is to allow for the terminating NUL
  while (keyLen < params.k) keyLen += 4;

  startTime = what_time_is_it();
  
  printf("Creating hash table with %d byte keys and %zu byte values\n",
		keyLen, sizeof(u_int) + globals.NUM_SYMBOLS_RU);
  globals.gMarkovHash = 
    dahash_create((u_char *)"globalVocab", params.hashBits, keyLen, sizeof(u_int) + globals.NUM_SYMBOLS_RU,
		  (double)0.9, FALSE);

  printf("Creating dynamic array initially with 1024 %zu byte values\n", sizeof(u_int) * globals.NUM_SYMBOLS);
  globals.externalBlocks = dyna_create(1024, sizeof(u_int) * globals.NUM_SYMBOLS);
    
  if (tailstr(params.inFileName, ".tsv") != NULL || tailstr(params.inFileName, ".TSV") != NULL) {
    processTSVFormat(&globals, FALSE);
   } else if (tailstr(params.inFileName, ".trec") != NULL || tailstr(params.inFileName, ".TREC") != NULL) {
    processTRECFormat(&globals, FALSE);
  } else {
    processSTARCFormat(&globals, FALSE);
  }
  
  globals.scanTime = what_time_is_it() - startTime;
  printf("Scan to build Markov model took %.3f seconds.  Here are some statistics: \n",
	  globals.scanTime);
  printSummary(&globals);
  dahash_print_key_stats(globals.gMarkovHash, TRUE, "after all docs scanned for buildMarkovModel()");

  if (globals.simplifiedText != NULL) fclose(globals.simplifiedText);
  unmmap_all_of(globals.inputInMemory, globals.inputFH, globals.inputMH, globals.inputSize);

  startTime = what_time_is_it();
  convertToProbs();

  globals.finalisationTime = what_time_is_it() - startTime;
  printf("Finalised transition table in %.3f sec.\n"
	   "Overall time to train the model: %.3f sec.\n",
	 globals.finalisationTime,  what_time_is_it() - globals.veryStart);

}


static void generateACorpus() {
  // Run the model to produce the outputCorpus, but first set the desired
  // number of postings to generate
  double startTime;
  
  if (params.postingsRequired != 0) {
    // Generate the specified number of postings.
    globals.desiredTotalPostings = params.postingsRequired;
    printf("Will generate %lld postings.\n", globals.desiredTotalPostings);
  } else {
    // Generate the a number of postings determined by the number scanned, and a scale factor.
    globals.desiredTotalPostings = (long long) ((double) globals.totalPostingsIn * params.postingsScaleFactor + 0.5);
    printf("Total postings In = %lld,  will try to generate %lld due to trainingFraction %.5f and scale factor %.5f\n",
	   globals.totalPostingsIn, globals.desiredTotalPostings, params.trainingFraction, params.postingsScaleFactor);
  }

  startTime = what_time_is_it();
  generateCorpusFromModel(NULL);
  globals.generationTime = what_time_is_it() - startTime;
  globals.generationRate = (double)globals.desiredTotalPostings / 1000000.0 / globals.generationTime;
}



static void saveModel() {
  int sfd;
  size_t toWrite;
  double startTime = what_time_is_it();
  sfd = open(params.saveModelTo, O_CREAT|O_WRONLY|O_TRUNC, 0755);
  if (sfd < 0) {
    printf("Error: Can't open %s for writing the trained model.\n", params.saveModelTo);
    exit(1);
  }
  robustWrite(sfd, (byte *)(&globals), sizeof(globals_t), "globals");

  robustWrite(sfd, (byte *)(globals.gMarkovHash), sizeof(dahash_table_t), "hash table struct");

  toWrite = globals.gMarkovHash->capacity * globals.gMarkovHash->entry_size;
  robustWrite(sfd, (byte *)(globals.gMarkovHash->table), toWrite, "hash table");

  toWrite = globals.externalBlocksUsed * sizeof(u_int) * globals.NUM_SYMBOLS + 2 * sizeof(long long);
  robustWrite(sfd, (byte *)(globals.externalBlocks), toWrite, "dynamic array");
  close(sfd);
  printf("Trained model saved to %s.  Time taken: %.1f sec.\n",
	 params.saveModelTo, what_time_is_it() - startTime);
}


static void restoreModel() {
  int sfd;
  size_t toRead;
  byte *tmp;
  double startTime = what_time_is_it();
  sfd = open(params.restoreModelFrom, O_RDONLY);
  if (sfd < 0) {
    printf("Error: Can't open %s for reading the trained model.\n", params.restoreModelFrom);
    exit(1);
  }

  // Globals struct
  robustRead(sfd, (byte *)(&globals), sizeof(globals_t), "read globals");

  showGlobals();
  params.k = globals.k;
  params.wildcards = globals.wildcards;


  // dahash struct
  tmp = cmalloc(sizeof(dahash_table_t), "restore dahash", 0); 
  robustRead(sfd, tmp, sizeof(dahash_table_t), "read dahash");
  globals.gMarkovHash = (dahash_table_t *)tmp;
  globals.gMarkovHash->name = make_a_copy_of("Restored hash");
  printf("Restored Markov hash is called '%s' and has a capacity of %zu\n",
	 globals.gMarkovHash->name, globals.gMarkovHash->capacity);

  // dahash table
  toRead = globals.gMarkovHash->capacity * globals.gMarkovHash->entry_size;
  tmp = cmalloc(toRead, "restore actual hash table", 0);
  robustRead(sfd, tmp, toRead, "read hash table");
  globals.gMarkovHash->table = tmp;
  
  // dynamic array
  toRead = globals.externalBlocksUsed * sizeof(u_int) * globals.NUM_SYMBOLS + 2 * sizeof(long long);
  tmp = cmalloc(toRead, "dynamic array for external blocks", 0);
  robustRead(sfd, tmp, toRead, "read dynamic array");
  globals.externalBlocks = (dyna_t)tmp;
  
  close(sfd);
  printf("Trained model restored from %s.  Time taken: %.1f sec.\n",
	 params.restoreModelFrom, what_time_is_it() - startTime);

  if (params.outFileName != NULL) {
    globals.corpusOut = fopen(params.outFileName, "wb");
    if (globals.corpusOut == NULL) {
      printf("Error: Can't open %s for writing.\n", params.outFileName);
      exit(1);
    }
    printf("Output file %s opened for writing.\n", params.outFileName);
  }
  if (!(params.interactive)) generateACorpus();
}

static   u_char lineBuf[2000];


static void interactive() {
  size_t len;
  restoreModel();
  printf("Model prompts must be on a single line. Only the last %d characters will be used.\n\n", params.k);
  printf("Model prompt >>> ");
  globals.desiredTotalPostings = params.postingsRequired;
  while (fgets(lineBuf, 2000, stdin) != NULL) {
    // Set context and generate model.
    len = strlen(lineBuf);
    if (lineBuf[len - 1] == '\n') lineBuf[len - 1] = 0;
    printf("\n\n%s", lineBuf);
    generateCorpusFromModel(lineBuf);
    printf("\n\n");
    printf("\nModel prompt >>> ");
  }
}


static void trainAndSaveModel() {
  mmapInput();
  loadOrLearnAlphabet();
  scanInput();
  saveModel();
}


static void emulateACorpus() {
  openOutAndSimple();
  mmapInput();
  loadOrLearnAlphabet();
  scanInput();
  generateACorpus();
}



int main(int argc, char **argv) {
  int a;
  u_char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
  char *ignore;
  double MB, MB1, MB2, totalMB = 0;

  globals.veryStart = what_time_is_it();
  setvbuf(stdout, NULL, _IONBF, 0);
  if (sizeof(u_char *) != 8) {
    printf("\n\nWarning: Pointers are only 4-byte. It is recommended that %s should be compiled for 64 bit.\n\n", argv[0]);
  }

  activate_preservation_of_newlines_in_split_lines();  // changes operation of utf8_split_line_into_null_terminated_words()
  
  initialiseParams();
  if (argc < 3) printUsage(argv[0], args);

  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);

  printf("Sizeof(size_t) = %zu, Sizeof(off_t) = %zu, Sizeof(byte *) = %zu\n",
	 sizeof(size_t), sizeof(off_t), sizeof(byte *));

  if (params.verbose) printf("Params initialised\n");
  initialiseGlobals(&globals);   // Includes the hashtables and dynamic array as well as scalar values
  if (params.verbose) printf("Globals initialised\n");
  
  // Process all the command line options
  for (a = 1; a < argc; a++) {
    assign_one_arg(argv[a], (arg_t *)(&args), &ignore);
  }

  checkParams(TRUE);
  // Copy a couple of key params to facilitate save/restore.
  globals.k = params.k;
  globals.wildcards = params.wildcards;

  // Initialise the TinyMT random generator
  if (params.randomSeed == -1) params.randomSeed = (u_ll)fmod(globals.veryStart * 1000000.0, 1000000.0);
  printf("Random seed: %llu\n", params.randomSeed);
  tinymt.mat1 = 0Xfa051f40;
  tinymt.mat2 = 0Xffd0fff4;
  tinymt.tmat = 0X58d02ffeffbfffbcULL;
  tinymt64_init(&tinymt, params.randomSeed);

  // Call the relevant main function depending upon what mode we're in.
  if (params.interactive) interactive();
  else if (params.restoreModelFrom != NULL) restoreModel();
  else if (params.saveModelTo != NULL) trainAndSaveModel();
  else emulateACorpus();
    
  MB =  (double)dahash_memory_used(globals.gMarkovHash) / 1048576.0;
  totalMB += MB;
  printf("\nMemory used by main Markov hash table: %.1fMB\n", MB);
  MB1 = (double)((externalBlockPromotions1 + externalBlockPromotions2) * (globals.NUM_SYMBOLS * sizeof(u_int))) / 1048576.0;
  MB2 = (double)(dyna_bytes_used(globals.externalBlocks)) / 1048576.0;
  printf("Memory used by external blocks: %.1fMB out of %.1fMB allocated in dynamic array.\n", MB1, MB2);
  totalMB += MB2;
  
  printf("\n===============================================\n"
	 "SMG(%d): Total memory used (ignoring mmapped input file): %.3fMB approx.\n"
	 "SMG(%d): Hash table entries: %zu.  Hash bits: %d\n",
	 params.k, totalMB, params.k, globals.gMarkovHash->entries_used, globals.gMarkovHash->bits);

  dahash_destroy(&(globals.gMarkovHash));
  if (globals.externalBlocks != NULL) free(globals.externalBlocks);
  if (globals.corpusOut != NULL) fclose(globals.corpusOut);
#ifdef __clang__
  printf("SMG(%d): Version: %s\n", params.k, __TIMESTAMP__);
#endif

  printf("SMG(%d): Overall runtime: %.3f sec.\n"
	 "SMG(%d): Time to load/learn alphabet: %.3f sec.\n"
	 "SMG(%d): Scan time: %.3f sec.\n"
	 "SMG(%d): Finalise time: %.3f sec.\n"
	 "SMG(%d): Generation time: %.3f sec.  Generation rate: %.3f Mpostings/sec.\n"
	 "SMG(%d): Failed lookups: %lld.\n"
	 "SMG(%d): Promotions to external block: %d + %d = %d.\n"
	 "SMG(%d): Distinct wildcard strings added: %lld\n"
	 "SMG(%d): Total counter overflows: %lld\n"
	 "SMG(%d): Input: %s\n"
	 "SMG(%d): Output: %s\n"
	 "SMG(%d): docs: %lld out v. %lld - %lld = %lld in.  %lld used for training.\n"
	 "SMG(%d): sentences in: %lld\n"
	 "SMG(%d): total_postings: %lld out v. %lld in  (due to scaleFactor %.2f)\n"
	 "SMG(%d): randomSeed: %lld\n\n",
	 params.k, what_time_is_it() - globals.veryStart,
	 params.k, globals.LATime,
	 params.k, globals.scanTime,
	 params.k, globals.finalisationTime,
	 params.k, globals.generationTime, globals.generationRate,
	 params.k, failed_lookups,
	 params.k, externalBlockPromotions1, externalBlockPromotions2, externalBlockPromotions1 + externalBlockPromotions2,
	 params.k, globals.wildcardStringsAdded,
	 params.k, totalOverflows,
	 params.k, params.inFileName,
	 params.k, params.outFileName,
	 params.k, globals.docsOut, globals.numDocs, globals.numEmptyDocs, globals.numDocs - globals.numEmptyDocs,
	           globals.docsUsedForTraining,
	 params.k, globals.sentenceCount,
	 params.k, globals.postingsOut, globals.totalPostingsIn, params.postingsScaleFactor,
	 params.k,params.randomSeed);

 
}
