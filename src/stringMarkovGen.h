// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

#define MAX_K 48   // Maximum permitted Markov order.
#define HIFREQFLAG 0x80000000  // Most Significant Bit in a u_int
#define COUNT_MASK 0x7FFFFFFF  // All except the HIFREQFLAG
#define CUMPROB_MASK 16777215  // 2^24 -1 -  represents a cumulative probability of 1 == 0xFFFFFF
#define DCUMPROB1 16777215.0  // Double version of CUMPROB1
#define TWO_TO_24 0x1000000  // 2^24
#define SYMBOL_MASK 0xFF000000
#define BASE_SYMBOL '0'
#define WILDCARD '*'  // Must be less than BASE_SYMBOL (or higher than the highest)
#define EOD_IN 0x7F  // This represents End of Document in the input (not internally)


typedef struct {
  int k, wildcards;  // Will be copied from params
  double startTime;
  long long numDocs, numEmptyDocs, totalPostingsIn, desiredTotalPostings,
    postingsOut, docsOut, vocabOut, wildcardStringsAdded, sentenceCount,
    docsUsedForTraining;
  dahash_table_t *gMarkovHash;
  dyna_t externalBlocks;
  int externalBlocksUsed;
  u_char *inputInMemory;
  byte *corpusInMemory;
  size_t inputSize;
  CROSS_PLATFORM_FILE_HANDLE inputFH;
  HANDLE inputMH;
  FILE *corpusOut, *simplifiedText;
  int debug;
  double veryStart, LATime, scanTime, finalisationTime, generationTime, generationRate;
  u_char charMap[128], rCharMap[128], EOSMap[128], EOWMap[128], EOD; 
  int NUM_SYMBOLS, // Size of the Markov alphabet
    NUM_SYMBOLS_RU, // Smallest multiple of 4 at least as large as NUM_SYMBOLS  (RU - Rounded Up)
    MAX_INPLACE_COUNTS // = NUM_SYMBOLS_RU / 4
;
 } globals_t;


typedef struct {
  u_char *inFileName;
  u_char *outFileName;
  u_char *simplifiedFileName;
  u_char *saveModelTo;
  u_char *restoreModelFrom;
  int k, wildcards;
  int hashBits;
  long long randomSeed, postingsRequired;
  double postingsScaleFactor;  // Ratio of output postings to input postings
  double lambda, trainingFraction;
  BOOL predefinedAlphabet;
  BOOL interactive;
  BOOL verbose;
} params_t;

extern params_t params;

