// Copyright David Hawking. All rights reserved.
// Licensed under the MIT license.

// Table of command line argument definitions for the Markov document
// generator.  The functions in argParser.c operate
// on this array.

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "../src/definitions.h"
#include "../src/utils/dahash.h"
#include "../src/utils/dynamicArrays.h"
#include "stringMarkovGen.h"
#include "../src/utils/argParser.h"

arg_t args[] = {
  { "inFile", ASTRING, (void *)&(params.inFileName), "This is the file of text comprising the corpus on which the Markov model will be trained.  Formats supported: TREC, TSV, or STARC."},
  { "outFile", ASTRING, (void *)&(params.outFileName), "The name of the file to which the generated corpus will be written, always in simple TREC format."},
  { "simplifiedFile", ASTRING, (void *)&(params.simplifiedFileName), "If a filename is specified a simplified version of the input will be written to it -- only the strings used to train the model."},
  { "saveModelTo", ASTRING, (void *)&(params.saveModelTo), "If a filename is specified the trained model will be saved to it, and no text will be generated."},
  { "restoreModelFrom", ASTRING, (void *)&(params.restoreModelFrom), "If a filename is specified the saved model will be restored and a corpus will be generated from it."},
  { "k", AINT, (void *)&(params.k), "The order of the Markov method, i.e. the number of symbols of context.  Minimum of 2."},
  { "lambda", AFLOAT, (void *)&(params.lambda), "Probability of fallback to a shortened context. Achieved by replacing the first symbols in the context with a wild-card."},
  { "wildcards", AINT, (void *)&(params.wildcards), "The minimum number of wild-cards in a context. If not given, will be set to k - 2."},  
  { "hashBits", AINT, (void *)&(params.hashBits), "The initial size (2**hashBits) of the hash table representing the transition matrix. Will double as required."},
  { "randomSeed", AINTLL, (void *)&(params.randomSeed), "Seed for the random generator.  If not given (i.e. -1), a seed based on date/time will be used."},
  { "postingsScaleFactor", AFLOAT, (void *)&(params.postingsScaleFactor), "Controls how many postings to generate relative to the training corpus ... if there is a training corpus."},
  { "postingsRequired", AINTLL, (void *)&(params.postingsRequired), "Specifies number of postings required as an absolute number."},
  { "trainingFraction", AFLOAT, (void *)&(params.trainingFraction), "Train on only a fraction of the documents in the training corpus."},
  { "predefinedAlphabet", ABOOL, (void *)&(params.predefinedAlphabet), "Rather than scanning the text to define the alphabet, use the pre-defined ASCII-letters-only one."},
  { "interactive", ABOOL, (void *)&(params.interactive), "If TRUE, don't just generate text.  Instead, repeatedly ask user for prompt string, and generate 1000 character responses based on the model."},
  { "verbose", ABOOL, (void *)&(params.verbose), "If TRUE, more progress information may be displayed."},
 
  { "", AEOL, NULL, "" }
  };


void initialiseParams() {
  params.inFileName = NULL;
  params.outFileName = NULL;
  params.simplifiedFileName = NULL;
  params.saveModelTo = NULL;
  params.restoreModelFrom = NULL;
  params.k = 7;
  params.wildcards = -1;
  params.lambda = 0;
  params.hashBits = 24;  // Enough for almost 16 million context strings.  Will double if not enough
  params.randomSeed = -1;  // Represents undefined
  params.postingsScaleFactor = 1;  // Default is emulation
  params.postingsRequired = 0;
  params.trainingFraction = 1.0;
  params.predefinedAlphabet = FALSE;
  params.interactive = FALSE;
  params.verbose = FALSE;
}


void checkParams(BOOL showInfo) {
  // *** Must call this after initialiseParams() ***
  // Many combinations of parameters don't make sense.
  // First work out the basic mode of operation:
  if (params.interactive) {
    // --- 1. Interactive.  No training, number of postings required defaults to 1000
    if (showInfo) printf(".... Interactive Mode ....\n");
    if (params.inFileName != NULL) {
      printf("Warning: inFileName incompatible with Interactive mode. Setting to NULL.\n");
      params.inFileName = NULL;
    }
    if (params.simplifiedFileName != NULL) {
      printf("Warning: simplifiedFileName incompatible with Interactive mode. Setting to NULL.\n");
      params.simplifiedFileName = NULL;
    }
    if (params.saveModelTo != NULL) {
      printf("Warning: saveModelTo incompatible with Interactive mode. Setting to NULL.\n");
      params.saveModelTo = NULL;
    }
    if (params.restoreModelFrom == NULL) {
      printf("Error: In Interactive mode must supply -restoreModelFrom=<saved-model-file>.\n");
      exit(1);
    }
    if (showInfo) printf("Info: Values of k, hashbits, predefinedAlphabet, and wildcards will be set by the\n"
			 "restored model.  Training fraction will be ignored.\n");
    
    if (params.postingsScaleFactor != 0) {
      printf("Warning: postingsScaleFactor makes no sense in Interactive mode. Setting to zero.\n");
      params.postingsScaleFactor = 0;
    }

    if (params.postingsRequired == 0) {
      if (showInfo) printf("Info: Setting postingsRequired to default value of 200.\n");
      params.postingsRequired = 50;
    }
  } else if (params.restoreModelFrom != NULL) {
    // --- 2. Generation of a corpus from a previously built model
    if (showInfo) printf(".... Generating a corpus from saved model %s ....\n", params.restoreModelFrom);	
    if (params.inFileName != NULL) {
      printf("Warning: inFileName incompatible with generation from a saved model. Setting to NULL.\n");
      params.inFileName = NULL;
    }
    if (params.simplifiedFileName != NULL) {
      printf("Warning: simplifiedFileName incompatible with generation from a saved model. Setting to NULL.\n");
      params.simplifiedFileName = NULL;
    }
    if (params.saveModelTo != NULL) {
      printf("Warning: saveModelTo incompatible with generation from a saved model. Setting to NULL.\n");
      params.saveModelTo = NULL;
    }
    if (showInfo) printf("Info: Values of k, hashbits, predefinedAlphabet, and wildcards will be set by the\n"
			 "restored model.  Training fraction will be ignored.\n");
    
    if (params.postingsScaleFactor != 0) {
      printf("Warning: postingsScaleFactor makes no sense with generation from a saved model. Setting to zero.\n");
      params.postingsScaleFactor = 0;
    }

    if (params.postingsRequired == 0) {
      if (showInfo) printf("Info: Setting postingsRequired to default value of 1000.\n");
      params.postingsRequired = 1000;
    }
  } else if (params.saveModelTo != NULL) {
    // --- 3. Training and saving a model
    if (params.inFileName == NULL) {
      printf("Error: When saving a model, must supply -inFileName=<training-file>.\n");
      exit(1);
    }    
    if (showInfo) printf(".... Training from %s and saving model to %s ....\n", params.inFileName, params.saveModelTo);	
    
    if (params.outFileName != NULL) {
      printf("Warning: outFileName incompatible with training and saving a model. Setting to NULL.\n");
      params.outFileName = NULL;
    }
    if (params.restoreModelFrom != NULL) {
      printf("Warning: restoreModelFrom incompatible with training and saving a model. Setting to NULL.\n");
      params.restoreModelFrom = NULL;
    }
    if (params.k > MAX_K  || params.k < 2) {
      printf("Warning value of k (%d) exceeds limit (%d) or is too small (< 2).  Setting k = %d\n",
	     params.k, MAX_K, MAX_K);
      params.k = MAX_K;
    }
    if (params.wildcards > params.k) {
      printf("Error: Value of wildcards must lie between zero and k\n");
      exit(1);
    }
    if (params.wildcards < 0) params.wildcards = params.k - 2;

    if (showInfo) printf("Info: Values of postingsRequired, postingsScaleFactor and lambda will be ignored.\n");
  } else if (params.inFileName != NULL  && params.outFileName != NULL) {
    // --- 4. Normal corpus emulation
    if (showInfo) printf(".... Training from %s and emulating it in %s, without saving the model ....\n",
			 params.inFileName, params.outFileName);
    if (params.k > MAX_K  || params.k < 1) {
      printf("Error: value of k (%d) exceeds limit (%d) or is < 1.\n",
	     params.k, MAX_K);
      exit(1);
    }
    if (params.lambda < 0.0 || params.lambda > 1.0) {
      printf("Error: Value of lambda must lie between zero and one.\n");
      exit(1);
    }
    if (params.wildcards > params.k) {
      printf("Error: Value of wildcards must lie between zero and k\n");
      exit(1);
    }
    if (params.wildcards < 0) {
      if (params.k > 1) params.wildcards = params.k - 2;
      else params.wildcards = params.k - 2;
    }
  } else {
    printf("Error: Combination of options doesn't match any of interactive, generate-from-saved-model, train-and-save-model,"
	   "or emulate-a-corpus modes of operation.\n");
    exit(1);
  }
}
    

    
  

