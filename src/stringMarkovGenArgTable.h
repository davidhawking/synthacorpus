// Copyright (c) David Hawking. All rights reserved.
// Licensed under the MIT license.

extern arg_t args[];

void initialiseParams();

void checkParams(BOOL showInfo);
