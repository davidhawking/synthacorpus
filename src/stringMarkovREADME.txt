Notes on StyleExpts/stringMarkovGen
------------------------------

The original version of stringMarkovGen in ../src was engineered to maximise the amount of training data which could be handled.   The
training text was filtered to include only words comprising only ASCII letters, and those words were case-folded.   The result is an alphabet
of 26 letters plus special symbols for end of word (EOW), end of sentence (EOS) and end of document (EOD), a total of 29.

To facilitate Emulation vs. Base comparisons, ../src/stringMarkovGen.exe is able to write a simplified version of the Base corpus restricted
to just the data used to train the Markov model.

After correspondence with Jacques Savoy about what would be needed to do a stylistic similarity measurement between Emulated and Base
versions of a text, I decided that it would be prudent to develop a version of stringMarkovGen which would, within some limitations, use the
full character set of the training data.  The file EX2W.trec in this directory is an example text supplied by Jacques.  It is a single document
comprising more than 10,000 words.

The version of stringMarkovGen in this directory should, when debugged, be able to operate like the one in ../src if the option
-predefinedAlphabet=TRUE is given.  Without that option, it does a preliminary scan of the text to build up an alphabet.  At the moment
bytes with leading bit set are ignored.   I should probably change that to extract unicode characters and test for letters.  Letters should be
mapped to internal code points, and punctuation either mapped to an ASCII near equivalent or ignored.

Training on a single document means that the old way of starting generation of a document is not appropriate.  When we try to choose a
successor for the context consisting entirely of EOD symbols, there is only one choice. Inevitably, the emulated text starts identically to
the Base.   In this circumstance, I chose to start with a context starting with wildcards and ending with ". ".  To support this, a minimal
but sufficient number of wildcards is active by default.

Example Base
-----------
 The 25th day of August, 1751, about two in the afternoon, I, David Balfour, came forth of the British Linen Company, a porter attending me with a bag of money, and some of the chief of these merchants bowing me from their doors.  Two days before, and even so late as yestermorning, I was like a beggar-man by the wayside, clad in rags, brought down to my last shillings, my companion a condemned traitor, a price set on my own head for a crime with the news of which the country rang.  To-day I was served heir to my position in life, a landed laird, a bank porter by me carrying my gold, recommendations in my pocket, and (in the words of the saying) the ball directly at my foot.

Example Emulation (k = 21, wildcards = 19, lambda = 0)  -- evidence of over-training.
---------------------------------------------
"Mr. Balfour, is all the wind sparkled, and let drop on their graves.  It is the Campbell's how they communicated one with another, what a rage of curiosity they conceived as to their employer's business, and how they were like eyes and fingers to the police.  It would be a piece of little wisdom, the way I was now placed, to take such a ferret to my tails.  I had three visits to make, all immediately needful: to my kinsman Mr. Balfour of Pilrig, to Stewart the Writer that was Appin's agent, and to William Grant Esquire of Prestongrange, Lord Advocate of Scotland.

