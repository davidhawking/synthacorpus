// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                                 trimToLength.c                                   //
//                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////

// Take a corpus C of documents in STARC, TSV or TREC format and copy it to an output file,
// stopping copying when a specified number of postings have been copied.

#ifdef WIN64
#include <windows.h>
#include <WinBase.h>
#include <strsafe.h>
#include <Psapi.h>  // Windows Process State API
#else
#include <errno.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>

#include "definitions.h"
#include "utils/dahash.h"
#include "characterSetHandling/unicode.h"
#include "utils/general.h"
#include "trimToLength.h"

static u_char docCopy[MAX_DOC_LEN + 1], *docWords[MAX_DOC_WORDS];

static void initialiseGlobals(globals_t *globals) {
  globals->startTime = what_time_is_it();
  globals->numDocs = 0;
  globals->postingsSeen = 0;

}


static int processOneDoc(globals_t *globals, u_char *docText, size_t docLen) {
  int numWords, w;
  
  // If docLen is greater than MAX, truncate and make sure that the first byte
  // after the chop isn't a UTF-8 continuation byte, leading '10' bits.  If it is we have to chop
  // before the start of the sequence.

  if (docLen == 0) {
    return 0;  // ----------------------------->
  }

  if (0) printf("Process_one_doc().  Length = %zu\n", docLen);
  if (docLen > MAX_DOC_LEN) docLen = MAX_DOC_LEN;
  if ((docText[docLen] & 0xC0) == 0x80) {
    do {
      if (0) printf("Stripping back a UTF-8 sequence\n");
      docLen--;
    } while ((docText[docLen] & 0xC0) == 0x80);
    // We should now be positioned on the start byte of a UTF-8 sequence which
    // we also need to zap.
    docLen--;
  }
  // Copy document text so we can write on it
  memcpy(docCopy, docText, docLen);
  docCopy[docLen] = 0;  // Put a NUL after the end of the doc for luck.
  if (0) {
    printf("processOneDoc: len=%zd, start_offset = %zd, end_offset = %zd, limit = %zd docCopy=\n",
	   docLen, docText - globals->inputInMemory, docText + docLen - globals->inputInMemory - 1,
	   globals->inputSize - 1);
    put_n_chars(docCopy, 100);
    printf("\n\n\n");
  }
  if (0) printf("%lld: %s\n", globals->numDocs, docCopy);
  
  numWords = utf8_split_line_into_null_terminated_words(docCopy, docLen, (byte **)(&docWords),
							MAX_DOC_WORDS, MAX_WORD_LEN,
							TRUE,  // case-fold line before splitting
							FALSE, // remove accents before splitting
							FALSE, // Perform some heuristic substitutions (maxwellize)
							TRUE  // words_must_have_an_ASCII_alnum
							);

  if (numWords <= 0) {
    return 0;  // ----------------------------->
  }

  fprintf(globals->out, "<DOC>\n<DOCNO>doc%08lld</DOCNO>\n<TEXT>\n", globals->numDocs);
  
  for (w = 0; w < numWords; w++) {
    if (0) printf("Word %d: %s\n", w, docWords[w]);
    if (w != 0) putc(' ', globals->out);
    fprintf(globals->out, "%s", docWords[w]);
    if (++globals->postingsSeen > globals->postingsLimit) break;   // ----------->
  }

  fprintf(globals->out, "\n</TEXT>\n</DOC>\n");
  
  
  return numWords;
}


static void processTSVFormat(globals_t *globals) {
  // Input is assumed to be in TSV format with an arbitrary (positive) number of
  // columns (including one column), in which the document text is in
  // column one and the other columns are ignored.   (All LFs and TABs are assumed
  // to have been removed from the document.)
  u_char *lineStart, *p, *inputEnd;
  long long printerval = 1000;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  lineStart = globals->inputInMemory;
  globals->numDocs = 0; 
  p = lineStart;
  while (p <= inputEnd) {
    if (globals->postingsSeen >= globals->postingsLimit) return;  // -------------------------->
    // Find the length of column 1, then process the doc.
    while (p <= inputEnd  && *p >= ' ') p++;  // Terminate with any ASCII control char
    docLen = p - lineStart;
    globals->numDocs++;
    if (0) printf("About to process a doc.\n");
    processOneDoc(globals, lineStart, docLen);
    if (globals->numDocs % printerval == 0) {
      printf("   --- %lld records scanned @ %.3f msec per record --- postings thus far: %lld\n",
	     globals->numDocs,
	     (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
	     globals->postingsSeen);
      if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    // Now skip to end of line (LF) or end of input.
    while (p <= inputEnd  && *p != '\n') p++;  // Terminate with LineFeed only
    p++;
    lineStart = p;
    if (0) printf("Found end of line\n");
  }
}



static void processTRECFormat(globals_t *globals) {
  // Input is assumed to be in highly simplified TREC format, such as that produced by
  // the detrec program.  Documents are assumed to be DOC elements, each starting with
  // a DOCNO element.  It is also assumed that the content contains no other sub-elements
  // and no stray angle brackets.  Ideally, the text is just words separated by spaces.
  u_char *docStart, *p, *q, *inputEnd;
  long long printerval = 1;
  size_t docLen;

  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  globals->numDocs = 0;
  while (p <= inputEnd) {
    if (globals->postingsSeen >= globals->postingsLimit) return;  // -------------------------->
    if (0) printf("Looking for <TEXT\n");
    q = mlstrstr(p, "<TEXT>", inputEnd - p - 6, 0, 0);
    if (q == NULL) break;   // Presumably the end of the file. ... or wrong format file.
    docStart = q + 6;
    if (0) printf("Looking for </TEXT>\n");
    q = mlstrstr(docStart, "</TEXT>", inputEnd - docStart - 7, 0, 0);
    if (q == NULL) break;   // Presumably file is incorrectly formatted.    
    docLen = q - docStart;
    globals->numDocs++;
    if (0) printf("About to process one doc, of len %zd\n", docLen);
    processOneDoc(globals, docStart, docLen);
    if (globals->numDocs % printerval == 0) {
      printf("   --- %lld records scanned @ %.3f msec per record --- postings thus far: %lld\n",
	     globals->numDocs,
	     (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
	     globals->postingsSeen);
      if (printerval < 100000 && globals->numDocs % (printerval * 10) == 0) printerval *= 10;
    }
    p = q + 7;  // Move to char after </TEXT>
  }
}




static void processSTARCFormat(globals_t *globals) {
  // Input is assumed to be records in in a very simple <STARC
  // header><content> format. The STARC header begins and ends with a
  // single ASCII space.  Following the leading space is the content
  // length in decimal bytes, represented as an ASCII string,
  // immediately followed by a letter indicating the type of record.
  // Record types are H - header, D - document, or T - Trailer.  The
  // decimal length is expressed in bytes and is represented in
  // ASCII. If the length is L, then there are L bytes of document
  // content following the whitespace character which terminates the
  // length representation.  For example: " 13D ABCDEFGHIJKLM 4D ABCD"
  // contains two documents, the first of 13 bytes and the second of 4
  // bytes.
  //
  // Although this representation is hard to view with editors and
  // simple text display tools, it completely avoids the problems with
  // TSV and other formats which rely on delimiters, that it's very
  // complicated to deal with documents which contain the delimiters.
  //
  // This function skips H and T records.
  // If encode is TRUE, each D record is passed to encodeOneDoc()
  // otherwise to processOneDoc().
  
  size_t docLen;
  u_char *docStart, *p, *q, *inputEnd;
  long long printerval = 10;
  byte recordType;

  globals->numDocs = 0; 
  inputEnd = globals->inputInMemory + globals->inputSize - 1;
  p = globals->inputInMemory;
  while (p <= inputEnd) {
    if (globals->postingsSeen >= globals->postingsLimit) return;  // -------------------------->
    // Decimal length should be encoded in ASCII at the start of this doc.
    if (*p != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     p - globals->inputInMemory);
      exit(1);

    }
    errno = 0;
    docLen = strtol(p, (char **)&q, 10);  // Making an assumption here that the number isn't terminated by EOF
    if (errno) {
      fprintf(stderr, "processSTARCFile: Error %d in strtol() at offset %zd\n",
	     errno, p - globals->inputInMemory);
      exit(1);
    }
    if (docLen <= 0) {
      printf("processSTARCFile: Zero or negative docLen %zd at offset %zd\n",
	     docLen, p - globals->inputInMemory);
      exit(1);
    }

    recordType = *q;    
    if (recordType != 'H' && recordType != 'D' && recordType != 'T') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't start with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);
    }
    q++;
    if (*q != ' ') {
      fprintf(stderr, "processSTARCFile: Error: STARC header doesn't end with space at offset %zd\n",
	     q - globals->inputInMemory);
      exit(1);

    }
    
    docStart = q + 1;  // Skip the trailing space.
    if (0) printf(" ---- Encountered %c record ---- \n", recordType);
    if (recordType == 'D') {
      globals->numDocs++;
      if (0) printf("About to process a doc.\n");
      processOneDoc(globals, docStart, docLen);
      if (globals->numDocs % printerval == 0) {
	printf("   --- %lld records scanned @ %.3f msec per record--- Postings thus far: %lld\n",
	       globals->numDocs,
	       (1000.0 * (what_time_is_it() - globals->startTime)) / (double)globals->numDocs,
	       globals->postingsSeen);
	if (globals->numDocs % (printerval * 10) == 0) printerval *= 10;
      }
    }
    p = docStart + docLen;  // Should point to the first digit of the next length, or EOF
  }
}



int main(int argc, char **argv) {
  int error_code;
  globals_t globals;
  char ASCIITokenBreakSet[] = DFLT_ASCII_TOKEN_BREAK_SET;
    
  setvbuf(stdout, NULL, _IONBF, 0);
  
  if (argc != 4) {
    printf("Usage: %s <name of input file> <name of output file> <maximum postings to copy>\n"
	   " - copies input to output, stopping when the specified number of word occurrences\n"
	   "   have been copied.\n"
	   " - Note that the output format will always be .TREC, regardless\n"
	   " - of the name of the output file.\n",argv[0]);
    exit(1);
  }

  // Make sure we can write to the output file
  globals.out = fopen(argv[2], "wb");
  if (globals.out == NULL) {
    printf("Error: Can't open %s for writing.\n", argv[2]);
    exit(1);
  }
  printf("Output file %s opened for writing.\n", argv[2]);

  globals.postingsLimit = strtoll(argv[3], NULL, 10);
  
  initialise_unicode_conversion_arrays(FALSE);
  initialise_ascii_tables(ASCIITokenBreakSet, TRUE);
  initialiseGlobals(&globals);   
  printf("Globals initialised\n");

 
  // Memory map the whole input file
  if (! exists(argv[1], "")) {
    printf("Error: Input file %s doesn't exist.\n", argv[1]);
    exit(1);
  }
  
  globals.inputInMemory = (char *)mmap_all_of(argv[1], &(globals.inputSize),
					       FALSE, &(globals.inputFH), &(globals.inputMH),
					       &error_code);
  if (globals.inputInMemory == NULL ) {
    printf("Error: mmap_all_of(%s) failed with code %d\n", argv[1], error_code);
    exit(1);
  }


  madvise(globals.inputInMemory, globals.inputSize, MADV_SEQUENTIAL);

  printf("Input mapped.\n");


  if (tailstr(argv[1], ".tsv") != NULL || tailstr(argv[1], ".TSV") != NULL) {
    processTSVFormat(&globals);
   } else if (tailstr(argv[1], ".trec") != NULL || tailstr(argv[1], ".TREC") != NULL) {
    processTRECFormat(&globals);
  } else {
    processSTARCFormat(&globals);
  }
     

  unmmap_all_of(globals.inputInMemory, globals.inputFH, globals.inputMH, globals.inputSize);
  fclose(globals.out);

  printf("\nAll done.  Run checkTRECFile.exe to confirm correct formatting.\n\n");
}
