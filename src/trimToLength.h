// Copyright (c)  David Hawking.  All rights reserved.
// Licensed under the MIT license.

typedef struct {
  double startTime;
  long long postingsSeen, postingsLimit, numDocs;
  u_char *inputInMemory;
  size_t inputSize;
  CROSS_PLATFORM_FILE_HANDLE inputFH;
  HANDLE inputMH;
  FILE *out;
 } globals_t;
